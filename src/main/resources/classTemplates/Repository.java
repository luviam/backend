package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.$E;

public interface $ERepository extends JpaRepository<$E, Long>{ 

}
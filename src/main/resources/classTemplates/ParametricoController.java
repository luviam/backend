package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.$E;
import ar.com.thinkup.luviam.service.def.IParametricoService;
import ar.com.thinkup.luviam.service.def.I$EService;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;
import ar.com.thinkup.luviam.vo.parametricos.TipoEventoVO;
import ar.com.thinkup.luviam.vo.parametricos.$EVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/$path")
@PreAuthorize("isAuthenticated()")
public class $EController extends ParametricosController<$EVO, $E> {

	private static final Logger LOG = LogManager.getLogger($EController.class);


	@Autowired
	private I$EService $EService;

	@GetMapping("/parametrico/all")
	public ResponseEntity<ResponseContainer<List<ParametricoVO>>> getParametricos() {
		return super.getParametricos();
	}

	@GetMapping("/all")
	public ResponseEntity<ResponseContainer<List<$EVO>>> getAll() {
		return super.getAll();
	}
	
	@PutMapping()
	public ResponseEntity<ResponseContainer<$EVO>> create(@RequestBody $EVO vo) {
		return super.create(vo);
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<$EVO>> update(@RequestBody $EVO vo) {
		return super.update(vo);
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		return super.delete(id);
	}

	@Override
	protected I$EService getService() {
		return this.$EService;
	}

	@Override
	protected Logger getLog() {
		return $EController.LOG;
	}

	@Override
	protected String getNombre() {

		return $NOMBRE;
	}
}

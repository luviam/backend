package ar.com.thinkup.luviam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "categoria_producto")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

public class CategoriaProducto extends Parametrico {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7680428864690551917L;

	public static final String IMPUESTO = "IMP";
	public static final String MENU = "MENU";

	@Column(name = "es_comisionable")
	private Boolean esComisionable = false;

	@Column(name = "peso")
	private Integer peso = 99;

	public CategoriaProducto() {
		super();

	}

	public CategoriaProducto(Boolean habilitado, String codigo, String descripcion, Boolean esSistema) {
		super(habilitado, codigo, descripcion, esSistema);

	}

	public CategoriaProducto(Long id, Boolean habilitado, String codigo, String descripcion, Boolean esSistema,
			Boolean esComisionable) {
		super(id, habilitado, codigo, descripcion, esSistema);
		this.esComisionable = esComisionable;

	}

	public Boolean getEsComisionable() {
		return esComisionable;
	}

	public void setEsComisionable(Boolean esComisionable) {
		this.esComisionable = esComisionable;
	}

	public Boolean esImpuesto() {
		return this.getCodigo().equals(IMPUESTO);
	}

	public Integer getPeso() {
		return peso;
	}

	public void setPeso(Integer peso) {
		this.peso = peso;
	}

}

package ar.com.thinkup.luviam.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import ar.com.thinkup.luviam.model.def.IDescriptivo;

@Entity
@Table(name = "centro")
@Where(clause = "activo='1'")
public class CentroSeleccion implements IDescriptivo{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5731593213993574909L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "codigo")
	private String codigo;

	@Column(name = "nombre")
	private String nombre;
	
	@OrderBy("nombre ASC")
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="centro_costo_id")
	@Where(clause = "deleted=0")
	private List<Cuenta> cuentasAplicacion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Cuenta> getCuentasAplicacion() {
		return cuentasAplicacion;
	}

	public void setCuentasAplicacion(List<Cuenta> cuentasAplicacion) {
		this.cuentasAplicacion = cuentasAplicacion;
	}

	@Override
	public String getDescripcion() {
		return this.nombre;
	}

}

package ar.com.thinkup.luviam.model.contratos;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.Producto;
import ar.com.thinkup.luviam.vo.Identificable;
import ar.com.thinkup.utils.MathUtils;

@Entity()
@Table(name = "plan_pago")
public class CuotaServicio implements Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6444539450418093835L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "servicio_contratado_id")
	private ServicioContratado servicio;

	@Column(name = "fecha_contrato")
	private Date fechaContrato;

	@Column(name = "fecha_vencimiento")
	private Date fechaVencimiento;

	@Column(name = "fecha_estimada_pago")
	private Date fechaEstimadaPago;

	private BigDecimal cantidad;

	@Column(name = "costo_unitario", precision = 10, scale = 3)
	private BigDecimal costoUnitario = new BigDecimal(0);

	@Column(name = "monto", precision = 10, scale = 2)
	private BigDecimal monto = new BigDecimal(0);

	@Column(name = "saldo", precision = 10, scale = 2)
	private BigDecimal saldo = new BigDecimal(0);

	@Column(name = "incremento")
	private BigDecimal incremento = new BigDecimal(0);

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "asiento_id", nullable = true)
	private Asiento asientoGenerado;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "grupo")
	private String grupo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaContrato() {
		return fechaContrato;
	}

	public void setFechaContrato(Date fechaContrato) {
		this.fechaContrato = fechaContrato;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public Date getFechaEstimadaPago() {
		return fechaEstimadaPago;
	}

	public void setFechaEstimadaPago(Date fechaEstimadaPago) {
		this.fechaEstimadaPago = fechaEstimadaPago;
	}

	public Double getCantidad() {
		return cantidad.doubleValue();
	}

	public void setCantidad(Double cantidad) {
		this.cantidad = new BigDecimal(cantidad);
	}

	public Double getCostoUnitario() {
		return costoUnitario.doubleValue();
	}

	public void setCostoUnitario(Double costoUnitario) {
		this.costoUnitario = new BigDecimal(costoUnitario);
	}

	public Double getMonto() {
		return monto != null ? monto.doubleValue() : 0d;
	}

	public void setMonto(Double monto) {
		if (monto == null) {
			this.monto = new BigDecimal(0);
		} else {
			this.monto = new BigDecimal(monto);
		}

	}

	public Double getSaldo() {
		return this.saldo != null ? saldo.doubleValue() : 0d;
	}

	public void setSaldo(Double saldo) {
		this.saldo = new BigDecimal(saldo);
	}

	public ServicioContratado getServicio() {
		return servicio;
	}

	public void setServicio(ServicioContratado servicio) {
		this.servicio = servicio;
	}

	public Double getIncremento() {
		return this.incremento != null ? incremento.doubleValue() : 0d;
	}

	public void setIncremento(Double incremento) {
		if (incremento == null) {
			this.incremento = new BigDecimal(0);
		} else {
			this.incremento = new BigDecimal(incremento);
		}

	}

	public Asiento getAsientoGenerado() {
		return asientoGenerado;
	}

	public void setAsientoGenerado(Asiento asientoGenerado) {
		this.asientoGenerado = asientoGenerado;
	}

	public Double getTotal() {

		return this.getMonto() * MathUtils.round(1 + this.getIncremento(), 2);
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public Boolean esImpuesto() {
		return this.servicio != null && this.servicio.esImpuesto();
	}

	public Producto getProducto() {
		return this.servicio.getProducto() != null ? this.servicio.getProducto() : null;
	}

}

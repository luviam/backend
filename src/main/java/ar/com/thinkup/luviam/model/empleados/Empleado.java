package ar.com.thinkup.luviam.model.empleados;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.Comisionista;
import ar.com.thinkup.luviam.model.Sector;
import ar.com.thinkup.luviam.model.TipoEmpleado;
import ar.com.thinkup.luviam.model.TipoJornada;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.repository.EmpleadoRepository.IEmpleadoCabecera;

/**
 * 
 * @author ThinkUp
 *
 */
@Entity
@Table(name = "empleado")
@Where(clause = "activo='1'")
public class Empleado implements IDescriptivo, IEmpleadoCabecera {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2768068004784738837L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "nombre")
	private String nombre;

	@Column(name = "fecha_nacimiento")
	private Date fechaNacimiento;

	@Column(name = "cuil")
	private String cuil;

	@Column(name = "domicilio")
	private String domicilio;

	@Column(name = "activo")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean activo;

	@Column(name = "verificacion_cv")
	private Boolean cvOk;

	@Column(name = "preocupacional_realizado")
	private Boolean preocupacionalOk;

	@Column(name = "fecha_preocupacional")
	private Date fechaRealizacionPreocupacional;

	@Column(name = "verificacion_dni")
	private Boolean dniOK;

	@Column(name = "verificacion_cuil")
	private Boolean cuilOK;

	@Column(name = "verificacion_constancia_domicilio")
	private Boolean constanciaDomicilioOK;

	@Column(name = "verificacion_seguro")
	private Boolean seguroOK;

	@Column(name = "art_contratada")
	private Boolean artOK;

	@ManyToOne
	@JoinColumn(name = "tipo_jornada_id")
	private TipoJornada tipoJornada;

	@ManyToOne()
	@JoinColumn(name = "sector_id")
	private Sector sector;

	@Column(name = "funcion")
	private String funcion;

	@Column(name = "responsabilidades")
	private String responsabilidades;

	@Column(name = "remuneracion")
	private Float remuneracion;

	@Column(name = "fecha_inicio_laboral")
	private Date fechaInicio;

	@Column(name = "fecha_fin_laboral")
	private Date fechaFin;

	@ManyToOne
	@JoinColumn(name = "tipo_empleado_id")
	private TipoEmpleado tipoEmpleado;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "empleado")
	private Set<AltaEmpleado> altas = new HashSet<>();

	@Column(name = "email")
	private String email;
	@Column(name = "telefono")
	private String telefono;

	@OneToOne(mappedBy = "vendedor", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Comisionista comisionista;

	@ManyToOne
	@JoinColumn(name = "centro_costo_id")
	private CentroCosto centroCosto;

	public Empleado() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	@Override
	public String getCodigo() {
		return String.valueOf(this.id);
	}

	@Override
	public String getDescripcion() {
		return this.nombre;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getCuil() {
		return cuil;
	}

	public void setCuil(String cuil) {
		this.cuil = cuil;
	}

	public Boolean getCvOk() {
		return cvOk;
	}

	public void setCvOk(Boolean cvOk) {
		this.cvOk = cvOk;
	}

	public Boolean getPreocupacionalOk() {
		return preocupacionalOk;
	}

	public void setPreocupacionalOk(Boolean preocupacionalOk) {
		this.preocupacionalOk = preocupacionalOk;
	}

	public Date getFechaRealizacionPreocupacional() {
		return fechaRealizacionPreocupacional;
	}

	public void setFechaRealizacionPreocupacional(Date fechaRealizacionPreocupacional) {
		this.fechaRealizacionPreocupacional = fechaRealizacionPreocupacional;
	}

	public Boolean getDniOK() {
		return dniOK;
	}

	public void setDniOK(Boolean dniOK) {
		this.dniOK = dniOK;
	}

	public Boolean getCuilOK() {
		return cuilOK;
	}

	public void setCuilOK(Boolean cuilOK) {
		this.cuilOK = cuilOK;
	}

	public Boolean getConstanciaDomicilioOK() {
		return constanciaDomicilioOK;
	}

	public void setConstanciaDomicilioOK(Boolean constanciaDomicilioOK) {
		this.constanciaDomicilioOK = constanciaDomicilioOK;
	}

	public Boolean getSeguroOK() {
		return seguroOK;
	}

	public void setSeguroOK(Boolean seguroOK) {
		this.seguroOK = seguroOK;
	}

	public Boolean getArtOK() {
		return artOK;
	}

	public void setArtOK(Boolean artOK) {
		this.artOK = artOK;
	}

	public TipoJornada getTipoJornada() {
		return tipoJornada;
	}

	public void setTipoJornada(TipoJornada tipoJornada) {
		this.tipoJornada = tipoJornada;
	}

	public Sector getSector() {
		return sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}

	public String getFuncion() {
		return funcion;
	}

	public void setFuncion(String funcion) {
		this.funcion = funcion;
	}

	public String getResponsabilidades() {
		return responsabilidades;
	}

	public void setResponsabilidades(String responsabilidades) {
		this.responsabilidades = responsabilidades;
	}

	public Float getRemuneracion() {
		return remuneracion;
	}

	public void setRemuneracion(Float remuneracion) {
		this.remuneracion = remuneracion;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public TipoEmpleado getTipoEmpleado() {
		return tipoEmpleado;
	}

	public void setTipoEmpleado(TipoEmpleado tipoEmpleado) {
		this.tipoEmpleado = tipoEmpleado;
	}

	public Set<AltaEmpleado> getAltas() {
		return altas;
	}

	public void setAltas(Set<AltaEmpleado> altas) {
		this.altas = altas;
	}

	public void addAlta(AltaEmpleado alta) {
		alta.setEmpleado(this);
		this.altas.add(alta);
	}

	public AltaEmpleado getAlta(Long id) {
		return this.altas.stream().filter(a -> a.getId() != null && a.getId().equals(id)).findFirst().orElse(null);
	}

	@Override
	public String toString() {

		return "Empleado " + this.nombre + " ID: " + this.getId();

	}

	public Integer cantidadAltas() {
		return this.altas.size();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public boolean esVendedor() {
		return this.getTipoEmpleado() != null && this.getTipoEmpleado().getCodigo().equals(TipoEmpleado.VENDEDOR);
	}

	public Comisionista getComisionista() {
		return comisionista;
	}

	public void setComisionista(Comisionista comisionista) {
		this.comisionista = comisionista;
	}

	public CentroCosto getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(CentroCosto centroCosto) {
		this.centroCosto = centroCosto;
	}

}

package ar.com.thinkup.luviam.model.enums;

public enum TipoIVAEnum {
	EXCENTO("0", "0%", 0d), P_10_5("105", "10.5%", .105d), P_21("21", "21%", .21d),

	P_27("27", "27%", .27d);
	private String codigo;
	private String descripcion;
	private Double valor;

	private TipoIVAEnum(String codigo, String descripcion, Double valor) {
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.valor = valor;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public static TipoIVAEnum getByCodigo(String codigo) {

		for (TipoIVAEnum ele : TipoIVAEnum.values()) {
			if (ele.getCodigo().equals(codigo)) {
				return ele;
			}

		}
		return null;

	}

}

package ar.com.thinkup.luviam.model.documentos.pagos;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.Proveedor;
import ar.com.thinkup.luviam.model.documentos.DocumentoPago;
import ar.com.thinkup.luviam.model.documentos.factura.Factura;
import ar.com.thinkup.luviam.repository.ItemOrdenPagoRepository.ItemOrdenPagoCabecera;
import ar.com.thinkup.luviam.repository.OrdenPagoRepository.OrdenPagoCabecera;
import ar.com.thinkup.luviam.service.contabilizacion.IContabilizable;

@Entity
@Table(name = "orden_pago")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class OrdenPago extends DocumentoPago<Pago> implements OrdenPagoCabecera, IContabilizable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8753313344073936695L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "proveedor_id", nullable = false)
	private Proveedor proveedor;

	@OrderBy("id ASC")
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "ordenPago")
	private Set<ItemOrdenPago> items = new HashSet<>();

	@ManyToOne(cascade = CascadeType.ALL, targetEntity = Factura.class)
	@JoinColumn(name = "pago_a_cuenta_id")
	private Factura pagoACuenta;

	@OrderBy("id ASC")
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "ordenPago")
	private Set<Pago> pagos = new HashSet<>();

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Set<ItemOrdenPago> getItems() {
		return items;
	}

	public void setItems(Set<ItemOrdenPago> items) {
		this.items = new HashSet<>();
		this.items.addAll(items);
	}

	public Set<Pago> getPagos() {
		return pagos;
	}

	public void setPagos(Set<Pago> pagos) {
		this.pagos = new HashSet<>();
		this.pagos.addAll(pagos);
	}

	public boolean addItem(ItemOrdenPago item) {
		item.setOrdenPago(this);
		return this.items.add(item);
	}

	public boolean addPago(Pago pago) {
		pago.setOrdenPago(this);
		return this.pagos.add(pago);
	}

	public ItemOrdenPago getItem(Long id) {
		return this.items.parallelStream().filter(i -> i.getId() != null && i.getId().equals(id)).findFirst()
				.orElse(null);

	}

	public Pago getPago(Long id) {
		return this.pagos.parallelStream().filter(i -> i.getId() != null && i.getId().equals(id)).findFirst()
				.orElse(null);

	}

	@Override
	public Set<Pago> getValores() {
		return this.getPagos();
	}

	@Override
	public void addValor(Pago valor) {
		this.addPago(valor);

	}

	// Este metodo usa los valores de los hijos para saber el total. El metodo
	// getImporte toma el valor que se guardó en base de datos.
	public Double getTotal() {
		Double total = this.getItems().stream().map(ItemOrdenPagoCabecera::getImporte).reduce(0d, Double::sum);
		return this.pagoACuenta != null ? (Math.abs(this.pagoACuenta.getImporte())) + total : total;
	}

	public Factura getPagoACuenta() {
		return pagoACuenta;
	}

	public void setPagoACuenta(Factura pagoACuenta) {
		if(pagoACuenta == null) {
			this.pagoACuenta = null;
		}else {
			this.pagoACuenta = pagoACuenta;
			this.pagoACuenta.setCentroCosto(this.getCentroCosto());
			this.pagoACuenta.setProveedor(this.getProveedor());
			this.pagoACuenta.setFecha(this.getFecha());
			this.pagoACuenta.setNumero("000000000000");
			this.pagoACuenta.setResponsable(this.getResponsable());
			this.pagoACuenta.setEstado(this.getEstadoOperacion().getCodigo());	
		}
		

	}

	@Override
	public Asiento getAsiento() {
		return this.getAsientoGenerado();
	}

}

package ar.com.thinkup.luviam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "proveedor_saldo_view")
public class ProveedorConSaldo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1961227803379660898L;
	@Id
	@Column(name = "id")
	private Long id;

	@Column(name = "nombre_proveedor")
	private String nombre;

	@Column(name = "codigo_tipo_proveedor")
	private String codigoTipoProveedor;

	@Column(name = "descripcion_tipo_proveedor")
	private String descripcionTipoProveedor;

	@Column(name = "codigo_tipo_cuenta")
	private String codigoTipoCuenta;

	@Column(name = "descripcion_tipo_cuenta")
	private String descripcionTipoCuenta;

	@Column(name = "codigo_tipo_iva")
	private String codigoTipoIva;

	@Column(name = "descripcion_tipo_iva")
	private String descripcionTipoIva;

	@Column(name = "nombre_contacto")
	private String contacto;
	private String telefono;
	private String celular;
	private String email;

	@Column(name = "saldo")
	private Float saldo = 0f;

	public Float getSaldo() {
		return saldo;
	}

	public void setSaldo(Float saldo) {
		this.saldo = saldo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigoTipoProveedor() {
		return codigoTipoProveedor;
	}

	public void setCodigoTipoProveedor(String codigoTipoProveedor) {
		this.codigoTipoProveedor = codigoTipoProveedor;
	}

	public String getDescripcionTipoProveedor() {
		return descripcionTipoProveedor;
	}

	public void setDescripcionTipoProveedor(String descripcionTipoProveedor) {
		this.descripcionTipoProveedor = descripcionTipoProveedor;
	}

	public String getCodigoTipoCuenta() {
		return codigoTipoCuenta;
	}

	public void setCodigoTipoCuenta(String codigoTipoCuenta) {
		this.codigoTipoCuenta = codigoTipoCuenta;
	}

	public String getDescripcionTipoCuenta() {
		return descripcionTipoCuenta;
	}

	public void setDescripcionTipoCuenta(String descripcionTipoCuenta) {
		this.descripcionTipoCuenta = descripcionTipoCuenta;
	}

	public String getCodigoTipoIva() {
		return codigoTipoIva;
	}

	public void setCodigoTipoIva(String codigoTipoIva) {
		this.codigoTipoIva = codigoTipoIva;
	}

	public String getDescripcionTipoIva() {
		return descripcionTipoIva;
	}

	public void setDescripcionTipoIva(String descripcionTipoIva) {
		this.descripcionTipoIva = descripcionTipoIva;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
}

package ar.com.thinkup.luviam.model.enums;

import java.util.NoSuchElementException;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public enum TipoPagoComision {
ADELANTO("A","Adelanto"),POR_PAGO("P","Por Pago"), LIQUIDACION("L","Liquidacion");
	
	String codigo;
	String descripcion;

	private TipoPagoComision(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}
	
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public static DescriptivoGeneric getDescriptivo(String codigo) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(codigo)) {
			throw new IllegalArgumentException();
		}
		TipoPagoComision t = Stream.of(TipoPagoComision.values()).filter(tc->tc.codigo.equals(codigo)).findFirst().get();
		return t!= null? new DescriptivoGeneric(t.codigo,t.descripcion) : DescriptivoGeneric.SIN_DEFINIR();
	}

	public static TipoPagoComision byCodigo(String codigo) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(codigo)) {
			throw new IllegalArgumentException();
		}
		return Stream.of(TipoPagoComision.values()).filter(tc->tc.codigo.equals(codigo)).findFirst().get();
	}

	
}

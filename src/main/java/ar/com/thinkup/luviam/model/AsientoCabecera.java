package ar.com.thinkup.luviam.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "asiento")
public class AsientoCabecera {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "numero")
	private Long numeroAsiento;

	@Column(name = "fecha")
	private Date fecha;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "codigo_estado")
	private String estado;

	@Column(name = "responsable")
	private String responsable;

	@Column(name = "mensaje")
	private String mensaje;

	@Column(name = "tipo_generador")
	private String tipoGenerador = "AA";

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNumeroAsiento() {
		return numeroAsiento;
	}

	public void setNumeroAsiento(Long numero) {
		this.numeroAsiento = numero;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getTipoGenerador() {
		return tipoGenerador;
	}

	public void setTipoGenerador(String tipoGenerador) {
		this.tipoGenerador = tipoGenerador;
	}

}

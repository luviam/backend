package ar.com.thinkup.luviam.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;

@Entity
@Table(name = "chequera")
@Where(clause = "deleted='0'")
public class Chequera implements Serializable {

	private static final long serialVersionUID = -8077627217561686140L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "cuenta_id")
	private CuentaAplicacion cuentaBancaria;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "chequera_id", nullable = false)
	private List<SecuenciaCheques> secuenciasCheques = new Vector<>();

	@OneToMany(mappedBy = "chequera", cascade = CascadeType.ALL, orphanRemoval = false)
	private List<Cheque> cheques = new Vector<>();

	@Column(name = "es_diferido")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean esDiferido;

	@Column(columnDefinition = "tinyint(1) default 0", name = "deleted", insertable = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean deleted;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the cuentaBancaria
	 */
	public CuentaAplicacion getCuentaBancaria() {
		return cuentaBancaria;
	}

	/**
	 * @param cuentaBancaria
	 *            the cuentaBancaria to set
	 */
	public void setCuentaBancaria(CuentaAplicacion cuentaBancaria) {
		this.cuentaBancaria = cuentaBancaria;
	}

	/**
	 * @return the secuenciasCheques
	 */
	public List<SecuenciaCheques> getSecuenciasCheques() {
		return secuenciasCheques;
	}

	/**
	 * @param secuenciasCheques
	 *            the secuenciasCheques to set
	 */
	public void setSecuenciasCheques(List<SecuenciaCheques> secuenciasCheques) {
		this.secuenciasCheques = secuenciasCheques;
	}

	/**
	 * @return the esDiferido
	 */
	public boolean isEsDiferido() {
		return esDiferido;
	}

	/**
	 * @param esDiferido
	 *            the esDiferido to set
	 */
	public void setEsDiferido(boolean esDiferido) {
		this.esDiferido = esDiferido;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public boolean addCheque(Cheque e) {
		e.setChequera(this);
		if (this.cheques.stream()
				.anyMatch(c -> c.getBanco().equals(e.getBanco()) && c.getNumero().equals(e.getNumero()))) {
			return false;
		}
		return cheques.add(e);

	}

	public boolean addAllCheques(Collection<? extends Cheque> c) {
		return cheques.addAll(c);
	}

	public List<Cheque> getCheques() {
		return cheques;
	}

	public void setCheques(List<Cheque> cheques) {
		this.cheques = cheques;
	}

	public boolean pertenece(Cheque cheque) {
		return this.getSecuenciasCheques().parallelStream()
				.anyMatch(s -> Integer.valueOf(cheque.getNumero()) >= s.getMinNumeracion()
						&& Integer.valueOf(cheque.getNumero()) <= s.getMaxNumeracion());
	}

}

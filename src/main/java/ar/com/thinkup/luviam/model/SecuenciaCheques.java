package ar.com.thinkup.luviam.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "secuencia_cheques")
public class SecuenciaCheques implements Serializable {

	private static final long serialVersionUID = -8655962211427461227L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name="min_numeracion")
	private Long minNumeracion;
	@Column(name="max_numeracion")
	private Long maxNumeracion;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the minNumeracion
	 */
	public Long getMinNumeracion() {
		return minNumeracion;
	}
	/**
	 * @param minNumeracion the minNumeracion to set
	 */
	public void setMinNumeracion(Long minNumeracion) {
		this.minNumeracion = minNumeracion;
	}
	/**
	 * @return the maxNumeracion
	 */
	public Long getMaxNumeracion() {
		return maxNumeracion;
	}
	/**
	 * @param maxNumeracion the maxNumeracion to set
	 */
	public void setMaxNumeracion(Long maxNumeracion) {
		this.maxNumeracion = maxNumeracion;
	}
	
	@Override
	public boolean equals(Object obj) {
		SecuenciaCheques sec = (SecuenciaCheques) obj;
		return 	this.id == sec.getId() && 
				this.minNumeracion == sec.minNumeracion && 
				this.maxNumeracion == sec.maxNumeracion;
	}
	
}

package ar.com.thinkup.luviam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.def.IDescriptivo;

@Entity()
@Table(name = "tipo_comprobante")
public class TipoComprobante implements IDescriptivo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String PAGO_ADELANTADO = "PA";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "codigo")
	private String codigo;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "es_facturable")
	private Boolean esFacturable = false;

	@Column(name = "es_interno")
	private Boolean esInterno = false;

	public TipoComprobante() {
		super();
	}

	public TipoComprobante(String codigo, String descripcion) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getEsFacturable() {
		return esFacturable;
	}

	public void setEsFacturable(Boolean esFacturable) {
		this.esFacturable = esFacturable;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getEsInterno() {
		return esInterno;
	}

	public void setEsInterno(Boolean esInterno) {
		this.esInterno = esInterno;
	}

}

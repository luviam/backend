package ar.com.thinkup.luviam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 * 
 * @author ThinkUp
 *
 */
@Entity
@Table(name = "centro_costo_tipo_comprobante")
public class CentroCostosTipoComprobante {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "tipo_comprobante_id")
	private TipoComprobante tipoComprobante;

	@Column(name = "valor_actual")
	private String valorActual;

	@Column(name = "activo")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean activo;

	public CentroCostosTipoComprobante() {
		// TODO Auto-generated constructor stub
	}

	public CentroCostosTipoComprobante(TipoComprobante tipoComprobante, String valorActual, Boolean activo) {
		super();
		this.tipoComprobante = tipoComprobante;
		this.valorActual = valorActual;
		this.activo = activo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoComprobante getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(TipoComprobante tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public String getValorActual() {
		return valorActual;
	}

	public void setValorActual(String valorActual) {
		this.valorActual = valorActual;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((activo == null) ? 0 : activo.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((tipoComprobante == null) ? 0 : tipoComprobante.hashCode());
		result = prime * result + ((valorActual == null) ? 0 : valorActual.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CentroCostosTipoComprobante other = (CentroCostosTipoComprobante) obj;
		if (activo == null) {
			if (other.activo != null)
				return false;
		} else if (!activo.equals(other.activo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (tipoComprobante == null) {
			if (other.tipoComprobante != null)
				return false;
		} else if (!tipoComprobante.equals(other.tipoComprobante))
			return false;
		if (valorActual == null) {
			if (other.valorActual != null)
				return false;
		} else if (!valorActual.equals(other.valorActual))
			return false;
		return true;
	}
	
	

}

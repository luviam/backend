package ar.com.thinkup.luviam.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import ar.com.thinkup.luviam.model.def.IDescriptivo;

@Entity
@Table(name = "comisionista")
@Where(clause = "activo='1'")
public class CabeceraComisionista implements IDescriptivo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2768068004784738837L;
	@Id
	private Long id;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "nombre_contacto")
	private String contacto;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "celular")
	private String celular;

	@Column(name = "email")
	private String email;

	@ManyToOne
	@JoinColumn(name = "tipo_comisionista_id")
	private TipoComisionista tipoComisionista;

	@Column(name = "activo")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean activo;
	@Transient
	private BigDecimal saldo;

	public CabeceraComisionista() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public TipoComisionista getTipoComisionista() {
		return tipoComisionista;
	}

	public void setTipoComisionista(TipoComisionista tipoComisionista) {
		this.tipoComisionista = tipoComisionista;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Double getSaldo() {
		return saldo != null ? this.saldo.doubleValue() : 0;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo != null ? new BigDecimal(saldo) : new BigDecimal(0);
	}

	@Override
	public String toString() {

		return "Comisionista " + this.getNombre() + " ID: " + this.getId();
	}

	@Override
	public String getCodigo() {
		return this.getId() != null ? this.getId() + "" : "SD";
	}

	@Override
	public String getDescripcion() {
		return this.getNombre();
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

}

package ar.com.thinkup.luviam.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;

import org.hibernate.annotations.Subselect;

@Entity
@NamedNativeQueries(value = {
		@NamedNativeQuery(name = TotalesCuentasView.Q_BUSCAR_POR_PERIODO_Y_CENTRO, resultClass = TotalesCuentasView.class, query = ""
				+ "select " + "c.id	as centro_id, " + "c.codigo	as centro_codigo, " + "c.nombre	as centro_nombre, "
				+ "r.id	as rubro_id, " + "r.codigo	as rubro_codigo, " + "r.nombre	as rubro_nombre, "
				+ "g.id	as grupo_id, " + "g.codigo	as grupo_codigo, " + "g.nombre	as grupo_nombre, "
				+ "sg.id	as sub_grupo_id, " + "sg.codigo	as sub_grupo_codigo, " + "sg.nombre	as sub_grupo_nombre, "
				+ "o.cuenta_id	as cuenta_id, " + "ca.codigo	as cuenta_codigo, " + "ca.nombre	as cuenta_nombre, "
				+ "SUM(o.debe)	as debe, " + "sum(o.haber)	as haber, " + "(SUM(o.debe) - sum(o.haber))	as balance "
				+ "from operacion o  " + "join cuenta_aplicacion ca on ca.id = o.cuenta_id  "
				+ "join sub_grupo sg on sg.id = ca.sub_grupo_id " + "join grupo g on g.id = sg.grupo_id "
				+ "join rubro r on r.id = g.rubro_id " + "join centro c on c.id = o.centro_id "
				+ "join asiento a on o.asiento_id = a.id " + "where a.fecha between ?1 and ?2 "
				+ "and ( c.id = ?3 or ?3 = -1 ) " + "and a.codigo_estado = \"A\" " + "group by o.cuenta_id, c.id "
				+ "order by r.id, g.id, sg.id, c.id") })
@Subselect(value = "select * from cuenta_aplicacion")
public class TotalesCuentasView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7989452379596946354L;
	public static final String Q_BUSCAR_POR_PERIODO_Y_CENTRO = "TotalesCuentasView.findByPeriodoAndCentro";
	@Column(name = "centro_id")
	private Long centroId;

	@Column(name = "centro_codigo")
	private String centroCodigo;

	@Column(name = "centro_nombre")
	private String centroNombre;

	@Column(name = "rubro_id")
	private Long rubroId;

	@Column(name = "rubro_codigo")
	private String rubroCodigo;

	@Column(name = "rubro_nombre")
	private String rubroNombre;

	@Column(name = "grupo_id")
	private Long grupoId;

	@Column(name = "grupo_codigo")
	private String grupoCodigo;

	@Column(name = "grupo_nombre")
	private String grupoNombre;

	@Column(name = "sub_grupo_id")
	private Long subGrupoId;

	@Column(name = "sub_grupo_codigo")
	private String subGrupoCodigo;

	@Column(name = "sub_grupo_nombre")
	private String subGrupoNombre;

	@Id
	@Column(name = "cuenta_id")
	private Long cuentaId;

	@Column(name = "cuenta_codigo")
	private String cuentaCodigo;

	@Column(name = "cuenta_nombre")
	private String cuentaNombre;

	@Column(name = "debe")
	private BigDecimal debe = new BigDecimal(0);

	@Column(name = "haber")
	private BigDecimal haber = new BigDecimal(0);

	@Column(name = "balance")
	private BigDecimal balance = new BigDecimal(0);

	public Long getCentroId() {
		return centroId;
	}

	public void setCentroId(Long centroId) {
		this.centroId = centroId;
	}

	public String getCentroCodigo() {
		return centroCodigo;
	}

	public void setCentroCodigo(String centroCodigo) {
		this.centroCodigo = centroCodigo;
	}

	public String getCentroNombre() {
		return centroNombre;
	}

	public void setCentroNombre(String centroNombre) {
		this.centroNombre = centroNombre;
	}

	public Long getRubroId() {
		return rubroId;
	}

	public void setRubroId(Long rubroId) {
		this.rubroId = rubroId;
	}

	public String getRubroCodigo() {
		return rubroCodigo;
	}

	public void setRubroCodigo(String rubroCodigo) {
		this.rubroCodigo = rubroCodigo;
	}

	public String getRubroNombre() {
		return rubroNombre;
	}

	public void setRubroNombre(String rubroNombre) {
		this.rubroNombre = rubroNombre;
	}

	public Long getGrupoId() {
		return grupoId;
	}

	public void setGrupoId(Long grupoId) {
		this.grupoId = grupoId;
	}

	public String getGrupoCodigo() {
		return grupoCodigo;
	}

	public void setGrupoCodigo(String grupoCodigo) {
		this.grupoCodigo = grupoCodigo;
	}

	public String getGrupoNombre() {
		return grupoNombre;
	}

	public void setGrupoNombre(String grupoNombre) {
		this.grupoNombre = grupoNombre;
	}

	public Long getSubGrupoId() {
		return subGrupoId;
	}

	public void setSubGrupoId(Long subGrupoId) {
		this.subGrupoId = subGrupoId;
	}

	public String getSubGrupoCodigo() {
		return subGrupoCodigo;
	}

	public void setSubGrupoCodigo(String subGrupoCodigo) {
		this.subGrupoCodigo = subGrupoCodigo;
	}

	public String getSubGrupoNombre() {
		return subGrupoNombre;
	}

	public void setSubGrupoNombre(String subGrupoNombre) {
		this.subGrupoNombre = subGrupoNombre;
	}

	public Long getCuentaId() {
		return cuentaId;
	}

	public void setCuentaId(Long cuentaId) {
		this.cuentaId = cuentaId;
	}

	public String getCuentaCodigo() {
		return cuentaCodigo;
	}

	public void setCuentaCodigo(String cuentaCodigo) {
		this.cuentaCodigo = cuentaCodigo;
	}

	public String getCuentaNombre() {
		return cuentaNombre;
	}

	public void setCuentaNombre(String cuentaNombre) {
		this.cuentaNombre = cuentaNombre;
	}

	public Float getDebe() {
		return debe.floatValue();
	}

	public void setDebe(Float debe) {
		this.debe = new BigDecimal(debe);
	}

	public Float getHaber() {
		return haber.floatValue();
	}

	public void setHaber(Float haber) {
		this.haber = new BigDecimal(haber);
	}

	public Float getBalance() {
		return balance.floatValue();
	}

	public void setBalance(Float balance) {
		this.balance = new BigDecimal(balance);
	}

}

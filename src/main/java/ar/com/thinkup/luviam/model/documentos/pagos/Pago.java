package ar.com.thinkup.luviam.model.documentos.pagos;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.documentos.Valores;

@Entity
@Table(name = "pago")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class Pago extends Valores{

	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orden_pago_id", nullable = false)
	private OrdenPago ordenPago;

	public OrdenPago getOrdenPago() {
		return ordenPago;
	}

	public void setOrdenPago(OrdenPago ordenPago) {
		this.ordenPago = ordenPago;
	}

	

}

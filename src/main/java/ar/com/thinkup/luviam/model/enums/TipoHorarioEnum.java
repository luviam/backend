package ar.com.thinkup.luviam.model.enums;

import java.util.NoSuchElementException;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

public enum TipoHorarioEnum {
DIA("D","Día"),NOCHE("N","Noche");
	
	String codigo;
	String descripcion;

	private TipoHorarioEnum(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}
	
	
	public String getDescripcion() {
		return descripcion;
	}

	public static TipoHorarioEnum byCodigo(String codigo) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(codigo)) {
			throw new IllegalArgumentException();
		}
		return Stream.of(TipoHorarioEnum.values()).filter(tc->tc.codigo.equals(codigo)).findFirst().orElse(null);
	}

	
}

package ar.com.thinkup.luviam.model;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author ThinkUp
 *
 */
@Entity
@Table(name = "asiento")
public class Asiento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "numero")
	private Long numero;

	@Column(name = "fecha")
	private Date fecha;

	@Column(name = "descripcion")
	private String descripcion;
	@Column(name = "codigo_estado")
	private String estado;

	@Column(name = "responsable")
	private String responsable;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "asiento")
	private List<Operacion> operaciones = new Vector<>();

	@Column(name = "mensaje")
	private String mensaje;

	@Column(name = "tipo_generador", length = 2, nullable = false)
	private String tipoGenerador = "AA";

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public List<Operacion> getOperaciones() {
		return operaciones;
	}

	public void setOperaciones(List<Operacion> operaciones) {
		this.operaciones = operaciones;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public boolean agregarOperacion(Operacion e) {
		if (e.getDebe() > 0 || e.getHaber() > 0) {
			e.setAsiento(this);

			return operaciones.add(e);
		} else
			return false;

	}

	public String getTipoGenerador() {
		return tipoGenerador;
	}

	public void setTipoGenerador(String tipoGenerador) {
		this.tipoGenerador = tipoGenerador;
	}

	public void agregarOperaciones(List<Operacion> operaciones) {
		operaciones.stream().forEach(o -> o.setAsiento(this));
		this.operaciones.addAll(operaciones);

	}

	@Override
	public String toString() {
		String str = StringUtils.join(this.numero + "", this.fecha.toString(), this.descripcion, "\r\n");
		for (Operacion o : operaciones) {
			str = StringUtils.join(str, "		", o.toString(), "\r\n");
		}

		return str;
	}

	public Double getTotalDebe() {
		Double d = this.operaciones.stream().mapToDouble(o -> o.getDebe()).sum();
		return  ((double)Math.round(d * 100) / 100);
	}

	public Double getTotalHaber() {
		Double d = this.operaciones.stream().mapToDouble(o -> o.getHaber()).sum();
		return  ((double)Math.round(d * 100) / 100);
	}

}

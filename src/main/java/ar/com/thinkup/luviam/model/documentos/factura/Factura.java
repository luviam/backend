package ar.com.thinkup.luviam.model.documentos.factura;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.Proveedor;
import ar.com.thinkup.luviam.model.TipoComprobante;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.repository.FacturaRepository.FacturaCabecera;
import ar.com.thinkup.luviam.service.contabilizacion.IContabilizable;

@Entity
@Table(name = "factura")
public class Factura implements FacturaCabecera, IContabilizable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "numero_comprobante")
	private String numero;

	@Column()
	private String descripcion;

	@Column(name = "fecha_facturacion")
	private Date fecha = new Date();

	@Column(name = "fecha_periodo_iva")
	private Date fechaIVA = new Date();

	@ManyToOne
	@JoinColumn(name = "tipo_comprobante_id", nullable = false)
	private TipoComprobante tipoComprobante;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "proveedor_id", nullable = false)
	private Proveedor proveedor;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "centro_id", nullable = false)
	private CentroCosto centroCosto;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "factura")
	private List<ItemFactura> items = new Vector<>();

	@Column(name = "iibb_CABA", nullable = false, precision = 10, scale = 2)
	private BigDecimal iibbCABA = new BigDecimal(0);

	@Column(name = "iibb_BSAS", nullable = false, precision = 10, scale = 2)
	private BigDecimal iibbBSAS = new BigDecimal(0);

	@Column(name = "iibb_otros", nullable = false, precision = 10, scale = 2)
	private BigDecimal iibbOtros = new BigDecimal(0);

	@Column(name = "impuesto_sellos", nullable = false, precision = 10, scale = 2)
	private BigDecimal impuestoSellos = new BigDecimal(0);

	@Column(name = "saldo", nullable = false, precision = 10, scale = 2)
	private BigDecimal saldo = new BigDecimal(0);

	@Column(name = "importe_total", nullable = false, precision = 10, scale = 2)
	private BigDecimal importe = new BigDecimal(0);

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "asiento_id", nullable = true)
	private Asiento asientoGenerado;

	@Column(name = "codigo_estado")
	private String estado;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "responsable_id", nullable = false)
	private Usuario responsable;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getFechaIVA() {
		return fechaIVA;
	}

	public void setFechaIVA(Date fechaIVA) {
		this.fechaIVA = fechaIVA;
	}

	public TipoComprobante getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(TipoComprobante tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public void setItems(List<ItemFactura> items) {
		this.items = items;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public CentroCosto getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(CentroCosto centroCosto) {
		this.centroCosto = centroCosto;
	}

	public Double getIibbCABA() {
		return iibbCABA.doubleValue();
	}

	public void setIibbCABA(Double iibbCABA) {
		this.iibbCABA = new BigDecimal(iibbCABA);
	}

	public Double getIibbBSAS() {
		return iibbBSAS.doubleValue();
	}

	public void setIibbBSAS(Double iibbBSAS) {
		this.iibbBSAS = new BigDecimal(iibbBSAS);
	}

	public Double getIibbOtros() {
		return iibbOtros.doubleValue();
	}

	public void setIibbOtros(Double iibbOtros) {
		this.iibbOtros = new BigDecimal(iibbOtros);
	}

	public Double getImpuestoSellos() {
		return impuestoSellos.doubleValue();
	}

	public void setImpuestoSellos(Double impuestoSellos) {
		this.impuestoSellos = new BigDecimal(impuestoSellos);
	}



	public void setSaldo(Double saldo) {
		this.saldo = new BigDecimal(saldo);
	}

	

	public Double getSaldo() {
		return saldo.doubleValue();
	}

	public Double getImporte() {
		return importe.doubleValue();
	}

	public void setImporte(Double importe) {
		this.importe = new BigDecimal(importe);
	}

	public Asiento getAsientoGenerado() {
		return asientoGenerado;
	}

	public void setAsientoGenerado(Asiento asientoGenerado) {
		this.asientoGenerado = asientoGenerado;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public List<ItemFactura> getItems() {
		return items;
	}

	public Usuario getResponsable() {
		return responsable;
	}

	public void setResponsable(Usuario responsable) {
		this.responsable = responsable;
	}

	public void addItemFactura(ItemFactura item) {
		if (!this.items.contains(item)) {
			item.setFactura(this);
			this.items.add(item);
		}
	}

	public Double sumarATotal(Double importe) {
		if (this.importe == null) {
			this.importe = new BigDecimal(0);
		}
		this.importe = this.importe.add(new BigDecimal(importe));
		return this.importe.doubleValue();
	}

	public Double restarATotal(Double importe) {
		if (this.importe == null) {
			this.importe = new BigDecimal(0);
		}
		this.importe = this.importe.subtract(new BigDecimal(importe));
		return this.importe.doubleValue();
	}

	public ItemFactura getItem(Long id) {
		return this.items.stream().filter(i -> i.getId() != null && i.getId().equals(id)).findFirst().orElse(null);
	}

	@Override
	public Asiento getAsiento() {
		return this.getAsientoGenerado();
	}

}

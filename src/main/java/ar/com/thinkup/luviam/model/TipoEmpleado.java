package ar.com.thinkup.luviam.model;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity()
@Table(name = "tipo_empleado")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class TipoEmpleado extends Parametrico {

	/**
	 * 
	 */
	public static final String VENDEDOR = "VEND";
	private static final long serialVersionUID = 5421268324452414731L;

	public TipoEmpleado() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TipoEmpleado(Boolean habilitado, String codigo, String descripcion, Boolean esSistema) {
		super(habilitado, codigo, descripcion, esSistema);
		// TODO Auto-generated constructor stub
	}

	public TipoEmpleado(Long id, Boolean habilitado, String codigo, String descripcion, Boolean esSistema) {
		super(id, habilitado, codigo, descripcion, esSistema);
		// TODO Auto-generated constructor stub
	}

}

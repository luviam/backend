package ar.com.thinkup.luviam.model.productoProveedor;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.Parametrico;

@Entity()
@Table(name = "tipo_producto_proveedor")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class TipoProducto extends Parametrico {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3632314873451003439L;

}

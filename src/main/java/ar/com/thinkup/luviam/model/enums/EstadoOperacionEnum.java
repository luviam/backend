package ar.com.thinkup.luviam.model.enums;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public enum EstadoOperacionEnum {
	PENDIENTE("P", "Pendiente aprobación"), APROBADO("A", "Aprobado"), RECHAZADO("R", "Rechazado"), POR_PAGAR("PP",
			"Pendiente de Pago"), LIQUIDADO("L", "Liquidado");

	private String codigo;
	private String descripcion;

	private EstadoOperacionEnum(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public static EstadoOperacionEnum getByCodigo(String codigo) {

		for (EstadoOperacionEnum ele : EstadoOperacionEnum.values()) {
			if (ele.getCodigo().equals(codigo)) {
				return ele;
			}

		}
		return null;

	}

	public static DescriptivoGeneric getDescriptivo(String codigoEstado) {
		EstadoOperacionEnum e = EstadoOperacionEnum.getByCodigo(codigoEstado);
		if (e == null) {
			return new DescriptivoGeneric("SD", "SIN DEFINIR");
		}
		return new DescriptivoGeneric(e.getCodigo(), e.getDescripcion());
	}

}

package ar.com.thinkup.luviam.model.plan;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.enums.TipoCuentaEnum;
import ar.com.thinkup.luviam.vo.CuentaPlanVO;

@Entity
@Table(name = "grupo")
@Where(clause = "deleted=0")
public class Grupo extends NodoPlanCuentaGrupo implements IDescriptivo {

	public Grupo(CuentaPlanVO cuenta, Rubro parent) {
		super(cuenta);
		this.subGrupos = new HashSet<>();
		this.subGrupos
				.addAll(cuenta.getCuentasHijas().stream().map(c -> new SubGrupo(c, this)).collect(Collectors.toList()));
		this.rubro = parent;
	}

	public Grupo() {
		super();
		// TODO Auto-generated constructor stub
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rubro_id")
	private Rubro rubro;

	@OrderBy("codigo ASC")
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "grupo")
	@Where(clause = "deleted=0")
	private Set<SubGrupo> subGrupos;

	public Rubro getRubro() {
		return rubro;
	}

	public void setRubro(Rubro rubro) {
		this.rubro = rubro;
	}

	public Set<SubGrupo> getSubGrupos() {
		return subGrupos;
	}

	public void setSubGrupos(Set<SubGrupo> subGrupos) {
		this.subGrupos = subGrupos;
	}

	@Override
	public Set<? extends NodoPlanCuenta> getHijos() {
		return this.subGrupos;
	}

	@Override
	public String getTipoCuenta() {
		return TipoCuentaEnum.GRUPO.getCodigo();
	}

	@Override
	public String getDescripcion() {
		return this.getNombre();
	}

}

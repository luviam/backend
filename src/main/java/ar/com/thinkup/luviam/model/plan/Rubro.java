package ar.com.thinkup.luviam.model.plan;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.enums.TipoCuentaEnum;
import ar.com.thinkup.luviam.vo.CuentaPlanVO;

@Entity
@Table(name = "rubro")
@Where(clause = "deleted=0")
public class Rubro extends NodoPlanCuentaGrupo implements IDescriptivo {

	public Rubro() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Rubro(CuentaPlanVO cuenta) {
		super(cuenta);
		this.grupos = new HashSet<>();
		this.grupos.addAll(cuenta.getCuentasHijas().stream().map(c -> new Grupo(c, this)).collect(Collectors.toList()));
	}

	@OrderBy("codigo ASC")
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "rubro")
	@Where(clause = "deleted=0")
	public Set<Grupo> grupos;

	public Set<Grupo> getGrupos() {
		return grupos;
	}

	public void setGrupos(Set<Grupo> grupos) {
		this.grupos = grupos;
	}

	@Override
	public Set<? extends NodoPlanCuenta> getHijos() {
		return this.grupos;
	}

	@Override
	public String getTipoCuenta() {
		return TipoCuentaEnum.RUBRO.getCodigo();
	}

	@Override
	public String getDescripcion() {
		return this.getNombre();
	}
}

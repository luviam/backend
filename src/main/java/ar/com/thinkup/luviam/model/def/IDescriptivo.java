package ar.com.thinkup.luviam.model.def;

import java.beans.Transient;
import java.io.Serializable;

/**
 * 
 * @author Thinkup
 *
 *         Interfaz para representar una entidad que permita ser Descripta.
 *
 */
public interface IDescriptivo extends Serializable {
	/**
	 * Codigo que identifica al Descriptivo
	 * 
	 * @return String Codigo
	 */
	String getCodigo();

	/**
	 * Descripción del Descriptivo. Se orienta el uso para mostrar en pantalla o en impresiones
	 * 
	 * @return String descriptivo.
	 */
	String getDescripcion();
	
	@Transient
	default DescriptivoGeneric getDescriptivo() {
		return new DescriptivoGeneric(this.getCodigo(),this.getDescripcion());
	}
	
}

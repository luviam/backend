package ar.com.thinkup.luviam.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.model.security.Usuario;

@Entity
@Table(name = "traspaso_caja_operacion")
public class TraspasoCaja {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "cuenta_caja_origen_id", nullable = false)
	private CuentaAplicacion cajaOrigen;

	@ManyToOne
	@JoinColumn(name = "cuenta_caja_destino_id", nullable = false)
	private CuentaAplicacion cajaDestino;

	@Column()
	private String descripcion = "Traspaso de caja";

	@ManyToOne
	@JoinColumn(name = "responsable_id", nullable = false)
	private Usuario responsable;

	@Column(name = "monto_traspasado", nullable = false, precision = 10, scale = 2)
	private BigDecimal monto = new BigDecimal(0);

	@Column(name = "estado_operacion", nullable = false, length = 1)
	private String codigoEstado = EstadoOperacionEnum.PENDIENTE.getCodigo();

	@Column(name = "fecha_operacion")
	private Date fechaOperacion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CuentaAplicacion getCajaOrigen() {
		return cajaOrigen;
	}

	public void setCajaOrigen(CuentaAplicacion cajaOrigen) {
		this.cajaOrigen = cajaOrigen;
	}

	public CuentaAplicacion getCajaDestino() {
		return cajaDestino;
	}

	public void setCajaDestino(CuentaAplicacion cajaDestino) {
		this.cajaDestino = cajaDestino;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Usuario getResponsable() {
		return responsable;
	}

	public void setResponsable(Usuario responsable) {
		this.responsable = responsable;
	}

	public Double getMonto() {
		return monto.doubleValue();
	}

	public void setMonto(Double monto) {
		this.monto = new BigDecimal(monto);
	}

	public String getCodigoEstado() {
		return codigoEstado;
	}

	public void setCodigoEstado(String codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	public Date getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(Date fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

}

package ar.com.thinkup.luviam.model.enums;

public enum CuentasPrincipalesEnum {
	CAJA("A.1.1"), BANCOS("A.1.2"), AJUSTE("AJUSTE"), CHEQUES("A.1.3"), BIENES_USO("A.BUSO"), EGRESOS("EG"), CTA_CTE_PROVEEDORES(
			"CC_PROV"), IVA_CREDITO_FISCAL("IVA_CRED"), IIBB_CREDITO_FISCAL("IIBB_CRED"), OTROS_IMPUESTOS("OTROS_IMP"),
	CHEQUES_CLIENTES("CHCLI"), CHEQUES_OTROS("CHOTR"),
	CC_PROVEEDORES_CON_CREDITO("P.CCP_CC"), CC_PROVEEDORES_SIN_CREDITO("P.CCP_SC"), CC_PROVEEDORES_NO_HABITUALES("P.CCP_NH"), CC_PROVEEDORES("P.CC_PROV"), 
	CREDITO_FISCAL("CRED_FISC"), SG_UNIDADES_NEGOCIO("P.2.1"), EVENTOS_A_REALIZARSE("EVE_REA"),
	CC_CLIENTE("CC_CLI"), IVA_DEBITO_FISCAL("IVA_DEB"), DEBITOS_FISCALES("DEB_FIS"), COMISIONES_A_DEVENGAR("COM_DEV_"), 
	COMISIONES_A_PAGAR("COM_");
	
	String codigo;

	private CuentasPrincipalesEnum(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	
}

package ar.com.thinkup.luviam.model;

import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.repository.ClienteRepository.ClienteCabecera;
import ar.com.thinkup.luviam.repository.ClienteRepository.ClienteDescriptivo;
import ar.com.thinkup.luviam.vo.ConvenioSalon;

/**
 * 
 * @author ThinkUp
 *
 */
@Entity
@Table(name = "cliente")
@Where(clause = "activo='1'")
public class Cliente implements ClienteCabecera, ClienteDescriptivo, IDescriptivo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5333185482780919958L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "nombreCliente")
	private String nombreCliente;

	@Column(name = "nombreContacto")
	private String nombreContacto;

	@Column(name = "domicilio")
	private String domicilio;

	@Column(name = "localidad")
	private String localidad;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "celular")
	private String celular;

	@Column(name = "email")
	private String email;


	@Column(name = "razon_social")
	private String razonSocial;

	@Column(name = "cuit")
	private String cuit;

	@ManyToOne
	@JoinColumn(name = "tipo_iva_id")
	private TipoIVA iva;

	@Column(name = "domicilio_facturacion")
	private String domicilioFacturacion;

	@Column(name = "localidad_facturacion")
	private String localidadFacturacion;

	@ManyToOne
	@JoinColumn(name = "tipo_cliente_id")
	private TipoCliente tipoCliente;

	@Transient
	private ConvenioSalon convenio;

	@Column(name = "nombre_firmante")
	private String nombreFirmante;

	@Column(name = "dni_firmante")
	private String dniFirmante;

	@Column(name = "porcentaje_comision")
	private Float porcComision;

	@Column(name = "valor_comision")
	private Float valorComision;

	@Column(name = "porcentaje_descuentos")
	private Float porcDescuentos;

	@Column(name = "valor_adicional")
	private Float valorAdicional;

	@Column(name = "activo")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean activo;
	@Column(name = "facebook", length = 200)
	private String facebook;
	@Column(name = "instagram", length = 200)
	private String instagram;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "cliente")
	private List<Contacto> contactos = new Vector<>();

	public Cliente() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getNombreContacto() {
		return nombreContacto;
	}

	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	
	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public TipoIVA getIva() {
		return iva;
	}

	public void setIva(TipoIVA iva) {
		this.iva = iva;
	}

	public String getDomicilioFacturacion() {
		return domicilioFacturacion;
	}

	public void setDomicilioFacturacion(String domicilioFacturacion) {
		this.domicilioFacturacion = domicilioFacturacion;
	}

	public String getLocalidadFacturacion() {
		return localidadFacturacion;
	}

	public void setLocalidadFacturacion(String localidadFacturacion) {
		this.localidadFacturacion = localidadFacturacion;
	}

	public ConvenioSalon getConvenio() {
		return convenio;
	}

	public void setConvenio(ConvenioSalon convenio) {
		this.convenio = convenio;
	}

	public String getNombreFirmante() {
		return nombreFirmante;
	}

	public void setNombreFirmante(String nombreFirmante) {
		this.nombreFirmante = nombreFirmante;
	}

	public String getDniFirmante() {
		return dniFirmante;
	}

	public void setDniFirmante(String dniFirmante) {
		this.dniFirmante = dniFirmante;
	}

	public Float getPorcComision() {
		return porcComision;
	}

	public void setPorcComision(Float porcComision) {
		this.porcComision = porcComision;
	}

	public Float getValorComision() {
		return valorComision;
	}

	public void setValorComision(Float valorComision) {
		this.valorComision = valorComision;
	}

	public Float getPorcDescuentos() {
		return porcDescuentos;
	}

	public void setPorcDescuentos(Float porcDescuentos) {
		this.porcDescuentos = porcDescuentos;
	}

	public Float getValorAdicional() {
		return valorAdicional;
	}

	public void setValorAdicional(Float valorAdicional) {
		this.valorAdicional = valorAdicional;
	}

	public TipoCliente getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(TipoCliente tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getInstagram() {
		return instagram;
	}

	public void setInstagram(String instagram) {
		this.instagram = instagram;
	}

	public List<Contacto> getContactos() {
		return contactos;
	}

	public void setContactos(List<Contacto> contactos) {
		this.contactos = contactos;
	}

	public void agregarContacto(Contacto contacto) {
		contacto.setCliente(this);
		this.contactos.add(contacto);

	}

	public Contacto getContacto(Long id) {
		return this.contactos.stream().filter(i -> i.getId() != null && i.getId().equals(id)).findFirst().orElse(null);
	}

	@PostLoad
	public void initIt() throws Exception {
		if (this.convenio == null) {
			this.convenio = new ConvenioSalon();
			this.convenio.setPorcComision(this.porcComision);
			this.convenio.setPorcDescuentos(this.porcDescuentos);
			this.convenio.setValorAdicional(this.valorAdicional);
			this.convenio.setValorComision(this.valorComision);
		} else {
			this.setPorcComision(this.getConvenio().getPorcComision());
			this.setPorcDescuentos(this.getConvenio().getPorcDescuentos());
			this.setValorAdicional(this.getConvenio().getValorAdicional());
			this.setValorComision(this.getConvenio().getValorComision());
		}
	}

	@Override
	public String getCodigo() {
		return this.id + "";
	}

	@Override
	public String getDescripcion() {

		return this.nombreCliente;
	}
}

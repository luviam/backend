package ar.com.thinkup.luviam.model.def;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Implementación default de {@link IDescriptivo}
 * 
 * @author Thinkup
 *
 */
@MappedSuperclass
public class DescriptivoGeneric implements IDescriptivo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6428179237416249079L;

	public static DescriptivoGeneric TODOS() {
		return new DescriptivoGeneric(-1, "Todos");
	}

	public static DescriptivoGeneric SIN_DEFINIR() {
		return new DescriptivoGeneric("SD", "SIN DEFINIR");
	}

	@Column(name = "codigo")
	private String codigo;

	@Column(name = "descripcion")
	private String descripcion;

	public DescriptivoGeneric() {
		super();
		this.codigo = "";
		this.descripcion = "";
	}

	public DescriptivoGeneric(IDescriptivo descritpivo) {
		super();
		if (descritpivo == null) {
			this.codigo = "SD";
			this.descripcion = "Sin Definir";
		} else {
			this.codigo = descritpivo.getCodigo();
			this.descripcion = descritpivo.getDescripcion();
		}

	}

	public DescriptivoGeneric(String codigo, String descripcion) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public DescriptivoGeneric(Number id, String nombre) {
		super();
		this.codigo = String.valueOf(id);
		this.descripcion = nombre;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String getCodigo() {
		return this.codigo;
	}

	@Override
	public String getDescripcion() {
		return this.descripcion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DescriptivoGeneric other = (DescriptivoGeneric) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return codigo + ", " + descripcion;
	}

}

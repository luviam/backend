package ar.com.thinkup.luviam.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.vo.TotalesGastosVO;
import ar.com.thinkup.luviam.vo.indicadores.TotalGastoVO;
import ar.com.thinkup.utils.MathUtils;

/**
 * 
 * @author ThinkUp
 *
 */

@SqlResultSetMappings(value = {
		@SqlResultSetMapping(name = "getGastosMapping", classes = {
				@ConstructorResult(targetClass = TotalesGastosVO.class, columns = {
						@ColumnResult(name = "idRubro", type = Long.class), @ColumnResult(name = "rubro"),
						@ColumnResult(name = "idGrupo", type = Long.class), @ColumnResult(name = "grupo"),
						@ColumnResult(name = "idSubGrupo", type = Long.class), @ColumnResult(name = "subGrupo"),
						@ColumnResult(name = "idCuenta", type = Long.class), @ColumnResult(name = "cuenta"),
						@ColumnResult(name = "debe"), @ColumnResult(name = "haber") }) }),
		@SqlResultSetMapping(name = "TotalGastoMap", classes = {
				@ConstructorResult(targetClass = TotalGastoVO.class, columns = { @ColumnResult(name = "codigo"),
						@ColumnResult(name = "descripcion"), @ColumnResult(name = "total", type = Double.class) }) }) })

@NamedNativeQueries(value = {
		@NamedNativeQuery(name = "Operacion.getTotalesPorGastos", resultSetMapping = "getGastosMapping", query = "select r.id as idRubro, r.nombre as rubro, g.id as idGrupo, g.nombre as grupo, sg.id as idSubGrupo, "
				+ "	sg.nombre as subGrupo , ca.id as idCuenta, ca.nombre as cuenta, "
				+ "	sum(o.debe) as debe, sum(o.haber) as haber" + " from cuenta_aplicacion ca "
				+ " join operacion o on ca.id = o.cuenta_id " + " join centro c on c.id = o.centro_id"
				+ " join asiento a on a.id = o.asiento_id" + " join sub_grupo sg on sg.id = ca.sub_grupo_id"
				+ " join grupo g on g.id = sg.grupo_id" + " join rubro r on r.id = g.rubro_id"
				+ " where r.codigo = 'EG' " + " and ( ?1 = '-1' or c.codigo = ?1)"
				+ " and ( ?2 = '-1' or g.codigo = ?2 )" + " and ( ?3 = '-1' or sg.codigo = ?3 )"
				+ " and a.fecha between ?4 and ?5"
				+ " group by r.id, r.nombre, g.id, g.nombre, sg.id, sg.nombre, ca.id, ca.nombre"
				+ " order by rubro asc, grupo asc, subgrupo asc, cuenta asc"),
		@NamedNativeQuery(name = "Operacion.getTotalSubgrupo", resultSetMapping = "TotalGastoMap", query = "select sg.codigo as codigo, "
				+ "	sg.nombre as descripcion, " + "	sum(o.debe - o.haber) as total" + " from cuenta_aplicacion ca "
				+ " join operacion o on ca.id = o.cuenta_id " + " join centro c on c.id = o.centro_id"
				+ " join asiento a on a.id = o.asiento_id" + " join sub_grupo sg on sg.id = ca.sub_grupo_id"
				+ " join grupo g on g.id = sg.grupo_id" + " join rubro r on r.id = g.rubro_id"
				+ " where r.codigo = 'EG' " + " and ( ?1 = '-1' or c.codigo = ?1)"
				+ " and ( ?2 = '-1' or sg.codigo = ?2 )" + " and a.fecha between ?3 and ?4"
				+ " group by sg.codigo, sg.nombre") })

@Entity
@Table(name = "operacion")
public class Operacion {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "asiento_id")
	private Asiento asiento;

	@ManyToOne
	@JoinColumn(name = "centro_id")
	private CentroCosto centro;

	@ManyToOne()
	@JoinColumn(name = "cuenta_id")
	private CuentaAplicacion cuenta;

	@Column(name = "detalle")
	private String detalle;

	@Column(name = "debe", precision = 10, scale = 2)
	private BigDecimal debe = new BigDecimal(0);

	@Column(name = "haber", precision = 10, scale = 2)
	private BigDecimal haber = new BigDecimal(0);

	@Column(name = "comprobante")
	private String comprobante;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CentroCosto getCentro() {
		return centro;
	}

	public void setCentro(CentroCosto centro) {
		this.centro = centro;
	}

	public CuentaAplicacion getCuenta() {
		return cuenta;
	}

	public void setCuenta(CuentaAplicacion cuenta) {
		this.cuenta = cuenta;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public Double getDebe() {
		return debe == null ? 0d : this.debe.doubleValue();
	}

	public void setDebe(Double debe) {
		this.debe = debe != null ? new BigDecimal(MathUtils.round(debe, 2)) : new BigDecimal(0);

	}

	public Double getHaber() {
		return haber == null ? 0d : this.haber.doubleValue();
	}

	public void setHaber(Double haber) {
		this.haber = haber != null ? new BigDecimal(MathUtils.round(haber, 2)) : new BigDecimal(0);
	}

	public String getComprobante() {
		return comprobante;
	}

	public void setComprobante(String comprobante) {
		this.comprobante = comprobante;
	}


	public Asiento getAsiento() {
		return asiento;
	}

	public void setAsiento(Asiento asiento) {
		this.asiento = asiento;
	}

	@Override
	public String toString() {
		return (this.centro != null ? this.centro.getCodigo() + " " : "SD ")
				+ (this.cuenta != null ? this.cuenta.getDescripcion() + "	" : "SD")
				+ (this.debe != null ? this.debe.doubleValue() + "	" : "0	")
				+ (this.haber != null ? this.haber.doubleValue() + "" : "0");
	}

}

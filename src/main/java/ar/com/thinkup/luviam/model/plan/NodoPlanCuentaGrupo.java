package ar.com.thinkup.luviam.model.plan;

import java.util.Set;

import javax.persistence.MappedSuperclass;

import ar.com.thinkup.luviam.vo.CuentaPlanVO;

@MappedSuperclass
public abstract class NodoPlanCuentaGrupo extends NodoPlanCuenta {

	public NodoPlanCuentaGrupo(CuentaPlanVO cuenta) {
		this.setCodigo(cuenta.getCodigo());
		this.setActivo(cuenta.getActivo());
		this.setNombre(cuenta.getDescripcion());

	}

	public NodoPlanCuentaGrupo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public abstract Set<? extends NodoPlanCuenta> getHijos();

}

package ar.com.thinkup.luviam.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.repository.CentroRepository.CentroDescriptivo;

/**
 * 
 * @author ThinkUp
 *
 */
@Entity
@Table(name = "centro")
@Where(clause = "activo='1'")
public class CentroCosto implements IDescriptivo, CentroDescriptivo{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6109984367446349980L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "codigo")
	private String codigo;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "razon_social")
	private String razonSocial;

	@Column(name = "cuit")
	private String cuit;

	@Column(name = "tipo_ingreso_bruto", length = 1)
	private String tipoIIBB;

	@Column(name = "iibb")
	private String iibb;

	@Column(name = "domicilio_fiscal")
	private String domicilioFiscal;

	@Column(name = "codigo_postal")
	private String codigoPostal;

	@ManyToOne
	@JoinColumn(name = "provincia_id")
	public Provincia provincia;

	@Column(name = "activo")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean activo;

	@OrderBy("tipo_comprobante_id ASC")
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "centro_costo_id", nullable = false)
	private Set<CentroCostosTipoComprobante> comprobantesDisponibles = new HashSet<>();

	@OrderBy("codigo ASC")
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "centroCosto")
	@Where(clause = "deleted=0")
	private Set<CuentaAplicacion> cuentasAplicacion=  new HashSet<>();;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Override
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	@Override
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public String getIibb() {
		return iibb;
	}

	public void setIibb(String iibb) {
		this.iibb = iibb;
	}

	public String getDomicilioFiscal() {
		return domicilioFiscal;
	}

	public void setDomicilioFiscal(String domicilioFiscal) {
		this.domicilioFiscal = domicilioFiscal;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
		
	}

	public Set<CentroCostosTipoComprobante> getComprobantesDisponibles() {
		return comprobantesDisponibles;
	}

	public void setComprobantesDisponibles(Set<CentroCostosTipoComprobante> comprobantesDisponibles) {
		this.comprobantesDisponibles = comprobantesDisponibles;
	}

	public Set<CuentaAplicacion> getCuentasAplicacion() {
		return cuentasAplicacion;
	}

	public void setCuentasAplicacion(Set<CuentaAplicacion> cuentasAplicacion) {
		this.cuentasAplicacion = cuentasAplicacion;
	}

	public String getTipoIIBB() {
		return tipoIIBB;
	}

	public void setTipoIIBB(String tipoIIBB) {
		this.tipoIIBB = tipoIIBB;
	}

	public Provincia getProvincia() {
		return provincia;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public void agregarCuenta(CuentaAplicacion cuentaDb) {
		if (this.cuentasAplicacion == null) {
			this.cuentasAplicacion = new HashSet<>();
		}

		if (!this.cuentasAplicacion.stream().anyMatch(ca -> ca.getCodigo().equals(cuentaDb.getCodigo()))) {
			this.cuentasAplicacion.add(cuentaDb);
			cuentaDb.setCentroCosto(this);
		}

	}

	@Override
	public String getDescripcion() {
		return this.nombre;
	}

	public CuentaAplicacion getCuentaByCodigo(String codigo) {
		return this.cuentasAplicacion.stream().filter(c -> StringUtils.isNotEmpty(c.getCodigo()) && c.getCodigo().equals(codigo)).findFirst().orElse(null);
	}

	public CentroCostosTipoComprobante getTipoComprobante(Long idComprobante) {
		return this.comprobantesDisponibles.stream().filter(c -> c.getId() != null && c.getId().equals(idComprobante)).findFirst().orElse(null);
		
	}

	public void agregarComprobante(CentroCostosTipoComprobante c) {
		this.comprobantesDisponibles.add(c);
		
	}

	public CuentaAplicacion getCuentaAplicacion(Long id) {
		return this.cuentasAplicacion.stream().filter(c -> c.getId()!= null && c.getId().equals(id)).findFirst().orElse(null);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((activo == null) ? 0 : activo.hashCode());
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CentroCosto other = (CentroCosto) obj;
		if (activo == null) {
			if (other.activo != null)
				return false;
		} else if (!activo.equals(other.activo))
			return false;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}

package ar.com.thinkup.luviam.model.enums;

public enum TipoImputacionEnum {
	FACTURACION("FC", "Facturación");
	private String codigo;
	private String descripcion;

	private TipoImputacionEnum(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;

	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public static TipoImputacionEnum getByCodigo(String codigo) {

		for (TipoImputacionEnum ele : TipoImputacionEnum.values()) {
			if (ele.getCodigo().equals(codigo)) {
				return ele;
			}

		}
		return null;

	}

}

package ar.com.thinkup.luviam.model.enums;

import java.util.NoSuchElementException;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

public enum TipoPagoEnum {
EFECTIVO("E","Efectivo"),CHEQUE_PROPIO("P","Cheque Propio"),
CHEQUE_CLIENTE("C","Cheque de Cliente"), CHEQUE_OTROS("O","Cheque Otros"), TRANSFERENCIA("T","Transferencia Bancaria");
	
	String codigo;
	String descripcion;

	private TipoPagoEnum(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}
	
	
	public String getDescripcion() {
		return descripcion;
	}

	public static TipoPagoEnum byCodigo(String codigo) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(codigo)) {
			throw new IllegalArgumentException();
		}
		return Stream.of(TipoPagoEnum.values()).filter(tc->tc.codigo.equals(codigo)).findFirst().get();
	}

	
}

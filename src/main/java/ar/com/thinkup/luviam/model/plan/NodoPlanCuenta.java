package ar.com.thinkup.luviam.model.plan;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.Type;

@MappedSuperclass
public abstract class NodoPlanCuenta implements Comparable<NodoPlanCuenta> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "codigo")
	private String codigo;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "activo")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean activo;
	
	@Column(columnDefinition="tinyint(1) default 1", name = "eliminable", insertable=false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean eliminable;
	
	@Column(columnDefinition="tinyint(1) default 0", name = "deleted", insertable=false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean deleted;

	public abstract String getTipoCuenta();
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	@Override
	public int compareTo(NodoPlanCuenta o) {
		return this.getCodigo().compareTo(o.getCodigo());
	}
	public Boolean getEliminable() {
		return eliminable;
	}
	public void setEliminable(Boolean eliminable) {
		this.eliminable = eliminable;
	}
	public Boolean getDeleted() {
		return deleted;
	}
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	
	


}

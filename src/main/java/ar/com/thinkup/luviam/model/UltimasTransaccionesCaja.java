/**
 * 
 */
package ar.com.thinkup.luviam.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import ar.com.thinkup.luviam.model.enums.TipoTransaccionCajaEnum;

/**
 * @author nobregon
 *
 */
@Entity
@Subselect("select * from ultimas_transacciones_caja")
@Immutable
public class UltimasTransaccionesCaja implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7466434838574262476L;

	@Id
	@Column(name="caja_id")
	private Long id;	
	
	@Column(name="fecha_ultima_apertura")
	private Date fechaUltimaApertura;
	
	@Column(name="fecha_ultimo_cierre")
	private Date fechaUltimoCierre;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaUltimaApertura() {
		return fechaUltimaApertura;
	}

	public void setFechaUltimaApertura(Date fechaUltimaApertura) {
		this.fechaUltimaApertura = fechaUltimaApertura;
	}

	public Date getFechaUltimoCierre() {
		return fechaUltimoCierre;
	}

	public void setFechaUltimoCierre(Date fechaUltimoCierre) {
		this.fechaUltimoCierre = fechaUltimoCierre;
	}
	
	public TipoTransaccionCajaEnum getUltimaTransaccion() {
		if (this.fechaUltimaApertura == null) {
			return null;
		}
		
		return this.fechaUltimoCierre == null || this.fechaUltimaApertura.after(this.fechaUltimoCierre) ? 
					TipoTransaccionCajaEnum.APERTURA : 
					TipoTransaccionCajaEnum.CIERRE; 
		
		
	}
}

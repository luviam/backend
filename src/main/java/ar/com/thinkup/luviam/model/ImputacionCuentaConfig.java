package ar.com.thinkup.luviam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.enums.TipoImputacionEnum;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;

@Entity
@Table(name = "configuracion_cuentas_imputacion")
public class ImputacionCuentaConfig {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="proveedor_id", nullable = false)
	private Proveedor proveedor;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="centro_costo_id", nullable = false)
	private CentroCosto centroCosto;
	
	@ManyToOne(fetch=FetchType.EAGER )
	@JoinColumn(name="cuenta_aplicacion_id", nullable = true)
	private CuentaAplicacion cuentaAplicacion;
	
	@Column(name="codigo_tipo_imputacion")
	private String codigoTipoImputacion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public CentroCosto getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(CentroCosto centroCosto) {
		this.centroCosto = centroCosto;
	}

	public CuentaAplicacion getCuentaAplicacion() {
		return cuentaAplicacion;
	}

	public void setCuentaAplicacion(CuentaAplicacion cuentaAplicacion) {
		this.cuentaAplicacion = cuentaAplicacion;
	}

	public String getCodigoTipoImputacion() {
		return codigoTipoImputacion;
	}

	public void setCodigoTipoImputacion(String codigoTipoImputacion) {
		this.codigoTipoImputacion = codigoTipoImputacion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoTipoImputacion == null) ? 0 : codigoTipoImputacion.hashCode());
		result = prime * result + ((cuentaAplicacion == null) ? 0 : cuentaAplicacion.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((proveedor == null) ? 0 : proveedor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImputacionCuentaConfig other = (ImputacionCuentaConfig) obj;
		if (codigoTipoImputacion == null) {
			if (other.codigoTipoImputacion != null)
				return false;
		} else if (!codigoTipoImputacion.equals(other.codigoTipoImputacion))
			return false;
		if (cuentaAplicacion == null) {
			if (other.cuentaAplicacion != null)
				return false;
		} else if (!cuentaAplicacion.equals(other.cuentaAplicacion))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (proveedor == null) {
			if (other.proveedor != null)
				return false;
		} else if (!proveedor.equals(other.proveedor))
			return false;
		return true;
	}

	public TipoImputacionEnum getTipoImputacion() {
		return this.codigoTipoImputacion != null? TipoImputacionEnum.getByCodigo(this.codigoTipoImputacion) : null;
	}

}

package ar.com.thinkup.luviam.model.enums;

import java.util.Arrays;
import java.util.Optional;

public enum TipoTransaccionCajaEnum {
	APERTURA(1), CIERRE(2);
	
	private Integer codigo;

	
	private TipoTransaccionCajaEnum(Integer cod) {
		this.codigo = cod;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	public static TipoTransaccionCajaEnum byCodigo(Integer cod) {
		Optional<TipoTransaccionCajaEnum> tipo = Arrays.asList(TipoTransaccionCajaEnum.values()).stream().filter(t->t.codigo == cod).findFirst();
		return tipo.isPresent() ? tipo.get() : null;
	}
	
	
}

package ar.com.thinkup.luviam.model.contratos;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "contratos_cabecera_totales")
@Entity
public class ContratoCabecera {

	@Id
	private Long id;

	@Column(name = "codigo_estado")
	private String codigoEstado;

	@Column(name = "codigo_horario")
	private String codigoHorario;

	@Column(name = "fecha_contratacion")
	private Date fechaContratacion;

	@Column(name = "vendedor")
	private String nombreVendedor;

	@Column(name = "fecha_evento")
	private Date fechaEvento;

	@Column(name = "locacion")
	private String locacion;

	@Column(name = "tipo_evento")
	private String tipoEvento;

	@Column(name = "cliente_id")
	private Long idCliente;

	private String cliente;

	@Column(name = "total", precision = 10, scale = 2)
	private BigDecimal total = new BigDecimal(0);

	@Column(name = "cobros", precision = 10, scale = 2)
	private BigDecimal cobros = new BigDecimal(0);

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigoEstado() {
		return codigoEstado;
	}

	public void setCodigoEstado(String codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	public String getCodigoHorario() {
		return codigoHorario;
	}

	public void setCodigoHorario(String codigoHorario) {
		this.codigoHorario = codigoHorario;
	}

	public Date getFechaContratacion() {
		return fechaContratacion;
	}

	public void setFechaContratacion(Date fechaContratacion) {
		this.fechaContratacion = fechaContratacion;
	}

	public Date getFechaEvento() {
		return fechaEvento;
	}

	public void setFechaEvento(Date fechaEvento) {
		this.fechaEvento = fechaEvento;
	}

	public String getLocacion() {
		return locacion;
	}

	public void setLocacion(String locacion) {
		this.locacion = locacion;
	}

	public String getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public Double getTotal() {
		return total != null ? total.floatValue() : 0d;
	}

	public void setTotal(Double total) {
		this.total = new BigDecimal(total);
	}

	public Double getCobros() {
		return cobros != null ? cobros.floatValue() : 0d;
	}

	public void setCobros(Double cobros) {
		this.cobros = new BigDecimal(cobros);
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getNombreVendedor() {
		return nombreVendedor;
	}

	public void setNombreVendedor(String nombreVendedor) {
		this.nombreVendedor = nombreVendedor;
	}

}

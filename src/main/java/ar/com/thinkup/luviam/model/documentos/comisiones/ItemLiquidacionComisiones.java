package ar.com.thinkup.luviam.model.documentos.comisiones;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.contratos.PagoComision;
import ar.com.thinkup.luviam.vo.Identificable;

@Table(name = "item_liquidacion_comision")
@Entity
public class ItemLiquidacionComisiones implements Identificable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1929520611889512202L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "liquidacion_id", nullable = false)
	private LiquidacionComisiones liquidacion;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pago_comision_id", nullable = false)
	private PagoComision pagoComision;

	@Column(name = "importe", nullable = false, precision = 10, scale = 2)
	private BigDecimal importe;
	public Double getImporte() {
		return this.importe != null ? this.importe.doubleValue() : 0d;
	}

	public void setImporte(Double val) {
		this.importe = val != null ? new BigDecimal(val) : new BigDecimal(0);
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LiquidacionComisiones getLiquidacion() {
		return liquidacion;
	}

	public void setLiquidacion(LiquidacionComisiones liquidacion) {
		this.liquidacion = liquidacion;
	}

	public PagoComision getPagoComision() {
		return pagoComision;
	}

	public void setPagoComision(PagoComision pagoComision) {
		this.pagoComision = pagoComision;
	}



}

package ar.com.thinkup.luviam.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="marca_producto")
public class MarcaProductos extends Parametrico {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6149178967961806677L;

	@ManyToOne(fetch=FetchType.EAGER)
	private Archivo archivo;

	public Archivo getArchivo() {
		return archivo;
	}

	public void setArchivo(Archivo archivo) {
		this.archivo = archivo;
	}
	
	
}

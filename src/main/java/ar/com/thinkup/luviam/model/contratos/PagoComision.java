package ar.com.thinkup.luviam.model.contratos;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import ar.com.thinkup.luviam.model.enums.TipoPagoComision;
import ar.com.thinkup.luviam.vo.Identificable;

@Entity
@Table(name = "pago_comision")
public class PagoComision implements Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4075420821866600959L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "contrato_comision_id")
	private ConfiguracionComision configComision;

	@Column(name = "cantidad")
	private Integer cantidad = 0;
	@Column(name = "total_comision", scale = 2, precision = 10)
	private BigDecimal totalComision = new BigDecimal(0d);

	@Column(name = "total_liquidado", scale = 2, precision = 10)
	private BigDecimal totalLiquidado = new BigDecimal(0d);
	@Column(name = "descripcion")
	private String descripcion = "";

	@Column(name = "tipo_pago_comision", nullable = false)
	private String codigoTipoPagoComision;

	@Column(name = "fecha_comision")
	private Date fechaComision;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ConfiguracionComision getConfigComision() {
		return configComision;
	}

	public void setConfigComision(ConfiguracionComision configComision) {
		this.configComision = configComision;
	}

	public Double getTotalComision() {
		return totalComision != null ? totalComision.floatValue() : 0d;
	}

	public void setTotalComision(Double totalComision) {
		this.totalComision = totalComision != null ? new BigDecimal(totalComision) : new BigDecimal(0);
	}

	public Double getTotalLiquidado() {
		return totalLiquidado != null ? totalLiquidado.floatValue() : 0d;
	}

	public void setTotalLiquidado(Double totalLiquidado) {
		this.totalLiquidado = totalLiquidado != null ? new BigDecimal(totalLiquidado) : new BigDecimal(0);
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodigoTipoPagoComision() {
		return codigoTipoPagoComision;
	}

	public void setCodigoTipoPagoComision(String codigoTipoPagoComision) {
		this.codigoTipoPagoComision = codigoTipoPagoComision;
	}

	public TipoPagoComision getTipoPagoComision() {
		return StringUtils.isEmpty(this.codigoTipoPagoComision) ? null
				: TipoPagoComision.byCodigo(this.codigoTipoPagoComision);
	}

	public void setTipoPagoComision(TipoPagoComision tipo) {
		if (tipo != null) {
			this.codigoTipoPagoComision = tipo.getCodigo();
		} else {
			this.codigoTipoPagoComision = null;
		}
	}

	public Date getFechaComision() {
		return fechaComision;
	}

	public void setFechaComision(Date fechaComision) {
		this.fechaComision = fechaComision;
	}

	public void addLiquidado(Double importe) {
		this.setTotalLiquidado(this.getTotalLiquidado() + importe);
		if (this.getTotalLiquidado() > this.getTotalComision()) {
			this.setTotalLiquidado(this.getTotalComision());
		}
		this.configComision.susSaldo(importe);

	}

	public void susLiquidado(Double importe) {
		this.setTotalLiquidado(this.getTotalLiquidado() - importe);
		if (this.getTotalLiquidado() < 0) {
			this.setTotalLiquidado(0d);
		}
		this.configComision.addSaldo(importe);

	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

}

package ar.com.thinkup.luviam.model.documentos.comisiones;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.Comisionista;
import ar.com.thinkup.luviam.model.documentos.DocumentoPago;
import ar.com.thinkup.luviam.repository.LiquidacionComisionesRepository.LiquidacionComisionesCabecera;
import ar.com.thinkup.luviam.service.contabilizacion.IContabilizable;

@Entity
@Table(name = "liquidacion_comision")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

public class LiquidacionComisiones extends DocumentoPago<PagoLiquidacion>
		implements LiquidacionComisionesCabecera, IContabilizable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3762683559833530506L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "comisionista_id", nullable = false)
	private Comisionista comisionista;

	@OrderBy("id ASC")
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "liquidacion")
	private Set<PagoLiquidacion> pagosLiquidacion = new HashSet<>();

	@OrderBy("id ASC")
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "liquidacion")
	private Set<ItemLiquidacionComisiones> items = new HashSet<>();

	@Column(name = "observaciones")
	private String observaciones;

	@Column(name = "premio_venta", scale = 2, precision = 10)
	private BigDecimal premio;
	
	@Column(name="fecha_pago")
	private Date fechaPago;

	@Override
	public Asiento getAsiento() {
		return this.getAsientoGenerado();
	}

	@Override
	public Set<PagoLiquidacion> getValores() {

		return this.pagosLiquidacion;
	}

	@Override
	public void addValor(PagoLiquidacion valor) {
		valor.setLiquidacion(this);
		this.pagosLiquidacion.add(valor);

	}

	public Set<PagoLiquidacion> getPagosLiquidacion() {
		return pagosLiquidacion;
	}

	public void setPagosLiquidacion(Set<PagoLiquidacion> pagosLiquidacion) {
		this.pagosLiquidacion = pagosLiquidacion;
	}

	public Set<ItemLiquidacionComisiones> getItems() {
		return items;
	}

	public void setItems(Set<ItemLiquidacionComisiones> items) {
		this.items = items;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Comisionista getComisionista() {
		return comisionista;
	}

	public void setComisionista(Comisionista comisionista) {
		this.comisionista = comisionista;
	}

	public ItemLiquidacionComisiones getItem(Long id) {
		return this.items.stream().filter(i -> i.getId() != null && i.getId().equals(id)).findFirst().orElse(null);
	}

	public void addItem(ItemLiquidacionComisiones item) {
		item.setLiquidacion(this);
		this.items.add(item);

	}

	public Double getTotal() {
		return this.items.stream().mapToDouble(p -> p.getImporte()).sum() + this.getPremio();
	}

	public Double getPremio() {
		return this.premio != null ? this.premio.doubleValue() : 0d;
	}

	public void setPremio(Double val) {
		this.premio = val != null ? new BigDecimal(val) : new BigDecimal(0);
	}

	public Integer getCantidadBase() {

		return this.items.stream()
				.mapToInt(i -> i.getPagoComision().getConfigComision().getServicioComisionado().getCantidad()).sum();
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	
	
}

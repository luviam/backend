package ar.com.thinkup.luviam.model;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;

@MappedSuperclass
public abstract class ImputacionCuenta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "centro_costo_id", nullable = false)
	private CentroCosto centroCosto;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cuenta_aplicacion_id", nullable = true)
	private CuentaAplicacion cuentaAplicacion;

	public ImputacionCuenta() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CentroCosto getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(CentroCosto centroCosto) {
		this.centroCosto = centroCosto;
	}

	public CuentaAplicacion getCuentaAplicacion() {
		return cuentaAplicacion;
	}

	public void setCuentaAplicacion(CuentaAplicacion cuentaAplicacion) {
		this.cuentaAplicacion = cuentaAplicacion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cuentaAplicacion == null) ? 0 : cuentaAplicacion.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImputacionCuenta other = (ImputacionCuenta) obj;

		if (cuentaAplicacion == null) {
			if (other.cuentaAplicacion != null)
				return false;
		} else if (!cuentaAplicacion.equals(other.cuentaAplicacion))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
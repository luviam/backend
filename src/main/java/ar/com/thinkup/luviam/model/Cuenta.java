package ar.com.thinkup.luviam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
@Entity
@Table(name = "cuenta_aplicacion")
public class Cuenta {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "codigo")
	private String codigo;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "activo")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean activo;

	@Column(columnDefinition="tinyint(1) default 1", name = "eliminable", insertable=false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean eliminable;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Boolean getEliminable() {
		return eliminable;
	}

	public void setEliminable(Boolean eliminable) {
		this.eliminable = eliminable;
	}
	
	
	

}

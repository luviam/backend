package ar.com.thinkup.luviam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.def.IDescriptivo;

@Entity
@Table(name="provincia")
public class Provincia implements IDescriptivo{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4631562166051106033L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="provincia_nombre")
	private String descripcion;
		
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return String.valueOf(this.id);
	}

}

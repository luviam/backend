package ar.com.thinkup.luviam.model.contratos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.Cliente;
import ar.com.thinkup.luviam.model.Salon;
import ar.com.thinkup.luviam.model.TipoEvento;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.empleados.Empleado;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.enums.TipoHorarioEnum;
import ar.com.thinkup.luviam.service.contabilizacion.IContabilizable;
import ar.com.thinkup.luviam.vo.Identificable;
import ar.com.thinkup.luviam.vo.contratos.ConfiguracionComisionVO;
import ar.com.thinkup.luviam.vo.contratos.ServicioContratadoVO;

@Entity()
@Table(name = "contrato")
public class Contrato implements Serializable, IContabilizable, Identificable, IDescriptivo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7455099568768594350L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "salon_id", nullable = true)
	private Salon salon;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "empleado_id", nullable = false)
	private Empleado vendedor;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tipo_evento_id", nullable = false)
	private TipoEvento tipoEvento;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cliente_id", nullable = false)
	private Cliente cliente;

	@Column(name = "codigo_horario")
	private String codigoHorario;

	@Column(name = "fecha_contratacion")
	private Date fechaContratacion;

	@Column(name = "fecha_evento")
	private Date fechaEvento;

	@Column(name = "base_invitados")
	private Integer baseInvitados;

	private Integer adultos;
	private Integer adolescentes;
	private Integer menores;
	private Integer bebes;

	@Column(name = "observaciones", columnDefinition = "TEXT")
	private String observaciones;

	@Column(name = "numero_presupuesto", length = 30)
	private String numeroPresupuesto;

	@OneToMany(mappedBy = "contrato", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	private Set<ServicioContratado> servicios = new HashSet<>();

	@OneToMany(mappedBy = "contrato", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	private Set<ConfiguracionComision> comisiones = new HashSet<>();

	@Column(name = "codigo_estado")
	private String codigoEstado;

	private String descripcion;

	@Column(name = "es_congelado")
	private Boolean esCongelado = false;

	// PARA EL SALON ALTERNATIVO
	@Column(name = "direccion_salon_alt")
	private String direccionSalon;

	@Column(name = "telefono_salon_alt")
	private String telefonoSalon;

	@Column(name = "email_salon_alt")
	private String emailSalon;

	@Column(name = "nombre_salon_alt")
	private String nombreSalon;

	@Column(name = "ip_maximo", scale = 2, precision = 10)
	private BigDecimal ipMaximo = new BigDecimal(0);

	@Column(name = "ip_monto", scale = 2, precision = 10)
	private BigDecimal ipMonto = new BigDecimal(0);

	@Column(name = "cobro_garantia")
	private BigDecimal cobroGarantia = new BigDecimal(0);

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "asiento_id", nullable = true)
	private Asiento asientoGenerado;

	@Column(name = "total_calculado", precision = 10, scale = 2)
	private BigDecimal totalCalculado;

	@Column(name = "agasajados")
	private String agasajados;

	@Column(name = "fecha_ultima_modificacion")
	private Date ultimaModificacion;

	public String getAgasajados() {
		return agasajados;
	}

	public void setAgasajados(String agasajados) {
		this.agasajados = agasajados;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Salon getSalon() {
		return salon;
	}

	public void setSalon(Salon salon) {
		this.salon = salon;
	}

	public Empleado getVendedor() {
		return vendedor;
	}

	public void setVendedor(Empleado vendedor) {
		this.vendedor = vendedor;
	}

	public TipoEvento getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(TipoEvento tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getCodigoHorario() {
		return codigoHorario;
	}

	public void setCodigoHorario(String codigoHorario) {
		this.codigoHorario = codigoHorario;
	}

	public Date getFechaContratacion() {
		return fechaContratacion;
	}

	public void setFechaContratacion(Date fechaContratacion) {
		this.fechaContratacion = fechaContratacion;
	}

	public Date getFechaEvento() {
		return fechaEvento;
	}

	public void setFechaEvento(Date fechaEvento) {
		this.fechaEvento = fechaEvento;
	}

	public Integer getBaseInvitados() {
		return baseInvitados;
	}

	public void setBaseInvitados(Integer baseInvitados) {
		this.baseInvitados = baseInvitados;
	}

	public Integer getAdultos() {
		return adultos;
	}

	public void setAdultos(Integer adultos) {
		this.adultos = adultos;
	}

	public Integer getAdolescentes() {
		return adolescentes;
	}

	public void setAdolescentes(Integer adolescentes) {
		this.adolescentes = adolescentes;
	}

	public Integer getMenores() {
		return menores;
	}

	public void setMenores(Integer menores) {
		this.menores = menores;
	}

	public Integer getBebes() {
		return bebes;
	}

	public void setBebes(Integer bebes) {
		this.bebes = bebes;
	}

	public String getNumeroPresupuesto() {
		return numeroPresupuesto;
	}

	public void setNumeroPresupuesto(String numeroPresupuesto) {
		this.numeroPresupuesto = numeroPresupuesto;
	}

	public String getDescripcion() {
		if (StringUtils.isEmpty(this.descripcion)) {
			SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
			return StringUtils.join(this.salon != null ? this.salon.getDescripcion() : "SIN SALON", " - ",
					this.fechaEvento != null ? sf.format(this.fechaEvento) : "SIN FECHA", " - ", this.codigoHorario);

		} else {
			return descripcion;
		}

	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public TipoHorarioEnum getTipoHorario() {
		return StringUtils.isEmpty(this.codigoHorario) ? null : TipoHorarioEnum.byCodigo(this.codigoHorario);
	}

	public void setTipoHorario(TipoHorarioEnum tipoHorario) {
		if (tipoHorario == null) {
			throw new IllegalArgumentException("El tipo de Horario es obligatorio");
		}
		this.codigoHorario = tipoHorario.getCodigo();

	}

	public boolean tieneServicio(ServicioContratado o) {
		return servicios.contains(o);
	}

	public boolean addServicio(ServicioContratado e) {
		e.setContrato(this);
		return servicios.add(e);
	}

	public boolean remove(ServicioContratado o) {
		return servicios.remove(o);
	}

	public List<ServicioContratado> getServicios() {
		return new Vector<>(this.servicios);
	}

	public void setServicios(List<ServicioContratado> servicios) {
		this.servicios = servicios.stream().collect(Collectors.toSet());
	}

	public Set<ServicioContratado> getServiciosSet() {
		return this.servicios;
	}

	public ServicioContratado getServicio(ServicioContratadoVO i) {
		if (i.getId() != null) {
			return this.servicios.stream().filter(s -> s.getId() != null && s.getId().equals(i.getId())).findFirst()
					.orElse(null);
		} else {
			return this.servicios.stream()
					.filter(s -> s.getProducto() != null && s.getProducto().getId().equals(i.getProducto().getId()))
					.findFirst().orElse(null);

		}

	}

	public List<ConfiguracionComision> getComisiones() {
		return new Vector<>(this.comisiones);
	}

	public Set<ConfiguracionComision> getComisionesSet() {
		return this.comisiones;
	}

	public void setComisiones(List<ConfiguracionComision> comisiones) {
		this.comisiones = comisiones.stream().collect(Collectors.toSet());
	}

	public ConfiguracionComision getComision(ConfiguracionComisionVO i) {

		return this.comisiones.stream().filter(c -> c.getId() != null && c.getId().equals(i.getId())).findFirst()
				.orElse(null);
	}

	public String getCodigoEstado() {
		return codigoEstado;
	}

	public void setCodigoEstado(String codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	public EstadoOperacionEnum getEstado() {
		return EstadoOperacionEnum.getByCodigo(this.codigoEstado);
	}

	public void setEstado(EstadoOperacionEnum estado) {
		this.setCodigoEstado(estado.getCodigo());
	}

	public Boolean getEsCongelado() {
		return esCongelado;
	}

	public void setEsCongelado(Boolean esCongelado) {
		this.esCongelado = esCongelado;
	}

	public String getDireccionSalon() {
		return direccionSalon;
	}

	public void setDireccionSalon(String direccionSalon) {
		this.direccionSalon = direccionSalon;
	}

	public String getTelefonoSalon() {
		return telefonoSalon;
	}

	public void setTelefonoSalon(String telefonoSalon) {
		this.telefonoSalon = telefonoSalon;
	}

	public String getEmailSalon() {
		return emailSalon;
	}

	public void setEmailSalon(String emailSalon) {
		this.emailSalon = emailSalon;
	}

	public String getNombreSalon() {
		return nombreSalon;
	}

	public void setNombreSalon(String nombreSalon) {
		this.nombreSalon = nombreSalon;
	}

	public Double getIpMaximo() {
		return ipMaximo.doubleValue();
	}

	public void setIpMaximo(Double ipMaximo) {
		this.ipMaximo = new BigDecimal(ipMaximo);
	}

	public Double getCobroGarantia() {
		return cobroGarantia.doubleValue();
	}

	public void setCobroGarantia(Double cobroGarantia) {
		this.cobroGarantia = new BigDecimal(cobroGarantia);
	}

	public Asiento getAsientoGenerado() {
		return asientoGenerado;
	}

	public void setAsientoGenerado(Asiento asientoGenerado) {
		this.asientoGenerado = asientoGenerado;
		this.getNoContabilizados().forEach(c -> c.setAsientoGenerado(asientoGenerado));
	}

	@Override
	public boolean fueContabilizado() {
		return this.getAsiento() != null && this.servicios.stream().allMatch(s -> s.fueContabilizado());
	}

	@Override
	public Asiento getAsiento() {

		return this.asientoGenerado;
	}

	public List<CuotaServicio> getNoContabilizados() {
		List<CuotaServicio> cuotas = new Vector<>();

		this.servicios.stream().filter(s -> !s.fueContabilizado()).forEach(s -> cuotas.addAll(s.getNoContabilizados()));
		return cuotas;
	}

	public ConfiguracionComision addComision(ConfiguracionComision c) {
		this.comisiones.add(c);
		c.setContrato(this);
		return c;
	}

	public ServicioContratado getServicioByProducto(DescriptivoGeneric producto) {
		if (producto == null) {
			return null;
		}

		return this.servicios.stream()
				.filter(c -> c.getProducto() != null && c.getProducto().getCodigo().equals(producto.getCodigo()))
				.findFirst().orElse(null);
	}

	public Double getTotalCalculado() {
		return totalCalculado != null ? this.totalCalculado.doubleValue() : 0d;
	}

	public void setTotalCalculado(Double totalCalculado) {
		this.totalCalculado = totalCalculado != null ? this.totalCalculado = new BigDecimal(totalCalculado)
				: new BigDecimal(0);
	}

	public String getLocacion() {
		return this.salon != null ? this.getSalon().getDescripcion() : this.getNombreSalon();
	}

	@Override
	public String getCodigo() {
		return this.id + "";
	}

	public Double getIpMonto() {
		return this.ipMonto != null ? this.ipMonto.doubleValue() : 0d;
	}

	public void setIpMonto(Double val) {
		this.ipMonto = val != null ? new BigDecimal(val) : new BigDecimal(0);
	}

	public boolean esAprobado() {

		return this.getEstado() != null && this.getEstado().equals(EstadoOperacionEnum.APROBADO);
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public boolean esContabilizable() {

		return this.asientoGenerado == null || this.servicios.stream()
				.anyMatch(s -> s.getPlanPago().stream().anyMatch(p -> p.getAsientoGenerado() == null));
	}

	public boolean esCerrado() {

		return this.getEstado() != null && this.getEstado().equals(EstadoOperacionEnum.LIQUIDADO);
	}

	public Date getUltimaModificacion() {
		return ultimaModificacion;
	}

	public void setUltimaModificacion(Date ultimaModificacion) {
		this.ultimaModificacion = ultimaModificacion;
	}

	public List<CuotaServicio> getCuotas() {
		return this.servicios.stream().map(ServicioContratado::getPlanPago).flatMap(List::stream)
				.collect(Collectors.toList());
	}

}

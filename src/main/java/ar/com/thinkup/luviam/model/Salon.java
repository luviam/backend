package ar.com.thinkup.luviam.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.repository.SalonRepository.SalonDescriptivo;
import ar.com.thinkup.luviam.vo.Identificable;

@Entity
@Table(name = "salon")
public class Salon implements IDescriptivo, SalonDescriptivo, Identificable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6798196729851355740L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String nombre;

	private String direccion;

	@Column(name = "codigo_postal")
	private String codigoPostal;

	@ManyToOne
	@JoinColumn(name = "provincia_id")
	private Provincia provincia;

	@Column(name = "nombre_contacto", nullable = false)
	private String nombreContacto;

	private String telefono;

	private String email;

	@ManyToOne
	@JoinColumn(name = "producto_id")
	private Producto producto;
	
	@Column(name = "latitud", precision = 8, scale = 8)
	public float latitud;

	@Column(name = "longitud", precision = 8, scale = 8)
	public float longitud;
	
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean activo;

	@OneToOne(mappedBy = "salon", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Comisionista comisionista;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public Provincia getProvincia() {
		return provincia;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}

	public String getNombreContacto() {
		return nombreContacto;
	}

	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	

	@Override
	public String getCodigo() {
		
		return String.valueOf(this.getId());
	}

	@Override
	public String getDescripcion() {
		return this.nombre;
	}

	public float getLatitud() {
		return latitud;
	}

	public void setLatitud(float latitud) {
		this.latitud = latitud;
	}

	public float getLongitud() {
		return longitud;
	}

	public void setLongitud(float longitud) {
		this.longitud = longitud;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Comisionista getComisionista() {
		return comisionista;
	}

	public void setComisionista(Comisionista comisionista) {
		this.comisionista = comisionista;
	}

	
	
	

}

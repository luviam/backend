package ar.com.thinkup.luviam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author ThinkUp
 *
 */
@Entity
@Table(name = "comprobante")
public class Comprobante {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "numero")
	private String numero;

	@ManyToOne
    @JoinColumn(name="tipo_comprobante_id")
	private TipoComprobante tipoComprobante;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="centro_id")
	private CentroCosto centro;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public TipoComprobante getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(TipoComprobante tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public CentroCosto getCentro() {
		return centro;
	}

	public void setCentro(CentroCosto centro) {
		this.centro = centro;
	}	
	

}

package ar.com.thinkup.luviam.model.enums;

public enum EmailTemplateEnum {

	RECUPERAR_PASSWORD("RECOVER");
	
	private String codigo;

	private EmailTemplateEnum(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	
	
}

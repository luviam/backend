package ar.com.thinkup.luviam.model.documentos.factura;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;

@Entity
@Table(name = "item_factura")
public class ItemFactura {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "factura_id", nullable = false)
	private Factura factura;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "centro_costo_id", nullable = false)
	private CentroCosto centroCosto;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cuenta_aplicacion_id", nullable = false)
	private CuentaAplicacion cuenta;

	@Column(name = "cantidad", precision = 10, scale = 2)
	private BigDecimal cantidad = new BigDecimal(0);

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "tipo_iva")
	private String tipoIVA;

	@Column(name = "importe", nullable = false, precision = 10, scale = 2)
	private BigDecimal importe = new BigDecimal(0);

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Factura getFactura() {
		return factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	public CentroCosto getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(CentroCosto centroCosto) {
		this.centroCosto = centroCosto;
	}

	public CuentaAplicacion getCuenta() {
		return cuenta;
	}

	public void setCuenta(CuentaAplicacion cuenta) {
		this.cuenta = cuenta;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTipoIVA() {
		return tipoIVA;
	}

	public void setTipoIVA(String tipoIVA) {
		this.tipoIVA = tipoIVA;
	}

	public Double getImporte() {
		return importe != null? importe.doubleValue() : 0d;
	}

	public void setImporte(Double importe) {
		this.importe = importe != null? new BigDecimal(importe) : new BigDecimal(0);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((centroCosto == null) ? 0 : centroCosto.hashCode());
		result = prime * result + ((cuenta == null) ? 0 : cuenta.hashCode());
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result + ((factura == null) ? 0 : factura.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((importe == null) ? 0 : importe.hashCode());
		result = prime * result + ((tipoIVA == null) ? 0 : tipoIVA.hashCode());
		return result;
	}

	public Double getCantidad() {
		return cantidad == null ? 0d : cantidad.doubleValue();
	}

	public void setCantidad(Double cantidad) {
		if (cantidad == null) {
			this.cantidad = new BigDecimal(0);
		} else {
			this.cantidad = new BigDecimal(cantidad);
		}

	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemFactura other = (ItemFactura) obj;
		if (centroCosto == null) {
			if (other.centroCosto != null)
				return false;
		} else if (!centroCosto.equals(other.centroCosto))
			return false;
		if (cuenta == null) {
			if (other.cuenta != null)
				return false;
		} else if (!cuenta.equals(other.cuenta))
			return false;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		if (factura == null) {
			if (other.factura != null)
				return false;
		} else if (!factura.equals(other.factura))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (importe == null) {
			if (other.importe != null)
				return false;
		} else if (!importe.equals(other.importe))
			return false;
		if (tipoIVA == null) {
			if (other.tipoIVA != null)
				return false;
		} else if (!tipoIVA.equals(other.tipoIVA))
			return false;
		return true;
	}

}

package ar.com.thinkup.luviam.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.thinkup.luviam.vo.Identificable;

@Entity
@Table(name = "escala_comisiones")
public class EscalaComisiones implements Identificable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Integer desde;
	private Integer hasta;
	private BigDecimal premio;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "centro_costo_id")
	public CentroCosto centroCosto;
	

	public Double getPremio() {
		return this.premio != null ? this.premio.doubleValue() : 0d;
	}

	public void setPremio(Double val) {
		this.premio = val != null ? new BigDecimal(val) : new BigDecimal(0);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDesde() {
		return desde;
	}

	public void setDesde(Integer desde) {
		this.desde = desde;
	}

	public Integer getHasta() {
		return hasta;
	}

	public void setHasta(Integer hasta) {
		this.hasta = hasta;
	}

	public void setPremio(BigDecimal premio) {
		this.premio = premio;
	}

	public CentroCosto getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(CentroCosto centroCosto) {
		this.centroCosto = centroCosto;
	}


	
	
}

package ar.com.thinkup.luviam.model.enums;

import java.util.NoSuchElementException;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import ar.com.thinkup.luviam.model.def.IDescriptivo;

public enum EstadoCheque implements IDescriptivo {
	DISPONIBLE("D", "Disponible"), ENTREGADO("E", "Entregado"), RECHAZADO_A_COBRAR("R",
			"Rechazado a cobrar"), RECHAZADO_COBRADO("C", "Rechazado cobrado"), ANULADO("A", "Anulado");

	String codigo;
	String descripcion;

	private EstadoCheque(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public static EstadoCheque byCodigo(String codigo) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(codigo)) {
			throw new IllegalArgumentException();
		}
		return Stream.of(EstadoCheque.values()).filter(tc -> tc.codigo.equals(codigo)).findFirst().get();
	}

}

package ar.com.thinkup.luviam.model.enums;

import java.util.NoSuchElementException;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

public enum TipoComision {
TOTAL("T","Sobre Total"),SIN_INCREMENTO("S","Sin Incrementos");
	
	String codigo;
	String descripcion;

	private TipoComision(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}
	
	
	public String getDescripcion() {
		return descripcion;
	}

	public static TipoComision byCodigo(String codigo) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(codigo)) {
			throw new IllegalArgumentException();
		}
		return Stream.of(TipoComision.values()).filter(tc->tc.codigo.equals(codigo)).findFirst().get();
	}

	
}

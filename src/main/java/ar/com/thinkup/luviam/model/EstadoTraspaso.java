package ar.com.thinkup.luviam.model;

import java.util.Date;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;

public class EstadoTraspaso {

	private Long id;
	private String origen;
	private Long idCajaOrigen;
	private String destino;
	private Long idCajaDestino;
	private String descripcion = "Traspaso de caja";
	private String responsable;
	private Double monto;
	private DescriptivoGeneric estado;
	private Date fechaOperacion;

	
	public EstadoTraspaso() {
		
	}
	public EstadoTraspaso(TraspasoCaja t) {
		this.id = t.getId();
		this.origen = t.getCajaOrigen().getDescripcion();
		this.idCajaOrigen = t.getCajaOrigen().getId();
		this.destino = t.getCajaDestino().getDescripcion();
		this.idCajaDestino = t.getCajaDestino().getId();
		this.descripcion = t.getDescripcion();
		this.responsable = t.getResponsable().getDescripcion();
		this.monto = t.getMonto();
		this.estado = new DescriptivoGeneric(t.getCodigoEstado(), EstadoOperacionEnum.getByCodigo(t.getCodigoEstado()).getDescripcion());
		this.fechaOperacion = t.getFechaOperacion();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public Long getIdCajaOrigen() {
		return idCajaOrigen;
	}

	public void setIdCajaOrigen(Long idCajaOrigen) {
		this.idCajaOrigen = idCajaOrigen;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public Long getIdCajaDestino() {
		return idCajaDestino;
	}

	public void setIdCajaDestino(Long idCajaDestino) {
		this.idCajaDestino = idCajaDestino;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public DescriptivoGeneric getEstado() {
		return estado;
	}

	public void setEstado(DescriptivoGeneric estado) {
		this.estado = estado;
	}

	public Date getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(Date fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

}

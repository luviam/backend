package ar.com.thinkup.luviam.model.documentos.comisiones;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.documentos.Valores;

@Entity
@Table(name = "pago_liquidacion")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class PagoLiquidacion extends Valores {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "liquidacion_id", nullable = false)
	private LiquidacionComisiones liquidacion;

	public LiquidacionComisiones getLiquidacion() {
		return liquidacion;
	}

	public void setLiquidacion(LiquidacionComisiones liquidacion) {
		this.liquidacion = liquidacion;
	}

}

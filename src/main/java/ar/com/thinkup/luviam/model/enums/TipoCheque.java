package ar.com.thinkup.luviam.model.enums;

import java.util.NoSuchElementException;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import ar.com.thinkup.luviam.model.def.IDescriptivo;

public enum TipoCheque implements IDescriptivo{
	PROPIO("P", "Propios"), CLIENTE("C", "Terceros X Cliente"), OTROS("O", "Terceros X Otros");

	String codigo;
	String descripcion;

	private TipoCheque(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public static TipoCheque byCodigo(String codigo) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(codigo)) {
			throw new IllegalArgumentException();
		}
		return Stream.of(TipoCheque.values()).filter(tc -> tc.codigo.equals(codigo)).findFirst().get();
	}

}

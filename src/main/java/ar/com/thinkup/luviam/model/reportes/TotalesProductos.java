package ar.com.thinkup.luviam.model.reportes;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

@Entity
@Table(name = "totales_por_producto")
@NamedStoredProcedureQueries({
		@NamedStoredProcedureQuery(name = "totalesPorProducto", procedureName = "totales_por_producto", 
				resultClasses = {TotalesProductos.class},
				parameters = {
				@StoredProcedureParameter(mode = ParameterMode.IN, name = "$desde", type = Date.class),
				@StoredProcedureParameter(mode = ParameterMode.IN, name = "$hasta", type = Date.class),
				@StoredProcedureParameter(mode = ParameterMode.IN, name = "$centro", type = String.class),
				@StoredProcedureParameter(mode = ParameterMode.IN, name = "$codigo_producto", type = String.class),
				@StoredProcedureParameter(mode = ParameterMode.IN, name = "$codigo_categoria", type = String.class) }) })
public class TotalesProductos {

	@Id
	@Column(name = "producto_id")
	private Long idProducto;

	@Column(name = "categoria")
	private String categoria;

	@Column(name = "producto")
	private String producto;

	@Column(name = "cantidad")
	private Integer cantidad;

	@Column(name = "vendidos", precision = 10, scale = 2)
	private BigDecimal ventas;

	@Column(name = "promedio", precision = 10, scale = 2)
	private BigDecimal cuPromedio;

	@Column(name = "abonado", precision = 10, scale = 2)
	private BigDecimal cobros;

	public Long getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Double getVentas() {
		return ventas != null ? ventas.doubleValue() : 0d;
	}

	public void setVentas(Double ventas) {
		this.ventas = new BigDecimal(ventas);
	}

	public Double getCuPromedio() {
		return cuPromedio != null ? cuPromedio.doubleValue() : 0d;
	}

	public void setCuPromedio(Double cuPromedio) {
		this.cuPromedio = new BigDecimal(cuPromedio);
	}

	public Double getCobros() {
		return cobros != null ? cobros.doubleValue() : 0d;
	}

	public void setCobros(Double cobros) {
		this.cobros = new BigDecimal(cobros);
	}

}

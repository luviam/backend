package ar.com.thinkup.luviam.model.enums;

public enum EstadoAsientoEnum {
PENDIENTE("P", "Pendiente"), APROBADO("A", "Aprobado"), RECHAZADO("R","Rechazado");
	
	private String codigo;
	private String descripcion;
	
	
	private EstadoAsientoEnum(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public static EstadoAsientoEnum getByCodigo(String codigo) {
		
		for (EstadoAsientoEnum ele : EstadoAsientoEnum.values()) {
			if(ele.getCodigo().equals(codigo)) {
				return ele;
			}
			
		}
		return null;
		
	}
	
	
}

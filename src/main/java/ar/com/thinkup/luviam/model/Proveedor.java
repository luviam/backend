package ar.com.thinkup.luviam.model;

import java.util.List;
import java.util.Optional;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.enums.TipoImputacionEnum;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.repository.ProveedorRepository.ProveedorCabecera;
import ar.com.thinkup.luviam.repository.ProveedorRepository.ProveedorDescriptivo;

/**
 * 
 * @author ThinkUp
 *
 */
@Entity
@Table(name = "proveedor")
@Where(clause = "activo='1'")
@NamedQuery(name = "Proveedor.getSaldo", query = "select SUM(f.saldo) as total from Factura f "
		+ "		where f.proveedor.id = ?1 \r\n" + "		and f.estado = 'A' \r\n")
public class Proveedor implements IDescriptivo, ProveedorCabecera, ProveedorDescriptivo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2768068004784738837L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "nombre_proveedor")
	private String nombreProveedor;

	@Column(name = "nombre_contacto")
	private String nombreContacto;

	@Column(name = "domicilio")
	private String domicilio;

	@Column(name = "localidad")
	private String localidad;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "celular")
	private String celular;

	@Column(name = "email")
	private String email;

	@Column(name = "razon_social")
	private String razonSocial;

	@Column(name = "cuit")
	private String cuit;

	@ManyToOne
	@JoinColumn(name = "tipo_iva_id")
	private TipoIVA iva;

	@Column(name = "domicilio_facturacion")
	private String domicilioFacturacion;

	@Column(name = "localidad_facturacion")
	private String localidadFacturacion;

	@ManyToOne
	@JoinColumn(name = "tipo_proveedor_id")
	private TipoProveedor tipoProveedor;

	@ManyToOne
	@JoinColumn(name = "tipo_cuenta_proveedor_id")
	private TipoCuentaProveedor tipoCuenta;

	@Column(name = "activo")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean activo;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "proveedor")
	private List<ImputacionCuentaConfig> imputacionCuentaConfigs = new Vector<>();

	public Proveedor() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreProveedor() {
		return nombreProveedor;
	}

	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}

	public String getNombreContacto() {
		return nombreContacto;
	}

	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public TipoIVA getIva() {
		return iva;
	}

	public void setIva(TipoIVA iva) {
		this.iva = iva;
	}

	public String getDomicilioFacturacion() {
		return domicilioFacturacion;
	}

	public void setDomicilioFacturacion(String domicilioFacturacion) {
		this.domicilioFacturacion = domicilioFacturacion;
	}

	public String getLocalidadFacturacion() {
		return localidadFacturacion;
	}

	public void setLocalidadFacturacion(String localidadFacturacion) {
		this.localidadFacturacion = localidadFacturacion;
	}

	public TipoProveedor getTipoProveedor() {
		return tipoProveedor;
	}

	public void setTipoProveedor(TipoProveedor tipoProveedor) {
		this.tipoProveedor = tipoProveedor;
	}

	public TipoCuentaProveedor getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(TipoCuentaProveedor tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	@Override
	public String getCodigo() {
		return String.valueOf(this.id);
	}

	@Override
	public String getDescripcion() {
		return this.nombreProveedor;
	}

	public List<ImputacionCuentaConfig> getImputacionCuentaConfigs() {
		return imputacionCuentaConfigs;
	}

	public void setImputacionCuentaConfigs(List<ImputacionCuentaConfig> imputacionCuentaConfigs) {
		this.imputacionCuentaConfigs = imputacionCuentaConfigs;
	}

	public Boolean addImputacionCuentaConfig(ImputacionCuentaConfig config) {
		if (this.imputacionCuentaConfigs == null) {
			this.imputacionCuentaConfigs = new Vector<>();
		}
		config.setProveedor(this);

		return this.imputacionCuentaConfigs.add(config);
	}

	public ImputacionCuentaConfig getItem(Long id) {
		Optional<ImputacionCuentaConfig> op = this.imputacionCuentaConfigs.parallelStream()
				.filter(i -> i.getId() != null && i.getId().equals(id)).findFirst();
		if (op.isPresent()) {
			return op.get();
		} else {
			// TIRO EXCEPCIóN?
			return null;
		}

	}

	public CuentaAplicacion getCuentaImputacion(TipoImputacionEnum tipoImputacion) {

		Optional<ImputacionCuentaConfig> op = this.imputacionCuentaConfigs.parallelStream()
				.filter(i -> i.getCodigoTipoImputacion() != null
						&& i.getCodigoTipoImputacion().equals(tipoImputacion.getCodigo()))
				.findFirst();
		if (op.isPresent()) {
			return op.get().getCuentaAplicacion();
		} else {
			// TIRO EXCEPCIóN?
			return null;
		}
	}

	@Override
	public String toString() {

		return "Proveedor " + this.getNombreProveedor() + " ID: " + this.getId();
	}

}

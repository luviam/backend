package ar.com.thinkup.luviam.model.comisionistas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.security.Usuario;

@Entity()
@Table(name = "layout_indicadores")
public class LayoutIndicadores implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1262830029518697626L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "indicadores_json", columnDefinition = "TEXT")
	private String indicadoresStr;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "responsable_id", nullable = true)
	private Usuario usuario;

	public LayoutIndicadores() {
		super();
	}

	public String getIndicadoresStr() {
		return indicadoresStr;
	}

	public void setIndicadoresStr(String indicadoresStr) {
		this.indicadoresStr = indicadoresStr;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setusuario(Usuario usuario) {
		this.usuario = usuario;
	}

}

package ar.com.thinkup.luviam.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OrderBy;

import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.vo.Identificable;

@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Parametrico implements IDescriptivo, Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2466665875471029097L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "es_habilitado")
	private Boolean habilitado;

	@Column(name = "codigo", nullable = false, length = 8)
	@OrderBy("ASC")
	private String codigo;

	@Column(name = "descripcion", nullable = false, length = 100)
	private String descripcion;

	@Column(name = "es_sistema")
	private Boolean esSistema = false;

	public Parametrico() {
		super();
	}

	public Parametrico(Boolean habilitado, String codigo, String descripcion, Boolean esSistema) {
		super();
		this.id = null;
		this.habilitado = habilitado;
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.esSistema = esSistema;
	}

	public Parametrico(Long id, Boolean habilitado, String codigo, String descripcion, Boolean esSistema) {
		super();
		this.id = id;
		this.habilitado = habilitado;
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.esSistema = esSistema;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getEsSistema() {
		return esSistema;
	}

	public void setEsSistema(Boolean esSistema) {
		this.esSistema = esSistema;
	}

}

package ar.com.thinkup.luviam.model;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity()
@Table(name = "tipo_iva")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class TipoIVA extends Parametrico {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7604153806193261878L;

	public TipoIVA() {
		super();
	}

	public TipoIVA(Boolean habilitado, String codigo, String descripcion, Boolean esSistema) {
		super(habilitado, codigo, descripcion, esSistema);
	}

	public TipoIVA(Long id, Boolean habilitado, String codigo, String descripcion, Boolean esSistema) {
		super(id, habilitado, codigo, descripcion, esSistema);
	}

}

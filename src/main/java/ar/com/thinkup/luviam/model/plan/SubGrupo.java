package ar.com.thinkup.luviam.model.plan;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.enums.TipoCuentaEnum;
import ar.com.thinkup.luviam.vo.CuentaPlanVO;

@Entity
@Table(name = "sub_grupo")
@Where(clause = "deleted=0")
public class SubGrupo extends NodoPlanCuenta implements IDescriptivo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3882929057679672826L;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "grupo_id")
	private Grupo grupo;

	public SubGrupo(CuentaPlanVO c, Grupo parent) {
		this.setCodigo(c.getCodigo());
		this.setActivo(c.getActivo());
		this.setNombre(c.getDescripcion());
		this.grupo = parent;
	}

	public SubGrupo() {
		super();

	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	@Override
	public String getTipoCuenta() {
		return TipoCuentaEnum.SUB_GRUPO.getCodigo();
	}

	@Override
	public String getDescripcion() {
		return this.getNombre();
	}
}

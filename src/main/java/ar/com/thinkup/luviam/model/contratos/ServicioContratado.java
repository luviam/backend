package ar.com.thinkup.luviam.model.contratos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.ImputacionProducto;
import ar.com.thinkup.luviam.model.Producto;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.vo.contratos.CuotaServicioVO;

@Entity()
@Table(name = "servicio_contratado")
public class ServicioContratado implements Serializable, IDescriptivo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 166559154814669661L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Contrato contrato;

	@Column(name = "fecha_contrato")
	private Date fechaContrato;

	@ManyToOne(fetch = FetchType.EAGER)
	private Producto producto;

	@OneToMany(mappedBy = "servicio", orphanRemoval = true, cascade = CascadeType.ALL)
	private List<CuotaServicio> planPago = new Vector<>();

	@Column(name = "costo_unitario", precision = 10, scale = 2)
	private BigDecimal costoUnitario = new BigDecimal(0);
	@Column(name = "grupo", length = 20)
	private String grupo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "centro_costo_id")
	private CentroCosto centroCosto;

	public Integer getPeso() {
		return this.producto.getCategoria().getPeso();
	}

	private Integer cantidad;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Date getFechaContrato() {
		return fechaContrato;
	}

	public void setFechaContrato(Date fechaContrato) {
		this.fechaContrato = fechaContrato;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Double getCostoUnitario() {
		return costoUnitario != null ? costoUnitario.doubleValue() : 0d;
	}

	public void setCostoUnitario(Double costoUnitario) {
		this.costoUnitario = new BigDecimal(costoUnitario);
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public List<CuotaServicio> getPlanPago() {
		return planPago;
	}

	public void setPlanPago(List<CuotaServicio> planPago) {
		this.planPago = planPago;
	}

	public void agregarCuota(CuotaServicio cuota) {
		cuota.setServicio(this);
		this.planPago.add(cuota);
	}

	public CuotaServicio getCuota(CuotaServicioVO vo) {
		return this.planPago.stream().filter(p -> p.getId() != null && p.getId().equals(vo.getId())).findFirst()
				.orElse(null);
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public CentroCosto getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(CentroCosto centroCosto) {
		this.centroCosto = centroCosto;
	}

	public boolean fueContabilizado() {
		return this.planPago.stream().allMatch(c -> c.getAsientoGenerado() != null);
	}

	public List<CuotaServicio> getNoContabilizados() {

		return this.planPago.stream().filter(c -> c.getAsientoGenerado() == null).collect(Collectors.toList());
	}

	public Double getTotal() {
		return this.planPago.stream().map(a -> a.getTotal()).mapToDouble(Double::new).sum();

	}

	@Override
	public String getCodigo() {
		return this.id + "";
	}

	@Override
	public String getDescripcion() {
		return this.producto != null ? this.producto.getDescripcion() : "SIN DEFINIR";

	}

	public Boolean esImpuesto() {

		return this.producto != null && this.producto.esImpuesto();
	}

	public CuentaAplicacion getCuentaAplicacion() {
		ImputacionProducto i = this.producto.getCuenta(this.centroCosto.getCodigo());
		if (i == null) {
			throw new IllegalArgumentException(
					"No hay cuenta de imputación para " + this.getProducto().getDescripcion());
		}
		return i.getCuentaAplicacion();
	}

}

package ar.com.thinkup.luviam.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.def.IDescriptivo;

@Table(name="parametros_sistema")
@Entity
public class SistemParameter implements IDescriptivo{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8680017267850035499L;

	public static String URL_LINKS ="URL";
	public static final String BASE_URL = System.getProperty("catalina.base") + "/webapps/";
	public static final String TEMPORAL_URL = BASE_URL + "temp/";
	public static final String LOGO_MARCA = "logos_marca/";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String codigo;
	
	private String valor;

	
	
	public SistemParameter() {
		super();
	}

	public SistemParameter(Integer id, String codigo, String valor) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.valor = valor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public String getDescripcion() {
		return this.valor;
	}
	
	
	
}

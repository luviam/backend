package ar.com.thinkup.luviam.model.documentos.cobros;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.contratos.CuotaServicio;
import ar.com.thinkup.luviam.repository.ItemOrdenCobranzaRepository.ItemOrdenCobranzaCabecera;

@Table(name = "item_orden_cobranza")
@Entity
public class ItemOrdenCobranza implements ItemOrdenCobranzaCabecera {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orden_cobranza_id", nullable = false)
	private OrdenCobranza ordenCobranza;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cuota_servicio_id", nullable = true)
	private CuotaServicio cuotaServicio;

	@Column(name = "importe", nullable = false, precision = 10, scale = 2)
	private BigDecimal importe = new BigDecimal(0);

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OrdenCobranza getOrdenCobranza() {
		return ordenCobranza;
	}

	public void setOrdenCobranza(OrdenCobranza ordenCobranza) {
		this.ordenCobranza = ordenCobranza;
	}

	public CuotaServicio getCuotaServicio() {
		return cuotaServicio;
	}

	public void setCuotaServicio(CuotaServicio cuotaServicio) {
		this.cuotaServicio = cuotaServicio;
	}

	public Double getImporte() {
		return importe.doubleValue();
	}

	public void setImporte(Double importe) {
		this.importe = new BigDecimal(importe);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cuotaServicio == null) ? 0 : cuotaServicio.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((importe == null) ? 0 : importe.hashCode());
		result = prime * result + ((ordenCobranza == null) ? 0 : ordenCobranza.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemOrdenCobranza other = (ItemOrdenCobranza) obj;
		if (cuotaServicio == null) {
			if (other.cuotaServicio != null)
				return false;
		} else if (!cuotaServicio.equals(other.cuotaServicio))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (importe == null) {
			if (other.importe != null)
				return false;
		} else if (!importe.equals(other.importe))
			return false;
		if (ordenCobranza == null) {
			if (other.ordenCobranza != null)
				return false;
		} else if (!ordenCobranza.equals(other.ordenCobranza))
			return false;
		return true;
	}

}

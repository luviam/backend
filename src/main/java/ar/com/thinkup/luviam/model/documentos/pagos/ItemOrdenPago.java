package ar.com.thinkup.luviam.model.documentos.pagos;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.documentos.factura.Factura;
import ar.com.thinkup.luviam.repository.ItemOrdenPagoRepository.ItemOrdenPagoCabecera;

@Table(name = "item_orden_pago")
@Entity
public class ItemOrdenPago implements ItemOrdenPagoCabecera {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orden_pago_id", nullable = false)
	private OrdenPago ordenPago;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "factura_pagada_id", nullable = false)
	private Factura factura;

	@Column(name = "importe", nullable = false, precision = 10, scale = 2)
	private BigDecimal importe = new BigDecimal(0);

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Factura getFactura() {
		return factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	public Double getImporte() {
		return importe.doubleValue();
	}

	public void setImporte(Double importe) {
		this.importe = new BigDecimal(importe);
	}

	public OrdenPago getOrdenPago() {
		return ordenPago;
	}

	public void setOrdenPago(OrdenPago ordenPago) {
		this.ordenPago = ordenPago;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((factura == null) ? 0 : factura.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((importe == null) ? 0 : importe.hashCode());
		result = prime * result + ((ordenPago == null) ? 0 : ordenPago.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemOrdenPago other = (ItemOrdenPago) obj;
		if (factura == null) {
			if (other.factura != null)
				return false;
		} else if (!factura.equals(other.factura))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (importe == null) {
			if (other.importe != null)
				return false;
		} else if (!importe.equals(other.importe))
			return false;
		if (ordenPago == null) {
			if (other.ordenPago != null)
				return false;
		} else if (!ordenPago.equals(other.ordenPago))
			return false;
		return true;
	}

}

package ar.com.thinkup.luviam.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import ar.com.thinkup.luviam.model.documentos.cobros.Cobro;
import ar.com.thinkup.luviam.model.documentos.pagos.Pago;
import ar.com.thinkup.luviam.model.enums.EstadoCheque;
import ar.com.thinkup.luviam.model.enums.TipoCheque;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.vo.Identificable;

@Entity
@Table(name = "cheque")
public class Cheque implements Identificable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "banco", nullable = false)
	private String banco;

	@Column(name = "numero", nullable = false)
	private String numero;

	@Column(name = "tipo_cheque", nullable = false)
	private String codigoTipoCheque = TipoCheque.PROPIO.getCodigo();

	@ManyToOne
	@JoinColumn(name = "cuenta_id", nullable = true)
	private CuentaAplicacion cuentaBancaria;

	@Column(name = "numero_cuenta", nullable = true)
	private String numeroCuenta;

	@Column(name = "titular_cuenta", nullable = true)
	private String titularCuenta;

	@Column(name = "cuit", nullable = true)
	private String cuit;

	@Column(name = "fecha_emision")
	private Date fechaEmision;

	@Column(name = "fecha_pago")
	private Date fechaPago;

	@Column(name = "importe", nullable = false, precision = 10, scale = 2)
	private BigDecimal importe = new BigDecimal(0);

	@Column(name = "estado", nullable = false)
	private String codigoEstadoCheque;

	@Column(name = "destino", nullable = true)
	private String destino;

	@Column(name = "es_diferido")
	private Boolean esDiferido = false;

	@OneToOne(mappedBy = "cheque")
	private Pago pago;

	@OneToOne(mappedBy = "cheque")
	private Cobro cobro;

	@Column(name = "origen")
	private String origen;

	@Column(name = "fecha_ingreso")
	private Date fechaIngreso;

	@Column(name = "numero_recibo")
	private String numeroRecibo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "chequera_id", nullable = true)
	private Chequera chequera;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCodigoTipoCheque() {
		return codigoTipoCheque;
	}

	public void setCodigoTipoCheque(String codigoTipoCheque) {
		this.codigoTipoCheque = codigoTipoCheque;
	}

	public CuentaAplicacion getCuentaBancaria() {
		return cuentaBancaria;
	}

	public void setCuentaBancaria(CuentaAplicacion cuentaBancaria) {
		this.cuentaBancaria = cuentaBancaria;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getTitularCuenta() {
		return titularCuenta;
	}

	public void setTitularCuenta(String titularCuenta) {
		this.titularCuenta = titularCuenta;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public Double getImporte() {
		return importe.doubleValue();
	}

	public void setImporte(Double importe) {
		this.importe = importe!= null? new BigDecimal(importe) : new BigDecimal(0);
	}

	public String getCodigoEstadoCheque() {
		return codigoEstadoCheque;
	}

	public void setCodigoEstadoCheque(String codigoEstadoCheque) {
		this.codigoEstadoCheque = codigoEstadoCheque;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public Boolean getEsDiferido() {
		return esDiferido;
	}

	public void setEsDiferido(Boolean esDiferido) {
		this.esDiferido = esDiferido;
	}

	public Pago getPago() {
		return pago;
	}

	public void setPago(Pago pago) {
		this.pago = pago;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getNumeroRecibo() {
		return numeroRecibo;
	}

	public void setNumeroRecibo(String numeroRecibo) {
		this.numeroRecibo = numeroRecibo;
	}

	public EstadoCheque getEstadoCheque() {
		return StringUtils.isEmpty(this.codigoEstadoCheque) ? null : EstadoCheque.byCodigo(this.codigoEstadoCheque);
	}

	public void setEstadoCheque(EstadoCheque estado) {
		if (estado == null) {
			throw new IllegalArgumentException("Sin estado de cheque");
		}
		this.codigoEstadoCheque = estado.getCodigo();

	}

	public TipoCheque getTipoCheque() {
		return StringUtils.isEmpty(this.codigoTipoCheque) ? null : TipoCheque.byCodigo(this.codigoTipoCheque);
	}

	public void setTipoCheque(TipoCheque tipo) {
		if (tipo == null) {
			throw new IllegalArgumentException("Sin tipo de cheque");
		}
		this.codigoTipoCheque = tipo.getCodigo();
	}

	public Chequera getChequera() {
		return chequera;
	}

	public void setChequera(Chequera chequera) {
		this.chequera = chequera;
	}

	public String getDescripcion() {
		return this.numero + " " + this.banco;

	}

	public Cobro getCobro() {
		return cobro;
	}

	public void setCobro(Cobro cobro) {
		this.cobro = cobro;
	}

}

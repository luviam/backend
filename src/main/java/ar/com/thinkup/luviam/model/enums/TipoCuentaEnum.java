package ar.com.thinkup.luviam.model.enums;

import java.util.NoSuchElementException;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

public enum TipoCuentaEnum {
RUBRO("R"),GRUPO("G"),SUB_GRUPO("SG"), APLICACION("A");
	
	String codigo;

	private TipoCuentaEnum(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}
	
	public static TipoCuentaEnum byCodigo(String codigo) throws IllegalArgumentException, NoSuchElementException {
		if (StringUtils.isEmpty(codigo)) {
			throw new IllegalArgumentException();
		}
		return Stream.of(TipoCuentaEnum.values()).filter(tc->tc.codigo.equals(codigo)).findFirst().get();
	}

	
}

package ar.com.thinkup.luviam.model.security;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.repository.UsuarioRepository.UsuarioDescriptivo;

/**
 * 
 * @author ThinkUp
 *
 */
@Entity
@Table(name = "usuario")
public class Usuario implements UserDetails, IDescriptivo, UsuarioDescriptivo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3229750187260241468L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "username")
	private String username;

	@Column(name = "password")
	private String password;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "apellido")
	private String apellido;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "activo")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean activo;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "usuario_rol", joinColumns = {
			@JoinColumn(name = "usuario_id", referencedColumnName = "id", unique = false) }, inverseJoinColumns = {
					@JoinColumn(name = "rol_id", referencedColumnName = "id", unique = false) }, uniqueConstraints = @UniqueConstraint(name = "uk_usuario_rol", columnNames = {
							"usuario_id", "rol_id" }))
	private List<Rol> roles;

	@Column(name = "ultima_actualizacion_password")
	private Date ultimaActualizacionPassword;

	public Usuario() {

	}

	public Usuario(String username, String password, String nombre, String apellido, String telefono, Boolean activo,
			List<Rol> roles) {

		this.username = username;
		this.password = password;
		this.nombre = nombre;
		this.apellido = apellido;
		this.telefono = telefono;
		this.activo = activo;
		if (roles != null) {
			this.roles = roles;
		} else {
			this.roles = new Vector<>();
		}
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return roles;
	}

	@Override
	public boolean isEnabled() {
		return activo;
	}

	public void actualizarPassword(String nuevaPass) {
		this.password = nuevaPass;
		this.ultimaActualizacionPassword = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public List<? extends GrantedAuthority> getRoles() {
		return roles;
	}

	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}

	public Date getUltimaActualizacionPassword() {
		return ultimaActualizacionPassword;
	}

	public void setUltimaActualizacionPassword(Date ultimaActualizacionPassword) {
		this.ultimaActualizacionPassword = ultimaActualizacionPassword;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String getCodigo() {
		return String.valueOf(this.id);
	}

	@Override
	public String getDescripcion() {
		return this.username;
	}

	public Boolean esAdministrador() {
		return this.roles.parallelStream().anyMatch(r -> r.getCodigo().equals(Rol.SUPER_USER));
	}
	
	public Boolean esCompras() {
		return this.roles.parallelStream().anyMatch(r -> r.getCodigo().equals(Rol.COMPRAS));
	}

	public Boolean esVentas() {
		return this.roles.parallelStream().anyMatch(r -> r.getCodigo().equals(Rol.VENTAS));
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((activo == null) ? 0 : activo.hashCode());
		result = prime * result + ((apellido == null) ? 0 : apellido.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((roles == null) ? 0 : roles.hashCode());
		result = prime * result + ((telefono == null) ? 0 : telefono.hashCode());
		result = prime * result + ((ultimaActualizacionPassword == null) ? 0 : ultimaActualizacionPassword.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (activo == null) {
			if (other.activo != null)
				return false;
		} else if (!activo.equals(other.activo))
			return false;
		if (apellido == null) {
			if (other.apellido != null)
				return false;
		} else if (!apellido.equals(other.apellido))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (roles == null) {
			if (other.roles != null)
				return false;
		} else if (!roles.equals(other.roles))
			return false;
		if (telefono == null) {
			if (other.telefono != null)
				return false;
		} else if (!telefono.equals(other.telefono))
			return false;
		if (ultimaActualizacionPassword == null) {
			if (other.ultimaActualizacionPassword != null)
				return false;
		} else if (!ultimaActualizacionPassword.equals(other.ultimaActualizacionPassword))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	@Override
	public String getFullName() {
		return this.getNombre() + " " + this.getApellido();
	}

}

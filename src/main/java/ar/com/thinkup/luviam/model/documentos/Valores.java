package ar.com.thinkup.luviam.model.documentos;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.apache.commons.lang3.StringUtils;

import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.Cheque;
import ar.com.thinkup.luviam.model.enums.TipoPagoEnum;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.vo.Identificable;

@MappedSuperclass
public abstract class Valores implements Identificable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "codigo_tipo_pago", nullable = false)
	private String codigoTipoPago;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "centro_costo_id", nullable = false)
	private CentroCosto centroCosto;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cuenta_aplicacion_id", nullable = false)
	private CuentaAplicacion cuenta;

	@Column(name = "monto", nullable = false, precision = 10, scale = 2)
	private BigDecimal monto = new BigDecimal(0d);

	@Column(name = "comprobante")
	private String comprobante;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
	@JoinColumn(name = "id_cheque", nullable = true)
	private Cheque cheque;

	public TipoPagoEnum getTipoPago() {
		return StringUtils.isBlank(this.codigoTipoPago) ? null : TipoPagoEnum.byCodigo(this.codigoTipoPago);
	}

	public void setTipoPago(TipoPagoEnum tipoPago) {
		this.codigoTipoPago = tipoPago.getCodigo();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigoTipoPago() {
		return codigoTipoPago;
	}

	public void setCodigoTipoPago(String codigoTipoPago) {
		this.codigoTipoPago = codigoTipoPago;
	}

	public CentroCosto getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(CentroCosto centroCosto) {
		this.centroCosto = centroCosto;
	}

	public CuentaAplicacion getCuenta() {
		return cuenta;
	}

	public void setCuenta(CuentaAplicacion cuenta) {
		this.cuenta = cuenta;
	}

	public Double getMonto() {
		return monto.doubleValue();
	}

	public void setMonto(Double monto) {
		this.monto = new BigDecimal(monto);
	}

	public String getComprobante() {
		return comprobante;
	}

	public void setComprobante(String comprobante) {
		this.comprobante = comprobante;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((centroCosto == null) ? 0 : centroCosto.hashCode());
		result = prime * result + ((cheque == null) ? 0 : cheque.hashCode());
		result = prime * result + ((codigoTipoPago == null) ? 0 : codigoTipoPago.hashCode());
		result = prime * result + ((comprobante == null) ? 0 : comprobante.hashCode());
		result = prime * result + ((cuenta == null) ? 0 : cuenta.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((monto == null) ? 0 : monto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Valores other = (Valores) obj;
		if (centroCosto == null) {
			if (other.centroCosto != null)
				return false;
		} else if (!centroCosto.equals(other.centroCosto))
			return false;
		if (cheque == null) {
			if (other.cheque != null)
				return false;
		} else if (!cheque.equals(other.cheque))
			return false;
		if (codigoTipoPago == null) {
			if (other.codigoTipoPago != null)
				return false;
		} else if (!codigoTipoPago.equals(other.codigoTipoPago))
			return false;
		if (comprobante == null) {
			if (other.comprobante != null)
				return false;
		} else if (!comprobante.equals(other.comprobante))
			return false;
		if (cuenta == null) {
			if (other.cuenta != null)
				return false;
		} else if (!cuenta.equals(other.cuenta))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (monto == null) {
			if (other.monto != null)
				return false;
		} else if (!monto.equals(other.monto))
			return false;
		return true;
	}

	public Cheque getCheque() {
		return cheque;
	}

	public void setCheque(Cheque cheque) {
		this.cheque = cheque;
	}

}

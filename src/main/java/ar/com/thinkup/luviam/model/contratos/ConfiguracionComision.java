package ar.com.thinkup.luviam.model.contratos;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.Comisionista;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.enums.TipoComision;
import ar.com.thinkup.luviam.vo.Identificable;

@Table(name = "comision_contrato")
@Entity
public class ConfiguracionComision implements Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7901785640037811471L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contrato_id")
	private Contrato contrato;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "comisionista_id")
	private Comisionista comisionista;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "servicio_contratado_id")
	private ServicioContratado servicioComisionado;

	@Column(name = "porcentaje_comision", precision = 10, scale = 2)
	private BigDecimal porcentajeComision = new BigDecimal(0);

	@Column(name = "total_a_comisionar", precision = 10, scale = 2)
	private BigDecimal totalAComisionar = new BigDecimal(0);

	@Column(name = "saldo_comision", precision = 10, scale = 2)
	private BigDecimal saldoComision = new BigDecimal(0);

	@Column(name = "codigo_tipo_comision")
	private String codigoTipoComision;

	@Column(name = "cobra_al_inicio")
	private Boolean cobraAlInicio;
	private BigDecimal porcentajeAdelanto;

	@Column(name = "cobra_al_liquidar")
	private Boolean cobraAlLiquidar;

	@Column(name = "cobra_por_cobro")
	private Boolean cobraPorCobro;

	@OneToMany(mappedBy = "configComision", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<PagoComision> pagosComision = new HashSet<>();

	@ManyToOne
	@JoinColumn(name = "centro_costo_id")
	private CentroCosto centroCosto;

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Comisionista getComisionista() {
		return comisionista;
	}

	public void setComisionista(Comisionista comisionista) {
		this.comisionista = comisionista;
	}

	public ServicioContratado getServicioComisionado() {
		return servicioComisionado;
	}

	public void setServicioComisionado(ServicioContratado servicioComisionado) {
		this.servicioComisionado = servicioComisionado;
	}

	public Double getPorcentajeComision() {
		return porcentajeComision != null ? this.porcentajeComision.floatValue() : 0d;
	}

	public void setPorcentajeComision(Double porcentajeComision) {
		this.porcentajeComision = porcentajeComision != null ? new BigDecimal(porcentajeComision) : new BigDecimal(0);
	}

	public Double getTotalAComisionar() {
		return totalAComisionar != null ? this.totalAComisionar.floatValue() : 0d;
	}

	public void setTotalAComisionar(Double totalAComisionar) {
		this.totalAComisionar = totalAComisionar != null ? new BigDecimal(totalAComisionar) : new BigDecimal(0);
	}

	public Double getSaldoComision() {
		return this.saldoComision != null ? this.saldoComision.doubleValue() : 0d;
	}

	public void setSaldoComision(Double saldoComision) {
		this.saldoComision = saldoComision != null ? new BigDecimal(saldoComision) : new BigDecimal(0);
	}

	public Boolean getCobraAlInicio() {
		return cobraAlInicio;
	}

	public void setCobraAlInicio(Boolean cobraAlInicio) {
		this.cobraAlInicio = cobraAlInicio;
	}

	public Boolean getCobraAlLiquidar() {
		return cobraAlLiquidar;
	}

	public void setCobraAlLiquidar(Boolean cobraAlLiquidar) {
		this.cobraAlLiquidar = cobraAlLiquidar;
	}

	public Boolean getCobraPorCobro() {
		return cobraPorCobro;
	}

	public void setCobraPorCobro(Boolean cobraPorCobro) {
		this.cobraPorCobro = cobraPorCobro;
	}

	public TipoComision getTipoComision() {
		return TipoComision.byCodigo(this.codigoTipoComision);
	}

	public void setTipoComision(TipoComision tc) {
		if (tc != null) {
			this.codigoTipoComision = tc.getCodigo();
		} else {
			this.codigoTipoComision = "SD";
		}

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CentroCosto getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(CentroCosto centroCosto) {
		this.centroCosto = centroCosto;
	}

	public Set<PagoComision> getPagosComision() {
		return pagosComision;
	}
	
	public Double getTotalPagos() {
		return this.pagosComision.stream().mapToDouble(c ->c.getTotalComision()).sum();
	}

	public void setPagosComision(Set<PagoComision> pagosComision) {
		this.pagosComision = pagosComision;
	}

	public void setTipoComision(IDescriptivo tipoComision) {
		if (tipoComision != null) {
			TipoComision tc = TipoComision.byCodigo(tipoComision.getCodigo());
			if (tc != null) {
				this.codigoTipoComision = tc.getCodigo();
			} else {
				this.codigoTipoComision = "SD";
			}

		} else {
			this.codigoTipoComision = "SD";
		}

	}

	public void addPagoComision(PagoComision pago) {
		pago.setConfigComision(this);
		this.pagosComision.add(pago);
	}

	public PagoComision getPagoComision(Long id) {
		return this.pagosComision.stream().filter(i -> i.getId() != null && i.getId().equals(id)).findFirst()
				.orElse(null);
	}

	public Double getPorcentajeAdelanto() {
		return this.porcentajeAdelanto != null ? this.porcentajeAdelanto.floatValue() : 0d;
	}

	public void setPorcentajeAdelanto(Double val) {
		this.porcentajeAdelanto = val != null ? new BigDecimal(val) : new BigDecimal(0);
	}

	public Integer getCantidadComisionada() {
		return this.getPagosComision().stream().mapToInt(PagoComision::getCantidad).sum();
	}

	public void susSaldo(Double importe) {
		this.saldoComision = this.saldoComision.subtract(new BigDecimal(importe));
		if (this.saldoComision.compareTo(new BigDecimal(0)) < 0) {
			this.saldoComision = new BigDecimal(0);
		}

	}

	public void addSaldo(Double importe) {
		this.saldoComision = this.saldoComision.add(new BigDecimal(importe));
		if (this.saldoComision.compareTo(this.totalAComisionar) > 0) {
			this.saldoComision = this.totalAComisionar;
		}

	}

}

package ar.com.thinkup.luviam.model.documentos.cobros;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.documentos.Valores;

@Entity
@Table(name = "cobro")
public class Cobro extends Valores {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orden_pago_id", nullable = false)
	private OrdenCobranza ordenCobranza;

	public OrdenCobranza getOrdenCobranza() {
		return ordenCobranza;
	}

	public void setOrdenCobranza(OrdenCobranza ordenCobranza) {
		this.ordenCobranza = ordenCobranza;
	}

}

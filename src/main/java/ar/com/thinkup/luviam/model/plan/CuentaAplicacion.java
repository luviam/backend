package ar.com.thinkup.luviam.model.plan;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.enums.TipoCuentaEnum;

@Entity
@Table(name = "cuenta_aplicacion")
@Where(clause="deleted=0")
public class CuentaAplicacion extends NodoPlanCuenta implements IDescriptivo {

	private static final long serialVersionUID = 4903034030854140602L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sub_grupo_id")
	private SubGrupo subGrupo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "centro_costo_id")
	public CentroCosto centroCosto;

	public SubGrupo getSubGrupo() {
		return subGrupo;
	}

	public void setSubGrupo(SubGrupo subGrupo) {
		this.subGrupo = subGrupo;
	}

	public CentroCosto getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(CentroCosto centroCosto) {
		this.centroCosto = centroCosto;
	}

	@Override
	public String getTipoCuenta() {
		return TipoCuentaEnum.APLICACION.getCodigo();
	}

	@Override
	public String getDescripcion() {

		return this.getNombre();
	}

	

	

	

	
	

}

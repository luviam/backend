package ar.com.thinkup.luviam.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import ar.com.thinkup.luviam.vo.CategoriaProductoVO;

@Entity
@Table(name = "tipo_comisionista")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class TipoComisionista extends Parametrico {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7680428864690551917L;
	public static final String VENDEDOR  = "VEND";
	public static final String SALON  = "SALON";

	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name = "tipo_comisionista_x_categoria_producto", joinColumns = {
			@JoinColumn(name = "tipo_comisionista_id") }, inverseJoinColumns = {
					@JoinColumn(name = "categoria_producto_id") })
	private Set<CategoriaProducto> categoriasComisionables = new HashSet<>();

	public TipoComisionista() {
		super();

	}

	public TipoComisionista(Boolean habilitado, String codigo, String descripcion, Boolean esSistema) {
		super(habilitado, codigo, descripcion, esSistema);

	}

	public TipoComisionista(Long id, Boolean habilitado, String codigo, String descripcion, Boolean esSistema,
			List<CategoriaProducto> categorias) {
		super(id, habilitado, codigo, descripcion, esSistema);
		this.categoriasComisionables = new HashSet<>();
		this.categoriasComisionables.addAll(categorias);

	}

	public List<CategoriaProducto> getCategorias() {
		return categoriasComisionables.stream().collect(Collectors.toList());
	}

	public void setCategorias(Set<CategoriaProducto> categorias) {
		this.categoriasComisionables = categorias;
	}

	public Set<CategoriaProducto> getCategoriasComisionables() {
		return categoriasComisionables;
	}

	public Set<CategoriaProducto> getCategoriasComisionablesSet() {
		return this.categoriasComisionables;
	}

	public CategoriaProducto getCategoria(CategoriaProductoVO dto) {
		return dto!= null ? this.categoriasComisionables.stream().filter(c -> c.getCodigo().equals(dto.getCodigo())).findFirst().orElse(null) : null;
	}

}

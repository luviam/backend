package ar.com.thinkup.luviam.model.documentos.cobros;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.contratos.Contrato;
import ar.com.thinkup.luviam.model.contratos.CuotaServicio;
import ar.com.thinkup.luviam.model.documentos.DocumentoPago;
import ar.com.thinkup.luviam.repository.OrdenCobranzaRepository.OrdenCobranzaCabecera;
import ar.com.thinkup.luviam.service.contabilizacion.IContabilizable;

@Entity
@Table(name = "orden_cobranza")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class OrdenCobranza extends DocumentoPago<Cobro> implements OrdenCobranzaCabecera, IContabilizable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8753313344073936695L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contrato_id", nullable = false)
	private Contrato contrato;

	@Column(name = "pago_a_cuenta", precision = 10, scale = 2)
	private BigDecimal pagoACuenta = new BigDecimal(0);

	@OrderBy("id ASC")
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "ordenCobranza")
	private Set<Cobro> cobros = new HashSet<>();

	@OrderBy("id ASC")
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "ordenCobranza")
	private Set<ItemOrdenCobranza> items = new HashSet<>();

	@Column(name = "observaciones")
	private String observaciones;

	@Column(name = "iva_cobrado")
	private BigDecimal ivaCobrado = new BigDecimal(0d);

	@Column(name = "importe_neto")
	private BigDecimal importeNeto = new BigDecimal(0d);

	public Double getImporteNeto() {
		return this.importeNeto != null ? this.importeNeto.doubleValue() : 0d;
	}

	public void setImporteNeto(Double val) {
		this.importeNeto = val != null ? new BigDecimal(val) : new BigDecimal(0);
	}

	public Double getIvaCobrado() {
		return this.ivaCobrado != null ? this.ivaCobrado.doubleValue() : 0d;
	}

	public void setIvaCobrado(Double val) {
		this.ivaCobrado = val != null ? new BigDecimal(val) : new BigDecimal(0);
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Double getCobroACuenta() {
		return pagoACuenta.doubleValue();
	}

	public void setCobroACuenta(Double pagoACuenta) {
		this.pagoACuenta = new BigDecimal(pagoACuenta);
	}

	public Set<Cobro> getCobros() {
		return cobros;
	}

	public void setCobros(Set<Cobro> cobros) {
		this.cobros = new HashSet<>();
		this.cobros.addAll(cobros);
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public boolean addCobro(Cobro cobro) {
		cobro.setOrdenCobranza(this);
		return this.cobros.add(cobro);
	}

	public Cobro getCobro(Long id) {
		return this.cobros.parallelStream().filter(i -> i.getId() != null && i.getId().equals(id)).findFirst()
				.orElse(null);
	}

	public boolean addItem(ItemOrdenCobranza item) {
		item.setOrdenCobranza(this);
		return this.items.add(item);
	}

	public ItemOrdenCobranza getItem(Long id) {
		return this.items.parallelStream().filter(i -> i.getId() != null && i.getId().equals(id)).findFirst()
				.orElse(null);
	}

	public Set<ItemOrdenCobranza> getItems() {
		return items;
	}

	public Set<ItemOrdenCobranza> getItemsNeto() {
		return items.stream().filter(i -> !i.getCuotaServicio().getServicio().getProducto().esImpuesto())
				.collect(Collectors.toSet());
	}

	public Set<ItemOrdenCobranza> getImpuestos() {
		return items.stream().filter(i -> i.getCuotaServicio().getServicio().getProducto().esImpuesto())
				.collect(Collectors.toSet());
	}

	public void setItems(Set<ItemOrdenCobranza> items) {
		this.items = new HashSet<>();
		this.items.addAll(items);
	}

	@Override
	public Set<Cobro> getValores() {
		return this.getCobros();
	}

	@Override
	public void addValor(Cobro valor) {
		this.addCobro(valor);
	}

	public Double getTotal() {
		return this.getItems().stream().map(ItemOrdenCobranza::getImporte).reduce(0d, Double::sum)
				+ this.getIvaCobrado();
	}

	@Override
	public Asiento getAsiento() {
		return this.getAsientoGenerado();
	}

	public List<CuotaServicio> getCuotasCobradas() {
		return this.getItems().stream().map(ItemOrdenCobranza::getCuotaServicio).collect(Collectors.toList());
	}
}

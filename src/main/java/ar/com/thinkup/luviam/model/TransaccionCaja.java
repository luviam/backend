package ar.com.thinkup.luviam.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transaccion_caja")
public class TransaccionCaja implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5215789349105801391L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "tipo_transaccion_caja")
	private Integer tipo;

	@Column(name = "caja_id")
	private Long cajaId;

	@Column(name = "monto_informado", precision = 10, scale = 2)
	private BigDecimal monto = new BigDecimal(0);

	@Column(name = "responsable_id")
	private Long responsableId;

	@Column(name = "fecha_transaccion")
	private Date fechaTransaccion;

	@Column(name = "diferencia_con_saldo", precision = 10, scale = 2)
	private BigDecimal diferenciaConSaldo = new BigDecimal(0);

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public Long getCajaId() {
		return cajaId;
	}

	public void setCajaId(Long cajaId) {
		this.cajaId = cajaId;
	}

	public Double getMonto() {
		return monto.doubleValue();
	}

	public void setMonto(Double monto) {
		this.monto = new BigDecimal(monto);
	}

	public Long getResponsableId() {
		return responsableId;
	}

	public void setResponsableId(Long responsableId) {
		this.responsableId = responsableId;
	}

	public Date getFechaTransaccion() {
		return fechaTransaccion;
	}

	public void setFechaTransaccion(Date fechaTransaccion) {
		this.fechaTransaccion = fechaTransaccion;
	}

	public Long getId() {
		return id;
	}

	public Double getDiferenciaConSaldo() {
		return diferenciaConSaldo.doubleValue();
	}

	public void setDiferenciaConSaldo(Double diferenciaConSaldo) {
		this.diferenciaConSaldo = new BigDecimal(diferenciaConSaldo);
	}

}

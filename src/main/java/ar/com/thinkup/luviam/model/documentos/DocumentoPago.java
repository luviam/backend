package ar.com.thinkup.luviam.model.documentos;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.StringUtils;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.vo.Identificable;

@MappedSuperclass
public abstract class DocumentoPago<T extends Valores> implements IDescriptivo, Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7347837787030234636L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "numero_doc")
	private String numero;

	@Column(name = "descripcion")
	private String descripcion;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "responsable_id", nullable = true)
	private Usuario responsable;

	@Column(name = "fecha_doc", nullable = false)
	private Date fecha = new Date();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "centro_costo_id", nullable = false)
	private CentroCosto centroCosto;

	@Column(name = "codigo_estado", nullable = false)
	private String codigoEstado;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "asiento_id", nullable = true)
	private Asiento asientoGenerado;

	@Column(name = "importe", nullable = false)
	private BigDecimal importe = new BigDecimal(0);
	
	@Column(name="numero_comprobante")
	private String numeroComprobante;
	public String getNumeroComprobante() {
		return numeroComprobante;
	}

	public void setNumeroComprobante(String numeroComprobante) {
		this.numeroComprobante = numeroComprobante;
	}


	public DocumentoPago() {
		super();
	}

	public Long getId() {
		return id;
	}

	@Override
	public String getCodigo() {
		return String.valueOf(this.getId());
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Override
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Usuario getResponsable() {
		return responsable;
	}

	public void setResponsable(Usuario responsable) {
		this.responsable = responsable;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public CentroCosto getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(CentroCosto centroCosto) {
		this.centroCosto = centroCosto;
	}

	public String getCodigoEstado() {
		return codigoEstado;
	}

	public void setCodigoEstado(String codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	public Asiento getAsientoGenerado() {
		return asientoGenerado;
	}

	public void setAsientoGenerado(Asiento asientoGenerado) {
		this.asientoGenerado = asientoGenerado;
	}

	public EstadoOperacionEnum getEstadoOperacion() {
		return StringUtils.isBlank(this.codigoEstado) ? null : EstadoOperacionEnum.getByCodigo(this.codigoEstado);
	}

	public void setEstado(EstadoOperacionEnum estado) {
		this.codigoEstado = estado.getCodigo();
	}

	@Override
	public String toString() {
		return "N° : " + this.getNumero() + " - "
				+ (StringUtils.isEmpty(this.getDescripcion()) ? "" : this.getDescripcion());
	}

	public abstract Set<T> getValores();

	public abstract void addValor(T valor);

	public T getValor(Long id) {
		return this.getValores().parallelStream().filter(i -> i.getId() != null && i.getId().equals(id)).findFirst()
				.orElse(null);

	}

	// este valor solo se actualiza al guardarse en base de datos. usar getTotal
	// para valor actualizado
	public Double getImporte() {
		return importe.doubleValue();
	}

	public void setImporte(Double importe) {
		this.importe = new BigDecimal(importe);
	}

}
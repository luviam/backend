package ar.com.thinkup.luviam.model;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "unidad_producto")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class UnidadProducto extends Parametrico {

	public UnidadProducto() {
		super();
	}

	public UnidadProducto(Boolean habilitado, String codigo, String descripcion, Boolean esSistema) {
		super(habilitado, codigo, descripcion, esSistema);
	}

	public UnidadProducto(Long id, Boolean habilitado, String codigo, String descripcion, Boolean esSistema) {
		super(id, habilitado, codigo, descripcion, esSistema);
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = -7680428864690551917L;

}

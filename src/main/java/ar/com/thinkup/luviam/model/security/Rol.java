package ar.com.thinkup.luviam.model.security;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "rol")
public class Rol implements GrantedAuthority {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String SUPER_USER = "ROLE_ADMIN";
	public static final String VENTAS = "ROLE_VENTAS";
	public static final String COMPRAS = "ROLE_COMPRAS";
	public static final String GERENCIA = "ROLE_GERENCIA";
	public static final String ADMINISTRATIVO = "ROLE_ADMINISTRATIVO";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String codigo;

	@Column
	private String descripcion;

	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean activo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rol other = (Rol) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String getAuthority() {
		return this.codigo;
	}

}

package ar.com.thinkup.luviam.model;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity()
@Table(name = "tipo_evento")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class TipoEvento extends Parametrico{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5421268324452414731L;

	
}

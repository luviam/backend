package ar.com.thinkup.luviam.model;

import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.vo.Identificable;
import ar.com.thinkup.luviam.vo.indicadores.CubiertoPromedioVO;

@SqlResultSetMapping(name = "CubiertoPromedioMap", classes = {
		@ConstructorResult(targetClass = CubiertoPromedioVO.class, columns = { @ColumnResult(name = "codigo"),
				@ColumnResult(name = "descripcion"), @ColumnResult(name = "promedio", type = Double.class),
				@ColumnResult(name = "minimo", type = Double.class),
				@ColumnResult(name = "maximo", type = Double.class) }) })

@NamedNativeQuery(name = "Producto.getPrecioPromedio", resultSetMapping = "CubiertoPromedioMap", query = "select p.codigo as codigo, p.nombre as descripcion,  "
		+ "avg(sc.costo_unitario) as promedio,  " + "min(sc.costo_unitario) as minimo,  "
		+ "max(sc.costo_unitario) as maximo  " + "from servicio_contratado sc "
		+ "join producto p on p.id = sc.producto_id " + "join centro c on c.id = sc.centro_costo_id "
		+ "join contrato con on con.id = sc.contrato_id " + "where c.codigo = ?1 " + "and p.codigo = ?2 "
		+ "and con.fecha_contratacion between ?3 and ?4 " + "group by p.codigo, p.nombre")

@Entity
@Table(name = "producto")
public class Producto implements IDescriptivo, Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 270546376724188760L;

	public final static String CODIGO_ALQUIER_SALON = "ALQSAL";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "codigo")
	private String codigo;

	@OrderBy("ASC")
	private String nombre;

	@ManyToOne
	@JoinColumn(name = "unidad_id")
	private UnidadProducto unidad;

	@ManyToOne
	@JoinColumn(name = "categoria_id")
	private CategoriaProducto categoria;

	
	@ManyToOne
	@JoinColumn(name = "marca_id")
	private MarcaProductos marca;

	
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean activo;

	@Column(name = "es_borrable")
	private Boolean esBorrable;

	@Column(name = "es_divisible")
	private Boolean divisible;

	@Column(name = "adolescente_proporcional")
	private Integer adolescenteProp = 100;

	@Column(name = "menor_proporcional")
	private Integer menorProp = 50;

	@Column(name = "bebe_proporcional")
	private Integer bebeProp = 0;

	@Column(name = "imputa_iva")
	private Boolean imputaIVA = true;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "producto")
	private List<ImputacionProducto> cuentas = new Vector<>();

	@ManyToOne
	@JoinColumn(name = "tipo_menu_id", nullable = true)
	private TipoMenu tipoMenu;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public UnidadProducto getUnidad() {
		return unidad;
	}

	public void setUnidad(UnidadProducto unidad) {
		this.unidad = unidad;
	}

	public CategoriaProducto getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaProducto categoria) {
		this.categoria = categoria;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Boolean getEsBorrable() {
		return esBorrable;
	}

	public void setEsBorrable(Boolean esBorrable) {
		this.esBorrable = esBorrable;
	}

	@Override
	public String getDescripcion() {

		return this.nombre;
	}

	public Boolean getDivisible() {
		return divisible;
	}

	public void setDivisible(Boolean divisible) {
		this.divisible = divisible;
	}

	public Integer getAdolescenteProp() {
		return adolescenteProp;
	}

	public void setAdolescenteProp(Integer adolescenteProp) {
		this.adolescenteProp = adolescenteProp;
	}

	public Integer getMenorProp() {
		return menorProp;
	}

	public void setMenorProp(Integer menorProp) {
		this.menorProp = menorProp;
	}

	public Integer getBebeProp() {
		return bebeProp;
	}

	public void setBebeProp(Integer bebeProp) {
		this.bebeProp = bebeProp;
	}

	public List<ImputacionProducto> getCuentas() {
		return cuentas;
	}

	public Boolean getImputaIVA() {
		return imputaIVA;
	}

	public void setImputaIVA(Boolean imputaIVA) {
		this.imputaIVA = imputaIVA;
	}

	public void setCuentas(List<ImputacionProducto> cuentas) {
		this.cuentas = cuentas;
	}

	public ImputacionProducto getCuenta(Long id) {
		return this.cuentas.stream().filter(c -> c.getId() != null && c.getId().equals(id)).findFirst().orElse(null);
	}

	public ImputacionProducto getCuenta(String centroCodigo) {
		return this.cuentas.stream()
				.filter(c -> c.getCentroCosto() != null && c.getCentroCosto().getCodigo().equals(centroCodigo))
				.findFirst().orElse(null);
	}

	public void addCuenta(ImputacionProducto cuenta) {
		if (this.cuentas == null)
			this.cuentas = new Vector<>();
		cuenta.setProducto(this);
		this.cuentas.add(cuenta);
	}

	public Boolean esImpuesto() {
		return this.categoria != null && this.categoria.esImpuesto();
	}

	public TipoMenu getTipoMenu() {
		return tipoMenu;
	}

	public void setTipoMenu(TipoMenu tipoMenu) {
		this.tipoMenu = tipoMenu;
	}

	public boolean esMenu() {
		return this.categoria != null && this.categoria.getCodigo().equals(CategoriaProducto.MENU);
	}

	public MarcaProductos getMarca() {
		return marca;
	}

	public void setMarca(MarcaProductos marca) {
		this.marca = marca;
	}
	
	

}

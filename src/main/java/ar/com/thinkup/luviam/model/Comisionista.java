package ar.com.thinkup.luviam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.empleados.Empleado;
import ar.com.thinkup.luviam.repository.ComisionistaRepository.IComisionistaDescriptivo;
import ar.com.thinkup.luviam.vo.Identificable;

/**
 * 
 * @author ThinkUp
 *
 */
@Entity
@Table(name = "comisionista")
@Where(clause = "activo='1'")
public class Comisionista implements IDescriptivo, Identificable, IComisionistaDescriptivo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2768068004784738837L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "nombre_contacto")
	private String contacto;

	@Column(name = "domicilio")
	private String domicilio;

	@Column(name = "localidad")
	private String localidad;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "celular")
	private String celular;

	@Column(name = "email")
	private String email;

	@Column(name = "razon_social")
	private String razonSocial;

	@Column(name = "cuit")
	private String cuit;

	@ManyToOne
	@JoinColumn(name = "tipo_iva_id")
	private TipoIVA iva;

	@Column(name = "domicilio_facturacion")
	private String domicilioFacturacion;

	@Column(name = "localidad_facturacion")
	private String localidadFacturacion;

	@ManyToOne
	@JoinColumn(name = "tipo_comisionista_id")
	private TipoComisionista tipoComisionista;

	@Column(name = "nombre_banco")
	private String nombreBanco;

	@Column(name = "cbu")
	private String cbu;

	@Column(name = "nombre_cuenta")
	private String numeroCuenta;

	@Column(name = "alias_bancario")
	private String aliasBancario;

	@Column(name = "activo")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean activo = true;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "salon_id", nullable = true)
	private Salon salon;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "empleado_id", nullable = true)
	private Empleado vendedor;

	public Comisionista() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public TipoComisionista getTipoComisionista() {
		return tipoComisionista;
	}

	public void setTipoComisionista(TipoComisionista tipoComisionista) {
		this.tipoComisionista = tipoComisionista;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public TipoIVA getIva() {
		return iva;
	}

	public void setIva(TipoIVA iva) {
		this.iva = iva;
	}

	public String getDomicilioFacturacion() {
		return domicilioFacturacion;
	}

	public void setDomicilioFacturacion(String domicilioFacturacion) {
		this.domicilioFacturacion = domicilioFacturacion;
	}

	public String getLocalidadFacturacion() {
		return localidadFacturacion;
	}

	public void setLocalidadFacturacion(String localidadFacturacion) {
		this.localidadFacturacion = localidadFacturacion;
	}

	public String getNombreBanco() {
		return nombreBanco;
	}

	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}

	public String getCbu() {
		return cbu;
	}

	public void setCbu(String cbu) {
		this.cbu = cbu;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getAliasBancario() {
		return aliasBancario;
	}

	public void setAliasBancario(String aliasBancario) {
		this.aliasBancario = aliasBancario;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	@Override
	public String toString() {

		return "Comisionista " + this.getNombre() + " ID: " + this.getId();
	}

	@Override
	public String getCodigo() {
		return this.getId() != null ? this.getId() + "" : "SD";
	}

	@Override
	public String getDescripcion() {
		return this.getNombre();
	}

	public Salon getSalon() {
		return salon;
	}

	public void setSalon(Salon salon) {
		this.salon = salon;
	}

	public Empleado getVendedor() {
		return vendedor;
	}

	public void setVendedor(Empleado vendedor) {
		this.vendedor = vendedor;
	}

	public boolean esVendedor() {

		return this.getVendedor() != null;
	}

}

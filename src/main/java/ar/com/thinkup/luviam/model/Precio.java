package ar.com.thinkup.luviam.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.vo.Identificable;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name="precio")
@DiscriminatorColumn(name="tipo")
public abstract class Precio implements Identificable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7212853309751416133L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="producto_id", nullable=false)
	private Producto producto;
	
	@Column(name="fecha_desde")
	private Date fechaDesde;
	
	@Column(name="fecha_hasta")
	private Date fechaHasta;
	
	private Boolean activo;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="responsable_id", nullable=false)
	private Usuario responsable;
	
	  @ElementCollection(fetch=FetchType.EAGER)
	  @CollectionTable(name ="dias_por_precio")
    private List<Integer> diasSemana = new Vector<>();
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="salon_id", nullable=false)
	private Salon salon;
	
	@Column(name="valor", scale=2,precision=10)
	private BigDecimal valor;
	@Column(name="ultima_modificacion")
	private Date ultimaModificacion = new Date();
	
	public Double getValor() {
		return this.valor != null ? this.valor.doubleValue() : 0d;
	}

	public void setValor(Double val) {
		this.valor = val != null ? new BigDecimal(val) : new BigDecimal(0);
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Usuario getResponsable() {
		return responsable;
	}

	public void setResponsable(Usuario responsable) {
		this.responsable = responsable;
	}

	public List<Integer> getDiasSemana() {
		return diasSemana;
	}

	public void setDiasSemana(List<Integer> diasSemana) {
		this.diasSemana = diasSemana;
	}

	public Salon getSalon() {
		return salon;
	}

	public void setSalon(Salon salon) {
		this.salon = salon;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Boolean coincideEnDias(List<Integer> dias) {

		return dias.stream().anyMatch(d -> this.getDiasSemana().contains(d));
	}

	public Date getUltimaModificacion() {
		return ultimaModificacion;
	}

	public void setUltimaModificacion(Date ultimaModificacion) {
		this.ultimaModificacion = ultimaModificacion;
	}

	
	
}

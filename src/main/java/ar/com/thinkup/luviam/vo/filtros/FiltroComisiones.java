package ar.com.thinkup.luviam.vo.filtros;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.enums.TipoPagoComision;

public class FiltroComisiones implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3319671803740153276L;

	private DescriptivoGeneric comisionista = DescriptivoGeneric.TODOS();
	private Date fechaDesde = new DateTime().withTimeAtStartOfDay().minusYears(2).toDate();
	private Date fechaHasta = new DateTime().withTimeAtStartOfDay().plusDays(1).toDate();;
	private DescriptivoGeneric centro = DescriptivoGeneric.TODOS();
	private DescriptivoGeneric salon = DescriptivoGeneric.TODOS();
	private DescriptivoGeneric horario = DescriptivoGeneric.TODOS();
	private DescriptivoGeneric contrato = DescriptivoGeneric.TODOS();
	private List<String> tiposComision = new Vector<>();

	public List<String> getTiposComision() {
		return tiposComision.size() == 0
				? Arrays.asList(TipoPagoComision.values()).stream().map(t -> t.getCodigo()).collect(Collectors.toList())
				: this.tiposComision;
	}

	public String getCodigoCentro() {
		return (this.centro == null || StringUtils.isEmpty(this.centro.getCodigo())) ? "-1" : this.centro.getCodigo();
	}

	public Long getIdComisionista() {
		return (this.comisionista == null || StringUtils.isEmpty(this.comisionista.getCodigo())) ? -1
				: Long.valueOf(this.comisionista.getCodigo());
	}

	public Long getIdContrato() {
		return (this.contrato == null || StringUtils.isEmpty(this.contrato.getCodigo())) ? -1
				: Long.valueOf(this.contrato.getCodigo());
	}

	public DescriptivoGeneric getComisionista() {
		return comisionista;
	}

	public void setComisionista(DescriptivoGeneric comisionista) {
		this.comisionista = comisionista;
	}

	public Date getFechaDesde() {
		return fechaDesde != null ? this.fechaDesde : new DateTime().withTimeAtStartOfDay().minusDays(100).toDate();
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta != null ? this.fechaHasta : new DateTime().withTimeAtStartOfDay().plusDays(1).toDate();
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public DescriptivoGeneric getCentro() {
		return centro;
	}

	public void setCentro(DescriptivoGeneric centro) {
		this.centro = centro;
	}

	public DescriptivoGeneric getSalon() {
		return salon;
	}

	public void setSalon(DescriptivoGeneric salon) {
		this.salon = salon;
	}

	public DescriptivoGeneric getHorario() {
		return horario;
	}

	public void setHorario(DescriptivoGeneric horario) {
		this.horario = horario;
	}

	public DescriptivoGeneric getContrato() {
		return contrato;
	}

	public void setContrato(DescriptivoGeneric contrato) {
		this.contrato = contrato;
	}

	public void setTiposComision(List<String> tiposComision) {
		this.tiposComision = tiposComision;
	}

	@Override
	public String toString() {
		return "FiltroComisiones [comisionista=" + comisionista + ", fechaDesde=" + fechaDesde + ", fechaHasta="
				+ fechaHasta + ", centro=" + centro + ", salon=" + salon + ", horario=" + horario + ", contrato="
				+ contrato + ", tiposComision=" + tiposComision + "]";
	}
	
	

}

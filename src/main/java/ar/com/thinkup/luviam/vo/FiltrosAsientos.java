package ar.com.thinkup.luviam.vo;

import java.util.Date;

import org.joda.time.DateTime;

public class FiltrosAsientos {
	
	private Date fechaDesde;
	private Date fechaHasta;
	
	public FiltrosAsientos() {
		
		this.fechaDesde = new DateTime().withTimeAtStartOfDay().toDate();
		this.fechaHasta = new Date();
	}
	public Date getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public Date getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	
	

}

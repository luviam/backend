package ar.com.thinkup.luviam.vo;

import java.io.Serializable;

import ar.com.thinkup.luviam.model.MarcaProductos;
import ar.com.thinkup.luviam.model.Producto;
import ar.com.thinkup.luviam.model.def.IDescriptivo;

public class PrecioProductoVO implements IDescriptivo , Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7599682600901108246L;
	private Long id;
	private String codigo;
	private String nombre;
	private MarcaVO marca;

	public PrecioProductoVO() {
		super();

	}
	
	

	public PrecioProductoVO(Long id, String codigo, String nombre,	MarcaProductos marca) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.nombre = nombre;
		if(marca!=null) {
			this.marca = new MarcaVO(marca);
		}
	}



	public PrecioProductoVO(Producto ent) {
		super();
		this.id = ent.getId();
		this.codigo = ent.getCodigo();
		this.nombre = ent.getDescripcion();
		if (ent.getMarca() != null)
			this.marca = new MarcaVO(ent.getMarca());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String descripcion) {
		this.nombre = descripcion;
	}

	public MarcaVO getMarca() {
		return marca;
	}

	public void setMarca(MarcaVO marca) {
		this.marca = marca;
	}

	@Override
	public String getDescripcion() {
		return this.getNombre();
	}

}

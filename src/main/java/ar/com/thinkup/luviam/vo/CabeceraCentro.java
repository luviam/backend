package ar.com.thinkup.luviam.vo;

import ar.com.thinkup.luviam.model.CentroCosto;

public class CabeceraCentro {
	
	private String codigo;
	private String nombre;
	private String cuit;
	private String iibb;
	private String domicilioFiscal;
	private Long id;
	
	public CabeceraCentro() {

	}
	
	public CabeceraCentro(CentroCosto cc) {
		this.codigo = cc.getId().toString();
		this.nombre = cc.getNombre();
		this.cuit = cc.getCuit();
		this.iibb = cc.getIibb();
		this.domicilioFiscal = cc.getDomicilioFiscal();
		this.id = cc.getId();
	}
		
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCuit() {
		return cuit;
	}
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	public String getIibb() {
		return iibb;
	}
	public void setIibb(String iibb) {
		this.iibb = iibb;
	}
	public String getDomicilioFiscal() {
		return domicilioFiscal;
	}
	public void setDomicilioFiscal(String domicilioFiscal) {
		this.domicilioFiscal = domicilioFiscal;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
		

}

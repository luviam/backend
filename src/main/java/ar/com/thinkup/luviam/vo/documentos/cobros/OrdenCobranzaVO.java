package ar.com.thinkup.luviam.vo.documentos.cobros;

import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import ar.com.thinkup.luviam.model.CategoriaProducto;
import ar.com.thinkup.luviam.model.documentos.cobros.Cobro;
import ar.com.thinkup.luviam.model.documentos.cobros.OrdenCobranza;
import ar.com.thinkup.luviam.vo.ContratoCabeceraVO;
import ar.com.thinkup.luviam.vo.documentos.DocumentoPagoVO;

public class OrdenCobranzaVO extends DocumentoPagoVO<Cobro, CobroVO, ItemOrdenCobranzaVO> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6868888468247077971L;
	private ContratoCabeceraVO contrato;
	private List<CobroVO> cobros = new Vector<>();
	private List<ItemOrdenCobranzaVO> items = new Vector<>();
	private Double pagoACuenta;
	private String observaciones;
	private Double ivaCobrado = 0d;
	private Double importeNeto = 0d;

	public OrdenCobranzaVO() {
		super();
		this.items = new Vector<>();
		this.cobros = new Vector<>();
	}

	public OrdenCobranzaVO(OrdenCobranza op) {
		super(op);
		this.items = new Vector<>();
		this.cobros = new Vector<>();
		this.cobros.addAll(op.getCobros().stream().map(p -> new CobroVO(p)).collect(Collectors.toList()));
		this.items.addAll(op.getItems().stream().map(i -> new ItemOrdenCobranzaVO(i)).collect(Collectors.toList()));
		this.contrato = new ContratoCabeceraVO(op.getContrato());
		this.pagoACuenta = op.getCobroACuenta();
		this.observaciones = op.getObservaciones();
		this.ivaCobrado = op.getIvaCobrado();
		this.importeNeto = op.getImporteNeto();
	}

	@Override
	public List<ItemOrdenCobranzaVO> getItems() {
		return items;
	}

	public void setItems(List<ItemOrdenCobranzaVO> items) {
		this.items = items;
	}

	public OrdenCobranzaVO(ContratoCabeceraVO contrato, List<CobroVO> cobros, List<ItemOrdenCobranzaVO> items) {
		super();
		this.contrato = contrato;
		this.cobros = cobros;
		this.items = items;
	}

	@Override
	public List<CobroVO> getValores() {
		return this.getCobros();
	}

	public List<CobroVO> getCobros() {
		return cobros;
	}

	public void setCobros(List<CobroVO> cobros) {
		this.cobros = cobros;
	}

	public ContratoCabeceraVO getContrato() {
		return contrato;
	}

	public void setContrato(ContratoCabeceraVO contrato) {
		this.contrato = contrato;
	}

	public Double getPagoACuenta() {
		return pagoACuenta;
	}

	public void setPagoACuenta(Double pagoACuenta) {
		this.pagoACuenta = pagoACuenta;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Double getIvaCobrado() {
		return ivaCobrado;
	}

	public void setIvaCobrado(Double ivaCobrado) {
		this.ivaCobrado = ivaCobrado;
	}

	public Double getImporteNeto() {
		return importeNeto;
	}

	public void setImporteNeto(Double importeNeto) {
		this.importeNeto = importeNeto;
	}

	public Double getImporte() {
		return this.importeNeto + this.ivaCobrado;
	}

	public Double getTotalImpuestos() {

		return this.items.stream().filter(
				i -> i.getCuotaServicio().getProducto().getCategoria().getCodigo().equals(CategoriaProducto.IMPUESTO))
				.mapToDouble(i -> i.getImporte()).sum();
	}

}

package ar.com.thinkup.luviam.vo;

import java.util.Date;

import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.repository.OrdenCobranzaRepository.OrdenCobranzaCabecera;

public class OrdenCobranzaCabeceraVO {

	private Long id;
	private Date fecha;
	private String numero;
	private String responsable;
	private String descripcion;
	private Double importeNeto;
	private String estado;
	private ContratoCabeceraVO contrato;
	private Double ivaCobrado;
	

	public OrdenCobranzaCabeceraVO() {
		super();
	}

	public OrdenCobranzaCabeceraVO(OrdenCobranzaCabecera op) {
		this();
		this.id = op.getId();
		this.fecha = op.getFecha();
		this.numero = op.getNumero();
		this.responsable = op.getResponsable().getFullName();
		this.descripcion = op.getDescripcion();
		this.importeNeto = op.getImporteNeto();
		this.estado = op.getCodigoEstado();
		this.contrato = new ContratoCabeceraVO(op.getContrato());
		this.ivaCobrado = op.getIvaCobrado() != null ? op.getIvaCobrado() : 0d;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getImporteNeto() {
		return importeNeto;
	}

	public void setImporteNeto(Double importeNeto) {
		this.importeNeto = importeNeto;
	}

	public void setEstado(EstadoOperacionEnum e) {
		this.estado = e.getCodigo();

	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public ContratoCabeceraVO getContrato() {
		return contrato;
	}

	public void setContrato(ContratoCabeceraVO contrato) {
		this.contrato = contrato;
	}

	public Double getIvaCobrado() {
		return ivaCobrado;
	}

	public void setIvaCobrado(Double ivaCobrado) {
		this.ivaCobrado = ivaCobrado;
	}

}

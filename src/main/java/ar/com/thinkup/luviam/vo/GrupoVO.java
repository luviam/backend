package ar.com.thinkup.luviam.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.plan.Grupo;

public class GrupoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7478402934300808895L;
	private Long id;
	private String codigo;
	private String descripcion;
	private List<DescriptivoGeneric> subGrupos = new Vector<>();

	public GrupoVO() {

	}

	public GrupoVO(Grupo g) {
		this.id = g.getId();
		this.codigo = g.getCodigo();
		this.descripcion = g.getDescripcion();
		this.subGrupos = g.getSubGrupos().stream().map(s -> new DescriptivoGeneric(s)).collect(Collectors.toList());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<DescriptivoGeneric> getSubGrupos() {
		return subGrupos;
	}

	public void setSubGrupos(List<DescriptivoGeneric> subGrupos) {
		this.subGrupos = subGrupos;
	}

}

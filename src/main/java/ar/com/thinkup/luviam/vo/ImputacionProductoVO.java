package ar.com.thinkup.luviam.vo;

import ar.com.thinkup.luviam.model.ImputacionProducto;

public class ImputacionProductoVO extends ImputacionCuentaVO {

	private Boolean habilitado;

	public ImputacionProductoVO() {
		super();
	}

	public ImputacionProductoVO(ImputacionProducto icc) {
		super(icc);
		this.habilitado = icc.getHabilitado();
	}

	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

	public Boolean getHabilitado() {
		return this.habilitado;
	}

}

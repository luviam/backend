package ar.com.thinkup.luviam.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Vector;

import org.joda.time.DateTime;

import ar.com.thinkup.luviam.model.Precio;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public abstract class PrecioVO implements Serializable, Identificable {

	private static final long serialVersionUID = 4601234218928293091L;
	private Long id;

	private PrecioProductoVO producto;

	private Date fechaDesde;

	private Date fechaHasta;

	private Boolean activo;

	private DescriptivoGeneric responsable;

	private List<Integer> diasSemana = new Vector<>();

	private DescriptivoGeneric salon;

	private Double valor;
	private Date ultimaModificacion = new Date();

	public PrecioVO() {
		super();
	}

	public PrecioVO(Precio ent) {
		this.id = ent.getId();
		this.producto = new PrecioProductoVO(ent.getProducto());
		this.fechaDesde = ent.getFechaDesde();
		this.fechaHasta = ent.getFechaHasta();
		this.activo = ent.getActivo();
		this.responsable = new DescriptivoGeneric(ent.getResponsable());
		this.diasSemana = ent.getDiasSemana();
		this.salon = new DescriptivoGeneric(ent.getSalon());
		this.valor = ent.getValor();
		this.ultimaModificacion = ent.getUltimaModificacion();

	}

	@Override
	public Long getId() {
		return this.id;
	}

	public PrecioProductoVO getProducto() {
		return producto;
	}

	public void setProducto(PrecioProductoVO producto) {
		this.producto = producto;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public DescriptivoGeneric getResponsable() {
		return responsable;
	}

	public void setResponsable(DescriptivoGeneric responsable) {
		this.responsable = responsable;
	}

	public List<Integer> getDiasSemana() {
		return diasSemana;
	}

	public void setDiasSemana(List<Integer> diasSemana) {
		this.diasSemana = diasSemana;
	}

	public DescriptivoGeneric getSalon() {
		return salon;
	}

	public void setSalon(DescriptivoGeneric salon) {
		this.salon = salon;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getUltimaModificacion() {
		return ultimaModificacion;
	}

	public void setUltimaModificacion(Date ultimaModificacion) {
		this.ultimaModificacion = ultimaModificacion;
	}

	public Boolean superponeFecha(PrecioVO precio) {
		Date thisHasta = this.fechaHasta !=null? this.fechaHasta : new DateTime().plusYears(1000).toDate();
		Date precioHasta = precio.fechaHasta !=null? precio.fechaHasta : new DateTime().plusYears(1000).toDate();
		
		return (this.fechaDesde.before(precioHasta)
				&& precio.fechaDesde.before(thisHasta));
	}

	public Boolean enConflicto(PrecioVO precio) {
		return precio != null && precio.activo && this.activo && Objects.equals(this.salon, precio.salon) && this.superponeFecha(precio) && this.coincideEnDias(precio);
	}

	public Boolean coincideEnDias(PrecioVO precio) {

		return precio != null && precio.getDiasSemana().stream().anyMatch(d -> this.getDiasSemana().contains(d));
	}



}
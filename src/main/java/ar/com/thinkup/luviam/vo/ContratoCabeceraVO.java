package ar.com.thinkup.luviam.vo;

import java.io.Serializable;
import java.util.Date;

import ar.com.thinkup.luviam.model.contratos.Contrato;
import ar.com.thinkup.luviam.model.contratos.ContratoCabecera;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;

public class ContratoCabeceraVO implements Serializable, Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7208561036056223838L;

	private Long id;

	private DescriptivoGeneric estado;

	private String codigoHorario;

	private Date fechaContratacion;

	private Date fechaEvento;

	private String locacion;

	private String tipoEvento;

	private Long idCliente;

	private DescriptivoGeneric cliente;

	private Double total;

	private Double cobros;

	private String nombreVendedor;

	public ContratoCabeceraVO() {
		super();
	}

	public ContratoCabeceraVO(ContratoCabecera ent) {
		super();
		this.id = ent.getId();
		this.estado = EstadoOperacionEnum.getDescriptivo(ent.getCodigoEstado());
		this.codigoHorario = ent.getCodigoHorario();
		this.fechaContratacion = ent.getFechaContratacion();
		this.fechaEvento = ent.getFechaEvento();
		this.locacion = ent.getLocacion();
		this.tipoEvento = ent.getTipoEvento();
		this.idCliente = ent.getIdCliente();
		this.cliente = new DescriptivoGeneric(ent.getIdCliente() + "", ent.getCliente());
		this.total = ent.getTotal();
		this.cobros = ent.getCobros();
		this.nombreVendedor = ent.getNombreVendedor();

	}

	public ContratoCabeceraVO(Contrato ent) {
		this.id = ent.getId();
		this.estado = EstadoOperacionEnum.getDescriptivo(ent.getCodigoEstado());
		this.codigoHorario = ent.getCodigoHorario();
		this.fechaContratacion = ent.getFechaContratacion();
		this.fechaEvento = ent.getFechaEvento();
		this.locacion = ent.getLocacion();
		this.tipoEvento = ent.getTipoEvento().getDescripcion();
		this.idCliente = ent.getCliente().getId();
		this.cliente = new DescriptivoGeneric(ent.getCliente());
		this.nombreVendedor = ent.getVendedor().getNombre();
		this.total = 0d;
		this.cobros = 0d;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DescriptivoGeneric getEstado() {
		return estado;
	}

	public void setEstado(DescriptivoGeneric estado) {
		this.estado = estado;
	}

	public String getCodigoHorario() {
		return codigoHorario;
	}

	public void setCodigoHorario(String codigoHorario) {
		this.codigoHorario = codigoHorario;
	}

	public Date getFechaContratacion() {
		return fechaContratacion;
	}

	public void setFechaContratacion(Date fechaContratacion) {
		this.fechaContratacion = fechaContratacion;
	}

	public Date getFechaEvento() {
		return fechaEvento;
	}

	public void setFechaEvento(Date fechaEvento) {
		this.fechaEvento = fechaEvento;
	}

	public String getLocacion() {
		return locacion;
	}

	public void setLocacion(String locacion) {
		this.locacion = locacion;
	}

	public String getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public DescriptivoGeneric getCliente() {
		return cliente;
	}

	public void setCliente(DescriptivoGeneric cliente) {
		this.cliente = cliente;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getCobros() {
		return cobros;
	}

	public void setCobros(Double cobros) {
		this.cobros = cobros;
	}

	public String getNombreVendedor() {
		return nombreVendedor;
	}

	public void setNombreVendedor(String nombreVendedor) {
		this.nombreVendedor = nombreVendedor;
	}

}

package ar.com.thinkup.luviam.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import ar.com.thinkup.luviam.model.plan.NodoPlanCuenta;
import ar.com.thinkup.luviam.model.plan.NodoPlanCuentaGrupo;
import ar.com.thinkup.luviam.model.plan.SubGrupo;

public class CuentaPlanVO implements Serializable, Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3050284611345351342L;
	private String codigo;
	private String descripcion;
	private String tipoCuenta;
	private Long id;
	private Boolean activo;
	private Boolean eliminable;
	private List<CuentaPlanVO> cuentasHijas = new Vector<>();

	
	public CuentaPlanVO(NodoPlanCuenta nodo) {
		this.id = nodo.getId();
		this.codigo = nodo.getCodigo();
		this.descripcion = nodo.getNombre();
		this.tipoCuenta = nodo.getTipoCuenta();
		this.activo = nodo.getActivo();
		this.eliminable = nodo.getEliminable();
		if(nodo instanceof NodoPlanCuentaGrupo) {
			this.cuentasHijas = ((NodoPlanCuentaGrupo)nodo).getHijos().stream().map(n -> new CuentaPlanVO(n)).collect(Collectors.toList());	
		}else {
			this.cuentasHijas = new Vector<>();
		}
		
		
	}

	public CuentaPlanVO(SubGrupo nodo) {
		this.cuentasHijas = null;
	}
	
	
	
	
	public CuentaPlanVO() {
		super();
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<CuentaPlanVO> getCuentasHijas() {
		return cuentasHijas;
	}

	public void setCuentasHijas(List<CuentaPlanVO> cuentasHijas) {
		this.cuentasHijas = cuentasHijas;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Boolean getEliminable() {
		return eliminable;
	}

	public void setEliminable(Boolean eliminable) {
		this.eliminable = eliminable;
	}
	

	
}

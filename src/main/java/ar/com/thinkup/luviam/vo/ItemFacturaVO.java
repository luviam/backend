package ar.com.thinkup.luviam.vo;

import java.io.Serializable;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.documentos.factura.ItemFactura;
import ar.com.thinkup.luviam.model.enums.TipoIVAEnum;

public class ItemFacturaVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6989727723186539784L;

	private Long id;

	private DescriptivoGeneric centroCosto;
	private DescriptivoGeneric cuenta;
	private String descripcion;
	private DescriptivoGeneric tipoIVA;
	private Double importe;

	public ItemFacturaVO() {
		super();
	}

	public ItemFacturaVO(ItemFactura i) {
		this.id = i.getId();
		this.centroCosto = new DescriptivoGeneric(i.getCentroCosto());
		this.cuenta = new DescriptivoGeneric(i.getCuenta());
		this.descripcion = i.getDescripcion();
		TipoIVAEnum t = TipoIVAEnum.getByCodigo(i.getTipoIVA());
		if (t != null) {
			this.tipoIVA = new DescriptivoGeneric(t.getCodigo(), t.getDescripcion());
		}

		this.importe = i.getImporte();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DescriptivoGeneric getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(DescriptivoGeneric centroCosto) {
		this.centroCosto = centroCosto;
	}

	public DescriptivoGeneric getCuenta() {
		return cuenta;
	}

	public void setCuenta(DescriptivoGeneric cuenta) {
		this.cuenta = cuenta;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public DescriptivoGeneric getTipoIVA() {
		return tipoIVA;
	}

	public void setTipoIVA(DescriptivoGeneric tipoIVA) {
		this.tipoIVA = tipoIVA;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

}

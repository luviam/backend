package ar.com.thinkup.luviam.vo;

import java.util.List;

public class PlanDeCuentasVO {

	private List<CuentaPlanVO> cuentas;

	public List<CuentaPlanVO> getCuentas() {
		return cuentas;
	}

	public void setCuentas(List<CuentaPlanVO> cuentas) {
		this.cuentas = cuentas;
	}

}

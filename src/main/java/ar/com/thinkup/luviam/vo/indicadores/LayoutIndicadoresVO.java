package ar.com.thinkup.luviam.vo.indicadores;

import java.io.Serializable;

import ar.com.thinkup.luviam.model.comisionistas.LayoutIndicadores;
import ar.com.thinkup.luviam.model.security.Usuario;

public class LayoutIndicadoresVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1262830029518697626L;
	private String indicadoresStr;
	private Usuario usuario;

	public LayoutIndicadoresVO() {
		super();
	}

	public LayoutIndicadoresVO(LayoutIndicadores l) {
		this.indicadoresStr = l.getIndicadoresStr();
		this.usuario = null;
	}

	public String getIndicadoresStr() {
		return indicadoresStr;
	}

	public void setIndicadoresStr(String indicadoresStr) {
		this.indicadoresStr = indicadoresStr;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}

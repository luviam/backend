package ar.com.thinkup.luviam.vo.empleados;

import java.io.Serializable;
import java.util.Date;

import ar.com.thinkup.luviam.model.empleados.AltaEmpleado;

public class AltaEmpleadoVO implements Serializable {

	private Long id;

	private Date fechaAlta;

	private Date fechaBaja;

	private String observacion;

	public AltaEmpleadoVO() {
		super();
	}

	public AltaEmpleadoVO(AltaEmpleado ent) {
		super();
		this.id = ent.getId();
		this.fechaAlta = ent.getFechaAlta();
		this.fechaBaja = ent.getFechaBaja();
		this.observacion = ent.getObservacion();

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
	

}

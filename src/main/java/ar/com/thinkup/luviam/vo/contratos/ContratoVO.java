package ar.com.thinkup.luviam.vo.contratos;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.persistence.Column;

import ar.com.thinkup.luviam.model.contratos.Contrato;
import ar.com.thinkup.luviam.model.contratos.ServicioContratado;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.vo.ClienteVO;
import ar.com.thinkup.luviam.vo.Identificable;
import ar.com.thinkup.luviam.vo.SalonVO;

public class ContratoVO implements Serializable, Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5953328712323279895L;

	private Long id;

	private SalonVO salon;

	private DescriptivoGeneric vendedor;

	private DescriptivoGeneric tipoEvento;

	private String observaciones;

	private ClienteVO cliente;

	private DescriptivoGeneric horario;

	private Date fechaContratacion;

	private Date fechaEvento;

	private Integer baseInvitados;

	private Integer adultos;

	private Integer adolescentes;

	private Integer menores;

	private Integer bebes;

	private String numeroPresupuesto;

	private List<ServicioContratadoVO> servicios = new Vector<>();
	private List<ConfiguracionComisionVO> comisiones = new Vector<>();

	private String descripcion;

	private DescriptivoGeneric estado;

	private Boolean esCongelado;

	private Double ipMaximo;
	private Double ipMonto;

	private Double cobroGarantia;
	private Double totalCalculado;

	private String agasajados;
	private Date ultimaModificacion;

	public ContratoVO() {
		super();
	}

	public ContratoVO(Contrato ent) {
		super();
		this.id = ent.getId();
		if (ent.getSalon() != null) {
			this.salon = new SalonVO(ent.getSalon());
		} else {
			SalonVO s = new SalonVO();
			s.setNombre(ent.getNombreSalon());
			s.setDireccion(ent.getDireccionSalon());
			s.setEmail(ent.getEmailSalon());
			s.setTelefono(ent.getTelefonoSalon());
			this.salon = s;
		}

		this.vendedor = new DescriptivoGeneric(ent.getVendedor());
		this.tipoEvento = new DescriptivoGeneric(ent.getTipoEvento());
		this.cliente = new ClienteVO(ent.getCliente());
		this.horario = ent.getTipoHorario() != null
				? new DescriptivoGeneric(ent.getTipoHorario().getCodigo(), ent.getTipoHorario().getDescripcion())
				: null;
		this.fechaContratacion = ent.getFechaContratacion();
		this.fechaEvento = ent.getFechaEvento();
		this.baseInvitados = ent.getBaseInvitados();
		this.adultos = ent.getAdultos();
		this.adolescentes = ent.getAdolescentes();
		this.menores = ent.getMenores();
		this.bebes = ent.getBebes();
		this.numeroPresupuesto = ent.getNumeroPresupuesto();
		this.servicios = ent.getServicios().stream().sorted(Comparator.comparing(ServicioContratado::getPeso))
				.map(s -> new ServicioContratadoVO(s)).collect(Collectors.toList());
		this.comisiones = ent.getComisiones().stream().map(s -> new ConfiguracionComisionVO(s))
				.collect(Collectors.toList());
		this.descripcion = ent.getDescripcion();
		if (ent.getEstado() != null) {
			this.estado = new DescriptivoGeneric(ent.getEstado().getCodigo(), ent.getEstado().getDescripcion());
		}
		this.esCongelado = ent.getEsCongelado();

		this.ipMaximo = ent.getIpMaximo();
		this.ipMonto = ent.getIpMonto();
		this.cobroGarantia = ent.getCobroGarantia();
		this.totalCalculado = ent.getTotalCalculado();
		this.agasajados = ent.getAgasajados();
		this.observaciones = ent.getObservaciones();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SalonVO getSalon() {
		return salon;
	}

	public void setSalon(SalonVO salon) {
		this.salon = salon;
	}

	public DescriptivoGeneric getVendedor() {
		return vendedor;
	}

	public void setVendedor(DescriptivoGeneric vendedor) {
		this.vendedor = vendedor;
	}

	public DescriptivoGeneric getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(DescriptivoGeneric tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public ClienteVO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteVO cliente) {
		this.cliente = cliente;
	}

	public Date getFechaContratacion() {
		return fechaContratacion;
	}

	public void setFechaContratacion(Date fechaContratacion) {
		this.fechaContratacion = fechaContratacion;
	}

	public Integer getBaseInvitados() {
		return baseInvitados;
	}

	public void setBaseInvitados(Integer baseInvitados) {
		this.baseInvitados = baseInvitados;
	}

	public Integer getAdultos() {
		return adultos;
	}

	public void setAdultos(Integer adultos) {
		this.adultos = adultos;
	}

	public Integer getAdolescentes() {
		return adolescentes;
	}

	public void setAdolescentes(Integer adolescentes) {
		this.adolescentes = adolescentes;
	}

	public Integer getMenores() {
		return menores;
	}

	public void setMenores(Integer menores) {
		this.menores = menores;
	}

	public Integer getBebes() {
		return bebes;
	}

	public void setBebes(Integer bebes) {
		this.bebes = bebes;
	}

	public String getNumeroPresupuesto() {
		return numeroPresupuesto;
	}

	public void setNumeroPresupuesto(String numeroPresupuesto) {
		this.numeroPresupuesto = numeroPresupuesto;
	}

	public List<ServicioContratadoVO> getServicios() {
		return servicios;
	}

	public void setServicios(List<ServicioContratadoVO> servicios) {
		this.servicios = servicios;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public DescriptivoGeneric getHorario() {
		return horario;
	}

	public void setHorario(DescriptivoGeneric horario) {
		this.horario = horario;
	}

	public Date getFechaEvento() {
		return fechaEvento;
	}

	public void setFechaEvento(Date fechaEvento) {
		this.fechaEvento = fechaEvento;
	}

	public ServicioContratadoVO getServicio(Long id) {
		return this.servicios.stream().filter(s -> s.getId() != null && s.getId().equals(id)).findFirst().orElse(null);
	}

	public ConfiguracionComisionVO getComision(Long id) {
		return this.comisiones.stream().filter(s -> s.getId() != null && s.getId().equals(id)).findFirst().orElse(null);
	}

	public DescriptivoGeneric getEstado() {
		return estado;
	}

	public void setEstado(DescriptivoGeneric estado) {
		this.estado = estado;
	}

	public Boolean getEsCongelado() {
		return esCongelado;
	}

	public void setEsCongelado(Boolean esCongelado) {
		this.esCongelado = esCongelado;
	}

	public Double getIpMaximo() {
		return ipMaximo;
	}

	public void setIpMaximo(Double ipMaximo) {
		this.ipMaximo = ipMaximo;
	}

	public Double getCobroGarantia() {
		return cobroGarantia;
	}

	public void setCobroGarantia(Double cobroGarantia) {
		this.cobroGarantia = cobroGarantia;
	}

	public List<ConfiguracionComisionVO> getComisiones() {
		return comisiones;
	}

	public void setComisiones(List<ConfiguracionComisionVO> comisiones) {
		this.comisiones = comisiones;
	}

	public Double getTotalCalculado() {
		return totalCalculado;
	}

	public void setTotalCalculado(Double totalCalculado) {
		this.totalCalculado = totalCalculado;
	}

	public Double getIpMonto() {
		return ipMonto;
	}

	public void setIpMonto(Double ipMonto) {
		this.ipMonto = ipMonto;
	}

	public String getAgasajados() {
		return agasajados;
	}

	public void setAgasajados(String agasajados) {
		this.agasajados = agasajados;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Date getUltimaModificacion() {
		return ultimaModificacion;
	}

	public void setUltimaModificacion(Date ultimaModificacion) {
		this.ultimaModificacion = ultimaModificacion;
	}

}

package ar.com.thinkup.luviam.vo;
import ar.com.thinkup.luviam.model.TipoIVA;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;

public class TipoIVAVO extends ParametricoVO{

private static final long serialVersionUID =4523379343631453141L;
	public TipoIVAVO(){
		super();
	}

	public TipoIVAVO(TipoIVA ent){
		super(ent);

	}
	
}
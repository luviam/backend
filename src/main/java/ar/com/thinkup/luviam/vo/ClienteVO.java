package ar.com.thinkup.luviam.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import ar.com.thinkup.luviam.model.Cliente;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public class ClienteVO implements Serializable, Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1590917297696912512L;

	private Long id;

	private String nombreCliente;

	private String nombreContacto;

	private String domicilio;

	private String localidad;

	private String telefono;

	private String celular;

	private String email;

	private String agasajados;

	private String razonSocial;

	private String cuit;

	private DescriptivoGeneric iva;

	private String domicilioFacturacion;

	private String localidadFacturacion;

	private DescriptivoGeneric tipoCliente;

	private ConvenioSalonVO convenio;

	private String nombreFirmante;

	private String dniFirmante;

	private Float porcComision;

	private Float valorComision;

	private Float porcDescuentos;

	private Float valorAdicional;

	private Boolean activo;

	private String facebook;
	private String instagram;

	private List<ContactoVO> contactos = new Vector<>();

	public ClienteVO() {
		super();
	}

	public ClienteVO(Cliente ent) {
		super();
		this.id = ent.getId();
		this.nombreCliente = ent.getNombreCliente();
		this.nombreContacto = ent.getNombreContacto();
		this.domicilio = ent.getDomicilio();
		this.localidad = ent.getLocalidad();
		this.telefono = ent.getTelefono();
		this.celular = ent.getCelular();
		this.email = ent.getEmail();
		this.razonSocial = ent.getRazonSocial();
		this.cuit = ent.getCuit();
		if (ent.getIva() != null)
			this.iva = new DescriptivoGeneric(ent.getIva());
		this.domicilioFacturacion = ent.getDomicilioFacturacion();
		this.localidadFacturacion = ent.getLocalidadFacturacion();
		if (ent.getTipoCliente() != null)
			this.tipoCliente = new DescriptivoGeneric(ent.getTipoCliente());
		if (ent.getConvenio() != null)
			this.convenio = new ConvenioSalonVO(ent.getConvenio());
		this.nombreFirmante = ent.getNombreFirmante();
		this.dniFirmante = ent.getDniFirmante();
		this.porcComision = ent.getPorcComision();
		this.valorComision = ent.getValorComision();
		this.porcDescuentos = ent.getPorcDescuentos();
		this.valorAdicional = ent.getValorAdicional();
		this.activo = ent.getActivo();
		this.facebook = ent.getFacebook();
		this.instagram = ent.getInstagram();
		if (ent.getContactos() != null)
			this.contactos = ent.getContactos().stream().map(c -> new ContactoVO(c)).collect(Collectors.toList());

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getNombreContacto() {
		return nombreContacto;
	}

	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAgasajados() {
		return agasajados;
	}

	public void setAgasajados(String agasajados) {
		this.agasajados = agasajados;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public DescriptivoGeneric getIva() {
		return iva;
	}

	public void setIva(DescriptivoGeneric iva) {
		this.iva = iva;
	}

	public String getDomicilioFacturacion() {
		return domicilioFacturacion;
	}

	public void setDomicilioFacturacion(String domicilioFacturacion) {
		this.domicilioFacturacion = domicilioFacturacion;
	}

	public String getLocalidadFacturacion() {
		return localidadFacturacion;
	}

	public void setLocalidadFacturacion(String localidadFacturacion) {
		this.localidadFacturacion = localidadFacturacion;
	}

	public DescriptivoGeneric getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(DescriptivoGeneric tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public String getNombreFirmante() {
		return nombreFirmante;
	}

	public void setNombreFirmante(String nombreFirmante) {
		this.nombreFirmante = nombreFirmante;
	}

	public String getDniFirmante() {
		return dniFirmante;
	}

	public void setDniFirmante(String dniFirmante) {
		this.dniFirmante = dniFirmante;
	}

	public Float getPorcComision() {
		return porcComision;
	}

	public void setPorcComision(Float porcComision) {
		this.porcComision = porcComision;
	}

	public Float getValorComision() {
		return valorComision;
	}

	public void setValorComision(Float valorComision) {
		this.valorComision = valorComision;
	}

	public Float getPorcDescuentos() {
		return porcDescuentos;
	}

	public void setPorcDescuentos(Float porcDescuentos) {
		this.porcDescuentos = porcDescuentos;
	}

	public Float getValorAdicional() {
		return valorAdicional;
	}

	public void setValorAdicional(Float valorAdicional) {
		this.valorAdicional = valorAdicional;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public ConvenioSalonVO getConvenio() {
		return convenio;
	}

	public void setConvenio(ConvenioSalonVO convenio) {
		this.convenio = convenio;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getInstagram() {
		return instagram;
	}

	public void setInstagram(String instagram) {
		this.instagram = instagram;
	}

	public List<ContactoVO> getContactos() {
		return contactos;
	}

	public void setContactos(List<ContactoVO> contactos) {
		this.contactos = contactos;
	}

	public ContactoVO getContacto(Long id) {
		return this.contactos.stream().filter(i -> i.getId() != null && i.getId().equals(id)).findFirst().orElse(null);
	}
}

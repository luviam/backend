package ar.com.thinkup.luviam.vo;

import ar.com.thinkup.luviam.model.ImputacionCuenta;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public abstract class ImputacionCuentaVO {

	private Long id;
	private DescriptivoGeneric centroCosto;
	private DescriptivoGeneric cuentaAplicacion;

	public ImputacionCuentaVO() {
		super();

	}

	public ImputacionCuentaVO(ImputacionCuenta icc) {
		super();
		this.id = icc.getId();
		this.centroCosto = new DescriptivoGeneric(icc.getCentroCosto());
		this.cuentaAplicacion = icc.getCuentaAplicacion() == null ? null
				: new DescriptivoGeneric(icc.getCuentaAplicacion());

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DescriptivoGeneric getCentroCosto() {
		return centroCosto;
	}

	public DescriptivoGeneric getCuentaAplicacion() {
		return cuentaAplicacion;
	}

	public void setCuentaAplicacion(DescriptivoGeneric cuentaAplicacion) {
		this.cuentaAplicacion = cuentaAplicacion;
	}

	public void setCentroCosto(DescriptivoGeneric centroCosto) {
		this.centroCosto = centroCosto;
	}

}
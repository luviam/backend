package ar.com.thinkup.luviam.vo;

import ar.com.thinkup.luviam.model.documentos.comisiones.PagoLiquidacion;
import ar.com.thinkup.luviam.vo.documentos.ValoresVO;

public class PagoLiquidacionVO extends ValoresVO<PagoLiquidacion> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2273928745839645332L;

	public PagoLiquidacionVO() {
		super();
	}

	public PagoLiquidacionVO(PagoLiquidacion p) {
		super(p);
	}

}

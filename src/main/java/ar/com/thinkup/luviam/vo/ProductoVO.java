package ar.com.thinkup.luviam.vo;

import java.util.List;
import java.util.stream.Collectors;

import ar.com.thinkup.luviam.model.Producto;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.def.IDescriptivo;

public class ProductoVO implements IDescriptivo, Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8421691463871107092L;

	private Long id;

	private String codigo;

	private String nombre;
	
	private MarcaVO marca;

	private DescriptivoGeneric unidad;

	private DescriptivoGeneric categoria;

	private Boolean activo;

	private Boolean esBorrable;

	private Boolean divisible;

	private Integer adolescenteProp;

	private Integer menorProp;

	private Integer bebeProp;
	private List<ImputacionProductoVO> cuentas;
	private Boolean imputaIVA;
	
	private TipoMenuVO tipoMenu;

	public ProductoVO() {
		super();
	}

	public ProductoVO(Producto ent) {
		super();
		this.id = ent.getId();
		this.codigo = ent.getCodigo();
		this.nombre = ent.getNombre();
		this.unidad = new DescriptivoGeneric(ent.getUnidad());
		this.categoria = new CategoriaProductoVO(ent.getCategoria());
		this.activo = ent.getActivo();
		this.esBorrable = ent.getEsBorrable();
		this.divisible = ent.getDivisible();
		this.adolescenteProp = ent.getAdolescenteProp();
		this.menorProp = ent.getMenorProp();
		this.bebeProp = ent.getBebeProp();
		this.cuentas = ent.getCuentas().stream().map(o -> new ImputacionProductoVO(o)).collect(Collectors.toList());
		this.imputaIVA = ent.getImputaIVA();
		if(ent.getTipoMenu()!=null) {
			this.tipoMenu = new TipoMenuVO(ent.getTipoMenu());	
		}
		if(ent.getMarca()!= null) {
			this.marca = new MarcaVO(ent.getMarca());
		}
		

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public DescriptivoGeneric getUnidad() {
		return unidad;
	}

	public void setUnidad(DescriptivoGeneric unidad) {
		this.unidad = unidad;
	}

	public DescriptivoGeneric getCategoria() {
		return categoria;
	}

	public void setCategoria(DescriptivoGeneric categoria) {
		this.categoria = categoria;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Boolean getEsBorrable() {
		return esBorrable;
	}

	public void setEsBorrable(Boolean esBorrable) {
		this.esBorrable = esBorrable;
	}

	public Boolean getDivisible() {
		return divisible;
	}

	public void setDivisible(Boolean divisible) {
		this.divisible = divisible;
	}

	public Integer getAdolescenteProp() {
		return adolescenteProp;
	}

	public void setAdolescenteProp(Integer adolescenteProp) {
		this.adolescenteProp = adolescenteProp;
	}

	public Integer getMenorProp() {
		return menorProp;
	}

	public void setMenorProp(Integer menorProp) {
		this.menorProp = menorProp;
	}

	public Integer getBebeProp() {
		return bebeProp;
	}

	public void setBebeProp(Integer bebeProp) {
		this.bebeProp = bebeProp;
	}

	@Override
	public String getDescripcion() {

		return this.getNombre();
	}

	public List<ImputacionProductoVO> getCuentas() {
		return cuentas;
	}

	public void setCuentas(List<ImputacionProductoVO> cuentas) {
		this.cuentas = cuentas;
	}

	public ImputacionProductoVO getCuenta(Long id) {
		return this.cuentas.stream().filter(c -> c.getId() != null && c.getId().equals(id)).findFirst().orElse(null);
	}

	public ImputacionProductoVO getCuenta(String centroCodigo) {
		return this.cuentas.stream()
				.filter(c -> c.getCentroCosto() != null && c.getCentroCosto().getCodigo().equals(centroCodigo))
				.findFirst().orElse(null);
	}

	@Override
	public String toString() {
		return codigo + ", " + nombre;
	}

	public Boolean getImputaIVA() {
		return imputaIVA;
	}

	public void setImputaIVA(Boolean imputaIVA) {
		this.imputaIVA = imputaIVA;
	}

	public TipoMenuVO getTipoMenu() {
		return tipoMenu;
	}

	public void setTipoMenu(TipoMenuVO tipoMenu) {
		this.tipoMenu = tipoMenu;
	}

	public MarcaVO getMarca() {
		return marca;
	}

	public void setMarca(MarcaVO marca) {
		this.marca = marca;
	}
	

	
}
package ar.com.thinkup.luviam.vo;

import java.io.Serializable;

import ar.com.thinkup.luviam.model.Contacto;

public class ContactoVO implements Serializable, Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3338579526537041965L;

	private Long id;

	private String relacionContacto;

	private String nombre;

	private String numeroDocumento;

	private String email;

	private String telefono;

	private String celular;

	private String facebook;

	private String instagram;

	public ContactoVO() {
		super();
	}

	public ContactoVO(Contacto ent) {
		super();
		this.id = ent.getId();
		this.relacionContacto = ent.getRelacionContacto();
		this.nombre = ent.getNombre();
		this.numeroDocumento = ent.getNumeroDocumento();
		this.email = ent.getEmail();
		this.telefono = ent.getTelefono();
		this.celular = ent.getCelular();
		this.facebook = ent.getFacebook();
		this.instagram = ent.getInstagram();

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getInstagram() {
		return instagram;
	}

	public void setInstagram(String instagram) {
		this.instagram = instagram;
	}

	public String getRelacionContacto() {
		return relacionContacto;
	}

	public void setRelacionContacto(String relacionContacto) {
		this.relacionContacto = relacionContacto;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}
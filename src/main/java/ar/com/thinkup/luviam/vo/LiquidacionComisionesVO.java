package ar.com.thinkup.luviam.vo;

import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import ar.com.thinkup.luviam.model.documentos.comisiones.LiquidacionComisiones;
import ar.com.thinkup.luviam.model.documentos.comisiones.PagoLiquidacion;
import ar.com.thinkup.luviam.vo.documentos.DocumentoPagoVO;

public class LiquidacionComisionesVO
		extends DocumentoPagoVO<PagoLiquidacion, PagoLiquidacionVO, ItemLiquidacionComisionesVO> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5628228493909444703L;
	private List<PagoLiquidacionVO> pagosLiquidacion = new Vector<>();
	private List<ItemLiquidacionComisionesVO> items = new Vector<>();
	private ComisionistaVO comisionista;
	private String observaciones;
	private Double premio;
	private Date fechaPago;

	public LiquidacionComisionesVO() {
		super();
	}

	public LiquidacionComisionesVO(LiquidacionComisiones ent) {
		super(ent);
		this.comisionista = new ComisionistaVO(ent.getComisionista());
		this.pagosLiquidacion = ent.getPagosLiquidacion().stream().map(p -> new PagoLiquidacionVO(p))
				.collect(Collectors.toList());
		this.items = ent.getItems().stream().map(p -> new ItemLiquidacionComisionesVO(p)).collect(Collectors.toList());
		this.observaciones = ent.getObservaciones();
		this.premio = ent.getPremio();
		this.fechaPago = ent.getFechaPago();
	}

	@Override
	public List<PagoLiquidacionVO> getValores() {

		return this.getPagosLiquidacion();
	}

	@Override
	public List<ItemLiquidacionComisionesVO> getItems() {
		return this.items;
	}

	public List<PagoLiquidacionVO> getPagosLiquidacion() {
		return pagosLiquidacion;
	}

	public void setPagosLiquidacion(List<PagoLiquidacionVO> pagosLiquidacion) {
		this.pagosLiquidacion = pagosLiquidacion;
	}

	public ComisionistaVO getComisionista() {
		return comisionista;
	}

	public void setComisionista(ComisionistaVO comisionista) {
		this.comisionista = comisionista;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public void setItems(List<ItemLiquidacionComisionesVO> items) {
		this.items = items;
	}

	public ItemLiquidacionComisionesVO getItems(Long id) {
		return this.items.stream().filter(i -> i.getId() != null && i.getId().equals(id)).findFirst().orElse(null);
	}

	public Double getPremio() {
		return premio;
	}

	public void setPremio(Double premio) {
		this.premio = premio;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	
	
}

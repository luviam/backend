package ar.com.thinkup.luviam.vo;

import java.io.Serializable;

public interface Identificable extends Serializable {

	public Long getId();

}

package ar.com.thinkup.luviam.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Vector;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.documentos.factura.Factura;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;

public class FacturaVO implements Serializable, Identificable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1754650027664808673L;
	public Long id;
	public String numero;
	public String descripcion;
	public Date fecha = new Date();
	public Date fechaIVA = new Date();
	public DescriptivoGeneric tipoComprobante;
	public DescriptivoGeneric proveedor;
	public DescriptivoGeneric centroCosto;
	public List<ItemFacturaVO> items = new Vector<>();
	public Double iibbCABA = 0d;
	public Double iibbBSAS = 0d;
	public Double iibbOtros = 0d;
	public Double impuestoSellos;
	public Double saldo = 0d;
	public Double importe = 0d;
	private DescriptivoGeneric estado;

	public FacturaVO() {
		super();
	}

	public FacturaVO(Factura entity) {
		this.id = entity.getId();
		this.numero = entity.getNumero();
		this.descripcion = entity.getDescripcion();
		this.fecha = entity.getFecha();
		this.fechaIVA = entity.getFechaIVA();
		this.tipoComprobante = new DescriptivoGeneric(entity.getTipoComprobante());
		this.proveedor = new DescriptivoGeneric(entity.getProveedor());
		this.centroCosto = new DescriptivoGeneric(entity.getCentroCosto());
		this.iibbBSAS = entity.getIibbBSAS();
		this.iibbCABA = entity.getIibbCABA();
		this.iibbOtros = entity.getIibbOtros();
		this.impuestoSellos = entity.getImpuestoSellos();
		this.saldo = entity.getSaldo();
		this.importe = entity.getImporte();
		EstadoOperacionEnum e = EstadoOperacionEnum.getByCodigo(entity.getEstado());
		this.estado = new DescriptivoGeneric(e.getCodigo(), e.getDescripcion());

		entity.getItems().stream().forEach(i -> this.addItem(new ItemFacturaVO(i)));

	}

	public void addItem(ItemFacturaVO item) {
		if (this.items == null)
			this.items = new Vector<>();
		this.items.add(item);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getFechaIVA() {
		return fechaIVA;
	}

	public void setFechaIVA(Date fechaIVA) {
		this.fechaIVA = fechaIVA;
	}

	public DescriptivoGeneric getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(DescriptivoGeneric tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public DescriptivoGeneric getProveedor() {
		return proveedor;
	}

	public void setProveedor(DescriptivoGeneric proveedor) {
		this.proveedor = proveedor;
	}

	public DescriptivoGeneric getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(DescriptivoGeneric centroCosto) {
		this.centroCosto = centroCosto;
	}

	public List<ItemFacturaVO> getItems() {
		return items;
	}

	public void setItems(List<ItemFacturaVO> items) {
		this.items = items;
	}

	public Double getIibbCABA() {
		return iibbCABA;
	}

	public void setIibbCABA(Double iibbCABA) {
		this.iibbCABA = iibbCABA;
	}

	public Double getIibbBSAS() {
		return iibbBSAS;
	}

	public void setIibbBSAS(Double iibbBSAS) {
		this.iibbBSAS = iibbBSAS;
	}

	public Double getIibbOtros() {
		return iibbOtros;
	}

	public void setIibbOtros(Double iibbOtros) {
		this.iibbOtros = iibbOtros;
	}

	public Double getImpuestoSellos() {
		return impuestoSellos;
	}

	public void setImpuestoSellos(Double impuestoSellos) {
		this.impuestoSellos = impuestoSellos;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public DescriptivoGeneric getEstado() {
		return estado;
	}

	public void setEstado(DescriptivoGeneric estado) {
		this.estado = estado;
	}

	public ItemFacturaVO getItem(Long id) {
		Optional<ItemFacturaVO> op = this.items.parallelStream().filter(i -> i.getId() != null && i.getId().equals(id))
				.findFirst();
		if (op.isPresent()) {
			return op.get();
		} else {
			// TIRO EXCEPCIóN?
			return null;
		}
	}

}

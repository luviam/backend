package ar.com.thinkup.luviam.vo;

import java.io.Serializable;

import ar.com.thinkup.luviam.model.CabeceraComisionista;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public class CabeceraComisionistaVO implements Serializable, Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2781987857505341918L;

	private Long id;

	private String nombre;
	private String contacto;

	private String telefono;

	private String celular;

	private String email;

	private  DescriptivoGeneric  tipoComisionista;

	private Boolean activo;

	private  Double  saldo;

	public CabeceraComisionistaVO(){
		super();
	}

	public CabeceraComisionistaVO(CabeceraComisionista ent){
		super();
		this.id =  ent.getId();
		this.nombre =  ent.getNombre();
		this.contacto = ent.getContacto();
		this.telefono =  ent.getTelefono();
		this.celular =  ent.getCelular();
		this.email =  ent.getEmail();
		this.tipoComisionista =  new DescriptivoGeneric(ent.getTipoComisionista());
		this.activo =  ent.getActivo();
		this.saldo =  ent.getSaldo();

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public DescriptivoGeneric getTipoComisionista() {
		return tipoComisionista;
	}

	public void setTipoComisionista(DescriptivoGeneric tipoComisionista) {
		this.tipoComisionista = tipoComisionista;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	
	

}


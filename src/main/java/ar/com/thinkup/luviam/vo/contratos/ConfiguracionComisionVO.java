package ar.com.thinkup.luviam.vo.contratos;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import ar.com.thinkup.luviam.model.contratos.ConfiguracionComision;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.vo.Identificable;
import ar.com.thinkup.luviam.vo.PagoComisionVO;

public class ConfiguracionComisionVO implements Serializable, Identificable {

	private static final long serialVersionUID = 8508319348649461419L;
	private Long id;

	private DescriptivoGeneric comisionista;

	private DescriptivoGeneric producto;

	private Double porcentajeComision;

	private Double totalAComisionar;

	private Double saldoComision;

	private DescriptivoGeneric tipoComision;

	private Boolean cobraAlInicio;
	private Double porcentajeAdelanto = 100d;

	private Boolean cobraAlLiquidar;

	private Boolean cobraPorCobro;

	private DescriptivoGeneric centro;

	private List<PagoComisionVO> pagos = new Vector<>();

	public ConfiguracionComisionVO() {
		super();
	}

	public ConfiguracionComisionVO(ConfiguracionComision ent) {
		this.id = ent.getId();
		this.comisionista = new DescriptivoGeneric(ent.getComisionista());
		this.producto = new DescriptivoGeneric(ent.getServicioComisionado().getProducto());
		this.porcentajeComision = ent.getPorcentajeComision();
		this.totalAComisionar = ent.getTotalAComisionar();
		this.saldoComision = ent.getSaldoComision();
		this.tipoComision = new DescriptivoGeneric(ent.getTipoComision().getCodigo(),
				ent.getTipoComision().getDescripcion());
		this.cobraAlInicio = ent.getCobraAlInicio();
		this.cobraAlLiquidar = ent.getCobraAlLiquidar();
		this.cobraPorCobro = ent.getCobraPorCobro();
		if (ent.getCentroCosto() != null) {
			this.centro = new DescriptivoGeneric(ent.getCentroCosto());
		}
		this.pagos = ent.getPagosComision().stream().map(p -> new PagoComisionVO(p)).collect(Collectors.toList());
		this.porcentajeAdelanto = ent.getPorcentajeAdelanto();

	}

	public DescriptivoGeneric getComisionista() {
		return comisionista;
	}

	public void setComisionista(DescriptivoGeneric comisionista) {
		this.comisionista = comisionista;
	}

	public DescriptivoGeneric getProducto() {
		return producto;
	}

	public void setProducto(DescriptivoGeneric producto) {
		this.producto = producto;
	}

	public Double getPorcentajeComision() {
		return porcentajeComision;
	}

	public void setPorcentajeComision(Double porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}

	public Double getTotalAComisionar() {
		return totalAComisionar;
	}

	public void setTotalAComisionar(Double totalAComisionar) {
		this.totalAComisionar = totalAComisionar;
	}

	public Double getSaldoComision() {
		return saldoComision;
	}

	public void setSaldoComision(Double saldoComision) {
		this.saldoComision = saldoComision;
	}

	public DescriptivoGeneric getTipoComision() {
		return tipoComision;
	}

	public void setTipoComision(DescriptivoGeneric tipoComision) {
		this.tipoComision = tipoComision;
	}

	public Boolean getCobraAlInicio() {
		return cobraAlInicio;
	}

	public void setCobraAlInicio(Boolean cobraAlInicio) {
		this.cobraAlInicio = cobraAlInicio;
	}

	public Boolean getCobraAlLiquidar() {
		return cobraAlLiquidar;
	}

	public void setCobraAlLiquidar(Boolean cobraAlLiquidar) {
		this.cobraAlLiquidar = cobraAlLiquidar;
	}

	public Boolean getCobraPorCobro() {
		return cobraPorCobro;
	}

	public void setCobraPorCobro(Boolean cobraPorCobro) {
		this.cobraPorCobro = cobraPorCobro;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	public DescriptivoGeneric getCentro() {
		return centro;
	}

	public void setCentro(DescriptivoGeneric centro) {
		this.centro = centro;
	}

	public List<PagoComisionVO> getPagos() {
		return pagos;
	}

	public void setPagos(List<PagoComisionVO> pagos) {
		this.pagos = pagos;
	}

	public PagoComisionVO getPagoComision(Long id) {
		return this.pagos.stream().filter(i -> i.getId() != null && i.getId().equals(id)).findFirst().orElse(null);
	}

	public Double getPorcentajeAdelanto() {
		return porcentajeAdelanto;
	}

	public void setPorcentajeAdelanto(Double porcentajeAdelanto) {
		this.porcentajeAdelanto = porcentajeAdelanto;
	}

}
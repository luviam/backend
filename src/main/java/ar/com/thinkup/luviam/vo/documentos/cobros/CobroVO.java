package ar.com.thinkup.luviam.vo.documentos.cobros;

import ar.com.thinkup.luviam.model.documentos.cobros.Cobro;
import ar.com.thinkup.luviam.vo.Identificable;
import ar.com.thinkup.luviam.vo.documentos.ValoresVO;

public class CobroVO extends ValoresVO<Cobro> implements Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3472509174350180885L;

	public CobroVO() {
		super();

	}

	public CobroVO(Cobro c) {
		super(c);
	}

}

package ar.com.thinkup.luviam.vo;

import java.io.Serializable;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;

public class CuentaAplicacionVO implements Serializable, Identificable {

	private static final long serialVersionUID = -7029312113315542553L;
	
	public Long id;
	private String codigo;
	private String nombre;
	public DescriptivoGeneric centroCosto;
	
	public CuentaAplicacionVO() {}
	
	public CuentaAplicacionVO(CuentaAplicacion cuenta) {
		super();
		this.id = cuenta.getId();
		this.codigo = cuenta.getCodigo();
		this.nombre = cuenta.getNombre();
		this.centroCosto = new DescriptivoGeneric(cuenta.getCentroCosto());
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}
	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the centroCosto
	 */
	public DescriptivoGeneric getCentroCosto() {
		return centroCosto;
	}
	/**
	 * @param centroCosto the centroCosto to set
	 */
	public void setCentroCosto(DescriptivoGeneric centroCosto) {
		this.centroCosto = centroCosto;
	}
	
	
	

}

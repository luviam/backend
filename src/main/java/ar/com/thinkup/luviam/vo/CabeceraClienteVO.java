package ar.com.thinkup.luviam.vo;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.repository.ClienteRepository.ClienteCabecera;

public class CabeceraClienteVO extends DescriptivoGeneric {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1590917297696912512L;

	private Long id;

	private String nombreCliente;
	private String telefono;
	private String celular;
	private String email;
	private String razonSocial;
	private DescriptivoGeneric tipoCliente;
	private String facebook;
	private String instagram;

	public CabeceraClienteVO() {
		super();
	}

	public CabeceraClienteVO(ClienteCabecera l) {
		super();
		this.id = l.getId();
		this.nombreCliente = l.getNombreCliente();
		this.telefono = l.getTelefono();
		this.celular = l.getCelular();
		this.email = l.getEmail();
		this.razonSocial = l.getRazonSocial();
		if (l.getTipoCliente() != null)
			this.tipoCliente = new DescriptivoGeneric(l.getTipoCliente());
		this.facebook = l.getFacebook();
		this.instagram = l.getInstagram();

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public DescriptivoGeneric getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(DescriptivoGeneric tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getInstagram() {
		return instagram;
	}

	public void setInstagram(String instagram) {
		this.instagram = instagram;
	}

}
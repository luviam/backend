package ar.com.thinkup.luviam.vo.contratos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import ar.com.thinkup.luviam.model.contratos.ServicioContratado;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.vo.Identificable;
import ar.com.thinkup.luviam.vo.ProductoVO;

public class ServicioContratadoVO implements Serializable, Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7735421774558179060L;

	private Long id;

	private Date fechaContrato;

	private ProductoVO producto;

	private List<CuotaServicioVO> planPago;

	private Double costoUnitario;

	private Integer cantidad;

	private String grupo;

	private DescriptivoGeneric centroCosto;

	public ServicioContratadoVO() {
		super();
	}

	public ServicioContratadoVO(ServicioContratado ent) {
		super();
		this.id = ent.getId();

		this.fechaContrato = ent.getFechaContrato();
		this.producto = new ProductoVO(ent.getProducto());
		this.planPago = ent.getPlanPago().stream().map(p -> new CuotaServicioVO(p)).collect(Collectors.toList());
		this.costoUnitario = ent.getCostoUnitario();
		this.cantidad = ent.getCantidad();
		this.grupo = ent.getGrupo();
		this.centroCosto = new DescriptivoGeneric(ent.getCentroCosto());

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaContrato() {
		return fechaContrato;
	}

	public void setFechaContrato(Date fechaContrato) {
		this.fechaContrato = fechaContrato;
	}

	public ProductoVO getProducto() {
		return producto;
	}

	public void setProducto(ProductoVO producto) {
		this.producto = producto;
	}

	public List<CuotaServicioVO> getPlanPago() {
		return planPago;
	}

	public void setPlanPago(List<CuotaServicioVO> planPago) {
		this.planPago = planPago;
	}

	public Double getCostoUnitario() {
		return costoUnitario;
	}

	public void setCostoUnitario(Double costoUnitario) {
		this.costoUnitario = costoUnitario;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public CuotaServicioVO getCuota(Long id) {
		return this.planPago.stream().filter(p -> p.getId() != null && p.getId().equals(id)).findFirst().orElse(null);
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public DescriptivoGeneric getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(DescriptivoGeneric centroCosto) {
		this.centroCosto = centroCosto;
	}

	@Override
	public String toString() {
		return  producto.toString();
	}
	
	

}

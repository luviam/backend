package ar.com.thinkup.luviam.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class TotalesGastosVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7997880550037467653L;
	private Long idRubro;
	private String rubro;
	private Long idGrupo;
	private String grupo;
	private Long idSubgrupo;
	private String subGrupo;
	private Long idCuenta;
	private String cuenta;
	private BigDecimal debe;
	private BigDecimal haber;

	public TotalesGastosVO() {
		super();

	}

	public TotalesGastosVO(Long idRubro, String rubro, Long idGrupo, String grupo, Long idSubgrupo, String subGrupo,
			Long idCuenta, String cuenta, BigDecimal debe, BigDecimal haber) {
		super();
		this.idRubro = idRubro;
		this.rubro = rubro;
		this.idGrupo = idGrupo;
		this.grupo = grupo;
		this.idSubgrupo = idSubgrupo;
		this.subGrupo = subGrupo;
		this.idCuenta = idCuenta;
		this.cuenta = cuenta;
		this.debe = debe;
		this.haber = haber;
	}

	public Long getIdRubro() {
		return idRubro;
	}

	public void setIdRubro(Long idRubro) {
		this.idRubro = idRubro;
	}

	public Long getIdGrupo() {
		return idGrupo;
	}

	public void setIdGrupo(Long idGrupo) {
		this.idGrupo = idGrupo;
	}

	public Long getIdSubgrupo() {
		return idSubgrupo;
	}

	public void setIdSubgrupo(Long idSubgrupo) {
		this.idSubgrupo = idSubgrupo;
	}

	public Long getIdCuenta() {
		return idCuenta;
	}

	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}

	public String getRubro() {
		return rubro;
	}

	public void setRubro(String rubro) {
		this.rubro = rubro;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getSubGrupo() {
		return subGrupo;
	}

	public void setSubGrupo(String subGrupo) {
		this.subGrupo = subGrupo;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public void setDebe(BigDecimal debe) {
		this.debe = debe;
	}

	public void setHaber(BigDecimal haber) {
		this.haber = haber;
	}

	public Double getHaber() {
		return this.haber != null ? this.haber.doubleValue() : 0d;
	}

	public void setHaber(Double val) {
		this.haber = val != null ? new BigDecimal(val) : new BigDecimal(0);
	}

	public Double getDebe() {
		return this.debe != null ? this.debe.doubleValue() : 0d;
	}

	public void setDebe(Double val) {
		this.debe = val != null ? new BigDecimal(val) : new BigDecimal(0);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCuenta == null) ? 0 : idCuenta.hashCode());
		result = prime * result + ((idGrupo == null) ? 0 : idGrupo.hashCode());
		result = prime * result + ((idRubro == null) ? 0 : idRubro.hashCode());
		result = prime * result + ((idSubgrupo == null) ? 0 : idSubgrupo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TotalesGastosVO other = (TotalesGastosVO) obj;
		if (idCuenta == null) {
			if (other.idCuenta != null)
				return false;
		} else if (!idCuenta.equals(other.idCuenta))
			return false;
		if (idGrupo == null) {
			if (other.idGrupo != null)
				return false;
		} else if (!idGrupo.equals(other.idGrupo))
			return false;
		if (idRubro == null) {
			if (other.idRubro != null)
				return false;
		} else if (!idRubro.equals(other.idRubro))
			return false;
		if (idSubgrupo == null) {
			if (other.idSubgrupo != null)
				return false;
		} else if (!idSubgrupo.equals(other.idSubgrupo))
			return false;
		return true;
	}

}

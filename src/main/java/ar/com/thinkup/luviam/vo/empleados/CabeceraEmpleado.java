package ar.com.thinkup.luviam.vo.empleados;

import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import ar.com.thinkup.luviam.repository.EmpleadoRepository.IEmpleadoCabecera;

public class CabeceraEmpleado {

	private Long id;
	private String nombre;
	private String telefono;
	private String email;
	private String tipo;
	private String sector;
	private List<AltaEmpleadoVO> altas = new Vector<>();

	public CabeceraEmpleado(IEmpleadoCabecera e) {
		this.id = e.getId();
		this.nombre = e.getNombre();
		this.telefono = e.getTelefono();
		this.email = e.getEmail();
		if (e.getTipoEmpleado() != null)
			this.tipo = e.getTipoEmpleado().getDescripcion();
		if (e.getSector() != null)
			this.sector = e.getSector().getDescripcion();

		this.setAltas(e.getAltas().stream().map(a -> new AltaEmpleadoVO(a)).collect(Collectors.toList()));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public List<AltaEmpleadoVO> getAltas() {
		return altas;
	}

	public void setAltas(List<AltaEmpleadoVO> altas) {
		this.altas = altas;
	}

}

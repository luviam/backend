package ar.com.thinkup.luviam.vo;
import ar.com.thinkup.luviam.model.TipoMenu;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;

public class TipoMenuVO extends ParametricoVO{

	private static final long serialVersionUID =-5081867613565024123L;
	public TipoMenuVO(){
		super();
	}

	public TipoMenuVO(TipoMenu ent){
		super(ent);

	}


}
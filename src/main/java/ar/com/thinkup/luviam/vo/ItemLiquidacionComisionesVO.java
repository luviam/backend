package ar.com.thinkup.luviam.vo;

import java.io.Serializable;

import ar.com.thinkup.luviam.model.documentos.comisiones.ItemLiquidacionComisiones;

public class ItemLiquidacionComisionesVO implements Identificable, Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 3378644709398276781L;
	private Long id;
	private PagoComisionVO pagoComision;
	private Double importe;
	
	public ItemLiquidacionComisionesVO() {
		
	}
	
	public ItemLiquidacionComisionesVO(ItemLiquidacionComisiones i) {
		this.id = i.getId();
		this.pagoComision = new PagoComisionVO(i.getPagoComision());
		this.importe = i.getImporte();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public PagoComisionVO getPagoComision() {
		return pagoComision;
	}
	public void setPagoComision(PagoComisionVO pagoComision) {
		this.pagoComision = pagoComision;
	}
	public Double getImporte() {
		return importe;
	}
	public void setImporte(Double importe) {
		this.importe = importe;
	}
	
	
	
	
}

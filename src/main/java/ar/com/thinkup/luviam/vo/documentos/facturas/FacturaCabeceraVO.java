package ar.com.thinkup.luviam.vo.documentos.facturas;

import java.util.Date;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.repository.FacturaRepository.FacturaCabecera;

public class FacturaCabeceraVO {

	Long id;
	String numero;
	String descripcion;
	Date fecha;
	DescriptivoGeneric tipoComprobante;
	DescriptivoGeneric proveedor;
	DescriptivoGeneric centro;
	Double saldo;
	Double importe;
	String estado;

	public FacturaCabeceraVO() {

	}

	public FacturaCabeceraVO(FacturaCabecera f) {

		this.id = f.getId();
		this.numero = f.getNumero();
		this.descripcion = f.getDescripcion();
		this.fecha = f.getFecha();
		if (f.getTipoComprobante() != null)
			this.tipoComprobante = new DescriptivoGeneric(f.getTipoComprobante());
		if (f.getProveedor() != null)
			this.proveedor = new DescriptivoGeneric(String.valueOf(f.getProveedor().getId()),
					f.getProveedor().getNombreProveedor());
		if (f.getCentroCosto() != null) {
			this.centro = new DescriptivoGeneric(f.getCentroCosto().getCodigo(), f.getCentroCosto().getNombre());
		}
		this.saldo = f.getSaldo();
		this.importe = f.getImporte();
		this.estado = f.getEstado();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public DescriptivoGeneric getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(DescriptivoGeneric tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public DescriptivoGeneric getProveedor() {
		return proveedor;
	}

	public void setProveedor(DescriptivoGeneric proveedor) {
		this.proveedor = proveedor;
	}

	public DescriptivoGeneric getCentro() {
		return centro;
	}

	public void setCentro(DescriptivoGeneric centro) {
		this.centro = centro;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

}

package ar.com.thinkup.luviam.vo;

import ar.com.thinkup.luviam.model.CentroCostosTipoComprobante;

public class CentroCostosComprobantesVO {
	
	private String codigo; 
	private String descripcion;
	private Boolean activo;
	private String numeracion;
	
	
	public CentroCostosComprobantesVO() {
		super();
	}
	
	
	public CentroCostosComprobantesVO(CentroCostosTipoComprobante ctc) {
		super();
		this.codigo = ctc.getId().toString();
		this.descripcion = ctc.getTipoComprobante().getDescripcion();
		this.activo = ctc.getActivo();
		this.numeracion = ctc.getValorActual();
	}


	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	public String getNumeracion() {
		return numeracion;
	}
	public void setNumeracion(String numeracion) {
		this.numeracion = numeracion;
	}
	
	

}

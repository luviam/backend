package ar.com.thinkup.luviam.vo.documentos.pagos;

import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.documentos.pagos.OrdenPago;
import ar.com.thinkup.luviam.model.documentos.pagos.Pago;
import ar.com.thinkup.luviam.vo.documentos.DocumentoPagoVO;

public class OrdenPagoVO extends DocumentoPagoVO<Pago, PagoVO, ItemOrdenPagoVO> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6868888468247077971L;
	private DescriptivoGeneric proveedor;
	private DescriptivoGeneric centroCosto;
	private Double pagoACuenta = 0d;
	private List<PagoVO> pagos = new Vector<>();
	private List<ItemOrdenPagoVO> items = new Vector<>();

	public OrdenPagoVO() {
		super();
		this.items = new Vector<>();
		this.pagos = new Vector<>();
		this.pagoACuenta = 0d;
	}

	public OrdenPagoVO(OrdenPago op) {
		super(op);
		this.items = new Vector<>();
		this.pagos = new Vector<>();
		this.proveedor = new DescriptivoGeneric(op.getProveedor());
		this.centroCosto = new DescriptivoGeneric(op.getCentroCosto());
		this.pagoACuenta = op.getPagoACuenta() != null ? Math.abs(op.getPagoACuenta().getImporte()): 0d;
		this.pagos.addAll(op.getPagos().stream().map(p -> new PagoVO(p)).collect(Collectors.toList()));
		
		this.items.addAll(op.getItems().stream().map(i -> new ItemOrdenPagoVO(i)).collect(Collectors.toList()));

	}

	@Override
	public List<ItemOrdenPagoVO> getItems() {
		return items;
	}

	public void setItems(List<ItemOrdenPagoVO> items) {
		this.items = items;
	}

	public DescriptivoGeneric getProveedor() {
		return proveedor;
	}

	public void setProveedor(DescriptivoGeneric proveedor) {
		this.proveedor = proveedor;
	}

	public DescriptivoGeneric getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(DescriptivoGeneric centroCosto) {
		this.centroCosto = centroCosto;
	}

	public Double getPagoACuenta() {
		return pagoACuenta;
	}

	public void setPagoACuenta(Double pagoACuenta) {
		this.pagoACuenta = pagoACuenta;
	}

	@Override
	public List<PagoVO> getValores() {
		return this.getPagos();
	}

	public List<PagoVO> getPagos() {
		return pagos;
	}

	public void setPagos(List<PagoVO> pagos) {
		this.pagos = pagos;
	}

}

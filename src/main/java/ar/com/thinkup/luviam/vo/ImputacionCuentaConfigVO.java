package ar.com.thinkup.luviam.vo;

import ar.com.thinkup.luviam.model.ImputacionCuentaConfig;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.enums.TipoImputacionEnum;

public class ImputacionCuentaConfigVO {
	private Long id;
	private DescriptivoGeneric centroCosto;
	private DescriptivoGeneric cuentaAplicacion;
	private DescriptivoGeneric tipoImputacion;

	public ImputacionCuentaConfigVO(ImputacionCuentaConfig icc) {
		super();
		this.id = icc.getId();
		this.centroCosto = new DescriptivoGeneric(icc.getCentroCosto());
		this.cuentaAplicacion = icc.getCuentaAplicacion() == null ? null :  new DescriptivoGeneric(icc.getCuentaAplicacion());
		TipoImputacionEnum tipo = icc.getTipoImputacion();
		if (tipo != null) {
			this.tipoImputacion = new DescriptivoGeneric(tipo.getCodigo(), tipo.getDescripcion());
		}

	}

	public ImputacionCuentaConfigVO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DescriptivoGeneric getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(DescriptivoGeneric centroCosto) {
		this.centroCosto = centroCosto;
	}

	public DescriptivoGeneric getCuentaAplicacion() {
		return cuentaAplicacion;
	}

	public void setCuentaAplicacion(DescriptivoGeneric cuentaAplicacion) {
		this.cuentaAplicacion = cuentaAplicacion;
	}

	public DescriptivoGeneric getTipoImputacion() {
		return tipoImputacion;
	}

	public void setTipoImputacion(DescriptivoGeneric codigoTipoImputacion) {
		this.tipoImputacion = codigoTipoImputacion;
	}

}

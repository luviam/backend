package ar.com.thinkup.luviam.vo.documentos.pagos;

import ar.com.thinkup.luviam.repository.ItemOrdenPagoRepository.ItemOrdenPagoCabecera;
import ar.com.thinkup.luviam.vo.Identificable;
import ar.com.thinkup.luviam.vo.documentos.facturas.FacturaCabeceraVO;

public class ItemOrdenPagoVO implements Identificable {

	private Long id;
	private FacturaCabeceraVO cabeceraFactura;
	private Double importe;

	public ItemOrdenPagoVO() {
		super();
	}

	public ItemOrdenPagoVO(ItemOrdenPagoCabecera item) {
		this.id = item.getId();
		this.importe = item.getImporte();
		this.cabeceraFactura = new FacturaCabeceraVO(item.getFactura());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public FacturaCabeceraVO getCabeceraFactura() {
		return cabeceraFactura;
	}

	public void setCabeceraFactura(FacturaCabeceraVO cabeceraFactura) {
		this.cabeceraFactura = cabeceraFactura;
	}

}

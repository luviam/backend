package ar.com.thinkup.luviam.vo;

import ar.com.thinkup.luviam.model.CategoriaProducto;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;

public class CategoriaProductoVO extends ParametricoVO {

	private static final long serialVersionUID = 7888022612845035255L;
	private Boolean esComisionable;
	private Integer peso;

	public CategoriaProductoVO() {
		super();
	}

	public CategoriaProductoVO(CategoriaProducto ent) {
		super(ent);
		this.esComisionable = ent.getEsComisionable();
		this.peso = ent.getPeso();

	}

	public Boolean getEsComisionable() {
		return esComisionable;
	}

	public void setEsComisionable(Boolean esComisionable) {
		this.esComisionable = esComisionable;
	}

	public Integer getPeso() {
		return peso;
	}

	public void setPeso(Integer peso) {
		this.peso = peso;
	}

}
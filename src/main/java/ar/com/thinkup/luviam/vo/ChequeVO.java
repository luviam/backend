package ar.com.thinkup.luviam.vo;

import java.io.Serializable;
import java.util.Date;

import ar.com.thinkup.luviam.model.Cheque;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public class ChequeVO implements Serializable, Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1354736610229558306L;

	private Long id;

	private String banco;

	private String numero;

	private DescriptivoGeneric tipoCheque;

	private CuentaAplicacionVO cuentaBancaria;

	private String numeroCuenta;

	private String titularCuenta;

	private String cuit;

	private Date fechaEmision;

	private Date fechaPago;

	private Double importe = 0d;

	private DescriptivoGeneric estado;

	private String destino;

	private Boolean esDiferido = false;

	private DescriptivoGeneric pago;

	private String origen;

	private Date fechaIngreso;

	private String numeroRecibo;

	public ChequeVO(Cheque chequeDB) {
		super();
		this.id = chequeDB.getId();
		this.banco = chequeDB.getBanco();
		this.numero = chequeDB.getNumero();
		if (chequeDB.getTipoCheque() != null) {
			this.tipoCheque = new DescriptivoGeneric(chequeDB.getTipoCheque());
		}
		if (chequeDB.getCuentaBancaria() != null) {
			this.cuentaBancaria = new CuentaAplicacionVO(chequeDB.getCuentaBancaria());
		}

		this.numeroCuenta = chequeDB.getNumeroCuenta();
		this.titularCuenta = chequeDB.getTitularCuenta();
		this.cuit = chequeDB.getCuit();
		this.fechaEmision = chequeDB.getFechaEmision();
		this.fechaPago = chequeDB.getFechaPago();
		this.importe = chequeDB.getImporte();
		if (chequeDB.getEstadoCheque() != null) {
			this.estado = new DescriptivoGeneric(chequeDB.getEstadoCheque());
		}

		this.destino = chequeDB.getDestino();
		this.esDiferido = chequeDB.getEsDiferido();
		if (chequeDB.getPago() != null) {
			this.pago = new DescriptivoGeneric(String.valueOf(chequeDB.getPago().getOrdenPago().getId()),
					chequeDB.getPago().getOrdenPago().toString());
		}
		this.origen = chequeDB.getOrigen();
		this.fechaIngreso = chequeDB.getFechaIngreso();
		this.numeroRecibo = chequeDB.getNumeroRecibo();
	}

	public ChequeVO() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public DescriptivoGeneric getTipoCheque() {
		return tipoCheque;
	}

	public void setTipoCheque(DescriptivoGeneric tipoCheque) {
		this.tipoCheque = tipoCheque;
	}

	public CuentaAplicacionVO getCuentaBancaria() {
		return cuentaBancaria;
	}

	public void setCuentaBancaria(CuentaAplicacionVO cuentaBancaria) {
		this.cuentaBancaria = cuentaBancaria;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getTitularCuenta() {
		return titularCuenta;
	}

	public void setTitularCuenta(String titularCuenta) {
		this.titularCuenta = titularCuenta;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public DescriptivoGeneric getEstado() {
		return estado;
	}

	public void setEstado(DescriptivoGeneric estado) {
		this.estado = estado;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public Boolean getEsDiferido() {
		return esDiferido;
	}

	public void setEsDiferido(Boolean esDiferido) {
		this.esDiferido = esDiferido;
	}

	public DescriptivoGeneric getPago() {
		return pago;
	}

	public void setPago(DescriptivoGeneric pago) {
		this.pago = pago;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getNumeroRecibo() {
		return numeroRecibo;
	}

	public void setNumeroRecibo(String numeroRecibo) {
		this.numeroRecibo = numeroRecibo;
	}

}

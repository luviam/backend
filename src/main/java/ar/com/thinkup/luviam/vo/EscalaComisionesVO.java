package ar.com.thinkup.luviam.vo;

import java.io.Serializable;

import ar.com.thinkup.luviam.model.EscalaComisiones;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public class EscalaComisionesVO implements Serializable, Identificable {

	private static final long serialVersionUID = 2792022420485541969L;
	private Long id;

	private Integer desde;

	private Integer hasta;

	private Double premio;
	private DescriptivoGeneric centro;

	public EscalaComisionesVO() {
		super();
	}

	public EscalaComisionesVO(EscalaComisiones ent) {
		this.id = ent.getId();
		this.desde = ent.getDesde();
		this.hasta = ent.getHasta();
		this.premio = ent.getPremio();
		this.centro = new DescriptivoGeneric(ent.getCentroCosto());

	}

	@Override
	public Long getId() {
		return this.id;
	}

	public Integer getDesde() {
		return desde;
	}

	public void setDesde(Integer desde) {
		this.desde = desde;
	}

	public Integer getHasta() {
		return hasta;
	}

	public void setHasta(Integer hasta) {
		this.hasta = hasta;
	}

	public Double getPremio() {
		return premio;
	}

	public void setPremio(Double premio) {
		this.premio = premio;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DescriptivoGeneric getCentro() {
		return centro;
	}

	public void setCentro(DescriptivoGeneric centro) {
		this.centro = centro;
	}

}
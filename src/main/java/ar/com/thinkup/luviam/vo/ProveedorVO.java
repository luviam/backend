package ar.com.thinkup.luviam.vo;

import java.util.List;
import java.util.Optional;
import java.util.Vector;
import java.util.stream.Collectors;

import ar.com.thinkup.luviam.model.Proveedor;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.def.IDescriptivo;

public class ProveedorVO implements IDescriptivo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2768068004784738837L;

	private Long id;
	private String nombreProveedor;
	private String nombreContacto;
	private String domicilio;
	private String localidad;
	private String telefono;
	private String celular;
	private String email;
	private String razonSocial;
	private String cuit;
	private DescriptivoGeneric iva;
	private String domicilioFacturacion;
	private String localidadFacturacion;
	private DescriptivoGeneric tipoProveedor;
	private DescriptivoGeneric tipoCuenta;
	private Boolean activo;
	private List<ImputacionCuentaConfigVO> imputacionCuentaConfigs = new Vector<>();

	public ProveedorVO() {
		super();
	}

	public ProveedorVO(Proveedor p) {
		this.id = p.getId();
		this.nombreProveedor = p.getNombreProveedor();
		this.nombreContacto = p.getNombreContacto();
		this.domicilio = p.getDomicilio();
		this.localidad = p.getLocalidad();
		this.telefono = p.getTelefono();
		this.celular = p.getCelular();
		this.email = p.getEmail();
		this.razonSocial = p.getRazonSocial();
		this.cuit = p.getCuit();
		if (p.getIva() != null) {
			this.iva = new DescriptivoGeneric(p.getIva());
		}

		this.domicilioFacturacion = p.getDomicilioFacturacion();
		this.localidadFacturacion = p.getLocalidadFacturacion();
		if (p.getTipoProveedor() != null) {
			this.tipoProveedor = new DescriptivoGeneric(p.getTipoProveedor());
		}
		if (p.getTipoCuenta() != null) {
			this.tipoCuenta = new DescriptivoGeneric(p.getTipoCuenta());
		}
		this.activo = p.getActivo();
		this.imputacionCuentaConfigs = p.getImputacionCuentaConfigs().stream().map(i -> new ImputacionCuentaConfigVO(i))
				.collect(Collectors.toList());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreProveedor() {
		return nombreProveedor;
	}

	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}

	public String getNombreContacto() {
		return nombreContacto;
	}

	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public DescriptivoGeneric getIva() {
		return iva;
	}

	public void setIva(DescriptivoGeneric iva) {
		this.iva = iva;
	}

	public String getDomicilioFacturacion() {
		return domicilioFacturacion;
	}

	public void setDomicilioFacturacion(String domicilioFacturacion) {
		this.domicilioFacturacion = domicilioFacturacion;
	}

	public String getLocalidadFacturacion() {
		return localidadFacturacion;
	}

	public void setLocalidadFacturacion(String localidadFacturacion) {
		this.localidadFacturacion = localidadFacturacion;
	}

	public DescriptivoGeneric getTipoProveedor() {
		return tipoProveedor;
	}

	public void setTipoProveedor(DescriptivoGeneric tipoProveedor) {
		this.tipoProveedor = tipoProveedor;
	}

	public DescriptivoGeneric getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(DescriptivoGeneric tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public List<ImputacionCuentaConfigVO> getImputacionCuentaConfigs() {
		return imputacionCuentaConfigs;
	}

	public void setImputacionCuentaConfigs(List<ImputacionCuentaConfigVO> imputacionCuentaConfigs) {
		this.imputacionCuentaConfigs = imputacionCuentaConfigs;
	}

	@Override
	public String getCodigo() {
		return String.valueOf(this.getId());
	}

	@Override
	public String getDescripcion() {
		return this.getNombreProveedor();
	}

	public ImputacionCuentaConfigVO getItem(Long id2) {
		Optional<ImputacionCuentaConfigVO> op = this.imputacionCuentaConfigs.parallelStream()
				.filter(i -> i.getId() != null && i.getId().equals(id)).findFirst();
		if (op.isPresent()) {
			return op.get();
		} else {
			// TIRO EXCEPCIóN?
			return null;
		}
	}

}

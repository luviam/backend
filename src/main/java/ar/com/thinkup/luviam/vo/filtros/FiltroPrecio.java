package ar.com.thinkup.luviam.vo.filtros;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.utils.DescriptivoParser;

public class FiltroPrecio implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2228165120095514743L;
	public Date fechaVigencia;
	public List<DescriptivoGeneric> salones = new Vector<>();
	public List<DescriptivoGeneric> productos = new Vector<>();
	public List<DescriptivoGeneric> marcas = new Vector<>();
	public Boolean soloActivos = true;

	public Date getFechaVigencia() {
		return fechaVigencia;
	}

	public void setFechaVigencia(Date fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

	public List<DescriptivoGeneric> getSalones() {
		return salones;
	}

	public void setSalones(List<DescriptivoGeneric> salones) {
		this.salones = salones;
	}

	public List<DescriptivoGeneric> getProductos() {
		return productos;
	}

	public void setProductos(List<DescriptivoGeneric> productos) {
		this.productos = productos;
	}

	public List<DescriptivoGeneric> getMarcas() {
		return marcas;
	}

	public void setMarcas(List<DescriptivoGeneric> marcas) {
		this.marcas = marcas;
	}
	
	public List<String> getCodigosMarcas() {
		return DescriptivoParser.getCodigos(this.marcas);
	}
	public List<String> getCodigosProductos() {
		return DescriptivoParser.getCodigos(this.productos);
	}
	public List<String> getCodigosSalones() {
		return DescriptivoParser.getCodigos(this.salones);
	}

	public List<Long> getIdSalones() {
		return this.salones.stream().map(s -> Long.valueOf(s.getCodigo())).collect(Collectors.toList());
	}
	
	public List<Long> getIdProductos() {
		return this.productos.stream().map(s -> Long.valueOf(s.getCodigo())).collect(Collectors.toList());
	}
	
	public List<Long> getIdMarcas() {
		return this.marcas.stream().map(s -> Long.valueOf(s.getCodigo())).collect(Collectors.toList());
	}

	public Boolean getSoloActivos() {
		return soloActivos;
	}

	public void setSoloActivos(Boolean soloActivos) {
		this.soloActivos = soloActivos;
	}
	
	
	

}

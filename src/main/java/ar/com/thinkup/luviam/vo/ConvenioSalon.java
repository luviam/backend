package ar.com.thinkup.luviam.vo;

public class ConvenioSalon {
	
	private Float porcComision;
    private Float valorComision;
    private Float porcDescuentos;
    private Float valorAdicional;
    
	public Float getPorcComision() {
		return porcComision;
	}
	public void setPorcComision(Float porcComision) {
		this.porcComision = porcComision;
	}
	public Float getValorComision() {
		return valorComision;
	}
	public void setValorComision(Float valorComision) {
		this.valorComision = valorComision;
	}
	public Float getPorcDescuentos() {
		return porcDescuentos;
	}
	public void setPorcDescuentos(Float porcDescuentos) {
		this.porcDescuentos = porcDescuentos;
	}
	public Float getValorAdicional() {
		return valorAdicional;
	}
	public void setValorAdicional(Float valorAdicional) {
		this.valorAdicional = valorAdicional;
	}
    
    

}

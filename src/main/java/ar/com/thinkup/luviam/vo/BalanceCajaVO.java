package ar.com.thinkup.luviam.vo;

import java.math.BigDecimal;

import org.apache.commons.lang3.ObjectUtils;

public class BalanceCajaVO {

	private Double debe = 0d;

	private Double haber = 0d;

	public BalanceCajaVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BalanceCajaVO(BigDecimal debe, BigDecimal haber) {
		super();
		this.debe = ObjectUtils.defaultIfNull(debe.doubleValue(), 0d);
		this.haber = ObjectUtils.defaultIfNull(haber.doubleValue(), 0d);
	}

	public Double getDebe() {
		return debe;
	}

	public void setDebe(Double debe) {
		this.debe = debe;
	}

	public Double getHaber() {
		return haber;
	}

	public void setHaber(Double haber) {
		this.haber = haber;
	}

	public Double calcularSaldo(Double montoApertura) {
		return montoApertura + (this.debe - this.haber);
	}

}

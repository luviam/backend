package ar.com.thinkup.luviam.vo;

import java.io.Serializable;

import ar.com.thinkup.luviam.model.Archivo;

public class ArchivoVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6463562839252082952L;
	private Long id;
	private String url;
	
	
	public ArchivoVO() {
		super();
		
	}
	public ArchivoVO(Archivo archivo) {
		this.id = archivo.getId();
		this.url = archivo.getUrl()  + "?" +archivo.getVersion();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	

}

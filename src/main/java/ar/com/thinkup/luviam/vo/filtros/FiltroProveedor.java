package ar.com.thinkup.luviam.vo.filtros;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public class FiltroProveedor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1639108454181797383L;

	private Date fechaDesde;
	private Date fechaHasta;
	private DescriptivoGeneric centro = new DescriptivoGeneric("-1", "TODOS");
	private DescriptivoGeneric proveedor = new DescriptivoGeneric("-1", "TODOS");
	private List<String> tiposComprobante = new Vector<>();
	private List<String> estados = new Vector<>();

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public DescriptivoGeneric getCentro() {
		return centro;
	}

	public void setCentro(DescriptivoGeneric centro) {
		this.centro = centro;
	}

	public DescriptivoGeneric getProveedor() {
		return proveedor;
	}

	public void setProveedor(DescriptivoGeneric proveedor) {
		this.proveedor = proveedor;
	}

	public List<String> getTiposComprobante() {
		return tiposComprobante;
	}

	public void setTiposComprobante(List<String> tiposComprobante) {
		this.tiposComprobante = tiposComprobante;
	}

	public List<String> getEstados() {
		return estados;
	}

	public void setEstados(List<String> estados) {
		this.estados = estados;
	}

	public void setCodigoProveedor(String codigo) {
		if (this.proveedor == null) {
			this.proveedor = new DescriptivoGeneric();
		}
		this.proveedor.setCodigo(codigo);

	}

	public void setCodigoCentro(String codigo) {
		if (this.centro == null) {
			this.centro = new DescriptivoGeneric();
		}
		this.centro.setCodigo(codigo);
	}

	public String getCodigoProveedor() {
		return this.proveedor == null ? null : this.proveedor.getCodigo();
	}

	public String getCodigoCentro() {

		return this.centro == null ? null : this.centro.getCodigo();
	}

}

package ar.com.thinkup.luviam.vo.filtros;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public class FiltroGastos implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3319671803740353276L;

	private Date fechaDesde = new DateTime().withTimeAtStartOfDay().minusDays(90).toDate();
	private Date fechaHasta = new DateTime().withTimeAtStartOfDay().plusDays(1).toDate();;
	private DescriptivoGeneric centro = DescriptivoGeneric.TODOS();
	private DescriptivoGeneric grupo = DescriptivoGeneric.TODOS();
	private DescriptivoGeneric subGrupo = DescriptivoGeneric.TODOS();

	public String getCodigoCentro() {
		return (this.centro == null || StringUtils.isEmpty(this.centro.getCodigo())) ? "-1" : this.centro.getCodigo();
	}

	public String getCodigoGrupo() {
		return (this.grupo == null || StringUtils.isEmpty(this.grupo.getCodigo())) ? "-1" : this.grupo.getCodigo();
	}

	public String getCodigoSubGrupo() {
		return (this.subGrupo == null || StringUtils.isEmpty(this.subGrupo.getCodigo())) ? "-1"
				: this.subGrupo.getCodigo();
	}

	public Date getFechaDesde() {
		return fechaDesde != null ? this.fechaDesde : new DateTime().withTimeAtStartOfDay().minusDays(100).toDate();
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta != null ? this.fechaHasta : new DateTime().withTimeAtStartOfDay().plusDays(1).toDate();
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public DescriptivoGeneric getCentro() {
		return centro;
	}

	public void setCentro(DescriptivoGeneric centro) {
		this.centro = centro;
	}

	public DescriptivoGeneric getGrupo() {
		return grupo;
	}

	public void setGrupo(DescriptivoGeneric grupo) {
		this.grupo = grupo;
	}

	public DescriptivoGeneric getSubGrupo() {
		return subGrupo;
	}

	public void setSubGrupo(DescriptivoGeneric horario) {
		this.subGrupo = horario;
	}

	@Override
	public String toString() {
		return "FiltroComisiones [ , fechaDesde=" + fechaDesde + ", fechaHasta=" + fechaHasta + ", centro=" + centro
				+ ", grupo=" + grupo + ", subGrupo=" + subGrupo + "]";
	}

}

package ar.com.thinkup.luviam.vo.indicadores;

import java.io.Serializable;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public class CubiertoPromedioVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1819046978747539047L;
	public DescriptivoGeneric producto;
	public Double promedio;
	public Double minimo;
	public Double maximo;

	public CubiertoPromedioVO(String codigo, String descripcion, Double promedio, Double minimo, Double maximo) {
		super();
		this.producto = new DescriptivoGeneric(codigo, descripcion);
		this.promedio = promedio;
		this.minimo = minimo;
		this.maximo = maximo;
	}

	public DescriptivoGeneric getProducto() {
		return producto;
	}

	public void setProducto(DescriptivoGeneric producto) {
		this.producto = producto;
	}

	public Double getPromedio() {
		return promedio;
	}

	public void setPromedio(Double promedio) {
		this.promedio = promedio;
	}

	public Double getMinimo() {
		return minimo;
	}

	public void setMinimo(Double minimo) {
		this.minimo = minimo;
	}

	public Double getMaximo() {
		return maximo;
	}

	public void setMaximo(Double maximo) {
		this.maximo = maximo;
	}

}

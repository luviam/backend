package ar.com.thinkup.luviam.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Vector;
import java.util.stream.Collectors;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.enums.EstadoAsientoEnum;

public class AsientoVO implements Serializable, Identificable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8033867798937474438L;
	private Long id;
	private Long numeroAsiento;
	private String descripcion;
	private String responsable;
	private Date fecha;
	private DescriptivoGeneric estado;
	private List<OperacionVO> operaciones;
	private String mensaje;

	public AsientoVO() {

	}

	public AsientoVO(Asiento a) {
		this.id = a.getId();
		// this.numeroAsiento = a.getNumero(); //TODO : GENERAR VALOR DE NUMERO ASIENTO
		this.numeroAsiento = a.getId();
		this.descripcion = a.getDescripcion();
		this.responsable = a.getResponsable();
		this.fecha = a.getFecha();
		EstadoAsientoEnum e = EstadoAsientoEnum.getByCodigo(a.getEstado());
		if (e != null) {
			this.estado = new DescriptivoGeneric(e.getCodigo(), e.getDescripcion());
		}
		this.mensaje = a.getMensaje();
		this.operaciones = new Vector<>();
		this.operaciones.addAll(a.getOperaciones().stream().map(o -> new OperacionVO(o)).collect(Collectors.toList()));

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNumeroAsiento() {
		return numeroAsiento;
	}

	public void setNumeroAsiento(Long numeroAsiento) {
		this.numeroAsiento = numeroAsiento;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public DescriptivoGeneric getEstado() {
		return estado;
	}

	public void setEstado(DescriptivoGeneric estado) {
		this.estado = estado;
	}

	public List<OperacionVO> getOperaciones() {
		return operaciones;
	}

	public void setOperaciones(List<OperacionVO> operaciones) {
		this.operaciones = operaciones;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public OperacionVO getOperacion(Long idOperacion) {
		Optional<OperacionVO> op = this.operaciones.stream()
				.filter(o -> o.getId() != null && o.getId().equals(idOperacion)).findFirst();

		return op.isPresent() ? op.get() : null;
	}

}

package ar.com.thinkup.luviam.vo;

import java.io.Serializable;
import java.util.Date;

import ar.com.thinkup.luviam.model.contratos.PagoComision;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.enums.TipoPagoComision;

public class PagoComisionVO implements Identificable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2769771830677911955L;
	private Long id;
	private DescriptivoGeneric salon;
	private DescriptivoGeneric cliente;
	private DescriptivoGeneric evento;

	private Long idServicio;
	private String nombreProducto;
	private DescriptivoGeneric categoria;
	private Integer cantidadBase;
	private Double precioUnitario;
	private DescriptivoGeneric tipoPago;
	private Double porcentajeComision;
	private Double totalComision;
	private Double totalLiquidado;
	private Date fechaComision;
	private DescriptivoGeneric tipoEvento;
	private DescriptivoGeneric vendedor;
	private Integer baseContrato;

	public PagoComisionVO() {
		super();

	}

	public PagoComisionVO(PagoComision p) {
		this.id = p.getId();
		this.cliente = p.getConfigComision().getContrato().getCliente() != null
				? new DescriptivoGeneric(p.getConfigComision().getContrato().getCliente())
				: null;
		this.salon = p.getConfigComision().getContrato().getSalon() != null
				? new DescriptivoGeneric(p.getConfigComision().getContrato().getSalon())
				: null;
		this.evento = new DescriptivoGeneric(p.getConfigComision().getContrato());
		this.nombreProducto = p.getConfigComision().getServicioComisionado().getDescripcion();
		this.idServicio = p.getConfigComision().getServicioComisionado().getId();
		this.cantidadBase = p.getCantidad();
		this.precioUnitario = p.getConfigComision().getServicioComisionado().getCostoUnitario();
		this.porcentajeComision = p.getConfigComision().getPorcentajeComision();
		this.totalComision = p.getTotalComision();
		this.totalLiquidado = p.getTotalLiquidado();
		this.tipoPago = TipoPagoComision.getDescriptivo(p.getCodigoTipoPagoComision());
		this.fechaComision = p.getFechaComision();
		this.tipoEvento = new DescriptivoGeneric(
				p.getConfigComision().getServicioComisionado().getContrato().getTipoEvento());
		this.categoria = new DescriptivoGeneric(
				p.getConfigComision().getServicioComisionado().getProducto().getCategoria());
		this.vendedor = new DescriptivoGeneric(
				p.getConfigComision().getServicioComisionado().getContrato().getVendedor());
		this.baseContrato = p.getConfigComision().getContrato().getBaseInvitados();
	}

	public DescriptivoGeneric getSalon() {
		return salon;
	}

	public void setSalon(DescriptivoGeneric salon) {
		this.salon = salon;
	}

	public DescriptivoGeneric getEvento() {
		return evento;
	}

	public void setEvento(DescriptivoGeneric evento) {
		this.evento = evento;
	}

	public DescriptivoGeneric getCliente() {
		return cliente;
	}

	public void setCliente(DescriptivoGeneric cliente) {
		this.cliente = cliente;
	}

	public Double getPorcentajeComision() {
		return porcentajeComision;
	}

	public void setPorcentajeComision(Double porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}

	public Double getTotalComision() {
		return totalComision;
	}

	public void setTotalComision(Double totalComision) {
		this.totalComision = totalComision;
	}

	public Double getTotalLiquidado() {
		return totalLiquidado;
	}

	public void setTotalLiquidado(Double totalLiquidado) {
		this.totalLiquidado = totalLiquidado;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public DescriptivoGeneric getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(DescriptivoGeneric tipoPago) {
		this.tipoPago = tipoPago;
	}

	public Date getFechaComision() {
		return fechaComision;
	}

	public void setFechaComision(Date fechaComision) {
		this.fechaComision = fechaComision;
	}

	public Long getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public Integer getCantidadBase() {
		return cantidadBase;
	}

	public void setCantidadBase(Integer cantidadBase) {
		this.cantidadBase = cantidadBase;
	}

	public Double getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(Double precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public DescriptivoGeneric getCategoria() {
		return categoria;
	}

	public void setCategoria(DescriptivoGeneric categoria) {
		this.categoria = categoria;
	}

	public DescriptivoGeneric getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(DescriptivoGeneric tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public DescriptivoGeneric getVendedor() {
		return vendedor;
	}

	public void setVendedor(DescriptivoGeneric vendedor) {
		this.vendedor = vendedor;
	}

	public Integer getBaseContrato() {
		return baseContrato;
	}

	public void setBaseContrato(Integer baseContrato) {
		this.baseContrato = baseContrato;
	}

}

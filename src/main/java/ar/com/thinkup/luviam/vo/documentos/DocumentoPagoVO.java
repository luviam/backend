package ar.com.thinkup.luviam.vo.documentos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.documentos.DocumentoPago;
import ar.com.thinkup.luviam.model.documentos.Valores;
import ar.com.thinkup.luviam.vo.Identificable;

public abstract class DocumentoPagoVO<T extends Valores, V extends ValoresVO<T>, R extends Identificable>
		implements Serializable, Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3946007526804479217L;

	private Long id;

	private String numero;

	private String descripcion;

	private DescriptivoGeneric responsable;

	private Date fecha;

	private DescriptivoGeneric centroCosto;

	private DescriptivoGeneric estado;

	private Double importe;

	private String numeroComprobante;

	public DocumentoPagoVO() {
		super();
	}

	public DocumentoPagoVO(DocumentoPago<?> ent) {
		super();
		this.id = ent.getId();
		this.numero = ent.getNumero();
		this.descripcion = ent.getDescripcion();
		this.responsable = ent.getResponsable() != null ? new DescriptivoGeneric(ent.getResponsable())
				: new DescriptivoGeneric("S", "Sistema");
		this.fecha = ent.getFecha();
		this.centroCosto = new DescriptivoGeneric(ent.getCentroCosto());
		this.estado = ent.getEstadoOperacion() != null ? new DescriptivoGeneric(ent.getEstadoOperacion().getCodigo(),
				ent.getEstadoOperacion().getDescripcion()) : null;
		this.importe = ent.getImporte();
		this.numeroComprobante = ent.getNumeroComprobante();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public DescriptivoGeneric getResponsable() {
		return responsable;
	}

	public void setResponsable(DescriptivoGeneric responsable) {
		this.responsable = responsable;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public DescriptivoGeneric getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(DescriptivoGeneric centroCosto) {
		this.centroCosto = centroCosto;
	}

	public DescriptivoGeneric getEstado() {
		return estado;
	}

	public void setEstado(DescriptivoGeneric estado) {
		this.estado = estado;
	}

	public abstract List<V> getValores();

	public abstract List<R> getItems();

	public R getItem(Long id) {

		return this.getItems().parallelStream().filter(i -> i.getId() != null && i.getId().equals(id)).findFirst()
				.orElse(null);
	}

	public V getValor(Long id) {
		return this.getValores().parallelStream().filter(i -> i.getId() != null && i.getId().equals(id)).findFirst()
				.orElse(null);

	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public String getNumeroComprobante() {
		return numeroComprobante;
	}

	public void setNumeroComprobante(String numeroComprobante) {
		this.numeroComprobante = numeroComprobante;
	}

}
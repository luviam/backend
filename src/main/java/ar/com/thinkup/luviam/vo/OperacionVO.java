package ar.com.thinkup.luviam.vo;

import java.io.Serializable;
import java.util.Date;

import ar.com.thinkup.luviam.model.Operacion;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public class OperacionVO implements Serializable, Identificable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7707196223047334831L;
	private Long id;
	private DescriptivoGeneric centroCosto;
	private DescriptivoGeneric cuenta;
	private String descripcion;
	private Double debe;
	private Double haber;
	private String comprobante;
	private Date fecha;
	private String responsable;
	private Long numeroAsiento;
	private String tipoGenerador;

	public OperacionVO() {

	}

	public OperacionVO(Operacion o) {
		this.id = o.getId();
		this.centroCosto = new DescriptivoGeneric(o.getCentro());
		this.cuenta = new DescriptivoGeneric(o.getCuenta());
		this.descripcion = o.getDetalle();
		this.debe = o.getDebe();
		this.haber = o.getHaber();
		this.comprobante = o.getComprobante();
		this.fecha = o.getAsiento().getFecha();
		this.responsable = o.getAsiento().getResponsable();
		this.numeroAsiento = o.getAsiento().getNumero();
		this.tipoGenerador = o.getAsiento().getTipoGenerador();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getDebe() {
		return debe == null ? 0f : this.debe;
	}

	public void setDebe(Double debe) {
		this.debe = debe;
	}

	public Double getHaber() {
		return haber == null ? 0f : this.haber;
	}

	public void setHaber(Double haber) {
		this.haber = haber;
	}

	public String getComprobante() {
		return comprobante;
	}

	public void setComprobante(String comprobante) {
		this.comprobante = comprobante;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	
	public DescriptivoGeneric getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(DescriptivoGeneric centro) {
		this.centroCosto = centro;
	}

	public DescriptivoGeneric getCuenta() {
		return cuenta;
	}

	public void setCuenta(DescriptivoGeneric cuenta) {
		this.cuenta = cuenta;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public Long getNumeroAsiento() {
		return numeroAsiento;
	}

	public void setNumeroAsiento(Long numeroAsiento) {
		this.numeroAsiento = numeroAsiento;
	}

	public String getTipoGenerador() {
		return tipoGenerador;
	}

	public void setTipoGenerador(String tipoGenerador) {
		this.tipoGenerador = tipoGenerador;
	}

}

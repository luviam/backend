package ar.com.thinkup.luviam.vo.documentos.pagos;

import ar.com.thinkup.luviam.model.documentos.pagos.Pago;
import ar.com.thinkup.luviam.vo.Identificable;
import ar.com.thinkup.luviam.vo.documentos.ValoresVO;

public class PagoVO extends ValoresVO<Pago> implements Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5581487473942134267L;

	public PagoVO() {
		super();

	}

	public PagoVO(Pago p) {
		super(p);
	}

}

package ar.com.thinkup.luviam.vo.parametricos;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;

import ar.com.thinkup.luviam.vo.PrecioVO;

public class ConflictoPrecio implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9097548192793400057L;
	private PrecioVO precio;
	private List<PrecioVO> conflictos = new Vector<>();
	public ConflictoPrecio(PrecioVO vo) {
		this.precio = vo;
	}
	public PrecioVO getPrecio() {
		return precio;
	}
	public void setPrecio(PrecioVO precio) {
		this.precio = precio;
	}
	public List<PrecioVO> getConflictos() {
		return conflictos;
	}
	public void setConflictos(List<PrecioVO> conflictos) {
		this.conflictos = conflictos;
	}
	public void addConflicto(PrecioVO vo) {
		this.conflictos.add(vo);
		
	}
	
	
}

package ar.com.thinkup.luviam.vo;

/**
 * @author ThinkUp
 *
 */
public class RefreshTokenVO {

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	
	
}

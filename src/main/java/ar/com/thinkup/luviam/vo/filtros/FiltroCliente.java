package ar.com.thinkup.luviam.vo.filtros;

import java.util.Date;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public class FiltroCliente {
	public DescriptivoGeneric cliente = DescriptivoGeneric.TODOS();
	public Date fechaDesde;
	public Date fechaHasta;
	public DescriptivoGeneric centro = DescriptivoGeneric.TODOS();
	public DescriptivoGeneric salon = DescriptivoGeneric.TODOS();
	public DescriptivoGeneric horario = DescriptivoGeneric.TODOS();

	public DescriptivoGeneric getCliente() {
		return cliente;
	}

	public void setCliente(DescriptivoGeneric cliente) {
		this.cliente = cliente;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public DescriptivoGeneric getCentro() {
		return centro;
	}

	public void setCentro(DescriptivoGeneric centro) {
		this.centro = centro;
	}

	public DescriptivoGeneric getSalon() {
		return salon;
	}

	public void setSalon(DescriptivoGeneric salon) {
		this.salon = salon;
	}

	public DescriptivoGeneric getHorario() {
		return horario;
	}

	public void setHorario(DescriptivoGeneric horario) {
		this.horario = horario;
	}

	public Long getIdCliente() {
		if (this.cliente == null) {
			this.cliente = DescriptivoGeneric.TODOS();
		}
		return Long.valueOf(this.cliente.getCodigo());
	}

	public Long getIdSalon() {
		if (this.salon == null) {
			this.salon = DescriptivoGeneric.TODOS();
		}
		return Long.valueOf(this.salon.getCodigo());
	}

	public String getCodigoCentro() {
		if (this.centro == null) {
			this.centro = DescriptivoGeneric.TODOS();
		}
		return this.centro.getCodigo();

	}

	public String getCodigoHorario() {
		if (this.horario == null) {
			this.horario = DescriptivoGeneric.TODOS();
		}
		return this.horario.getCodigo();

	}
}

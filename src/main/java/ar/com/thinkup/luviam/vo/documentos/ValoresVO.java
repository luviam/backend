package ar.com.thinkup.luviam.vo.documentos;

import java.io.Serializable;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.documentos.Valores;
import ar.com.thinkup.luviam.vo.ChequeVO;

public class ValoresVO<T extends Valores> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5964047131894286831L;
	private Long id;
	private DescriptivoGeneric tipoOperacion;
	private DescriptivoGeneric centroCosto;
	private DescriptivoGeneric cuenta;
	private Double monto = 0d;
	private ChequeVO comprobante;

	public ValoresVO() {
		super();
	}

	public ValoresVO(T p) {
		this.id = p.getId();
		this.tipoOperacion = new DescriptivoGeneric(p.getTipoPago().getCodigo(), p.getTipoPago().getDescripcion());
		this.centroCosto = new DescriptivoGeneric(p.getCentroCosto());
		this.cuenta = new DescriptivoGeneric(p.getCuenta());
		this.monto = p.getMonto();
		if (p.getCheque() != null) {
			this.comprobante = new ChequeVO(p.getCheque());
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DescriptivoGeneric getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(DescriptivoGeneric tipoPago) {
		this.tipoOperacion = tipoPago;
	}

	public DescriptivoGeneric getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(DescriptivoGeneric centroCosto) {
		this.centroCosto = centroCosto;
	}

	public DescriptivoGeneric getCuenta() {
		return cuenta;
	}

	public void setCuenta(DescriptivoGeneric cuenta) {
		this.cuenta = cuenta;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public ChequeVO getComprobante() {
		return comprobante;
	}

	public void setComprobante(ChequeVO comprobante) {
		this.comprobante = comprobante;
	}

}
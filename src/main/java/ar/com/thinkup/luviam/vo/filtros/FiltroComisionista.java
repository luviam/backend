package ar.com.thinkup.luviam.vo.filtros;

import java.io.Serializable;
import java.util.Date;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public class FiltroComisionista implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3319671803740153276L;
	
	public DescriptivoGeneric comisionista = DescriptivoGeneric.TODOS();
	public Date fechaDesde;
	public Date fechaHasta;
	public DescriptivoGeneric centro = DescriptivoGeneric.TODOS();
	public DescriptivoGeneric salon = DescriptivoGeneric.TODOS();
	public DescriptivoGeneric horario = DescriptivoGeneric.TODOS();

}

package ar.com.thinkup.luviam.vo;

import java.util.Date;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.repository.OrdenPagoRepository.OrdenPagoCabecera;

public class OrdenPagoCabeceraVO {

	private Long id;
	private Date fecha;
	private String numero;
	private String responsable;
	private String descripcion;
	private Double importe;
	private String estado;
	private DescriptivoGeneric proveedor;
	private DescriptivoGeneric centro;

	public OrdenPagoCabeceraVO() {
		super();

	}

	public OrdenPagoCabeceraVO(OrdenPagoCabecera op) {
		this();
		this.id = op.getId();
		this.fecha = op.getFecha();
		this.numero = op.getNumero();
		this.responsable = op.getResponsable().getFullName();
		this.descripcion = op.getDescripcion();
		this.importe = op.getImporte();
		this.estado = op.getCodigoEstado();
		if (op.getCentroCosto() != null) {
			this.centro = new DescriptivoGeneric(op.getCentroCosto().getCodigo(), op.getCentroCosto().getNombre());
		}
		if (op.getProveedor() != null) {
			this.proveedor = new DescriptivoGeneric(String.valueOf(op.getProveedor().getId()),
					op.getProveedor().getNombreProveedor());
		}

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public void setEstado(EstadoOperacionEnum e) {
		this.estado = e.getCodigo();

	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public DescriptivoGeneric getProveedor() {
		return proveedor;
	}

	public void setProveedor(DescriptivoGeneric proveedor) {
		this.proveedor = proveedor;
	}

	public DescriptivoGeneric getCentro() {
		return centro;
	}

	public void setCentro(DescriptivoGeneric centro) {
		this.centro = centro;
	}

}

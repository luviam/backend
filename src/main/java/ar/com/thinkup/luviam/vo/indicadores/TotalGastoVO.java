package ar.com.thinkup.luviam.vo.indicadores;

import java.io.Serializable;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public class TotalGastoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2522318140565381360L;
	private DescriptivoGeneric gasto;
	private Double total = 0d;

	public TotalGastoVO() {
		super();
	}

	public TotalGastoVO(String codigo, String descripcion, Double total) {
		this.gasto = new DescriptivoGeneric(codigo, descripcion);
		this.total = total;
	}

	public DescriptivoGeneric getGasto() {
		return gasto;
	}

	public void setGasto(DescriptivoGeneric gasto) {
		this.gasto = gasto;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

}

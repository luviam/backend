package ar.com.thinkup.luviam.vo;

import java.io.Serializable;

public class ConvenioSalonVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7014121315778927002L;

	private Float porcComision;

	private Float valorComision;

	private Float porcDescuentos;

	private Float valorAdicional;

	public ConvenioSalonVO() {
		super();
	}

	public ConvenioSalonVO(ConvenioSalon ent) {
		super();
		this.porcComision = ent.getPorcComision();
		this.valorComision = ent.getValorComision();
		this.porcDescuentos = ent.getPorcDescuentos();
		this.valorAdicional = ent.getValorAdicional();

	}

	public Float getPorcComision() {
		return porcComision;
	}

	public void setPorcComision(Float porcComision) {
		this.porcComision = porcComision;
	}

	public Float getValorComision() {
		return valorComision;
	}

	public void setValorComision(Float valorComision) {
		this.valorComision = valorComision;
	}

	public Float getPorcDescuentos() {
		return porcDescuentos;
	}

	public void setPorcDescuentos(Float porcDescuentos) {
		this.porcDescuentos = porcDescuentos;
	}

	public Float getValorAdicional() {
		return valorAdicional;
	}

	public void setValorAdicional(Float valorAdicional) {
		this.valorAdicional = valorAdicional;
	}


}
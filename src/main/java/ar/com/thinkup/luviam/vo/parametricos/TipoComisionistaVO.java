package ar.com.thinkup.luviam.vo.parametricos;

import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import ar.com.thinkup.luviam.model.TipoComisionista;
import ar.com.thinkup.luviam.vo.CategoriaProductoVO;

public class TipoComisionistaVO extends ParametricoVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9175058955085357399L;

	public List<CategoriaProductoVO> categorias = new Vector<>();

	public TipoComisionistaVO() {
		super();

	}

	public TipoComisionistaVO(TipoComisionista ent) {
		super(ent);
		this.categorias = new Vector<>();
		if (ent.getCategorias() != null) {
			this.categorias.addAll(
					ent.getCategorias().stream().map(c -> new CategoriaProductoVO(c)).collect(Collectors.toList()));
		}

	}

	public List<CategoriaProductoVO> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<CategoriaProductoVO> categorias) {
		this.categorias = categorias;
	}

	public CategoriaProductoVO getCategoria(Long id) {

		return this.categorias.parallelStream().filter(c -> c.getId() != null && c.getId().equals(id)).findFirst()
				.orElse(null);
	}

}

package ar.com.thinkup.luviam.vo.operaciones;

import java.util.Date;

public class AltaTraspasoVO {

	private Long idCajaOrigen;
	private Long idCajaDestino;
	private String descripcion;
	private Double monto;
	private Date fecha = new Date();

	
	public AltaTraspasoVO() {
		super();
	}
	

	public AltaTraspasoVO(Long idCajaOrigen, Long idCajaDestino, String descripcion, Double monto, Date fecha) {
		super();
		this.idCajaOrigen = idCajaOrigen;
		this.idCajaDestino = idCajaDestino;
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.monto = monto;
	}




	public Long getIdCajaOrigen() {
		return idCajaOrigen;
	}

	public void setIdCajaOrigen(Long idCajaDesde) {
		this.idCajaOrigen = idCajaDesde;
	}

	public Long getIdCajaDestino() {
		return idCajaDestino;
	}

	public void setIdCajaDestino(Long idCajaHasta) {
		this.idCajaDestino = idCajaHasta;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}


	public Date getFecha() {
		return fecha;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	

}

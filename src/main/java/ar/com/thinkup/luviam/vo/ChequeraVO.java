package ar.com.thinkup.luviam.vo;

import java.util.List;

import ar.com.thinkup.luviam.model.Chequera;
import ar.com.thinkup.luviam.model.SecuenciaCheques;

public class ChequeraVO {

	private Long id;
	private CuentaAplicacionVO cuentaBancaria;
	private List<SecuenciaCheques> secuencias;
	private boolean esDiferido;

	public ChequeraVO() {
		// TODO Auto-generated constructor stub
	}

	public ChequeraVO(Chequera chequera) {
		super();
		this.id = chequera.getId();
		this.cuentaBancaria = new CuentaAplicacionVO(chequera.getCuentaBancaria());
		this.secuencias = chequera.getSecuenciasCheques();
		this.esDiferido = chequera.isEsDiferido();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the cuentaBancaria
	 */
	public CuentaAplicacionVO getCuentaBancaria() {
		return cuentaBancaria;
	}

	/**
	 * @param cuentaBancaria
	 *            the cuentaBancaria to set
	 */
	public void setCuentaBancaria(CuentaAplicacionVO cuentaBancaria) {
		this.cuentaBancaria = cuentaBancaria;
	}

	/**
	 * @return the secuencia
	 */
	public List<SecuenciaCheques> getSecuencias() {
		return secuencias;
	}

	/**
	 * @param secuencia
	 *            the secuencia to set
	 */
	public void setSecuencias(List<SecuenciaCheques> secuencias) {
		this.secuencias = secuencias;
	}

	/**
	 * @return the esDiferido
	 */
	public boolean isEsDiferido() {
		return esDiferido;
	}

	/**
	 * @param esDiferido
	 *            the esDiferido to set
	 */
	public void setEsDiferido(boolean esDiferido) {
		this.esDiferido = esDiferido;
	}	

}

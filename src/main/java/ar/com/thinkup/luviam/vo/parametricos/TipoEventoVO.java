package ar.com.thinkup.luviam.vo.parametricos;

import ar.com.thinkup.luviam.model.TipoEvento;

public class TipoEventoVO extends ParametricoVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3907864868846119485L;

	public TipoEventoVO(TipoEvento ent) {
		super(ent);
	}

	public TipoEventoVO() {
		super();
	}

}

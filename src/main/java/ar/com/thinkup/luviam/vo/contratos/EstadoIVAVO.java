package ar.com.thinkup.luviam.vo.contratos;

import java.io.Serializable;

import ar.com.thinkup.luviam.repository.ContratoRepository.EstadoIVA;

public class EstadoIVAVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6929349037925233823L;
	private Double ivaMaximo;
	private Double ivaCobrado;

	public EstadoIVAVO() {

	}

	public EstadoIVAVO(EstadoIVA e) {
		this.ivaMaximo = e.getIvaMaximo() != null ? e.getIvaMaximo().doubleValue() : 0d;
		this.ivaCobrado = e.getIvaCobrado() != null ? e.getIvaCobrado().doubleValue() : 0d;
	}

	public Double getIvaMaximo() {
		return ivaMaximo;
	}

	public void setIvaMaximo(Double ivaMaximo) {
		this.ivaMaximo = ivaMaximo;
	}

	public Double getIvaCobrado() {
		return ivaCobrado;
	}

	public void setIvaCobrado(Double ivaCobrado) {
		this.ivaCobrado = ivaCobrado;
	}

}

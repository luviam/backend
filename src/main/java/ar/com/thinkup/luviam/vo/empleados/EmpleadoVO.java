package ar.com.thinkup.luviam.vo.empleados;

import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.empleados.Empleado;

public class EmpleadoVO implements IDescriptivo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2768068004784738837L;

	private Long id;
	private String nombre;
	private Date fechaNacimiento;
	private String cuil;
	private String domicilio;
	private Boolean activo;
	private boolean cvOk;
	private boolean preocupacionalOk;
	private Date fechaRealizacionPreocupacional;
	private boolean dniOK;
	private boolean cuilOK;
	private boolean constanciaDomicilioOK;
	private boolean seguroOK;
	private boolean artOK;
	private DescriptivoGeneric tipoJornada;
	private DescriptivoGeneric sector;
	private String funcion;
	private String responsabilidades;
	private Float remuneracion;
	private Date fechaInicio;
	private Date fechaFin;
	private DescriptivoGeneric tipoEmpleado;
	private List<AltaEmpleadoVO> altas = new Vector<>();
	private String email;
	private String telefono;
	private DescriptivoGeneric centro;

	public EmpleadoVO() {
		super();
	}

	public EmpleadoVO(Empleado e) {
		this.id = e.getId();
		this.nombre = e.getNombre();
		this.fechaNacimiento = e.getFechaNacimiento();
		this.domicilio = e.getDomicilio();
		this.cuil = e.getCuil();
		this.cvOk = e.getCvOk();
		this.preocupacionalOk = e.getPreocupacionalOk();
		this.fechaRealizacionPreocupacional = e.getFechaRealizacionPreocupacional();
		this.dniOK = e.getDniOK();
		this.cuilOK = e.getCuilOK();
		this.constanciaDomicilioOK = e.getConstanciaDomicilioOK();
		this.seguroOK = e.getSeguroOK();
		this.artOK = e.getArtOK();
		this.remuneracion = e.getRemuneracion();
		this.responsabilidades = e.getResponsabilidades();
		this.funcion = e.getFuncion();
		if (e.getSector() != null)
			this.sector = new DescriptivoGeneric(e.getSector());
		if (e.getTipoJornada() != null)
			this.tipoJornada = new DescriptivoGeneric(e.getTipoJornada());

		if (e.getCentroCosto() != null) {
			this.centro = new DescriptivoGeneric(e.getCentroCosto());
		}
		this.fechaInicio = e.getFechaInicio();
		this.fechaFin = e.getFechaFin();
		if (e.getTipoEmpleado() != null)
			this.tipoEmpleado = new DescriptivoGeneric(e.getTipoEmpleado());
		this.setAltas(e.getAltas().stream().map(a -> new AltaEmpleadoVO(a)).collect(Collectors.toList()));
		this.email = e.getEmail();
		this.telefono = e.getTelefono();

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getCuil() {
		return cuil;
	}

	public void setCuil(String cuil) {
		this.cuil = cuil;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	@Override
	public String getCodigo() {
		return String.valueOf(this.getId());
	}

	public boolean getCvOk() {
		return cvOk;
	}

	public void setCvOk(boolean cvOk) {
		this.cvOk = cvOk;
	}

	public boolean getPreocupacionalOk() {
		return preocupacionalOk;
	}

	public void setPreocupacionalOk(boolean preocupacionalOk) {
		this.preocupacionalOk = preocupacionalOk;
	}

	public Date getFechaRealizacionPreocupacional() {
		return fechaRealizacionPreocupacional;
	}

	public void setFechaRealizacionPreocupacional(Date fechaRealizacionPreocupacional) {
		this.fechaRealizacionPreocupacional = fechaRealizacionPreocupacional;
	}

	public boolean getDniOK() {
		return dniOK;
	}

	public void setDniOK(boolean dniOK) {
		this.dniOK = dniOK;
	}

	public boolean getCuilOK() {
		return cuilOK;
	}

	public void setCuilOK(boolean cuilOK) {
		this.cuilOK = cuilOK;
	}

	public boolean getConstanciaDomicilioOK() {
		return constanciaDomicilioOK;
	}

	public void setConstanciaDomicilioOK(boolean constanciaDomicilioOK) {
		this.constanciaDomicilioOK = constanciaDomicilioOK;
	}

	public boolean getSeguroOK() {
		return seguroOK;
	}

	public void setSeguroOK(boolean seguroOK) {
		this.seguroOK = seguroOK;
	}

	public boolean getArtOK() {
		return artOK;
	}

	public void setArtOK(boolean artOK) {
		this.artOK = artOK;
	}

	public DescriptivoGeneric getTipoJornada() {
		return tipoJornada;
	}

	public void setTipoJornada(DescriptivoGeneric tipoJornada) {
		this.tipoJornada = tipoJornada;
	}

	public DescriptivoGeneric getSector() {
		return sector;
	}

	public void setSector(DescriptivoGeneric sector) {
		this.sector = sector;
	}

	public String getFuncion() {
		return funcion;
	}

	public void setFuncion(String funcion) {
		this.funcion = funcion;
	}

	public String getResponsabilidades() {
		return responsabilidades;
	}

	public void setResponsabilidades(String responsabilidades) {
		this.responsabilidades = responsabilidades;
	}

	public Float getRemuneracion() {
		return remuneracion;
	}

	public void setRemuneracion(Float remuneracion) {
		this.remuneracion = remuneracion;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	@Override
	public String getDescripcion() {
		return this.getNombre();
	}

	public DescriptivoGeneric getTipoEmpleado() {
		return tipoEmpleado;
	}

	public void setTipoEmpleado(DescriptivoGeneric tipoEmpleado) {
		this.tipoEmpleado = tipoEmpleado;
	}

	public List<AltaEmpleadoVO> getAltas() {
		return altas;
	}

	public void setAltas(List<AltaEmpleadoVO> altas) {
		this.altas = altas;
	}

	public AltaEmpleadoVO getAlta(Long id) {
		return this.altas.stream().filter(i -> i.getId() != null && i.getId().equals(id)).findFirst().orElse(null);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public DescriptivoGeneric getCentro() {
		return centro;
	}

	public void setCentro(DescriptivoGeneric centro) {
		this.centro = centro;
	}

}

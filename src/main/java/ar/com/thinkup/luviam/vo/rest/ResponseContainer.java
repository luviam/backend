package ar.com.thinkup.luviam.vo.rest;

import java.io.Serializable;

/**
 * 
 * @author ThinkUp
 *
 * @param <T> Tipo de contenido que tendra el objeto de respuesta
 * 
 */
public class ResponseContainer<T>implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private T respuesta;
	private String mensaje;

	

	public ResponseContainer(T respuesta, String mensaje) {
		super();
		this.respuesta = respuesta;
		this.mensaje = mensaje;
	}
	public ResponseContainer(T respuesta) {
		super();
		this.respuesta = respuesta;
		this.mensaje = "";
	}

	public ResponseContainer(){
		super();
	}
	public T getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(T respuesta) {
		this.respuesta = respuesta;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	
}

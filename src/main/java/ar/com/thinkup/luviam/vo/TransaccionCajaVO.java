package ar.com.thinkup.luviam.vo;

public class TransaccionCajaVO {

	private Double monto;

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

}

package ar.com.thinkup.luviam.vo;

import java.io.Serializable;

import ar.com.thinkup.luviam.model.Salon;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public class SalonVO implements Serializable, Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8401029824575853269L;

	private Long id;

	private String nombre;

	private String direccion;

	private String codigoPostal;

	private DescriptivoGeneric provincia;

	private String nombreContacto;

	private String telefono;

	private String email;

	private ProductoVO productoAlquiler;

	public float latitud;

	public float longitud;

	private Boolean activo;
	private Boolean esComisionista;

	public SalonVO() {
		super();
	}

	public SalonVO(Salon ent) {
		super();
		this.id = ent.getId();
		this.nombre = ent.getNombre();
		this.direccion = ent.getDireccion();
		this.codigoPostal = ent.getCodigoPostal();
		this.provincia = new DescriptivoGeneric(ent.getProvincia());
		this.nombreContacto = ent.getNombreContacto();
		this.telefono = ent.getTelefono();
		this.email = ent.getEmail();
		this.productoAlquiler = ent.getProducto() != null ? new ProductoVO(ent.getProducto()) : null;
		this.latitud = ent.getLatitud();
		this.longitud = ent.getLongitud();
		this.activo = ent.getActivo();
		this.esComisionista = ent.getComisionista() != null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public DescriptivoGeneric getProvincia() {
		return provincia;
	}

	public void setProvincia(DescriptivoGeneric provincia) {
		this.provincia = provincia;
	}

	public String getNombreContacto() {
		return nombreContacto;
	}

	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ProductoVO getProductoAlquiler() {
		return productoAlquiler;
	}

	public void setProductoAlquiler(ProductoVO productoAlquiler) {
		this.productoAlquiler = productoAlquiler;
	}

	public float getLatitud() {
		return latitud;
	}

	public void setLatitud(float latitud) {
		this.latitud = latitud;
	}

	public float getLongitud() {
		return longitud;
	}

	public void setLongitud(float longitud) {
		this.longitud = longitud;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Boolean getEsComisionista() {
		return esComisionista;
	}

	public void setEsComisionista(Boolean esComisionista) {
		this.esComisionista = esComisionista;
	}

}

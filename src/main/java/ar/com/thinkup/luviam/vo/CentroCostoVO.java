package ar.com.thinkup.luviam.vo;

import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public class CentroCostoVO {

	private Long id;

	private String codigoCentro;

	private String descripcionCentro;

	private String razonSocial;

	private String cuit;

	private String tipoIIBB;

	private String iibb;

	private String domicilioFiscal;

	private Boolean activo;

	private DescriptivoGeneric provincia;

	private List<CuentaConfig> cuentas = new Vector<>();

	private List<CentroCostosComprobantesVO> comprobantes = new Vector<>();

	private String codigoPostal;

	public CentroCostoVO() {
			this.comprobantes = new Vector<>();
			this.cuentas = new Vector<>();
	}

	public CentroCostoVO(CentroCosto centroDB) {
		super();
		this.id = centroDB.getId();
		this.codigoCentro = centroDB.getCodigo();
		this.descripcionCentro = centroDB.getNombre();
		this.razonSocial = centroDB.getRazonSocial();
		this.cuit = centroDB.getCuit();
		this.tipoIIBB = centroDB.getTipoIIBB();
		this.iibb = centroDB.getIibb();
		this.domicilioFiscal = centroDB.getDomicilioFiscal();
		this.activo = centroDB.getActivo() != null ? centroDB.getActivo() : true;
		this.codigoPostal = centroDB.getCodigoPostal();
		if (centroDB.getProvincia() != null)
			this.provincia = new DescriptivoGeneric(centroDB.getProvincia());

		this.comprobantes = centroDB.getComprobantesDisponibles().stream()
				.map(cdb -> new CentroCostosComprobantesVO(cdb)).collect(Collectors.toList());
		this.cuentas = centroDB.getCuentasAplicacion().stream().map(cuenta -> new CuentaConfig(cuenta))
				.collect(Collectors.toList());

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigoCentro() {
		return codigoCentro;
	}

	public void setCodigoCentro(String codigoCentro) {
		this.codigoCentro = codigoCentro;
	}

	public String getDescripcionCentro() {
		return descripcionCentro;
	}

	public void setDescripcionCentro(String descripcionCentro) {
		this.descripcionCentro = descripcionCentro;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public String getTipoIIBB() {
		return tipoIIBB;
	}

	public void setTipoIIBB(String tipoIIBB) {
		this.tipoIIBB = tipoIIBB;
	}

	public String getIibb() {
		return iibb;
	}

	public void setIibb(String iibb) {
		this.iibb = iibb;
	}

	public String getDomicilioFiscal() {
		return domicilioFiscal;
	}

	public void setDomicilioFiscal(String domicilioFiscal) {
		this.domicilioFiscal = domicilioFiscal;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public List<CuentaConfig> getCuentas() {
		return cuentas;
	}

	public void setCuentas(List<CuentaConfig> cuentas) {
		this.cuentas = cuentas;
	}

	public List<CentroCostosComprobantesVO> getComprobantes() {
		return comprobantes;
	}

	public void setComprobantes(List<CentroCostosComprobantesVO> comprobantes) {
		this.comprobantes = comprobantes;
	}

	public DescriptivoGeneric getProvincia() {
		return provincia;
	}

	public void setProvincia(DescriptivoGeneric provincia) {
		this.provincia = provincia;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public CentroCostosComprobantesVO getComprobante(String codigo) {
		return this.comprobantes.stream()
				.filter(cc -> StringUtils.isNotEmpty(cc.getCodigo()) && cc.getCodigo().equals(codigo)).findFirst()
				.orElse(null);
	}

	public CuentaConfig getCuenta(Long id) {
		return this.cuentas.stream().filter(cc -> cc.getId()!= null && cc.getId().equals(id) ).findFirst().orElse(null);
	}

}

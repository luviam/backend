package ar.com.thinkup.luviam.vo;

import ar.com.thinkup.luviam.model.MarcaProductos;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;

public class MarcaVO extends ParametricoVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7626089345962014815L;
	
	private ArchivoVO logo;

	public MarcaVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MarcaVO(MarcaProductos ent) {
		super(ent);
		if(ent.getArchivo()!= null) {
			this.logo = new ArchivoVO(ent.getArchivo());
		}
	}

	public ArchivoVO getLogo() {
		return logo;
	}

	public void setLogo(ArchivoVO logo) {
		this.logo = logo;
	}

	

	
}

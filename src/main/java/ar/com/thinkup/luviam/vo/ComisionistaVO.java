package ar.com.thinkup.luviam.vo;

import ar.com.thinkup.luviam.model.Comisionista;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.vo.parametricos.TipoComisionistaVO;

public class ComisionistaVO extends DescriptivoGeneric implements Identificable {

	private static final long serialVersionUID = 7078791219367677738L;
	private Long id;

	private String nombre;

	private String contacto;

	private String domicilio;

	private String localidad;

	private String telefono;

	private String celular;

	private String email;

	private String razonSocial;

	private String cuit;

	private DescriptivoGeneric iva;

	private String domicilioFacturacion;

	private String localidadFacturacion;

	private TipoComisionistaVO tipoComisionista;

	private String nombreBanco;

	private String cbu;

	private String numeroCuenta;

	private String aliasBancario;

	private Boolean activo;

	private DescriptivoGeneric salon;
	private DescriptivoGeneric vendedor;

	public ComisionistaVO() {
		super();
	}

	public ComisionistaVO(Comisionista ent) {
		super(ent);
		this.id = ent.getId();
		this.nombre = ent.getNombre();
		this.contacto = ent.getContacto();
		this.domicilio = ent.getDomicilio();
		this.localidad = ent.getLocalidad();
		this.telefono = ent.getTelefono();
		this.celular = ent.getCelular();
		this.email = ent.getEmail();
		this.razonSocial = ent.getRazonSocial();
		this.cuit = ent.getCuit();
		this.iva = new DescriptivoGeneric(ent.getIva());
		this.domicilioFacturacion = ent.getDomicilioFacturacion();
		this.localidadFacturacion = ent.getLocalidadFacturacion();
		this.tipoComisionista = new TipoComisionistaVO(ent.getTipoComisionista());
		this.nombreBanco = ent.getNombreBanco();
		this.cbu = ent.getCbu();
		this.numeroCuenta = ent.getNumeroCuenta();
		this.aliasBancario = ent.getAliasBancario();
		this.activo = ent.getActivo();
		if (ent.getSalon() != null) {
			this.salon = new DescriptivoGeneric(ent.getSalon());
		} else if (ent.getVendedor() != null) {
			this.vendedor = new DescriptivoGeneric(ent.getVendedor());

		}

	}

	@Override
	public Long getId() {
		return this.id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public DescriptivoGeneric getIva() {
		return iva;
	}

	public void setIva(DescriptivoGeneric iva) {
		this.iva = iva;
	}

	public String getDomicilioFacturacion() {
		return domicilioFacturacion;
	}

	public void setDomicilioFacturacion(String domicilioFacturacion) {
		this.domicilioFacturacion = domicilioFacturacion;
	}

	public String getLocalidadFacturacion() {
		return localidadFacturacion;
	}

	public void setLocalidadFacturacion(String localidadFacturacion) {
		this.localidadFacturacion = localidadFacturacion;
	}

	public TipoComisionistaVO getTipoComisionista() {
		return tipoComisionista;
	}

	public void setTipoComisionista(TipoComisionistaVO tipoComisionista) {
		this.tipoComisionista = tipoComisionista;
	}

	public String getNombreBanco() {
		return nombreBanco;
	}

	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}

	public String getCbu() {
		return cbu;
	}

	public void setCbu(String cbu) {
		this.cbu = cbu;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getAliasBancario() {
		return aliasBancario;
	}

	public void setAliasBancario(String aliasBancario) {
		this.aliasBancario = aliasBancario;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DescriptivoGeneric getSalon() {
		return salon;
	}

	public void setSalon(DescriptivoGeneric salon) {
		this.salon = salon;
	}

	public DescriptivoGeneric getVendedor() {
		return vendedor;
	}

	public void setVendedor(DescriptivoGeneric vendedor) {
		this.vendedor = vendedor;
	}

}
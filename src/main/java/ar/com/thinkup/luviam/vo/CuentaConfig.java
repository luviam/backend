package ar.com.thinkup.luviam.vo;

import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;

public class CuentaConfig {

	private Long id;
	private String codigo;
	private String descripcion;
	private Boolean activo;
	private String tipoCuenta;
	private Long idParent;
	private Boolean eliminable;

	public CuentaConfig() {

	}

	public CuentaConfig(CuentaAplicacion ca) {
		super();
		this.id = ca.getId();
		this.codigo = ca.getCodigo();
		this.descripcion = ca.getNombre();
		this.activo =ca.getActivo();
		this.idParent = ca.getSubGrupo().getId();
		this.eliminable = ca.getEliminable();
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	

	public String getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public Long getIdParent() {
		return idParent;
	}

	public void setIdParent(Long idParent) {
		this.idParent = idParent;
	}

	public Boolean getEliminable() {
		return eliminable;
	}

	public void setEliminable(Boolean eliminable) {
		this.eliminable = eliminable;
	}
	

	
}

package ar.com.thinkup.luviam.vo.parametricos;

import java.io.Serializable;

import ar.com.thinkup.luviam.model.Parametrico;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.vo.Identificable;

public class ParametricoVO extends DescriptivoGeneric implements Serializable, Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5720990901992485894L;

	private Long id;

	private Boolean habilitado;
	private Boolean esSistema;

	public ParametricoVO() {
		super();
	}

	public ParametricoVO(Parametrico ent) {
		super(ent);
		this.id = ent.getId();
		this.habilitado = ent.getHabilitado();
		this.esSistema = ent.getEsSistema();

	}

	public ParametricoVO(ParametricoVO ent) {
		super(ent);
		this.id = ent.getId();
		this.habilitado = ent.getHabilitado();
		this.esSistema = ent.getEsSistema();

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

	public Boolean getEsSistema() {
		return esSistema;
	}

	public void setEsSistema(Boolean esSistema) {
		this.esSistema = esSistema;
	}

}
package ar.com.thinkup.luviam.vo.rest;

import java.io.Serializable;

public class RequestContainer<T> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private T request;
//	private String usuarioLogeado;
	
	public T getRequest() {
		return request;
	}
	public void setRequest(T request) {
		this.request = request;
	}
//	public String getUsuarioLogeado() {
//		return usuarioLogeado;
//	}
//	public void setUsuarioLogeado(String usuarioLogeado) {
//		this.usuarioLogeado = usuarioLogeado;
//	}

}

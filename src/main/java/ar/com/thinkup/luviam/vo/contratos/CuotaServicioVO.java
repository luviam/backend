package ar.com.thinkup.luviam.vo.contratos;

import java.io.Serializable;
import java.util.Date;

import ar.com.thinkup.luviam.model.contratos.CuotaServicio;
import ar.com.thinkup.luviam.vo.Identificable;
import ar.com.thinkup.luviam.vo.ProductoVO;

public class CuotaServicioVO implements Serializable, Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6577075129932083921L;

	private Long id;

	private Date fechaContrato;

	private Date fechaVencimiento;

	private Date fechaEstimadaPago;

	private Double cantidad;

	private Double costoUnitario;

	private Double pIncremento;

	private Double monto;

	private Double saldo;

	private ProductoVO producto;

	private String descripcion;
	private String grupo;

	public CuotaServicioVO() {
		super();
	}

	public CuotaServicioVO(CuotaServicio ent) {
		super();
		this.id = ent.getId();

		this.fechaContrato = ent.getFechaContrato();
		this.fechaVencimiento = ent.getFechaVencimiento();
		this.fechaEstimadaPago = ent.getFechaEstimadaPago();
		this.cantidad = ent.getCantidad();
		this.costoUnitario = ent.getCostoUnitario();
		this.monto = ent.getMonto();
		this.saldo = ent.getSaldo();
		this.producto = new ProductoVO(ent.getServicio().getProducto());
		this.pIncremento = ent.getIncremento();
		this.descripcion = ent.getDescripcion();
		this.grupo = ent.getGrupo();

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaContrato() {
		return fechaContrato;
	}

	public void setFechaContrato(Date fechaContrato) {
		this.fechaContrato = fechaContrato;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public Date getFechaEstimadaPago() {
		return fechaEstimadaPago;
	}

	public void setFechaEstimadaPago(Date fechaEstimadaPago) {
		this.fechaEstimadaPago = fechaEstimadaPago;
	}

	public Double getCantidad() {
		return cantidad;
	}

	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}

	public Double getCostoUnitario() {
		return costoUnitario;
	}

	public void setCostoUnitario(Double costoUnitario) {
		this.costoUnitario = costoUnitario;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public ProductoVO getProducto() {
		return producto;
	}

	public void setProducto(ProductoVO producto) {
		this.producto = producto;
	}

	public Double getpIncremento() {
		return pIncremento;
	}

	public void setpIncremento(Double pIncremento) {
		this.pIncremento = pIncremento;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

}
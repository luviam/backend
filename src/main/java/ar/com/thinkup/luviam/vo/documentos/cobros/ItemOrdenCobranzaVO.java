package ar.com.thinkup.luviam.vo.documentos.cobros;

import ar.com.thinkup.luviam.repository.ItemOrdenCobranzaRepository.ItemOrdenCobranzaCabecera;
import ar.com.thinkup.luviam.vo.Identificable;
import ar.com.thinkup.luviam.vo.contratos.CuotaServicioVO;

public class ItemOrdenCobranzaVO implements Identificable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3495284072422269565L;
	private Long id;
	private CuotaServicioVO cuotaServicio;
	private Double importe;

	public ItemOrdenCobranzaVO() {
		super();
	}

	public ItemOrdenCobranzaVO(ItemOrdenCobranzaCabecera item) {
		this.id = item.getId();
		this.importe = item.getImporte();
		this.cuotaServicio = new CuotaServicioVO(item.getCuotaServicio());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CuotaServicioVO getCuotaServicio() {
		return cuotaServicio;
	}

	public void setCuotaServicio(CuotaServicioVO cuotaServicio) {
		this.cuotaServicio = cuotaServicio;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

}

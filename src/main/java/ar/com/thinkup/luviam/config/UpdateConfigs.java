package ar.com.thinkup.luviam.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ar.com.thinkup.luviam.service.def.ISistemParametersService;

@Component
public class UpdateConfigs implements InitializingBean  {
 
	private static final Logger LOG = LogManager.getLogger(UpdateConfigs.class);
	

	@Autowired
	private ISistemParametersService sysParamsService;
	
		
	@Override
	public void afterPropertiesSet() throws Exception {

		
		this.sysParamsService.initialize();
	}
 
  
}
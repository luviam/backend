package ar.com.thinkup.luviam.config.jwt;

import java.io.Serializable;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * 
 * @author ThinkUp
 *
 */
public class JwtAuthenticationResponse implements Serializable {

    private static final long serialVersionUID = 1250166508152483573L;

    private final String token;
    private final UserDetails usuario;

    public JwtAuthenticationResponse(String token, UserDetails usuario) {
        this.token = token;
        this.usuario = usuario;
    }

    public String getToken() {
        return this.token;
    }
    
    public UserDetails getUsuario() {
    	return usuario;
    }
}

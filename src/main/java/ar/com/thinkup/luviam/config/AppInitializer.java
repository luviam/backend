package ar.com.thinkup.luviam.config;

import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import ar.com.thinkup.luviam.config.jwt.JwtAuthenticationTokenFilter;
import ar.com.thinkup.luviam.model.SistemParameter;


public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { AppConfig.class, WebSecurityConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	@Override
	protected Filter[] getServletFilters() {
		return new Filter[] { new JwtAuthenticationTokenFilter() };
	}
	
    @Override
    protected void customizeRegistration(Dynamic registration) {

        MultipartConfigElement multiPart = new MultipartConfigElement(SistemParameter.TEMPORAL_URL,
                1024 * 1024 * 5, 1024 * 1024 * 10, 1024 * 1024 * 3);

        registration.setMultipartConfig(multiPart);
    }
	
	

}

package ar.com.thinkup.luviam.config.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.repository.UsuarioRepository;

/**
 * 
 * @author ThinkUp
 *
 */
@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private UsuarioRepository userRepo;
	
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    		Usuario u = userRepo.findByUsernameAndActivoIsTrue(username);
    		if(u== null){
    			throw new UsernameNotFoundException(username);
    		}
            return u;
        
    }
}

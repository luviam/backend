package ar.com.thinkup.luviam.exceptions;

public class SubGrupoNoExistente extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6428530074040444957L;

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SubGrupoNoExistente(Long id) {
		super();
		this.id = id;
	}

	public SubGrupoNoExistente(String string) {
		super(string);
	}

	
}

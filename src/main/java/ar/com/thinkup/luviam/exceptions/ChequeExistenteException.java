package ar.com.thinkup.luviam.exceptions;

import ar.com.thinkup.luviam.model.Cheque;

public class ChequeExistenteException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1595983377619587225L;

	private String banco;

	private String numero;

	public ChequeExistenteException(Cheque c) {
		super("Ya existe el cheque del banco " + c.getBanco() + " N° " + c.getNumero());
		this.banco = c.getBanco();
		this.numero = c.getNumero();
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

}

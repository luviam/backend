package ar.com.thinkup.luviam.exceptions;

public class RubroNoExistenteException extends RuntimeException {

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RubroNoExistenteException(Long id) {
		super();
		this.id = id;
	}
	
	
}

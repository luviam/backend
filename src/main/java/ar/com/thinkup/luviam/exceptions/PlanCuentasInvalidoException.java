package ar.com.thinkup.luviam.exceptions;

public class PlanCuentasInvalidoException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PlanCuentasInvalidoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}

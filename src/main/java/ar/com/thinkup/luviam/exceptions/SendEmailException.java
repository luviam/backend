package ar.com.thinkup.luviam.exceptions;

public class SendEmailException extends Exception {

	public SendEmailException(Exception ex) {
		super(ex);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -9066531360186967673L;

}

package ar.com.thinkup.luviam.exceptions;

public class ComprobanteNoExistenteExcepcion extends Exception {

	public ComprobanteNoExistenteExcepcion(Exception ex) {
		super(ex);
	}

	public ComprobanteNoExistenteExcepcion(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -9066531360186967673L;

}

package ar.com.thinkup.luviam.scheduler;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import ar.com.thinkup.luviam.service.def.ILiquidacionComisionesService;

@Component
@Transactional
public class LiquidacionVendedoresScheduler {

	@Autowired
	private ILiquidacionComisionesService service;

	@Scheduled(cron = "0 0 0 1 * ?") // Corre todos los comienzos de mes
	public void generarLiquidadcionesVendedores() {

		this.service.generarLiquidacionesVendedoresMensuales();
	}
}

package ar.com.thinkup.luviam.scheduler;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import ar.com.thinkup.luviam.exceptions.CajaCerradaException;
import ar.com.thinkup.luviam.model.TransaccionCaja;
import ar.com.thinkup.luviam.model.enums.CuentasPrincipalesEnum;
import ar.com.thinkup.luviam.model.enums.TipoTransaccionCajaEnum;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.repository.CuentaAplicacionRepository;
import ar.com.thinkup.luviam.service.def.ICajaService;

@Component
@Transactional
public class CierreCajaScheduler implements Serializable {

	private static final long serialVersionUID = 3469022311817086757L;

	private static final Logger LOG = LogManager.getLogger(CierreCajaScheduler.class);
	
	@Autowired
	private ICajaService cajaService;
	
	@Autowired
	private CuentaAplicacionRepository cuentasRepo;


	@Scheduled(cron = "0 55 23 ? * *")
	public void cerrarCajas() {
		
		List<CuentaAplicacion> cajas = cuentasRepo.findBySubGrupoCodigoAndCentroCostoActivoTrueOrderByCodigoAsc(CuentasPrincipalesEnum.CAJA.getCodigo());
		if (!CollectionUtils.isEmpty(cajas)){
			cajas.forEach(caja->{
				TransaccionCaja trx = this.cajaService.getEstadoCaja(caja.getId());
				if (trx!=null && TipoTransaccionCajaEnum.APERTURA.getCodigo().equals(trx.getTipo())) {
					try {
						cajaService.cerrarCaja(caja, null, null);
					} catch (CajaCerradaException e) {
						// TODO Auto-generated catch block
						LOG.error("No se pudo cerrar la caja "+caja.getId(), e);
					}
				}
			});
		}
		
	}
	
}

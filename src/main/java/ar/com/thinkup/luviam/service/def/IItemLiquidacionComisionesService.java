package ar.com.thinkup.luviam.service.def;

import java.util.List;

import ar.com.thinkup.luviam.model.documentos.comisiones.ItemLiquidacionComisiones;
import ar.com.thinkup.luviam.vo.ItemLiquidacionComisionesVO;

public interface IItemLiquidacionComisionesService extends IModelService<ItemLiquidacionComisionesVO,ItemLiquidacionComisiones>{

	List<ItemLiquidacionComisionesVO> getByLiquidacionId(Long idLiquidacion);

	
}

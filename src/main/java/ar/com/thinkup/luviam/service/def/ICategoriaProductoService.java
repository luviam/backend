package ar.com.thinkup.luviam.service.def;

import java.util.List;

import ar.com.thinkup.luviam.model.CategoriaProducto;
import ar.com.thinkup.luviam.vo.CategoriaProductoVO;

public interface ICategoriaProductoService extends IParametricoService<CategoriaProductoVO, CategoriaProducto> {

	List<CategoriaProductoVO> getComisionables();

}

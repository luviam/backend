package ar.com.thinkup.luviam.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.Comisionista;
import ar.com.thinkup.luviam.model.Salon;
import ar.com.thinkup.luviam.model.TipoComisionista;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.empleados.Empleado;
import ar.com.thinkup.luviam.repository.CabeceraComisionistaRepository;
import ar.com.thinkup.luviam.repository.ComisionistaRepository;
import ar.com.thinkup.luviam.service.def.IComisionistaService;
import ar.com.thinkup.luviam.service.def.IEmpleadoService;
import ar.com.thinkup.luviam.service.def.ISalonService;
import ar.com.thinkup.luviam.service.def.ITipoComisionistaService;
import ar.com.thinkup.luviam.service.def.ITipoIVAService;
import ar.com.thinkup.luviam.vo.CabeceraComisionistaVO;
import ar.com.thinkup.luviam.vo.ComisionistaVO;
import ar.com.thinkup.luviam.vo.SalonVO;
import ar.com.thinkup.luviam.vo.empleados.EmpleadoVO;

@Service("IComisionistaService")
public class ComisionistaService extends ModelService<Comisionista, ComisionistaVO, ComisionistaRepository>
		implements IComisionistaService {

	@Autowired
	private ComisionistaRepository repo;

	@Autowired
	private CabeceraComisionistaRepository cabRepo;

	@Autowired
	private ITipoIVAService tiService;

	@Autowired
	private ITipoComisionistaService tcService;

	@Autowired
	private ISalonService salonService;

	@Autowired
	private IEmpleadoService empleadoService;

	@Override
	public Comisionista getByDescriptivo(IDescriptivo descriptivo) {
		if (descriptivo == null || StringUtils.isEmpty(descriptivo.getCodigo())) {
			throw new IllegalArgumentException("El Comisionista es obligatorio");
		}
		Comisionista a = this.getRepository().findOne(Long.valueOf(descriptivo.getCodigo()));
		return a;
	}

	@Override
	protected ComisionistaRepository getRepository() {
		return this.repo;
	}

	@Override
	protected ComisionistaVO toVO(Comisionista ent) {

		return new ComisionistaVO(ent);
	}

	@Override
	protected Comisionista newEnt() {
		return new Comisionista();
	}

	@Override
	public Comisionista parseToEnt(ComisionistaVO vo, Comisionista e) {
		Comisionista ent = e;
		if (ent == null)
			ent = new Comisionista();
		ent.setNombre(vo.getNombre());
		ent.setContacto(vo.getContacto());
		ent.setDomicilio(vo.getDomicilio());
		ent.setLocalidad(vo.getLocalidad());
		ent.setTelefono(vo.getTelefono());
		ent.setCelular(vo.getCelular());
		ent.setEmail(vo.getEmail());
		ent.setRazonSocial(vo.getRazonSocial());
		ent.setCuit(vo.getCuit());
		if(vo.getActivo() == null) {
			ent.setActivo(true);
		}else {
			ent.setActivo(vo.getActivo());
		}
		if (vo.getIva() != null) {
			ent.setIva(this.tiService.getByDescriptivo(vo.getIva()));
		}
		ent.setDomicilioFacturacion(vo.getDomicilioFacturacion());
		ent.setLocalidadFacturacion(vo.getLocalidadFacturacion());
		ent.setTipoComisionista(this.tcService.getByDescriptivo(vo.getTipoComisionista()));
		ent.setNombreBanco(vo.getNombreBanco());
		ent.setCbu(vo.getCbu());
		ent.setNumeroCuenta(vo.getNumeroCuenta());
		ent.setAliasBancario(vo.getAliasBancario());
		if (ent.getTipoComisionista().getCodigo().equals(TipoComisionista.SALON)) {
			ent.setSalon(this.salonService.getByDescriptivo(vo.getSalon()));
			ent.setVendedor(null);
		} else if (ent.getTipoComisionista().getCodigo().equals(TipoComisionista.VENDEDOR)) {
			ent.setSalon(null);
			ent.setVendedor(this.empleadoService.getByDescriptivo(vo.getVendedor()));
		} else {
			ent.setVendedor(null);
			ent.setSalon(null);
		}

		return ent;
	}

	@Override
	@Transactional
	public List<CabeceraComisionistaVO> getCabeceras() {

		return this.cabRepo.findAll().stream().map(c -> new CabeceraComisionistaVO(c)).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public Comisionista updateByEmpleado(Empleado e) {
		Comisionista c = new Comisionista();
		if (e.getId() != null) {
			c = this.repo.findOneByVendedorId(e.getId());
			if (c == null) {
				c = new Comisionista();
			}
		}
		c.setActivo(e.getActivo());
		c.setNombre(e.getNombre());
		c.setContacto(e.getNombre());
		c.setTelefono(e.getTelefono());
		c.setCuit(e.getCuil());
		c.setEmail(e.getEmail());
		c.setDomicilio(e.getDomicilio());
		c.setTipoComisionista(this.tcService.getByCodigo(TipoComisionista.VENDEDOR));
		c.setVendedor(e);
		c.setSalon(null);
		return c;

	}

	@Override
	@Transactional
	public Comisionista updateBySalon(Salon s) {
		Comisionista c = new Comisionista();
		if (s.getId() != null) {
			c = this.repo.findOneBySalonId(s.getId());
			if (c == null) {
				c = new Comisionista();
			}
		}
		c.setActivo(s.getActivo());
		c.setNombre(s.getNombre());
		c.setContacto(s.getNombreContacto());
		c.setTelefono(s.getTelefono());
		c.setEmail(s.getEmail());
		c.setDomicilio(s.getDireccion());
		c.setTipoComisionista(this.tcService.getByCodigo(TipoComisionista.SALON));
		c.setVendedor(null);
		c.setSalon(s);
		return c;
	}

	@Override
	public Comisionista getByCodigo(String codigo) {
		return this.repo.findOne(Long.valueOf(codigo));
	}

	@Override
	public Boolean delete(Long id) {
		Comisionista c = this.repo.findOne(id);
		if (c == null) {
			throw new IllegalArgumentException("No existe Comisionista con id " + id);
		}
		if (c.getVendedor() != null) {
			try {
				return this.empleadoService.delete(c.getVendedor().getId());
			} catch (JpaSystemException e) {
				c.getVendedor().setActivo(false);
				this.empleadoService.guardar(new EmpleadoVO(c.getVendedor()));
				return true;
			}

		} else if (c.getSalon() != null) {
			c.getSalon().setComisionista(null);
			this.salonService.update(new SalonVO(c.getSalon()));
			return true;
		} else {
			return super.delete(id);
		}

	}

}

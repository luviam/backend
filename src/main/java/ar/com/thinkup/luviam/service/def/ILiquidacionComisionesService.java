package ar.com.thinkup.luviam.service.def;

import java.util.Date;
import java.util.List;

import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.Comisionista;
import ar.com.thinkup.luviam.model.TipoComisionista;
import ar.com.thinkup.luviam.model.documentos.comisiones.LiquidacionComisiones;
import ar.com.thinkup.luviam.model.enums.TipoPagoComision;
import ar.com.thinkup.luviam.vo.ItemLiquidacionComisionesVO;
import ar.com.thinkup.luviam.vo.LiquidacionComisionesVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroComisiones;
import ar.com.thinkup.luviam.vo.filtros.FiltroComisionista;

public interface ILiquidacionComisionesService extends IDocumentoPago<LiquidacionComisiones, LiquidacionComisionesVO> {
	List<LiquidacionComisionesVO> getCabeceras(FiltroComisionista filtro);

	List<ItemLiquidacionComisionesVO> getItems(Long idLiquidacion);

	List<LiquidacionComisionesVO> getComisionesByFitro(FiltroComisiones filtro);

	LiquidacionComisiones generarLiquidacion(Comisionista comisionista, Date fechaDesde, Date fechaHasta,
			List<TipoPagoComision> tiposComisiones, CentroCosto centro);

	List<LiquidacionComisionesVO> generarLiquidaciones(List<TipoPagoComision> tiposComisiones,
			List<TipoComisionista> tiposComisionista, Date desde, Date hasta);

	List<LiquidacionComisionesVO> generarLiquidacionesMensuales(List<TipoPagoComision> tiposComisiones,
			List<TipoComisionista> tiposComisionista);

	List<LiquidacionComisionesVO> generarLiquidacionesVendedoresMensuales();

}

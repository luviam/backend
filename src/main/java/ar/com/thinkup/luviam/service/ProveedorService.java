package ar.com.thinkup.luviam.service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.ImputacionCuentaConfig;
import ar.com.thinkup.luviam.model.Proveedor;
import ar.com.thinkup.luviam.model.TipoCuentaProveedor;
import ar.com.thinkup.luviam.model.TipoIVA;
import ar.com.thinkup.luviam.model.TipoProveedor;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.repository.CentroRepository;
import ar.com.thinkup.luviam.repository.CuentaAplicacionRepository;
import ar.com.thinkup.luviam.repository.ProveedorRepository;
import ar.com.thinkup.luviam.repository.TipoCuentaProveedorRepository;
import ar.com.thinkup.luviam.repository.TipoIVARepository;
import ar.com.thinkup.luviam.repository.TipoProveedorRepository;
import ar.com.thinkup.luviam.service.def.IProveedorService;
import ar.com.thinkup.luviam.vo.ImputacionCuentaConfigVO;
import ar.com.thinkup.luviam.vo.ProveedorVO;
@Service("IProveedorService")
@Transactional
public class ProveedorService implements IProveedorService {

	@Autowired
	private ProveedorRepository repo;

	@Autowired
	private TipoIVARepository ivaRepo;

	@Autowired
	private TipoProveedorRepository tipoProveedorRepo;

	@Autowired
	private TipoCuentaProveedorRepository tipoCuentaProveedorRepo;

	@Autowired
	private CuentaAplicacionRepository cuentaRepo;

	@Autowired
	private CentroRepository centroRepo;

	@Override
	@Transactional
	public ProveedorVO getById(Long id) {

		if (id == null) {
			throw new IllegalArgumentException("El id de proveedor es obligatorio");
		}
		Proveedor p = this.repo.findOne(id);
		if (p == null) {
			throw new EntityNotFoundException("No se encuentra el proveedor con ID : " + id);
		}

		return new ProveedorVO(p);
	}

	@Override
	@Transactional
	public ProveedorVO guardarProveedor(ProveedorVO proveedor) {
		final Proveedor p;
		if (proveedor.getId() != null) {
			p = this.repo.getOne(proveedor.getId());
			if (p == null) {
				throw new EntityNotFoundException("No se encuentra el proveedor con ID : " + proveedor.getId());
			}
		} else {
			p = new Proveedor();
		}

		p.setActivo(proveedor.getActivo());
		p.setCelular(proveedor.getCelular());
		p.setCuit(proveedor.getCuit());
		p.setDomicilio(proveedor.getDomicilio());
		p.setDomicilioFacturacion(proveedor.getDomicilioFacturacion());
		p.setEmail(proveedor.getEmail());
		p.setLocalidad(proveedor.getLocalidad());
		p.setLocalidadFacturacion(proveedor.getLocalidadFacturacion());
		p.setNombreContacto(proveedor.getNombreContacto());
		p.setNombreProveedor(proveedor.getNombreProveedor());
		p.setRazonSocial(proveedor.getRazonSocial());
		p.setTelefono(proveedor.getTelefono());
		
		
		if (proveedor.getIva() != null) {
			TipoIVA tipoIva = this.ivaRepo.findByCodigo(proveedor.getIva().getCodigo());
			p.setIva(tipoIva);
		}
		if (proveedor.getTipoProveedor() != null) {
			TipoProveedor tipoProveedor = this.tipoProveedorRepo.findByCodigo(proveedor.getTipoProveedor().getCodigo());
			p.setTipoProveedor(tipoProveedor);
		} else {
			throw new IllegalArgumentException("El tipo de Proveedor es obligatorio");
		}
		p.setActivo(proveedor.getActivo());
		if (proveedor.getTipoCuenta() != null) {
			TipoCuentaProveedor tipoCuentaProveedor = this.tipoCuentaProveedorRepo
					.findByCodigo(proveedor.getTipoCuenta().getCodigo());
			p.setTipoCuenta(tipoCuentaProveedor);
		}
		/*if (!CollectionUtils.isEmpty(proveedor.getImputacionCuentaConfigs())) {
			
			// Busco nuevos items
			proveedor.getImputacionCuentaConfigs().stream().filter(i -> i.getId() == null)
					.forEach(i -> p.addImputacionCuentaConfig(parseToEntity(new ImputacionCuentaConfig(), i)));

			// Busco items modificados
			proveedor.getImputacionCuentaConfigs().stream().filter(i -> i.getId() != null)
					.forEach(i -> this.parseToEntity(p.getItem(i.getId()), i));

			// Quito Eliminados
			p.getImputacionCuentaConfigs().removeIf(i -> i.getId() != null && proveedor.getItem(i.getId()) == null);

		} else {
			throw new IllegalArgumentException("Las configuraciones de cuenta son obligatorios");
		}*/

		return new ProveedorVO(this.repo.save(p));
	}

	private ImputacionCuentaConfig parseToEntity(ImputacionCuentaConfig e, ImputacionCuentaConfigVO vo) {
		e.setCentroCosto(this.getCentro(vo.getCentroCosto()));
		e.setCodigoTipoImputacion(vo.getTipoImputacion().getCodigo());
		e.setCuentaAplicacion(vo.getCuentaAplicacion()== null? null : this.getCuenta(vo.getCuentaAplicacion(), vo.getCentroCosto()));
		return e;
	}

	private CuentaAplicacion getCuenta(DescriptivoGeneric cuenta, DescriptivoGeneric centro) {
		if (cuenta == null || StringUtils.isBlank(cuenta.getCodigo())) {
			throw new IllegalArgumentException("La cuenta es obligatoria");
		}
		if (centro == null || StringUtils.isBlank(centro.getCodigo())) {
			throw new IllegalArgumentException("El Centro es obligatorio");
		}
		CuentaAplicacion c = this.cuentaRepo.findByCodigoAndCentroCostoCodigoAndCentroCostoActivoTrue(cuenta.getCodigo(), centro.getCodigo());
		if (c == null) {
			throw new EntityNotFoundException("No se encuentra la cuenta con codigo: " + cuenta.getCodigo());
		}
		return c;
	}

	private CentroCosto getCentro(DescriptivoGeneric centroCosto) {
		if (centroCosto == null || StringUtils.isBlank(centroCosto.getCodigo())) {
			throw new IllegalArgumentException("El Centro e obligatorio");
		}

		CentroCosto c = this.centroRepo.findOneByCodigo(centroCosto.getCodigo());
		if (c == null) {
			throw new EntityNotFoundException("No se encuentra el centro con codigo: " + centroCosto.getCodigo());
		}
		return c;
	}
}

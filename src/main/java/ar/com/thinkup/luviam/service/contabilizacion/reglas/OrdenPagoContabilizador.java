package ar.com.thinkup.luviam.service.contabilizacion.reglas;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.Operacion;
import ar.com.thinkup.luviam.model.documentos.pagos.OrdenPago;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.service.contabilizacion.IContabilizable;
import ar.com.thinkup.luviam.service.contabilizacion.ICuentasContablesService;
import ar.com.thinkup.luviam.service.contabilizacion.IGeneradorAsiento;

public class OrdenPagoContabilizador implements IGeneradorAsiento<OrdenPago> {


	@Autowired
	private ICuentasContablesService cuentasService;

	@Override
	public Asiento generarAsiento(OrdenPago ordenPago) {

		/*
		 * Activo - activo corriente - banco - cuenta de aplicación de los cheques.
		 * HABER suma de los cheques (del banco que corresponda)
		 * 
		 * Se genera el asiento con estado Pendiente de aprobación.
		 * 
		 * La op tendrá el mismo estado que el asiento.
		 */

		if (ordenPago.getAsientoGenerado() != null) {
			throw new IllegalArgumentException("Orden de Pago ya contabilizada");
		}
		Asiento a = new Asiento();
		a.setDescripcion(ordenPago.getProveedor().getNombreProveedor() + " " + ordenPago.getDescripcion() + "-"
				+ ordenPago.getNumero());
		a.setEstado(EstadoOperacionEnum.APROBADO.getCodigo());
		a.setMensaje("Generado por sistema");
		a.setFecha(ordenPago.getFecha() != null ? ordenPago.getFecha() : new Date());
		a.setResponsable(ordenPago.getResponsable().getUsername());
		a.setTipoGenerador("AA");

		List<Operacion> operaciones = new Vector<>();

		Operacion aProveedoresDebe = new Operacion();
		aProveedoresDebe.setCentro(ordenPago.getCentroCosto());
		aProveedoresDebe.setComprobante("OP N°: " + ordenPago.getNumero());
		aProveedoresDebe.setCuenta(
				this.cuentasService.getCuentaProveedor(ordenPago.getCentroCosto(), ordenPago.getProveedor()));
		
		aProveedoresDebe.setDetalle(ordenPago.getProveedor().getNombreProveedor());
		aProveedoresDebe.setDebe(ordenPago.getItems().stream().filter(i -> i.getImporte() > 0)
				.mapToDouble(i -> new Double(i.getImporte())).sum());
		
		operaciones.add(aProveedoresDebe);

		Operacion aProveedoresHaber = new Operacion();
		aProveedoresHaber.setCentro(ordenPago.getCentroCosto());
		aProveedoresHaber.setComprobante("OP N°: " + ordenPago.getNumero());
		aProveedoresHaber.setCuenta(
				this.cuentasService.getCuentaProveedor(ordenPago.getCentroCosto(), ordenPago.getProveedor()));
		
		aProveedoresHaber.setDetalle(ordenPago.getProveedor().getNombreProveedor());
		aProveedoresHaber.setHaber(Math.abs(ordenPago.getItems().stream().filter(i -> i.getImporte() < 0)
				.mapToDouble(i -> new Double(i.getImporte())).sum()));
		
		operaciones.add(aProveedoresHaber);

		if (ordenPago.getPagoACuenta() != null) {
			Operacion aCuenta = new Operacion();
			aCuenta.setCentro(ordenPago.getCentroCosto());
			aCuenta.setComprobante("Adelanto " + ordenPago.getNumero());
			aCuenta.setCuenta(
					this.cuentasService.getCuentaProveedor(ordenPago.getCentroCosto(), ordenPago.getProveedor()));
		
			aCuenta.setDetalle("A cuenta ");
			aCuenta.setDebe(Math.abs(ordenPago.getPagoACuenta().getImporte()));
			// aProveedoresDebe.setDebe((aProveedoresDebe.getDebe()) > 0 ?
			// aProveedoresDebe.getDebe() - aCuenta.getHaber()
			// : aCuenta.getHaber());
		
			a.agregarOperacion(aCuenta);
		}
		ordenPago.getPagos().forEach(p -> {
			Optional<Operacion> op = operaciones.stream()
					.filter(o -> o.getCentro().getCodigo().equals(p.getCentroCosto().getCodigo())
							&& o.getCuenta().getCodigo().equals(p.getCuenta().getCodigo()))
					.findFirst();
			if (op.isPresent()) {
				op.get().setHaber(op.get().getHaber() + p.getMonto());
			} else {
				Operacion o = new Operacion();
				o.setCentro(p.getCentroCosto());
				if (p.getCheque() != null) {
					o.setComprobante("Cheque " + p.getCheque().getDescripcion());
				}

				o.setCuenta(p.getCuenta());
				o.setHaber(p.getMonto());
				o.setDetalle("Pago proveedor " + ordenPago.getProveedor().getNombreProveedor());
		
				operaciones.add(o);
			}
		});
		operaciones.forEach(o -> a.agregarOperacion(o));
		return a;
	}

	@Override
	public Boolean esAplicable(IContabilizable o) {
		return OrdenPago.class.isAssignableFrom(o.getClass());
	}

}

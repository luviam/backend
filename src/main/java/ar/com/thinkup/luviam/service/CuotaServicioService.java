package ar.com.thinkup.luviam.service;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.thinkup.luviam.model.contratos.CuotaServicio;
import ar.com.thinkup.luviam.repository.CuotaServicioRepository;
import ar.com.thinkup.luviam.service.def.ICuotaServicioService;
import ar.com.thinkup.luviam.vo.contratos.CuotaServicioVO;

@Service("ICuotaServicioService")
public class CuotaServicioService extends ModelService<CuotaServicio, CuotaServicioVO, CuotaServicioRepository>
		implements ICuotaServicioService {

	@Autowired
	private CuotaServicioRepository cuotaRepo;

	@Override
	@Transactional
	public List<CuotaServicio> actualizarSaldos(List<CuotaServicio> cuotas) {

		cuotas.forEach(c -> this.actualizarSaldo(c));
		return cuotas;
	}

	@Override
	@Transactional
	public CuotaServicio actualizarSaldo(CuotaServicio cuota) {
		cuota.setSaldo(
				cuota.getMonto() * (1 + cuota.getIncremento()) - cuotaRepo.getPagos(cuota.getId()).doubleValue());
		this.cuotaRepo.save(cuota);
		return cuota;
	}

	@Override
	protected CuotaServicioRepository getRepository() {
		return this.cuotaRepo;
	}

	@Override
	protected CuotaServicioVO toVO(CuotaServicio ent) {
		return new CuotaServicioVO(ent);
	}

	@Override
	protected CuotaServicio newEnt() {
		return new CuotaServicio();
	}

	@Override
	public CuotaServicio parseToEnt(CuotaServicioVO vo, CuotaServicio entity) {
		CuotaServicio ent = entity;
		if (ent == null)
			ent = new CuotaServicio();
		ent.setFechaContrato(vo.getFechaContrato());
		ent.setFechaVencimiento(vo.getFechaVencimiento());
		ent.setFechaEstimadaPago(vo.getFechaEstimadaPago());
		ent.setCantidad(vo.getCantidad());
		ent.setCostoUnitario(vo.getCostoUnitario());
		ent.setMonto(vo.getCantidad() * vo.getCostoUnitario());
		ent.setIncremento(vo.getpIncremento());
		if (ent.getId() != null) {
			ent.setSaldo(ent.getTotal() - this.cuotaRepo.getPagos(ent.getId()).doubleValue());
		} else {
			ent.setSaldo(ent.getTotal());
		}

		
		if (StringUtils.isEmpty(vo.getDescripcion())) {
			ent.setDescripcion(vo.getProducto() != null ? vo.getProducto().getDescripcion() : null);
		} else {
			ent.setDescripcion(vo.getDescripcion());
		}
		ent.setGrupo(vo.getGrupo());

		return ent;
	}

	@Override
	@Transactional
	public List<CuotaServicio> getCuotasServicioContratado(Long contratoId) {
		return this.cuotaRepo.findByServicioContratoId(contratoId);
	}

	@Override
	@Transactional
	public List<CuotaServicio> getCuotasImpagas(Long idContrato) {
		return this.cuotaRepo.getCuotasImpagas(idContrato);
	}

}

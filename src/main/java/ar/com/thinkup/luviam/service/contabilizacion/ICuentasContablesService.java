package ar.com.thinkup.luviam.service.contabilizacion;

import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.Proveedor;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;

public interface ICuentasContablesService {

	CuentaAplicacion getCuentaProveedor(CentroCosto c, Proveedor p);

	CuentaAplicacion getCuenta(String codigo, CentroCosto centroCosto);

}

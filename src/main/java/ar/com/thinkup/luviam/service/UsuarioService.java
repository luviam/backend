package ar.com.thinkup.luviam.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.repository.UsuarioRepository;
import ar.com.thinkup.luviam.service.def.IUsuarioService;

@Service("IUsuarioService")
@Transactional
public class UsuarioService implements IUsuarioService {

	@Autowired
	private UsuarioRepository repo;

	@Override
	public Usuario getByDescriptivo(IDescriptivo desc) {
		if (desc == null || StringUtils.isEmpty(desc.getCodigo())) {
			throw new IllegalArgumentException("El id es obligatorio para buscar el usuario");
		}
		Usuario u;
		try {
			u = this.repo.findOne(Long.valueOf(desc.getCodigo()));
		} catch (NumberFormatException e) {
			return null;
		}
		return u;
	}

}

package ar.com.thinkup.luviam.service;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.contratos.ConfiguracionComision;
import ar.com.thinkup.luviam.model.contratos.Contrato;
import ar.com.thinkup.luviam.model.contratos.PagoComision;
import ar.com.thinkup.luviam.model.documentos.cobros.OrdenCobranza;
import ar.com.thinkup.luviam.model.enums.TipoComision;
import ar.com.thinkup.luviam.model.enums.TipoPagoComision;
import ar.com.thinkup.luviam.repository.ComisionRepository;
import ar.com.thinkup.luviam.repository.PagoComisionRepository;
import ar.com.thinkup.luviam.service.def.ICentroCostoService;
import ar.com.thinkup.luviam.service.def.IComisionService;
import ar.com.thinkup.luviam.service.def.IComisionistaService;
import ar.com.thinkup.luviam.service.def.IPagoComisionService;
import ar.com.thinkup.luviam.vo.contratos.ConfiguracionComisionVO;

@Service("IComisionService")
public class ComisionService extends ModelService<ConfiguracionComision, ConfiguracionComisionVO, ComisionRepository>
		implements IComisionService {

	@Autowired
	private ComisionRepository repo;

	@Autowired
	private IComisionistaService cService;

	@Autowired
	private ICentroCostoService centroService;

	@Autowired

	private IPagoComisionService pagoService;

	@Autowired
	private PagoComisionRepository pagosRepo;

	@Override
	protected ComisionRepository getRepository() {
		return this.repo;
	}

	@Override
	protected ConfiguracionComisionVO toVO(ConfiguracionComision ent) {

		return new ConfiguracionComisionVO(ent);
	}

	@Override
	protected ConfiguracionComision newEnt() {
		return new ConfiguracionComision();
	}

	@Override
	@Transactional
	public ConfiguracionComision parseToEnt(ConfiguracionComisionVO vo, ConfiguracionComision entity) {

		ConfiguracionComision ent;
		if (entity == null) {
			ent = new ConfiguracionComision();

		} else {
			ent = entity;
		}

		ent.setId(vo.getId());
		ent.setComisionista(this.cService.getByDescriptivo(vo.getComisionista()));
		ent.setPorcentajeComision(vo.getPorcentajeComision());
		ent.setTotalAComisionar(vo.getTotalAComisionar());

		ent.setTipoComision(vo.getTipoComision());
		ent.setCobraAlInicio(vo.getCobraAlInicio());
		ent.setCobraAlLiquidar(vo.getCobraAlLiquidar());
		ent.setCobraPorCobro(vo.getCobraPorCobro());
		ent.setCentroCosto(this.centroService.getByDescriptivo(vo.getCentro()));
		ent.setPorcentajeAdelanto(vo.getPorcentajeAdelanto());

		// Quito Eliminados
		ent.getPagosComision().removeIf(i -> i.getId() != null && vo.getPagoComision(i.getId()) == null);

		// Busco nuevos items
		vo.getPagos().stream().filter(i -> i.getId() == null)
				.forEach(i -> ent.addPagoComision(this.pagoService.parseToEnt(i, new PagoComision())));

		// Busco items modificados
		vo.getPagos().stream().filter(i -> i.getId() != null)
				.forEach(i -> this.pagoService.parseToEnt(i, ent.getPagoComision(i.getId())));

		if (ent.getId() == null) {
			ent.setSaldoComision(ent.getTotalAComisionar());
		} else {
			ent.setSaldoComision(ent.getTotalAComisionar()
					- ent.getPagosComision().stream().mapToDouble(PagoComision::getTotalLiquidado).sum());
		}

		return ent;
	}

	@Override
	@Transactional
	public Contrato generarAdelantos(Contrato contrato) {

		contrato.getComisiones().stream()
				.filter(c -> c.getCobraAlInicio() && c.getSaldoComision() != 0 && c.getPagosComision().size() == 0)
				.forEach(c -> {
					PagoComision p = new PagoComision();
					p.setDescripcion("Adelanto " + c.getServicioComisionado().getProducto().getDescripcion());
					p.setTipoPagoComision(TipoPagoComision.ADELANTO);
					p.setFechaComision(contrato.getFechaContratacion());
					p.setConfigComision(c);

					if (c.getTipoComision().equals(TipoComision.SIN_INCREMENTO)) {
						p.setTotalComision(c.getServicioComisionado().getCantidad() * (c.getPorcentajeComision() / 100)
								* c.getServicioComisionado().getCostoUnitario() * (c.getPorcentajeAdelanto() / 100));
					} else {
						p.setTotalComision(c.getServicioComisionado().getTotal() * (c.getPorcentajeAdelanto() / 100)
								* (c.getPorcentajeComision() / 100));
					}
					p.setCantidad(c.getServicioComisionado().getCantidad());
					c.setSaldoComision(c.getSaldoComision() - p.getTotalComision());
					c.addPagoComision(p);

				});

		return contrato;

	}

	@Override
	@Transactional
	public OrdenCobranza generarPorCobros(OrdenCobranza oc) {

		Contrato contrato = oc.getContrato();
		List<PagoComision> nuevas = new Vector<>();
		contrato.getComisiones().stream().filter(c -> c.getCobraPorCobro() && c.getSaldoComision() > 0).forEach(c -> {
			PagoComision p = new PagoComision();
			p.setDescripcion("Por Cobro " + c.getServicioComisionado().getProducto().getDescripcion());
			p.setTotalLiquidado(0d);
			p.setTipoPagoComision(TipoPagoComision.POR_PAGO);
			p.setConfigComision(c);
			p.setFechaComision(oc.getFecha());

			Double aComisionar = oc.getItems().stream().filter(
					cob -> cob.getCuotaServicio().getServicio().getId().equals(c.getServicioComisionado().getId()))
					.mapToDouble(cob -> cob.getImporte()).sum();
			if (aComisionar > 0) {
				p.setTotalComision((oc.getTotal() * (c.getPorcentajeComision() / 100)));
				c.addPagoComision(p);
				p.setCantidad(0);
				nuevas.add(p);
				c.setSaldoComision(c.getSaldoComision() - p.getTotalComision());

			}
		});
		if (nuevas.size() > 0)
			this.pagosRepo.save(nuevas);
		return oc;

	}

	@Override
	@Transactional
	public Contrato generarPorLiquidacion(Contrato contrato) {

		contrato.getComisiones().stream().filter(c -> c.getCobraAlLiquidar() && c.getSaldoComision() != 0).forEach(c -> {
			PagoComision p = new PagoComision();
			p.setDescripcion("Liquidacion " + c.getServicioComisionado().getProducto().getDescripcion());
			p.setTotalLiquidado(0d);
			p.setTipoPagoComision(TipoPagoComision.LIQUIDACION);
			p.setFechaComision(new Date());
			p.setConfigComision(c);
			p.setTotalComision(c.getTotalAComisionar() - c.getTotalPagos());
			c.setSaldoComision(0d);
			p.setCantidad(c.getServicioComisionado().getCantidad() - c.getCantidadComisionada());
			c.addPagoComision(p);

		});

		return contrato;
	}

}

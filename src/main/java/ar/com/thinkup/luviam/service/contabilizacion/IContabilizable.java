package ar.com.thinkup.luviam.service.contabilizacion;

import java.io.Serializable;

import ar.com.thinkup.luviam.model.Asiento;

public interface IContabilizable extends Serializable{

	public default boolean fueContabilizado() {
		return this.getAsiento() != null;
	}
	
	public Asiento getAsiento();
}

package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.model.contratos.ConfiguracionComision;
import ar.com.thinkup.luviam.vo.contratos.ConfiguracionComisionVO;

public interface IConfiguracionComisionService extends IModelService<ConfiguracionComisionVO,ConfiguracionComision>{

	
}

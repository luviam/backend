package ar.com.thinkup.luviam.service.def;

import java.util.List;

import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.vo.CentroCostoVO;

public interface ICentroCostoService{
	
	CentroCostoVO getByID(Long id);

	CentroCostoVO update(CentroCostoVO vo);

	CentroCostoVO create(CentroCostoVO vo);

	CentroCostoVO delete(Long centroCostoId);

	List<CentroCostoVO> getAll();

	CentroCosto getByDescriptivo(IDescriptivo centroCosto);

}

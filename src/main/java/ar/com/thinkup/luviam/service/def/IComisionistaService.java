package ar.com.thinkup.luviam.service.def;

import java.util.List;

import ar.com.thinkup.luviam.model.Comisionista;
import ar.com.thinkup.luviam.model.Salon;
import ar.com.thinkup.luviam.model.empleados.Empleado;
import ar.com.thinkup.luviam.vo.CabeceraComisionistaVO;
import ar.com.thinkup.luviam.vo.ComisionistaVO;

public interface IComisionistaService extends IModelService<ComisionistaVO,Comisionista>, IDescriptivoService<Comisionista> {

	List<CabeceraComisionistaVO> getCabeceras();

	Comisionista updateByEmpleado(Empleado e);

	Comisionista updateBySalon(Salon s);

}

package ar.com.thinkup.luviam.service;

import java.awt.IllegalComponentStateException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.Proveedor;
import ar.com.thinkup.luviam.model.enums.CuentasPrincipalesEnum;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.repository.CuentaAplicacionRepository;
import ar.com.thinkup.luviam.service.contabilizacion.ICuentasContablesService;
@Service("ICuentasContablesService")
public class CuentaContableService implements ICuentasContablesService {
	
	@Autowired
	private CuentaAplicacionRepository cuentaRepo;
	
	@Override
	public CuentaAplicacion getCuentaProveedor(CentroCosto c, Proveedor p) {
		if (p.getTipoCuenta().getCodigo().equals("SC")) {
			return this.getCuenta(CuentasPrincipalesEnum.CC_PROVEEDORES_SIN_CREDITO.getCodigo(), c);
		} else if (p.getTipoCuenta().getCodigo().equals("CC")) {
			return this.getCuenta(CuentasPrincipalesEnum.CC_PROVEEDORES_CON_CREDITO.getCodigo(), c);
		} else {
			return this.getCuenta(CuentasPrincipalesEnum.CC_PROVEEDORES_NO_HABITUALES.getCodigo(), c);
		}

	}
	@Override
	public CuentaAplicacion getCuenta(String codigoCuenta, CentroCosto centro) {
		CuentaAplicacion cuenta = this.cuentaRepo.findByCodigoAndCentroCostoCodigoAndCentroCostoActivoTrue(codigoCuenta,
				centro.getCodigo());
		if (cuenta == null) {
			throw new IllegalComponentStateException(
					"No se encontró la cuenta: " + codigoCuenta + " para el centro " + centro.getDescripcion());

		}
		return cuenta;
	}
}

package ar.com.thinkup.luviam.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.service.def.IModelService;
import ar.com.thinkup.luviam.vo.Identificable;

public abstract class ModelService<E extends Identificable, V extends Identificable, R extends JpaRepository<E, Long>>
		implements IModelService<V, E> {

	public ModelService() {
		super();
	}

	protected abstract R getRepository();

	protected String getNombre() {
		return "Parmaetro";
	}

	@Override
	@Transactional
	public V getByID(Long id) {
		if (id == null) {
			throw new IllegalArgumentException("El id es obligatorio");
		}
		return this.toVO(this.getRepository().findOne(id));
	}

	protected abstract V toVO(E ent);

	protected abstract E newEnt();

	@Override
	@Transactional
	public V update(V vo) {
		if (vo == null || vo.getId() == null) {
			throw new IllegalArgumentException("Debe indicar el id para actualizar");
		}
		E ent = this.getRepository().findOne(vo.getId());
		if (ent == null) {
			throw new IllegalArgumentException("No existe " + this.getNombre() + " con ID " + vo.getId());
		}

		ent = this.parseToEnt(vo, ent);
		this.getRepository().save(ent);
		return this.toVO(ent);
	}

	@Transactional
	public abstract E parseToEnt(V vo, E ent);

	@Override
	@Transactional
	public V create(V vo) {
		if (vo == null || vo.getId() != null) {
			throw new IllegalArgumentException("No debe indicar el id para crear");
		}
		E ent = this.parseToEnt(vo, this.newEnt());
		this.getRepository().save(ent);
		return this.toVO(ent);
	}

	@Override
	@Transactional
	public Boolean delete(Long id) {
		if (id == null) {
			throw new IllegalArgumentException("Debe indicar el id para eliminar");
		}
		E ent = this.getRepository().findOne(id);
		if (ent == null) {
			throw new IllegalArgumentException("No existe " + this.getNombre() + " con ID " + id);
		}
		try {
			this.getRepository().delete(ent);
			return true;
		} catch (Exception e) {

			return false;
		}

	}

	@Override
	public E getEntity(Long id) {
		return this.getRepository().findOne(id);
	}

	@Override
	@Transactional
	public List<V> getAll() {
		return this.getRepository().findAll().stream().map(p -> this.toVO(p)).collect(Collectors.toList());
	}

}
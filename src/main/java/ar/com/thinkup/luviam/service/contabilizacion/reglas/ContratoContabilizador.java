package ar.com.thinkup.luviam.service.contabilizacion.reglas;

import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.Operacion;
import ar.com.thinkup.luviam.model.contratos.Contrato;
import ar.com.thinkup.luviam.model.contratos.CuotaServicio;
import ar.com.thinkup.luviam.model.enums.CuentasPrincipalesEnum;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.service.contabilizacion.IContabilizable;
import ar.com.thinkup.luviam.service.contabilizacion.ICuentasContablesService;
import ar.com.thinkup.luviam.service.contabilizacion.IGeneradorAsiento;

public class ContratoContabilizador implements IGeneradorAsiento<Contrato> {

	@Autowired
	private ICuentasContablesService cuentasService;

	@Override
	public Asiento generarAsiento(Contrato c) {

		List<CuotaServicio> cuotas = c.getNoContabilizados();
		List<CentroCosto> centros = cuotas.stream().map(cu -> cu.getServicio().getCentroCosto()).distinct()
				.collect(Collectors.toList());
		Asiento a = new Asiento();

		a.setEstado(EstadoOperacionEnum.APROBADO.getCodigo());
		a.setMensaje("Generado por sistema");
		if (c.getAsiento() == null) {
			a.setFecha(c.getFechaContratacion() != null ? c.getFechaContratacion() : new Date());
			a.setDescripcion("Contratación " + c.getDescripcion());
		} else {
			a.setFecha(c.getUltimaModificacion() != null ? c.getUltimaModificacion() : new Date());
			a.setDescripcion("Modificación Liquidación " + c.getDescripcion());
		}

		a.setResponsable(c.getVendedor().getDescripcion());
		a.setTipoGenerador("AA");

		for (CentroCosto centroCosto : centros) {
			List<Operacion> oCuotas = new Vector<>();
			Operacion oARealizar = new Operacion();
			oARealizar.setCentro(centroCosto);
			oARealizar.setCuenta(this.cuentasService.getCuenta(CuentasPrincipalesEnum.EVENTOS_A_REALIZARSE.getCodigo(),
					centroCosto));
			oARealizar.setComprobante("Ppto -" + c.getNumeroPresupuesto());
			oARealizar.setDetalle(c.getDescripcion());

			CuentaAplicacion cCtaCteCliente = this.cuentasService
					.getCuenta(CuentasPrincipalesEnum.CC_CLIENTE.getCodigo(), centroCosto);
			cuotas.stream().filter(cu -> cu.getAsientoGenerado() == null && !cu.esImpuesto()
					&& cu.getServicio().getCentroCosto().getId().equals(centroCosto.getId())).forEach(cu -> {
						Operacion ccCliente = new Operacion();
						ccCliente.setCentro(centroCosto);
						ccCliente.setCuenta(cCtaCteCliente);

						ccCliente.setComprobante("Ppto" + c.getNumeroPresupuesto());
						ccCliente.setDetalle(cu.getCantidad() + " " + cu.getServicio().getProducto().getDescripcion());
						if (cu.getMonto() != 0) {
							if (cu.getCantidad() > 0) {
								ccCliente.setDebe(cu.getTotal());
								oARealizar.setHaber(oARealizar.getHaber() + ccCliente.getDebe());
							} else {
								ccCliente.setHaber(cu.getTotal() * -1);
								oARealizar.setDebe(oARealizar.getDebe() + ccCliente.getHaber());
							}
							oCuotas.add(ccCliente);
						}

					});
			a.agregarOperacion(oARealizar);
			if (oARealizar.getDebe() > 0 && oARealizar.getHaber() > 0) {
				Operacion oHaber = new Operacion();
				oHaber.setCentro(centroCosto);
				oHaber.setCuenta(oARealizar.getCuenta());
				oHaber.setComprobante("Ppto" + c.getNumeroPresupuesto());
				oHaber.setDetalle(c.getDescripcion());
				oHaber.setHaber(oARealizar.getHaber());
				oARealizar.setHaber(0d);
				a.agregarOperacion(oHaber);
			}

			cuotas.stream().filter(cu -> cu.getAsientoGenerado() == null && cu.esImpuesto()
					&& cu.getServicio().getCentroCosto().getId().equals(centroCosto.getId())).forEach(cu -> {
						Operacion ccCliente = new Operacion();
						Operacion cImpuesto = new Operacion();
						ccCliente.setCentro(centroCosto);
						cImpuesto.setCentro(centroCosto);
						ccCliente.setCuenta(cCtaCteCliente);

						if (cu.getProducto().getCuenta(centroCosto.getCodigo()) == null) {
							throw new IllegalArgumentException("No hay cuenta de imputación para "
									+ centroCosto.getCodigo() + " " + cu.getProducto().getNombre());
						}
						cImpuesto.setCuenta(cu.getProducto().getCuenta(centroCosto.getCodigo()).getCuentaAplicacion());
						ccCliente.setComprobante("Ppto" + c.getNumeroPresupuesto());
						cImpuesto.setComprobante("Ppto" + c.getNumeroPresupuesto());
						ccCliente.setDetalle(cu.getCantidad() + " " + cu.getServicio().getProducto().getDescripcion());
						cImpuesto.setDetalle(cu.getDescripcion());

						if (cu.getMonto() != 0) {
							if (cu.getCantidad() > 0) {
								ccCliente.setDebe(cu.getTotal());
								cImpuesto.setHaber(cu.getTotal());
							} else {
								ccCliente.setHaber(cu.getTotal() * -1);
								cImpuesto.setDebe(cu.getTotal() * -1);
							}
							oCuotas.add(cImpuesto);
							oCuotas.add(ccCliente);
							
						}
					});
			a.agregarOperaciones(oCuotas);
		}

		return a;

	}

	@Override
	public Boolean esAplicable(IContabilizable o) {
		if (Contrato.class.isAssignableFrom(o.getClass())) {
			Contrato c = (Contrato) o;
			return c.getAsiento() == null
					|| (c.getEstado() != null && !c.getEstado().equals(EstadoOperacionEnum.LIQUIDADO));
		}
		return false;
	}

}

package ar.com.thinkup.luviam.service;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.repository.CentroRepository;
import ar.com.thinkup.luviam.repository.CuentaAplicacionRepository;
import ar.com.thinkup.luviam.repository.TipoComprobanteRepository;

@Service("ModelEntityProvider")
public class ModelEntityProvider {

	@Autowired
	private CentroRepository centroRepo;

	@Autowired
	private CuentaAplicacionRepository cuentaRepo;

	@Autowired
	private TipoComprobanteRepository tipoComRepo;

	public CuentaAplicacion getCuenta(DescriptivoGeneric cuenta, DescriptivoGeneric centro) {
		if (cuenta == null || StringUtils.isBlank(cuenta.getCodigo())) {
			throw new IllegalArgumentException("La cuenta es obligatoria");
		}
		if (centro == null || StringUtils.isBlank(centro.getCodigo())) {
			throw new IllegalArgumentException("El Centro es obligatorio");
		}
		CuentaAplicacion c = this.cuentaRepo
				.findByCodigoAndCentroCostoCodigoAndCentroCostoActivoTrue(cuenta.getCodigo(), centro.getCodigo());
		if (c == null) {
			throw new EntityNotFoundException("No se encuentra la cuenta con codigo: " + cuenta.getCodigo());
		}
		return c;
	}

	public CentroCosto getCentro(DescriptivoGeneric centroCosto) {
		if (centroCosto == null || StringUtils.isBlank(centroCosto.getCodigo())) {
			throw new IllegalArgumentException("El Centro es obligatorio");
		}

		CentroCosto c = this.centroRepo.findOneByCodigo(centroCosto.getCodigo());
		if (c == null) {
			throw new EntityNotFoundException("No se encuentra el centro con codigo: " + centroCosto.getCodigo());
		}
		return c;
	}

}

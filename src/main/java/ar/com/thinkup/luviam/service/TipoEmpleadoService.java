package ar.com.thinkup.luviam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.thinkup.luviam.model.TipoEmpleado;
import ar.com.thinkup.luviam.repository.ParameterRepository;
import ar.com.thinkup.luviam.repository.TipoEmpleadoRepository;
import ar.com.thinkup.luviam.service.def.ITipoEmpleadoService;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;

@Service("ITipoEmpleadoService")
public class TipoEmpleadoService extends ParametricoService<ParametricoVO, TipoEmpleado> implements ITipoEmpleadoService {

	@Autowired
	private TipoEmpleadoRepository tipoEmpleadoRepo;

	@Override
	protected ParameterRepository<TipoEmpleado> getRepository() {

		return tipoEmpleadoRepo;
	}

	@Override
	protected ParametricoVO toVO(TipoEmpleado ent) {
		return new ParametricoVO(ent);
	}

	@Override
	protected TipoEmpleado newEnt() {

		return new TipoEmpleado();
	}

}

package ar.com.thinkup.luviam.service.def;

import java.util.List;

import ar.com.thinkup.luviam.vo.CuentaConfig;

public interface ICuentaService {

	List<CuentaConfig> getCuentasDefault();

}

package ar.com.thinkup.luviam.service.contabilizacion.reglas;

import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.Operacion;
import ar.com.thinkup.luviam.model.contratos.Contrato;
import ar.com.thinkup.luviam.model.contratos.ServicioContratado;
import ar.com.thinkup.luviam.model.enums.CuentasPrincipalesEnum;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.service.contabilizacion.IContabilizable;
import ar.com.thinkup.luviam.service.contabilizacion.ICuentasContablesService;
import ar.com.thinkup.luviam.service.contabilizacion.IGeneradorAsiento;

public class CierreContratoContabilizador implements IGeneradorAsiento<Contrato> {

	@Autowired
	private ICuentasContablesService cuentasService;

	@Override
	public Asiento generarAsiento(Contrato c) {

		List<CentroCosto> centros = c.getServicios().stream().filter(s -> !s.getProducto().esImpuesto())
				.map(ServicioContratado::getCentroCosto).distinct().collect(Collectors.toList());
		Asiento a = new Asiento();

		a.setEstado(EstadoOperacionEnum.APROBADO.getCodigo());
		a.setMensaje("Generado por sistema");
		a.setFecha(c.getUltimaModificacion() != null ? c.getUltimaModificacion() : new Date());
		a.setDescripcion("Cierre de Contrato - " + c.getDescripcion());
		a.setResponsable(c.getVendedor().getDescripcion());
		a.setTipoGenerador("AA");

		for (CentroCosto centroCosto : centros) {
			List<Operacion> oCuotas = new Vector<>();
			Operacion oARealizar = new Operacion();
			oARealizar.setCentro(centroCosto);
			oARealizar.setCuenta(this.cuentasService.getCuenta(CuentasPrincipalesEnum.EVENTOS_A_REALIZARSE.getCodigo(),
					centroCosto));
			oARealizar.setComprobante("Ppto -" + c.getNumeroPresupuesto());
			oARealizar.setDetalle(c.getDescripcion());

			// CuentaAplicacion cCtaCteCliente = this.cuentasService
			// .getCuenta(CuentasPrincipalesEnum.CC_CLIENTE.getCodigo(), centroCosto);
			c.getServicios().stream().filter(
					s -> !s.getProducto().esImpuesto() && s.getCentroCosto().getId().equals(centroCosto.getId()))
					.forEach(s -> {
						Operacion ccCliente = new Operacion();
						ccCliente.setCentro(centroCosto);

						ccCliente.setCuenta(s.getCuentaAplicacion());
						ccCliente.setComprobante("Ppto" + c.getNumeroPresupuesto());
						ccCliente.setDetalle(s.getCantidad() + " " + s.getProducto().getDescripcion());
						if (s.getTotal() != 0) {
							if (s.getCantidad() > 0) {
								ccCliente.setHaber(s.getTotal());
								oARealizar.setDebe(oARealizar.getDebe() + ccCliente.getHaber());
							} else {
								ccCliente.setDebe(s.getTotal() * -1);
								oARealizar.setHaber(oARealizar.getHaber() + ccCliente.getDebe());
							}
							oCuotas.add(ccCliente);
						}

					});
			a.agregarOperacion(oARealizar);
			if (oARealizar.getDebe() > 0 && oARealizar.getHaber() > 0) {
				Operacion oHaber = new Operacion();
				oHaber.setCentro(centroCosto);
				oHaber.setCuenta(oARealizar.getCuenta());
				oHaber.setComprobante("Ppto" + c.getNumeroPresupuesto());
				oHaber.setDetalle(c.getDescripcion());
				oHaber.setHaber(oARealizar.getHaber());
				oARealizar.setHaber(0d);
				a.agregarOperacion(oHaber);
			}
			a.agregarOperaciones(oCuotas);
		}

		return a;

	}

	@Override
	public Boolean esAplicable(IContabilizable o) {
		if (Contrato.class.isAssignableFrom(o.getClass())) {
			Contrato c = (Contrato) o;
			return c.getEstado() != null && c.getEstado().equals(EstadoOperacionEnum.LIQUIDADO);
		}
		return false;
	}

}

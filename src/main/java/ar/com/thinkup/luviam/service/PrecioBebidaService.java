package ar.com.thinkup.luviam.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.PrecioBebida;
import ar.com.thinkup.luviam.repository.PrecioBebidaRepository;
import ar.com.thinkup.luviam.service.def.IPrecioBebidaService;
import ar.com.thinkup.luviam.vo.PrecioBebidaVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroPrecio;

@Service("IPrecioBebidaService")
@Transactional
public class PrecioBebidaService extends PrecioService<PrecioBebida, PrecioBebidaVO, PrecioBebidaRepository>
		implements IPrecioBebidaService {

	@Autowired
	private PrecioBebidaRepository repo;

	@Override
	protected PrecioBebidaRepository getRepository() {
		return this.repo;
	}

	@Override
	@Transactional
	protected PrecioBebidaVO toVO(PrecioBebida ent) {
		return new PrecioBebidaVO(ent);
	}

	@Override
	protected PrecioBebida newEnt() {
		return new PrecioBebida();
	}

	@Override
	@Transactional
	public PrecioBebida parseToEnt(PrecioBebidaVO vo, PrecioBebida ent) {
		PrecioBebida pb = ent != null ? ent : this.newEnt();
		super.parseToEntity(vo, pb);
		return pb;
	}

	@Override
	@Transactional
	public List<PrecioBebidaVO> getByFiltro(FiltroPrecio filtro) {

		return this.repo
				.buscarPorFiltros(filtro.getFechaVigencia(), filtro.getIdMarcas(), filtro.getCodigosProductos(), filtro.getIdSalones(), filtro.getSoloActivos())
				.stream().map(p -> new PrecioBebidaVO(p)).collect(Collectors.toList());
	}

}

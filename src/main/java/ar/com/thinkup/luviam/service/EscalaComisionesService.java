package ar.com.thinkup.luviam.service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.EscalaComisiones;
import ar.com.thinkup.luviam.repository.EscalaComisionesRepository;
import ar.com.thinkup.luviam.service.def.ICentroCostoService;
import ar.com.thinkup.luviam.service.def.IEscalaComisionesService;
import ar.com.thinkup.luviam.vo.EscalaComisionesVO;

@Service("IEscalaComisionesService")
public class EscalaComisionesService
		extends ModelService<EscalaComisiones, EscalaComisionesVO, EscalaComisionesRepository>
		implements IEscalaComisionesService {

	@Autowired
	private EscalaComisionesRepository repo;

	@Autowired
	private ICentroCostoService centroService;

	@Override
	protected EscalaComisionesRepository getRepository() {
		return this.repo;
	}

	@Override
	protected EscalaComisionesVO toVO(EscalaComisiones ent) {

		return new EscalaComisionesVO(ent);
	}

	@Override
	protected EscalaComisiones newEnt() {
		return new EscalaComisiones();
	}

	public EscalaComisiones parseToEnt(EscalaComisionesVO vo, EscalaComisiones entity) {

		EscalaComisiones ent = entity;
		if (ent == null)
			ent = new EscalaComisiones();
		ent.setId(vo.getId());
		ent.setDesde(vo.getDesde());
		ent.setHasta(vo.getHasta());
		ent.setPremio(vo.getPremio());
		ent.setCentroCosto(this.centroService.getByDescriptivo(vo.getCentro()));

		return ent;
	}

	@Override
	@Transactional
	public List<EscalaComisionesVO> getByCentro(String centro) {
		return this.getRepository().findByCentroCostoCodigo(centro).stream().map(c -> new EscalaComisionesVO(c))
				.collect(Collectors.toList());
	}

	@Override
	@Transactional
	public EscalaComisiones getEscala(CentroCosto centro, Integer cantidad) {

		Set<EscalaComisiones> r = this.getRepository().getAplicable(centro.getId(), cantidad);
		return (r != null && r.size() > 0) ? r.iterator().next() : null;
	}

}

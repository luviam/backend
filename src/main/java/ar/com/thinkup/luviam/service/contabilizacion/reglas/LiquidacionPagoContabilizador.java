package ar.com.thinkup.luviam.service.contabilizacion.reglas;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.Operacion;
import ar.com.thinkup.luviam.model.documentos.comisiones.LiquidacionComisiones;
import ar.com.thinkup.luviam.model.enums.CuentasPrincipalesEnum;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.service.contabilizacion.IContabilizable;
import ar.com.thinkup.luviam.service.contabilizacion.ICuentasContablesService;
import ar.com.thinkup.luviam.service.contabilizacion.IGeneradorAsiento;

public class LiquidacionPagoContabilizador implements IGeneradorAsiento<LiquidacionComisiones> {

	@Autowired
	private ICuentasContablesService cuentasService;

	@Override
	public Boolean esAplicable(IContabilizable o) {
		if (!LiquidacionComisiones.class.isAssignableFrom(o.getClass())) {
			return false;
		}
		LiquidacionComisiones l = (LiquidacionComisiones) o;
		return l.getPagosLiquidacion().size() > 0;

	}

	@Override
	public Asiento generarAsiento(LiquidacionComisiones o) {

		String codigoPagar = CuentasPrincipalesEnum.COMISIONES_A_PAGAR.getCodigo()
				+ o.getComisionista().getTipoComisionista().getCodigo();

		CuentaAplicacion comPagar = this.cuentasService.getCuenta(codigoPagar, o.getCentroCosto());

		Asiento a = new Asiento();
		a.setDescripcion("Pago comisiones " + o.getComisionista().getDescripcion());
		a.setEstado(EstadoOperacionEnum.APROBADO.getCodigo());
		a.setMensaje("Generado por sistema");
		a.setFecha(o.getFechaPago() != null? o.getFechaPago() : new Date());
		a.setResponsable(o.getResponsable() == null ? "SISTEMA" : o.getResponsable().getUsername());
		a.setTipoGenerador("AA");

		Operacion aPagar = new Operacion();
		aPagar.setCentro(o.getCentroCosto());
		aPagar.setComprobante("Liq. N° " + o.getNumero());
		aPagar.setCuenta(comPagar);
		aPagar.setDebe(o.getImporte());
		aPagar.setDetalle(o.getDescripcion());
		a.agregarOperacion(aPagar);

		List<Operacion> operaciones = new Vector<>();
		o.getPagosLiquidacion().forEach(p -> {
			Optional<Operacion> op = operaciones.stream()
					.filter(oo -> oo.getCentro().getCodigo().equals(p.getCentroCosto().getCodigo())
							&& oo.getCuenta().getCodigo().equals(p.getCuenta().getCodigo()))
					.findFirst();
			if (op.isPresent()) {
				op.get().setHaber(op.get().getHaber() + p.getMonto());
			} else {
				Operacion newOp = new Operacion();
				newOp.setCentro(p.getCentroCosto());
				if (p.getCheque() != null) {
					newOp.setComprobante("Cheque " + p.getCheque().getDescripcion());
				}

				newOp.setCuenta(p.getCuenta());
				newOp.setHaber(p.getMonto());
				newOp.setDetalle("Pago Comisionista " + o.getComisionista().getDescripcion());
				
				operaciones.add(newOp);
			}
		});
		a.agregarOperaciones(operaciones);
		return a;
	}

}

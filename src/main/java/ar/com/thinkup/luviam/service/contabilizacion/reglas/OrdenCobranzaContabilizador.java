package ar.com.thinkup.luviam.service.contabilizacion.reglas;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.Operacion;
import ar.com.thinkup.luviam.model.documentos.cobros.Cobro;
import ar.com.thinkup.luviam.model.documentos.cobros.OrdenCobranza;
import ar.com.thinkup.luviam.model.enums.CuentasPrincipalesEnum;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.service.contabilizacion.IContabilizable;
import ar.com.thinkup.luviam.service.contabilizacion.ICuentasContablesService;
import ar.com.thinkup.luviam.service.contabilizacion.IGeneradorAsiento;

public class OrdenCobranzaContabilizador implements IGeneradorAsiento<OrdenCobranza> {

	@Autowired
	private ICuentasContablesService cuentasService;

	@Override
	public Asiento generarAsiento(OrdenCobranza oc) {

		List<CentroCosto> centrosPib = oc.getItems().stream()
				.map(i -> i.getCuotaServicio().getServicio().getCentroCosto()).distinct().collect(Collectors.toList());
		centrosPib.addAll(oc.getCobros().stream().map(Cobro::getCentroCosto).collect(Collectors.toList()));

		Set<CentroCosto> centros = new HashSet<>(centrosPib);
		Asiento a = new Asiento();
		a.setDescripcion("Cobranza - " + oc.getContrato().getDescripcion());
		a.setEstado(EstadoOperacionEnum.APROBADO.getCodigo());
		a.setMensaje("Generado por sistema");
		a.setFecha(oc.getFecha() != null ? oc.getFecha() : new Date());
		a.setResponsable(oc.getResponsable().getDescripcion());
		a.setTipoGenerador("AA");

		Map<Long, Double> totalesCentros = new HashMap<>();
		Map<Long, Double> totalesCobros = new HashMap<>();
		Map<Long, CentroCosto> centrosCosto = new HashMap<>();

		for (CentroCosto centroCosto : centros) {
			centrosCosto.putIfAbsent(centroCosto.getId(), centroCosto);

			List<Operacion> oCuotas = new Vector<>();
			totalesCentros.put(centroCosto.getId(), 0d);

			CuentaAplicacion cCtaCteCliente = this.cuentasService
					.getCuenta(CuentasPrincipalesEnum.CC_CLIENTE.getCodigo(), centroCosto);
			oc.getItems().stream().filter(
					cu -> cu.getCuotaServicio().getServicio().getCentroCosto().getId().equals(centroCosto.getId()))
					.forEach(cu -> {
						Operacion ccCliente = new Operacion();
						ccCliente.setCentro(centroCosto);
						if (cu.getCuotaServicio().esImpuesto()) {
							if (cu.getCuotaServicio().getProducto().getCuenta(centroCosto.getCodigo()) == null) {
								throw new IllegalArgumentException(
										"No hay cuenta de imputación para " + centroCosto.getCodigo() + " "
												+ cu.getCuotaServicio().getProducto().getNombre());
							}
							ccCliente.setCuenta(cu.getCuotaServicio().getProducto().getCuenta(centroCosto.getCodigo())
									.getCuentaAplicacion());

						} else {
							ccCliente.setCuenta(cCtaCteCliente);
						}

						ccCliente.setComprobante(oc.getNumeroComprobante());

						ccCliente.setDetalle(cu.getCuotaServicio().getServicio().getProducto().getDescripcion());
						if (cu.getImporte() != 0) {
							totalesCentros.put(centroCosto.getId(),
									totalesCentros.get(centroCosto.getId()) + cu.getImporte());
							if (cu.getImporte() > 0) {
								ccCliente.setHaber(cu.getImporte());
							} else {
								ccCliente.setDebe(Math.abs(cu.getImporte()));
							}
							oCuotas.add(ccCliente);
						}

					});

			a.agregarOperaciones(oCuotas);

		}
		for (Cobro cc : oc.getCobros()) {
			totalesCobros.putIfAbsent(cc.getCentroCosto().getId(), 0d);
			Operacion oCobro = new Operacion();
			oCobro.setCentro(cc.getCentroCosto());
			oCobro.setCuenta(cc.getCuenta());
			String label = "Cobro - ";
			if (cc.getMonto() > 0) {
				oCobro.setDebe(cc.getMonto());
				
			} else {
				oCobro.setHaber(Math.abs(cc.getMonto()));
				label = "Devolución - ";
			}
			if (StringUtils.isEmpty(cc.getComprobante())) {
				oCobro.setDetalle(label+ oc.getContrato().getDescripcion());
			} else {
				oCobro.setDetalle(label + cc.getComprobante());
			}
			
			totalesCobros.put(cc.getCentroCosto().getId(),
					totalesCobros.get(cc.getCentroCosto().getId()) + cc.getMonto());
			a.agregarOperacion(oCobro);
		}

		for (Entry<Long, Double> cobro : totalesCobros.entrySet()) {
			// Ajusto para el mismo centro lo que ya contabilizó
			Double tAcumulado = totalesCentros.get(cobro.getKey());
			if (tAcumulado != null && tAcumulado > 0) {
				if (tAcumulado > cobro.getValue()) {
					totalesCentros.put(cobro.getKey(), tAcumulado - cobro.getValue());
					cobro.setValue(0d);
				} else {
					totalesCentros.put(cobro.getKey(), 0d);
					cobro.setValue(cobro.getValue() - tAcumulado);
				}
			}
			// Busco saldar la cuenta de los demás centros
			totalesCentros.entrySet().stream()
					// BUsco los totales que no sean del centro que lupeo y que tengan saldo
					.filter(c -> !c.getKey().equals(cobro.getKey())).filter(c -> totalesCentros.get(c.getKey()) > 0)
					.forEach(totalProducto -> {
						// ACA GENERO OPERACIONES DE AJUSTE ENTRE CENTROS
						if (cobro.getValue() > 0) {
							Operacion oHaber = new Operacion();
							oHaber.setCentro(centrosCosto.get(cobro.getKey()));
							oHaber.setComprobante("");
							oHaber.setCuenta(this.cuentasService.getCuenta(
									"UN_" + centrosCosto.get(totalProducto.getKey()).getCodigo(),
									centrosCosto.get(cobro.getKey())));
							oHaber.setDetalle("Ajuste Cobranza transfiere a "
									+ centrosCosto.get(totalProducto.getKey()).getCodigo());

							Operacion oDebe = new Operacion();
							oDebe.setCentro(centrosCosto.get(totalProducto.getKey()));
							oDebe.setComprobante("");
							oDebe.setCuenta(
									this.cuentasService.getCuenta("UN_" + centrosCosto.get(cobro.getKey()).getCodigo(),
											centrosCosto.get(totalProducto.getKey())));
							oDebe.setDetalle(
									"Ajuste Cobranza recibe de " + centrosCosto.get(cobro.getKey()).getCodigo());

							if (totalProducto.getValue() >= cobro.getValue()) {
								oHaber.setHaber(cobro.getValue());
								oDebe.setDebe(cobro.getValue());
								totalProducto.setValue(totalProducto.getValue() - cobro.getValue());

								cobro.setValue(0d);
							} else {
								oHaber.setHaber(cobro.getValue() - totalProducto.getValue());
								oDebe.setDebe(cobro.getValue() - totalProducto.getValue());
								totalesCentros.put(cobro.getKey(), 0d);
								cobro.setValue(cobro.getValue() - tAcumulado);
							}
							a.agregarOperacion(oHaber);
							a.agregarOperacion(oDebe);
						}
					});
		}

		return a;
	}

	@Override
	public Boolean esAplicable(IContabilizable o) {
		return OrdenCobranza.class.isAssignableFrom(o.getClass());
	}

}

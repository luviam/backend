package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.model.TipoEvento;
import ar.com.thinkup.luviam.vo.parametricos.TipoEventoVO;

public interface ITipoEventoService extends IParametricoService<TipoEventoVO, TipoEvento>{

}

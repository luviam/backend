package ar.com.thinkup.luviam.service;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.exceptions.UsuarioNoAutorizadoException;
import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.Comisionista;
import ar.com.thinkup.luviam.model.EscalaComisiones;
import ar.com.thinkup.luviam.model.TipoComisionista;
import ar.com.thinkup.luviam.model.contratos.PagoComision;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.documentos.comisiones.ItemLiquidacionComisiones;
import ar.com.thinkup.luviam.model.documentos.comisiones.LiquidacionComisiones;
import ar.com.thinkup.luviam.model.documentos.comisiones.PagoLiquidacion;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.enums.TipoPagoComision;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.repository.LiquidacionComisionesRepository;
import ar.com.thinkup.luviam.repository.PagoComisionRepository;
import ar.com.thinkup.luviam.service.def.IComisionistaService;
import ar.com.thinkup.luviam.service.def.IContableService;
import ar.com.thinkup.luviam.service.def.IEscalaComisionesService;
import ar.com.thinkup.luviam.service.def.IItemLiquidacionComisionesService;
import ar.com.thinkup.luviam.service.def.ILiquidacionComisionesService;
import ar.com.thinkup.luviam.service.def.ITipoComisionistaService;
import ar.com.thinkup.luviam.vo.ItemLiquidacionComisionesVO;
import ar.com.thinkup.luviam.vo.LiquidacionComisionesVO;
import ar.com.thinkup.luviam.vo.PagoLiquidacionVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroComisiones;
import ar.com.thinkup.luviam.vo.filtros.FiltroComisionista;

@Service("ILiquidacionComisionesService")
public class LiquidacionComisionesService
		extends DocumentoPagoService<PagoLiquidacion, PagoLiquidacionVO, LiquidacionComisiones, LiquidacionComisionesVO>
		implements ILiquidacionComisionesService {

	@Autowired
	private LiquidacionComisionesRepository repo;

	@Autowired
	private IComisionistaService comisionistaService;

	@Autowired
	private IItemLiquidacionComisionesService itemService;

	@Autowired
	private IContableService contabilizadorService;

	@Autowired
	private PagoComisionRepository pagoComisionRepo;

	@Autowired
	private IEscalaComisionesService escalasService;

	@Autowired
	private ITipoComisionistaService tcService;

	@Override
	public LiquidacionComisionesVO guardarDocumento(LiquidacionComisionesVO vo, Usuario responsable) {
		Boolean modificable = false;
		LiquidacionComisiones ent;
		if (vo.getId() != null) {
			ent = repo.findOne(vo.getId());
			// SOlo se puede modificar si el estado no es aprobado y el usuario es quien
			// creó la odc o un administrador
			if (ent == null) {
				throw new EntityNotFoundException("No existe la odc con ID: " + vo.getId());
			}
			modificable = (responsable.esAdministrador() || ent.getResponsable().equals(responsable))
					&& !ent.getEstadoOperacion().equals(EstadoOperacionEnum.APROBADO);

		} else {
			ent = new LiquidacionComisiones();
			modificable = true;
			ent.setResponsable(responsable);
		}

		if (modificable) {
			ent = this.parseToEnt(vo, ent);
		}

		LiquidacionComisiones o = repo.save(ent);
		if (StringUtils.isBlank(o.getNumero())) {
			o.setNumero(String.valueOf(ent.getId()));

			repo.save(ent);
		}
		return new LiquidacionComisionesVO(o);
	}

	private void contabilizar(LiquidacionComisiones ent) {
		this.contabilizadorService.contabilizar(ent);

	}

	@Override
	public Boolean cambioEstadoDocumento(Long idDocumento, DescriptivoGeneric estado, Usuario responsable) {

		if (idDocumento == null || estado == null) {
			throw new IllegalArgumentException("El id de la Liquidación y el estado son obligatorios");
		}

		LiquidacionComisiones liquidacion = this.repo.findOne(idDocumento);
		if (liquidacion == null) {
			throw new EntityNotFoundException("No se encuentra la Liquidación con id: " + idDocumento);
		}
		if (EstadoOperacionEnum.getByCodigo(estado.getCodigo()) != null) {
			liquidacion.setCodigoEstado(estado.getCodigo());
			if (liquidacion.getEstadoOperacion().equals(EstadoOperacionEnum.APROBADO)) {
				if (liquidacion.getPagosLiquidacion().size() == 0 && liquidacion.getImporte() > 0) {
					liquidacion.setEstado(EstadoOperacionEnum.POR_PAGAR);

				} else {

					List<PagoComision> aActuralizar = new Vector<>();
					this.pagoComisionRepo.save(aActuralizar);
				}
				liquidacion.setAsientoGenerado(this.contabilizadorService.contabilizar(liquidacion));

			}
			repo.save(liquidacion);
		}

		return true;
	}

	@Override
	public LiquidacionComisionesVO findById(Long idDocumento) {
		return super.findById(idDocumento);
	}

	@Override
	@Transactional
	public Boolean delete(Long id) {
		LiquidacionComisiones l = this.getEntity(id);
		List<PagoComision> aActualizar = new Vector<>();
		if (l != null) {
			l.getItems().stream().forEach(i -> {
				i.getPagoComision().susLiquidado(i.getImporte());
				aActualizar.add(i.getPagoComision());
			});
		}
		this.pagoComisionRepo.save(aActualizar);
		this.repo.save(l);

		return super.delete(id);
	}

	@Override
	@Transactional
	public Boolean borrar(Long idDocumento, Usuario responsable) throws UsuarioNoAutorizadoException {
		return this.delete(idDocumento);
	}

	@Override
	public List<LiquidacionComisionesVO> getCabeceras(FiltroComisionista filtro) {

		return null;
	}

	@Override
	public List<ItemLiquidacionComisionesVO> getItems(Long idLiquidacion) {
		return this.itemService.getByLiquidacionId(idLiquidacion);
	}

	@Override
	protected PagoLiquidacion getNewValores() {
		return new PagoLiquidacion();
	}

	@Override
	protected LiquidacionComisionesRepository getRepository() {
		return this.repo;
	}

	@Override
	protected LiquidacionComisionesVO toVO(LiquidacionComisiones ent) {

		return new LiquidacionComisionesVO(ent);
	}

	@Override
	protected LiquidacionComisiones newEnt() {
		return new LiquidacionComisiones();
	}

	@Override
	public LiquidacionComisiones parseToEnt(LiquidacionComisionesVO vo, LiquidacionComisiones ent) {
		super.parseToEnt(vo, ent);
		ent.setObservaciones(vo.getObservaciones());
		ent.setPremio(vo.getPremio());

		ent.setComisionista(this.comisionistaService.getByDescriptivo(vo.getComisionista()));

		// Quito Eliminados
		List<ItemLiquidacionComisiones> aEliminar = ent.getItems().stream()
				.filter(i -> i.getId() != null && vo.getItem(i.getId()) == null).collect(Collectors.toList());
		List<PagoComision> aActualizar = new Vector<>();
		aEliminar.forEach(i -> {
			PagoComision a = i.getPagoComision();
			a.susLiquidado(i.getImporte());
			aActualizar.add(i.getPagoComision());
		});
		ent.getItems().removeAll(aEliminar);

		// Busco nuevos items
		vo.getItems().stream().filter(i -> i.getId() == null)
				.forEach(i -> ent.addItem(this.itemService.parseToEnt(i, new ItemLiquidacionComisiones())));

		// Busco items modificados
		vo.getItems().stream().filter(i -> i.getId() != null).forEach(i -> {
			ent.getItem(i.getId()).getPagoComision().susLiquidado(ent.getItem(i.getId()).getImporte());
			this.itemService.parseToEnt(i, ent.getItem(i.getId()));
		});

		if (vo.getEstado() != null && vo.getEstado().getCodigo().equals(EstadoOperacionEnum.POR_PAGAR.getCodigo())) {
			if (vo.getPagosLiquidacion().size() > 0) {
				ent.setEstado(EstadoOperacionEnum.APROBADO);

			} else {
				ent.setEstado(EstadoOperacionEnum.POR_PAGAR);
			}

		} else {
			ent.setEstado(EstadoOperacionEnum.PENDIENTE);
		}
		ent.setImporte(ent.getTotal());
		if (ent.getPagosLiquidacion().size() > 0) {
			if (vo.getFechaPago() == null) {
				ent.setFechaPago(new Date());
			} else {
				ent.setFechaPago(vo.getFechaPago());
			}
		}
		this.pagoComisionRepo.save(aActualizar);
		if (ent.getEstadoOperacion().equals(EstadoOperacionEnum.APROBADO)) {
			this.contabilizar(ent);

		}
		return ent;
	}

	@Override
	public List<LiquidacionComisionesVO> getComisionesByFitro(FiltroComisiones filtro) {

		return this.repo
				.findByFiltros(filtro.getIdComisionista(), filtro.getCodigoCentro(), filtro.getIdContrato(),
						filtro.getFechaDesde(), filtro.getFechaHasta())
				.stream().map(l -> new LiquidacionComisionesVO(l)).collect(Collectors.toList());
	}

	@Transactional
	private LiquidacionComisiones crearLiquidacion(Comisionista comisionista, CentroCosto centro,
			Set<PagoComision> pagosPendientes, Date periodo) {

		if (comisionista == null) {
			throw new IllegalArgumentException("El comisionista es obligatorio");
		}
		if (centro == null) {
			throw new IllegalArgumentException("El centro es obligatorio");
		}
		if (pagosPendientes.size() == 0) {
			return null;
		}

		LiquidacionComisiones l = new LiquidacionComisiones();
		l.setCentroCosto(centro);
		l.setComisionista(comisionista);
		l.setDescripcion(
				"Liquidación periodo " + new DateTime(periodo).toString("MMM-yyyy") + " - " + centro.getCodigo());
		l.setCodigoEstado(EstadoOperacionEnum.PENDIENTE.getCodigo());
		l.setFecha(periodo);
		l.setObservaciones("Generado por sistema");

		pagosPendientes.forEach(p -> {
			ItemLiquidacionComisiones i = new ItemLiquidacionComisiones();
			i.setImporte(p.getTotalComision() - p.getTotalLiquidado());
			i.setPagoComision(p);
			l.addItem(i);
		});

		l.setImporte(l.getItems().parallelStream().mapToDouble(i -> i.getImporte()).sum());
		if (l.getComisionista().esVendedor()) {
			EscalaComisiones e = this.escalasService.getEscala(centro, l.getCantidadBase());
			if (e != null) {
				l.setPremio(e.getPremio());
			}
		}

		return l;

	}

	@Override
	@Transactional
	public List<LiquidacionComisionesVO> generarLiquidaciones(List<TipoPagoComision> tiposComisiones,
			List<TipoComisionista> tiposComisionista, Date desde, Date hasta) {

		Set<PagoComision> pagosPendientes = this.pagoComisionRepo.findEntByFiltros(-1l, "-1", -1l,
				tiposComisiones.stream().map(TipoPagoComision::getCodigo).collect(Collectors.toList()), desde, hasta,
				tiposComisionista.stream().map(TipoComisionista::getCodigo).collect(Collectors.toList()));

		Map<LiquidacionKey, Set<PagoComision>> keys = new HashMap<>();
		pagosPendientes.forEach(p -> {
			LiquidacionKey l = new LiquidacionKey(p.getConfigComision().getComisionista(),
					p.getConfigComision().getCentroCosto());
			keys.putIfAbsent(l, new HashSet<>());
			keys.get(l).add(p);
		});
		List<LiquidacionComisiones> liquidacionesGeneradas = new Vector<>();
		for (Entry<LiquidacionKey, Set<PagoComision>> k : keys.entrySet()) {
			liquidacionesGeneradas.add(
					this.crearLiquidacion(k.getKey().getComisionista(), k.getKey().getCentro(), k.getValue(), hasta));
		}
		this.guardar(liquidacionesGeneradas);
		return liquidacionesGeneradas.stream().map(l -> new LiquidacionComisionesVO(l)).collect(Collectors.toList());
	}

	@Override
	public List<LiquidacionComisionesVO> generarLiquidacionesMensuales(List<TipoPagoComision> tiposComisiones,
			List<TipoComisionista> tiposComisionista) {
		return this.generarLiquidaciones(tiposComisiones, tiposComisionista,
				new DateTime().withDayOfMonth(1).withTimeAtStartOfDay().minusMonths(1).withDayOfMonth(1).toDate(),
				new DateTime().withDayOfMonth(1).withTimeAtStartOfDay().minusSeconds(1).toDate());

	}

	private List<LiquidacionComisiones> guardar(List<LiquidacionComisiones> liquidaciones) {

		liquidaciones = this.repo.save(liquidaciones);
		liquidaciones.forEach(l -> l.setNumero(l.getId() + ""));
		return this.repo.save(liquidaciones);

	}

	protected class LiquidacionKey {
		private Comisionista comisionista;
		private CentroCosto centro;

		public LiquidacionKey(Comisionista comisionista, CentroCosto centro) {
			super();
			this.comisionista = comisionista;
			this.centro = centro;
		}

		public Comisionista getComisionista() {
			return comisionista;
		}

		public void setComisionista(Comisionista comisionista) {
			this.comisionista = comisionista;
		}

		public CentroCosto getCentro() {
			return centro;
		}

		public void setCentro(CentroCosto centro) {
			this.centro = centro;
		}

		@Override
		public String toString() {
			return " comisionista=" + comisionista.getNombre() + ", centro=" + centro.getCodigo();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((centro == null) ? 0 : centro.getId().hashCode());
			result = prime * result + ((comisionista == null) ? 0 : comisionista.getId().hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			LiquidacionKey other = (LiquidacionKey) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (centro == null) {
				if (other.centro != null)
					return false;
			} else if (!centro.getId().equals(other.centro.getId()))
				return false;
			if (comisionista == null) {
				if (other.comisionista != null)
					return false;
			} else if (!comisionista.getId().equals(other.comisionista.getId()))
				return false;
			return true;
		}

		private LiquidacionComisionesService getOuterType() {
			return LiquidacionComisionesService.this;
		}

	}

	@Override
	public LiquidacionComisiones generarLiquidacion(Comisionista comisionista, Date fechaDesde, Date fechaHasta,
			List<TipoPagoComision> tiposComisiones, CentroCosto centro) {
		List<String> tiposComisionista = new Vector<>();
		tiposComisionista.add(comisionista.getTipoComisionista().getCodigo());
		Set<PagoComision> pagosPendientes = this.pagoComisionRepo.findEntByFiltros(comisionista.getId(),
				centro.getCodigo(), -1l,
				tiposComisiones.stream().map(TipoPagoComision::getCodigo).collect(Collectors.toList()), fechaDesde,
				fechaHasta, tiposComisionista);

		return this.crearLiquidacion(comisionista, centro, pagosPendientes, fechaDesde);
	}

	@Override
	public List<LiquidacionComisionesVO> generarLiquidacionesVendedoresMensuales() {
		List<TipoPagoComision> tipos = new Vector<>();
		tipos.add(TipoPagoComision.ADELANTO);
		tipos.add(TipoPagoComision.LIQUIDACION);
		List<TipoComisionista> tiposComisionista = new Vector<>();
		tiposComisionista.add(tcService.getByCodigo(TipoComisionista.VENDEDOR));
		return this.generarLiquidacionesMensuales(tipos, tiposComisionista);
	}
}

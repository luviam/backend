package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.security.Usuario;

public interface IUsuarioService {

	Usuario getByDescriptivo(IDescriptivo desc);

}

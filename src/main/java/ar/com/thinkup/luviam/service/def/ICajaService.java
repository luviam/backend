package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.exceptions.CajaAbiertaException;
import ar.com.thinkup.luviam.exceptions.CajaCerradaException;
import ar.com.thinkup.luviam.model.EstadoTraspaso;
import ar.com.thinkup.luviam.model.TransaccionCaja;
import ar.com.thinkup.luviam.model.TraspasoCaja;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.vo.operaciones.AltaTraspasoVO;

public interface ICajaService {

	TraspasoCaja generarTraspaso(AltaTraspasoVO traspaso, Usuario responsable);

	void confirmarTraspaso(EstadoTraspaso traspaso, Usuario responsable);

	void rechazarTraspaso(EstadoTraspaso traspaso, Usuario responsable);

	void cerrarCaja(CuentaAplicacion caja, Double monto, Usuario usuario) throws CajaCerradaException;
	
	void abrirCaja(CuentaAplicacion caja, Double monto, Usuario usuario) throws CajaAbiertaException;
	
	TransaccionCaja getEstadoCaja(Long cajaId);
	
	Double getSaldoCaja(Long cajaId); 
	
	Double getSaldoCaja(Long cajaId, TransaccionCaja trx); 
	
	
}

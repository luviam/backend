package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.model.TipoEmpleado;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;

public interface ITipoEmpleadoService extends IParametricoService<ParametricoVO,TipoEmpleado>{

}

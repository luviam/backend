package ar.com.thinkup.luviam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.thinkup.luviam.model.UnidadProducto;
import ar.com.thinkup.luviam.repository.ParameterRepository;
import ar.com.thinkup.luviam.repository.UnidadRepository;
import ar.com.thinkup.luviam.service.def.IUnidadProductoService;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;

@Service("IUnidadProductoService")
public class UnidadProductoService extends ParametricoService<ParametricoVO, UnidadProducto>
		implements IUnidadProductoService {

	@Autowired
	private UnidadRepository repo;

	@Override
	protected ParameterRepository<UnidadProducto> getRepository() {

		return this.repo;
	}

	@Override
	protected ParametricoVO toVO(UnidadProducto ent) {

		return new ParametricoVO(ent);
	}

	@Override
	protected UnidadProducto newEnt() {
		return new UnidadProducto();
	}

}

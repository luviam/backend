package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.enums.TipoTransaccionCajaEnum;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.service.contabilizacion.IContabilizable;

public interface IContableService {

	Asiento contabilizar(IContabilizable o);
	Asiento ajustarDiferenciaCaja(CuentaAplicacion caja, Double diferencia, TipoTransaccionCajaEnum tipoTrx,
			Usuario usuario);

}

package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.model.Salon;
import ar.com.thinkup.luviam.vo.SalonVO;

public interface ISalonService extends IModelService<SalonVO,Salon>, IDescriptivoService<Salon>{

}

package ar.com.thinkup.luviam.service.def;

import java.util.List;

import ar.com.thinkup.luviam.exceptions.UsuarioNoAutorizadoException;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.vo.FacturaVO;
import ar.com.thinkup.luviam.vo.documentos.facturas.FacturaCabeceraVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroProveedor;

public interface IFacturaService {

	FacturaVO guardarFactura(FacturaVO factura, Usuario responsable);

	Boolean cambioEstadoFactura(Long idFactura, DescriptivoGeneric estado, Usuario responsable);

	FacturaVO findById(Long idFactura);

	Boolean borrar(Long idFactura, Usuario responsable) throws UsuarioNoAutorizadoException;

	List<FacturaCabeceraVO> getCabeceras(FiltroProveedor filtros);

	List<FacturaCabeceraVO> getImpagas(Long idProveedor, String codigoCentro);

}

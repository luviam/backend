package ar.com.thinkup.luviam.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import ar.com.thinkup.luviam.exceptions.SendEmailException;
import ar.com.thinkup.luviam.model.EmailTemplate;
import ar.com.thinkup.luviam.service.def.MailService;
@Service("MailService")
public class MailServiceImpl extends Thread implements MailService {

	private Message message;

	private static final Logger LOG = LogManager.getLogger(MailServiceImpl.class);
	@Override
	public void send(String to, String subject, EmailTemplate html_template, Map<String, String> variables ) throws SendEmailException {

		Properties prop = new Properties();
		InputStream input = null;
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		try {
			input = loader.getResourceAsStream("application.properties");
			prop.load(input);
			String userName = prop.getProperty("mail.smtp.username");
			String password = prop.getProperty("mail.smtp.password");


			// creates a new session with an authenticator
			Authenticator auth = new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(userName, password);
				}
			};

			Session session = Session.getInstance(prop, auth);

			// creates a new e-mail message
			message = new MimeMessage(session);
			Multipart multipart = new MimeMultipart("alternative");

			message.setFrom(new InternetAddress(userName));
			InternetAddress[] toAddresses = { new InternetAddress(to) };
			message.setRecipients(Message.RecipientType.TO, toAddresses);
			message.setSubject(subject);
			message.setSentDate(new Date());

			String html = obtenerEmail(html_template, variables);
			MimeBodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(html, "text/html; charset=utf-8");

			multipart.addBodyPart(htmlPart);
			message.setContent(multipart);

			// sends the e-mail
			Transport.send(message);

		} catch (IOException | MessagingException ex) {
			throw new SendEmailException(ex);
		}

	}

	private String obtenerEmail(EmailTemplate html_template, Map<String, String> variables) {

		String html = html_template.getTemplate() ;

		for (Map.Entry<String, String> entry : variables.entrySet()) {
			html = html.replace(entry.getKey(), entry.getValue());
		}

		return html!=null ?html:"";
	}

	

}

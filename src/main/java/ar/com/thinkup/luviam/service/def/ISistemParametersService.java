package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;

public interface ISistemParametersService {

	void initialize();

	DescriptivoGeneric getByCodigo(String codigo);

}

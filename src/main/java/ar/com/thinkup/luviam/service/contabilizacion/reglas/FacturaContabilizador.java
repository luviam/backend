package ar.com.thinkup.luviam.service.contabilizacion.reglas;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.Operacion;
import ar.com.thinkup.luviam.model.documentos.factura.Factura;
import ar.com.thinkup.luviam.model.enums.CuentasPrincipalesEnum;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.enums.TipoIVAEnum;
import ar.com.thinkup.luviam.service.contabilizacion.IContabilizable;
import ar.com.thinkup.luviam.service.contabilizacion.ICuentasContablesService;
import ar.com.thinkup.luviam.service.contabilizacion.IGeneradorAsiento;

public class FacturaContabilizador implements IGeneradorAsiento<Factura> {
	@Autowired
	private ICuentasContablesService cuentasService;

	@Override
	public Asiento generarAsiento(Factura f) {

		if (f.getAsientoGenerado() != null) {
			throw new IllegalArgumentException("Factura ya contabilizada");
		}
		Asiento a = new Asiento();
		a.setDescripcion(f.getProveedor().getNombreProveedor() + " " + f.getDescripcion() + " "
				+ f.getTipoComprobante().getDescripcion() + "-" + f.getNumero());
		a.setEstado(EstadoOperacionEnum.APROBADO.getCodigo());
		a.setMensaje("Generado por sistema");
		a.setFecha(f.getFecha() != null ? f.getFecha() : new Date());
		a.setResponsable(f.getResponsable().getUsername());
		a.setTipoGenerador("AA");

		Operacion aProveedores = new Operacion();
		aProveedores.setCentro(f.getCentroCosto());
		aProveedores.setComprobante(f.getNumero());

		aProveedores.setCuenta(this.cuentasService.getCuentaProveedor(f.getCentroCosto(), f.getProveedor()));
		
		aProveedores.setDetalle("A proveedores");

		Operacion ivaCredito = new Operacion();
		ivaCredito.setCentro(f.getCentroCosto());
		ivaCredito.setComprobante(f.getNumero());
		ivaCredito.setCuenta(this.cuentasService.getCuenta(CuentasPrincipalesEnum.IVA_CREDITO_FISCAL.getCodigo(),
				f.getCentroCosto()));
		ivaCredito.setDetalle("IVA");
		

		Operacion iibbCredito = new Operacion();
		iibbCredito.setCentro(f.getCentroCosto());
		iibbCredito.setComprobante(f.getNumero());
		iibbCredito.setCuenta(this.cuentasService.getCuenta(CuentasPrincipalesEnum.IIBB_CREDITO_FISCAL.getCodigo(),
				f.getCentroCosto()));
		iibbCredito.setDetalle("IIBB");
		

		Operacion otrosImpuestos = new Operacion();
		otrosImpuestos.setCentro(f.getCentroCosto());
		otrosImpuestos.setComprobante(f.getNumero());
		otrosImpuestos.setCuenta(
				this.cuentasService.getCuenta(CuentasPrincipalesEnum.OTROS_IMPUESTOS.getCodigo(), f.getCentroCosto()));
		otrosImpuestos.setDetalle("Otros Impuestos");
		
		List<Operacion> operaciones = new Vector<>();

		if (f.getTipoComprobante().getCodigo().startsWith("NC")) {
			aProveedores.setDebe(Math.abs(f.getImporte()));
			f.getItems().forEach(i -> {
				Optional<Operacion> op = operaciones.stream()
						.filter(o -> o.getCentro().getCodigo().equals(i.getCentroCosto().getCodigo())
								&& o.getCuenta().getCodigo().equals(i.getCuenta().getCodigo()))
						.findFirst();
				if (op.isPresent()) {
					op.get().setHaber(op.get().getHaber() + Math.abs(i.getImporte()));
				} else {
					Operacion o = new Operacion();
					o.setCentro(f.getCentroCosto());
					o.setComprobante(f.getNumero());
					o.setCuenta(i.getCuenta());
					o.setHaber(Math.abs(i.getImporte()));
					o.setDetalle(i.getDescripcion());
		
					operaciones.add(o);
				}
				ivaCredito.setHaber(
						ivaCredito.getHaber() + TipoIVAEnum.getByCodigo(i.getTipoIVA()).getValor() * i.getImporte());
			});
			iibbCredito.setHaber(f.getIibbBSAS() + f.getIibbCABA() + f.getIibbOtros());
			otrosImpuestos.setHaber(f.getImpuestoSellos());
		} else {
			aProveedores.setHaber(f.getImporte());
			
			f.getItems().forEach(i -> {
				Optional<Operacion> op = operaciones.stream()
						.filter(o -> o.getCentro().getCodigo().equals(i.getCentroCosto().getCodigo())
								&& o.getCuenta().getCodigo().equals(i.getCuenta().getCodigo()))
						.findFirst();
				if (op.isPresent()) {
					op.get().setDebe(op.get().getDebe() + i.getImporte());
				} else {
					Operacion o = new Operacion();
					o.setCentro(f.getCentroCosto());
					o.setComprobante(f.getNumero());
					o.setCuenta(i.getCuenta());
					o.setDebe(i.getImporte());
					o.setDetalle(i.getDescripcion());
		
					operaciones.add(o);
				}
				ivaCredito.setDebe(
						ivaCredito.getDebe() + (TipoIVAEnum.getByCodigo(i.getTipoIVA()).getValor() * i.getImporte()));
			});
			iibbCredito.setDebe(f.getIibbBSAS() + f.getIibbCABA() + f.getIibbOtros());
			otrosImpuestos.setDebe(f.getImpuestoSellos());
		}
		a.agregarOperacion(aProveedores);
		a.agregarOperacion(ivaCredito);
		a.agregarOperacion(iibbCredito);
		a.agregarOperacion(otrosImpuestos);
		operaciones.forEach(o -> a.agregarOperacion(o));
		return a;
	}

	@Override
	public Boolean esAplicable(IContabilizable o) {
		return Factura.class.isAssignableFrom(o.getClass());
	}

}

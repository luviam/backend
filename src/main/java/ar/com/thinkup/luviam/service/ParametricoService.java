package ar.com.thinkup.luviam.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.Parametrico;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.repository.ParameterRepository;
import ar.com.thinkup.luviam.service.def.IParametricoService;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;

public abstract class ParametricoService<V extends ParametricoVO, E extends Parametrico>
		extends ModelService<E, V, ParameterRepository<E>> implements IParametricoService<V, E> {
	@Override
	public E getByDescriptivo(IDescriptivo descriptivo) {
		if (descriptivo == null || StringUtils.isEmpty(descriptivo.getCodigo())) {
			throw new IllegalArgumentException("El parametro Descriptivo es Obligatorio");
		}
		E a = this.getRepository().findByCodigo(descriptivo.getCodigo());
		return a;
	}

	@Override
	public List<V> getHabilitados() {
		return this.getRepository().findByHabilitadoIsTrue().stream().map(p -> this.toVO(p))
				.collect(Collectors.toList());
	};

	@Transactional
	public E parseToEnt(V vo, E ent) {
		if (ent == null) {
			ent = this.newEnt();
		}
		ent.setCodigo(vo.getCodigo());
		ent.setDescripcion(vo.getDescripcion());
		ent.setHabilitado(vo.getHabilitado());
		ent.setEsSistema(vo.getEsSistema());

		return ent;
	}

	@Override
	@Transactional
	public V update(V vo) {
		if (vo == null || vo.getId() == null) {
			throw new IllegalArgumentException("Debe indicar el id para actualizar");
		}
		E ent = this.getRepository().findOne(vo.getId());
		if (ent == null) {
			throw new IllegalArgumentException("No existe " + this.getNombre() + " con ID " + vo.getId());
		}
		if (!ent.getCodigo().equals(vo.getCodigo())) {
			E existente = this.getRepository().findByCodigo(vo.getCodigo());
			if (existente != null && !existente.getId().equals(vo.getId())) {
				throw new IllegalArgumentException("No pueden existir 2 con el mismo código " + vo.getCodigo());
			}
		}

		ent = this.parseToEnt(vo, ent);
		ent = this.getRepository().save(ent);
		if(StringUtils.isEmpty(ent.getCodigo())){
			ent.setCodigo(ent.getId()+"");
			ent = this.getRepository().save(ent);
		}
		return this.toVO(ent);
	}

	@Override
	@Transactional
	public V create(V vo) {
		if (vo == null || vo.getId() != null) {
			throw new IllegalArgumentException("No debe indicar el id para crear");
		}
		E existente = this.getRepository().findByCodigo(vo.getCodigo());
		if (existente != null) {
			throw new IllegalArgumentException("No pueden existir 2 con el mismo código " + vo.getCodigo());
		}
		E ent = this.parseToEnt(vo, null);
		
		ent = this.getRepository().save(ent);
		if(StringUtils.isEmpty(ent.getCodigo())){
			ent.setCodigo(ent.getId()+"");
			ent = this.getRepository().save(ent);
		}
		return this.toVO(ent);
	}

	@Override
	public E getByCodigo(String codigo) {
		return this.getRepository().findByCodigo(codigo);
	}

}

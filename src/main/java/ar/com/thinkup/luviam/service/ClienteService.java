package ar.com.thinkup.luviam.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.Cliente;
import ar.com.thinkup.luviam.model.Contacto;
import ar.com.thinkup.luviam.model.TipoCliente;
import ar.com.thinkup.luviam.model.TipoIVA;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.repository.ClienteRepository;
import ar.com.thinkup.luviam.repository.ClienteRepository.ClienteCabecera;
import ar.com.thinkup.luviam.repository.TipoClienteRepository;
import ar.com.thinkup.luviam.repository.TipoIVARepository;
import ar.com.thinkup.luviam.service.def.IClienteService;
import ar.com.thinkup.luviam.vo.ClienteListaVO;
import ar.com.thinkup.luviam.vo.ClienteVO;
import ar.com.thinkup.luviam.vo.ContactoVO;

@Service("IClienteService")
@Transactional
public class ClienteService implements IClienteService {

	@Autowired
	private ClienteRepository clienteRepo;

	@Autowired
	private TipoIVARepository ivaRepo;

	@Autowired
	private TipoClienteRepository tipoClienteRepo;

	@Override
	public ClienteVO getByID(Long id) {
		if (id == null) {
			throw new EntityNotFoundException("El cliente que quiere obtener no existe.");
		}
		Cliente cliente = this.clienteRepo.findOne(id);
		if (cliente.getId() == null)
			throw new EntityNotFoundException("El cliente que quiere obtener no existe.");
		return new ClienteVO(cliente);
	}

	@Override
	public List<ClienteListaVO> getCabeceras() {

		List<ClienteCabecera> lista = this.clienteRepo.findByActivoIsTrueOrderByIdDesc();
		return lista.stream().map(l -> new ClienteListaVO(l)).collect(Collectors.toList());
	}

	@Override
	public ClienteVO update(ClienteVO vo) {
		if (vo.getId() == null) {
			throw new EntityNotFoundException("El cliente que quiere modificar no existe.");
		}
		Cliente cliente = this.clienteRepo.findOne(vo.getId());
		if (cliente.getId() == null)
			throw new EntityNotFoundException("El cliente que quiere modificar no existe.");
		Cliente clienteModificado = this.parseToEntity(vo, cliente);
		this.clienteRepo.save(clienteModificado);
		return vo;
	}

	@Override
	public ClienteVO create(ClienteVO vo) {
		if (vo.getId() != null) {
			throw new EntityNotFoundException("El cliente que quiere crear ya existe.");
		}
		Cliente cliente = this.parseToEntity(vo, null);
		cliente.setActivo(true);
		this.clienteRepo.save(cliente);
		vo.setId(cliente.getId());
		return vo;
	}

	@Override
	public ClienteVO delete(ClienteVO vo) {
		if (vo.getId() == null) {
			throw new EntityNotFoundException("El cliente que quiere eliminar no existe.");
		}
		Cliente cliente = this.clienteRepo.findOne(vo.getId());
		if (cliente.getId() == null)
			throw new EntityNotFoundException("El cliente que quiere eliminar no existe.");
		cliente.setActivo(false);
		this.clienteRepo.save(cliente);
		vo.setId(cliente.getId());
		vo.setActivo(cliente.getActivo());
		return null;
	}

	@Override
	public List<ClienteVO> getAll() {
		List<Cliente> clientes = this.clienteRepo.findAll();
		List<ClienteVO> clientesVO = clientes.parallelStream().map(p -> new ClienteVO(p)).collect(Collectors.toList());
		return clientesVO;
	}

	public Cliente parseToEntity(ClienteVO vo, Cliente entity) {

		Cliente ent = entity != null ? entity : new Cliente();
		ent.setId(vo.getId());
		ent.setNombreCliente(vo.getNombreCliente());
		ent.setNombreContacto(vo.getNombreContacto());
		ent.setDomicilio(vo.getDomicilio());
		ent.setLocalidad(vo.getLocalidad());
		ent.setTelefono(vo.getTelefono());
		ent.setCelular(vo.getCelular());
		ent.setEmail(vo.getEmail());
		ent.setRazonSocial(vo.getRazonSocial());
		ent.setCuit(vo.getCuit());

		ent.setDomicilioFacturacion(vo.getDomicilioFacturacion());
		ent.setLocalidadFacturacion(vo.getLocalidadFacturacion());
		ent.setNombreFirmante(vo.getNombreFirmante());
		ent.setDniFirmante(vo.getDniFirmante());
		ent.setPorcComision(vo.getPorcComision());
		ent.setValorComision(vo.getValorComision());
		ent.setPorcDescuentos(vo.getPorcDescuentos());
		ent.setValorAdicional(vo.getValorAdicional());
		ent.setActivo(vo.getActivo());
		ent.setFacebook(vo.getFacebook());
		ent.setInstagram(vo.getInstagram());
		if(vo.getIva()!= null)
			ent.setIva(this.getTipoIva(vo.getIva()));

		ent.setTipoCliente(this.getTipoCliente(vo.getTipoCliente()));

		if (vo.getConvenio() != null) {
			ent.setPorcComision(vo.getConvenio().getPorcComision());
			ent.setValorAdicional(vo.getConvenio().getValorAdicional());
			ent.setPorcDescuentos(vo.getConvenio().getPorcDescuentos());
			ent.setValorComision(vo.getConvenio().getValorComision());
		} else {
			ent.setConvenio(null);
		}
		// Busco nuevos items
		vo.getContactos().stream().filter(i -> i.getId() == null)
				.forEach(i -> ent.agregarContacto(parseToEntity(i, new Contacto())));

		// Busco items modificados
		vo.getContactos().stream().filter(i -> i.getId() != null)
				.forEach(i -> this.parseToEntity(i, ent.getContacto(i.getId())));

		// Quito Eliminados
		ent.getContactos().removeIf(i -> i.getId() != null && vo.getContacto(i.getId()) == null);

		return ent;
	}

	private TipoCliente getTipoCliente(DescriptivoGeneric dg) {
		if (dg == null || StringUtils.isBlank(dg.getCodigo())) {
			throw new IllegalArgumentException("El Tipo de Cliente es obligatorio");
		}

		TipoCliente c = this.tipoClienteRepo.findByCodigo(dg.getCodigo());
		if (c == null) {
			throw new EntityNotFoundException("No se encuentra el Tipo de Cliente con codigo: " + dg.getCodigo());
		}
		return c;
	}

	private TipoIVA getTipoIva(DescriptivoGeneric dg) {
		if (dg == null || StringUtils.isBlank(dg.getCodigo())) {
			throw new IllegalArgumentException("El Tipo de IVA es obligatorio");
		}

		TipoIVA c = this.ivaRepo.findByCodigo(dg.getCodigo());
		if (c == null) {
			throw new EntityNotFoundException("No se encuentra el Tipo de IVA con codigo: " + dg.getCodigo());
		}
		return c;
	}

	private Contacto parseToEntity(ContactoVO vo, Contacto entity) {

		Contacto ent = entity;
		if (ent == null)
			ent = new Contacto();
		ent.setId(vo.getId());
		ent.setRelacionContacto(vo.getRelacionContacto());
		ent.setNombre(vo.getNombre());
		ent.setNumeroDocumento(vo.getNumeroDocumento());
		ent.setEmail(vo.getEmail());
		ent.setTelefono(vo.getTelefono());
		ent.setCelular(vo.getCelular());
		ent.setFacebook(vo.getFacebook());
		ent.setInstagram(vo.getInstagram());

		return ent;
	}

}

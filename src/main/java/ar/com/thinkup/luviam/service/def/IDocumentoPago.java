package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.exceptions.UsuarioNoAutorizadoException;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.documentos.DocumentoPago;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.vo.documentos.DocumentoPagoVO;

public interface IDocumentoPago<E extends DocumentoPago<?>, V extends DocumentoPagoVO<?, ?, ?>> extends IModelService<V,E> {

	V guardarDocumento(V doc, Usuario responsable);

	Boolean cambioEstadoDocumento(Long idDocumento, DescriptivoGeneric estado, Usuario responsable);

	V findById(Long idDocumento);

	Boolean borrar(Long idDocumento, Usuario responsable) throws UsuarioNoAutorizadoException;

	// List<DocumentoCabeceraVO> getCabeceras(String codigoCentro, Long idProveedor,
	// Date desdeDate, Date hastaDate);
}

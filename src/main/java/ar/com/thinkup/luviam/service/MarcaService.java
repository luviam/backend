package ar.com.thinkup.luviam.service;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.transaction.Transactional;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import ar.com.thinkup.luviam.model.Archivo;
import ar.com.thinkup.luviam.model.MarcaProductos;
import ar.com.thinkup.luviam.model.SistemParameter;
import ar.com.thinkup.luviam.repository.ArchivoRepository;
import ar.com.thinkup.luviam.repository.MarcaProductosReposirtory;
import ar.com.thinkup.luviam.service.def.IMarcaService;
import ar.com.thinkup.luviam.service.def.ISistemParametersService;
import ar.com.thinkup.luviam.vo.ArchivoVO;
import ar.com.thinkup.luviam.vo.MarcaVO;

@Service("IMarcaService")
public class MarcaService extends ParametricoService<MarcaVO, MarcaProductos> implements IMarcaService {

	@Autowired
	private MarcaProductosReposirtory repo;

	@Autowired
	private ArchivoRepository aRepo;

	@Autowired
	private ISistemParametersService paramService;

	private String getNewFileName(MultipartFile file) {
		MarcaProductos m = this.repo.findTopByOrderByIdDesc();
		String ext = "." + FilenameUtils.getExtension(file.getOriginalFilename());
		return "LM_" + (m != null ? m.getId() + 1 : 1) + ext;
	}

	@Override
	protected MarcaProductosReposirtory getRepository() {
		return this.repo;
	}

	@Override
	protected MarcaVO toVO(MarcaProductos ent) {
		return new MarcaVO(ent);
	}

	@Override
	protected MarcaProductos newEnt() {
		return new MarcaProductos();
	}

	@Override
	@Transactional
	public MarcaVO create(MultipartFile logo, MarcaVO marca) {

		Archivo a = new Archivo();
		a.setFechaModificacion(new Date());
		a.setNombreOriginal(logo.getOriginalFilename());
		a.setTipo(logo.getContentType());
		a.setVersion(1);
		String filename = this.getNewFileName(logo);
		a.setRuta(SistemParameter.BASE_URL + SistemParameter.LOGO_MARCA + filename);
		a.setUrl(SistemParameter.LOGO_MARCA + filename);
		try {
			logo.transferTo(new File(a.getRuta()));
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
			throw new IllegalArgumentException("Error al crear el logo");
		}
		a = this.aRepo.save(a);
		marca.setLogo(new ArchivoVO(a));
		return super.create(marca);
	}

	@Override
	@Transactional
	public MarcaVO update(MultipartFile logo, MarcaVO marca) {
		if (marca.getLogo() != null) {
			Archivo a = aRepo.findOne(marca.getLogo().getId());

			a.setFechaModificacion(new Date());
			a.setNombreOriginal(logo.getOriginalFilename());
			a.setTipo(logo.getContentType());
			a.setVersion(a.getId() != null ? a.getVersion() + 1 : 1);
			String filename = this.getNewFileName(logo);
			a.setRuta(a.getId() != null ? a.getRuta() : SistemParameter.LOGO_MARCA + filename);
			a.setUrl(a.getId() != null ? a.getUrl() : SistemParameter.LOGO_MARCA + filename);
			try {

				logo.transferTo(new File(a.getRuta()));
			} catch (IllegalStateException | IOException e) {
				e.printStackTrace();
				throw new IllegalArgumentException("Error al crear el logo");
			}
			a = this.aRepo.save(a);

			marca.setLogo(new ArchivoVO(a));
		}
		return super.update(marca);
	}

	@Override
	public MarcaProductos parseToEnt(MarcaVO vo, MarcaProductos ent) {

		ent = super.parseToEnt(vo, ent);
		if (vo.getLogo() != null) {
			ent.setArchivo(this.aRepo.findOne(vo.getLogo().getId()));
		} else {
			ent.setArchivo(null);
		}
		return ent;

	}

}

package ar.com.thinkup.luviam.service.def;

import java.util.List;

import ar.com.thinkup.luviam.model.Cheque;
import ar.com.thinkup.luviam.vo.ChequeVO;

public interface IChequeService extends IModelService<ChequeVO,Cheque>{

	List<ChequeVO> getDisponibles();

}

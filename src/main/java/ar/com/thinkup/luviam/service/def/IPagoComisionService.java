package ar.com.thinkup.luviam.service.def;

import java.util.List;

import ar.com.thinkup.luviam.model.contratos.PagoComision;
import ar.com.thinkup.luviam.vo.PagoComisionVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroComisiones;

public interface IPagoComisionService extends IModelService<PagoComisionVO,PagoComision>{

	List<PagoComisionVO> getImpagos(FiltroComisiones filtro);

	
}

package ar.com.thinkup.luviam.service;

import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.Precio;
import ar.com.thinkup.luviam.repository.PrecioRepository;
import ar.com.thinkup.luviam.service.def.IPrecioService;
import ar.com.thinkup.luviam.service.def.IProductoService;
import ar.com.thinkup.luviam.service.def.ISalonService;
import ar.com.thinkup.luviam.service.def.IUsuarioService;
import ar.com.thinkup.luviam.vo.PrecioVO;
import ar.com.thinkup.luviam.vo.parametricos.ConflictoPrecio;

@Transactional
public abstract class PrecioService<P extends Precio, V extends PrecioVO, R extends PrecioRepository<P>>
		implements IPrecioService<V, P> {

	@Autowired
	private IProductoService productoService;

	@Autowired
	private ISalonService salonService;

	@Autowired
	private IUsuarioService uService;

	protected abstract R getRepository();

	protected String getNombre() {
		return "Precio";
	}

	@Override
	@Transactional
	public V getByID(Long id) {
		if (id == null) {
			throw new IllegalArgumentException("El id es obligatorio");
		}
		return this.toVO(this.getRepository().findOne(id));
	}

	protected abstract V toVO(P ent);

	protected abstract P newEnt();

	@Override
	@Transactional
	public List<ConflictoPrecio> update(V vo) {
		return persist(vo);
	}

	private List<P> getConflictos(V vo) {
		Long id = vo.getId() != null? vo.getId() : -1l;
		Date hasta = vo.getFechaHasta() != null? vo.getFechaHasta() : new DateTime().plusYears(1000).toDate();
		if(vo.getSalon() == null || StringUtils.isEmpty(vo.getSalon().getCodigo())) {
			throw new IllegalArgumentException("El salon es obligatorio");
		}
		if(vo.getProducto() == null || StringUtils.isEmpty(vo.getProducto().getCodigo())) {
			throw new IllegalArgumentException("El salon es obligatorio");
		}
		Long idSalon =  Long.valueOf(vo.getSalon().getCodigo());
		
		return this.getRepository().getConflictos(id,vo.getProducto().getCodigo(),vo.getFechaDesde(),
				hasta,idSalon).stream().filter(p -> p.coincideEnDias(vo.getDiasSemana())).collect(Collectors.toList());
	}

	@Transactional
	public abstract P parseToEnt(V vo, P ent);

	@Override
	@Transactional
	public List<ConflictoPrecio> create(V vo) {
		if (vo == null || vo.getId() != null) {
			throw new IllegalArgumentException("No debe indicar el id para crear");
		}
		List<ConflictoPrecio> conflictos = persist(vo);

		return conflictos;
	}

	@Transactional
	private List<ConflictoPrecio> persist(V vo) {
		List<P> preciosEnConflicto = this.getConflictos(vo);
		List<ConflictoPrecio> conflictos = new Vector<>();
		ConflictoPrecio cp = new ConflictoPrecio(vo);
		conflictos.add(cp);
		if (preciosEnConflicto.size() > 0) {
			preciosEnConflicto.stream().forEach(p -> {
				cp.addConflicto(this.toVO(p));
			});
		} else {
			P ent;
			if(vo.getId()!=null) {
				ent = this.getEntity(vo.getId());
			}else {
				ent = this.newEnt();
			}
			ent = this.parseToEnt(vo, ent);
			this.getRepository().save(ent);
		}
		return conflictos;
	}

	@Override
	@Transactional
	public Boolean delete(Long id) {
		if (id == null) {
			throw new IllegalArgumentException("Debe indicar el id para eliminar");
		}
		P ent = this.getRepository().findOne(id);
		if (ent == null) {
			throw new IllegalArgumentException("No existe " + this.getNombre() + " con ID " + id);
		}
		try {
			this.getRepository().delete(ent);
			return true;
		} catch (Exception e) {
			ent.setActivo(false);
			this.getRepository().save(ent);
			return true;
		}

	}

	@Override
	public P getEntity(Long id) {
		return this.getRepository().findOne(id);
	}

	@Override
	@Transactional
	public List<V> getAll() {
		return this.getRepository().findAll().stream().map(p -> this.toVO(p)).collect(Collectors.toList());
	}
	@Transactional
	public Precio parseToEntity(PrecioVO vo, Precio entity) {
		Precio ent = entity != null ? entity : this.newEnt();
		ent.setProducto(this.productoService.getByDescriptivo(vo.getProducto()));
		ent.setFechaDesde(vo.getFechaDesde());
		ent.setFechaHasta(vo.getFechaHasta());
		ent.setActivo(vo.getActivo());
		ent.setResponsable(this.uService.getByDescriptivo(vo.getResponsable()));
		ent.setDiasSemana(vo.getDiasSemana());
		ent.setSalon(this.salonService.getByDescriptivo(vo.getSalon()));
		ent.setValor(vo.getValor());
		ent.setUltimaModificacion(new Date());

		return ent;

	}

	@Override
	@Transactional
	public List<ConflictoPrecio> create(List<V> vo) {
		List<ConflictoPrecio> conflictos = new Vector<>();
		List<V> sinConflictos = new Vector<>();
		vo.stream().forEach(p -> {
			List<V> conf =  vo.stream().filter(pp-> pp != p && p.enConflicto(pp)).collect(Collectors.toList());
			if(conf.size()>0) {
				ConflictoPrecio c = new ConflictoPrecio(p);
				conf.forEach(cc -> c.addConflicto(cc));
			}else {
				sinConflictos.add(p);
			}
		});
		for (V v : sinConflictos) {
			conflictos.addAll(this.create(v));
		}
		return conflictos;
	}

	@Override
	@Transactional
	public List<ConflictoPrecio> update(List<V> vo) {
		List<ConflictoPrecio> conflictos = new Vector<>();
		List<V> sinConflictos = new Vector<>();
		vo.forEach(p -> {
			List<V> conf =  vo.stream().filter(pp-> pp != p && p.enConflicto(pp)).collect(Collectors.toList());
			if(conf.size()>0) {
				ConflictoPrecio c = new ConflictoPrecio(p);
				conf.forEach(cc -> c.addConflicto(cc));
				conflictos.add(c);
			}else {
				sinConflictos.add(p);
			}
		});
		for (V v : sinConflictos) {
			conflictos.addAll(this.update(v));	
		}
		return conflictos;
	}

	@Override
	@Transactional
	public Boolean delete(List<V> precios) {
		Boolean state = true;
		for (V v : precios) {
			state = state && this.delete(v.getId());
		}
		return state;
	}
	
	

}

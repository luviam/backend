package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.controller.ICommonService;

public interface IModelService<V, E> extends ICommonService<V, E>{

	V update(V vo);

	V create(V vo);

	

}

package ar.com.thinkup.luviam.service.def;

import java.util.List;

import ar.com.thinkup.luviam.controller.ICommonService;
import ar.com.thinkup.luviam.model.Precio;
import ar.com.thinkup.luviam.vo.PrecioVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroPrecio;
import ar.com.thinkup.luviam.vo.parametricos.ConflictoPrecio;

public interface IPrecioService<V extends PrecioVO, P extends Precio> extends ICommonService<V,P>{ 
	
	List<ConflictoPrecio> update(V vo);

	List<ConflictoPrecio> create(List<V> vo);
	
	List<ConflictoPrecio> update(List<V> vo);

	List<ConflictoPrecio> create(V vo);

	Boolean delete(List<V> precios);

	List<V> getByFiltro(FiltroPrecio filtro);
	

	
}

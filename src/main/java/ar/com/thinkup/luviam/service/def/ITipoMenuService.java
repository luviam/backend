package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.model.TipoMenu;
import ar.com.thinkup.luviam.vo.TipoMenuVO;

public interface ITipoMenuService extends IParametricoService<TipoMenuVO, TipoMenu>{

	
}

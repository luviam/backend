package ar.com.thinkup.luviam.service;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.Producto;
import ar.com.thinkup.luviam.model.Salon;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.repository.ProductoRepository;
import ar.com.thinkup.luviam.repository.SalonRepository;
import ar.com.thinkup.luviam.service.def.IComisionistaService;
import ar.com.thinkup.luviam.service.def.ISalonService;
import ar.com.thinkup.luviam.vo.ProductoVO;
import ar.com.thinkup.luviam.vo.SalonVO;

@Service("ISalonService")
public class SalonService extends ModelService<Salon, SalonVO, SalonRepository> implements ISalonService {

	@Autowired
	private SalonRepository repo;

	@Autowired
	private ProductoRepository productoRepo;

	@Autowired
	private IComisionistaService cService;

	@Override
	@Transactional
	public Salon parseToEnt(SalonVO vo, Salon salon) {
		if (salon == null)
			salon = new Salon();
		salon.setCodigoPostal(vo.getCodigoPostal());
		salon.setDireccion(vo.getDireccion());
		salon.setEmail(vo.getEmail());
		salon.setLatitud(vo.getLatitud());
		salon.setLongitud(vo.getLongitud());
		salon.setNombre(vo.getNombre());
		salon.setNombreContacto(vo.getNombreContacto());
		salon.setProducto(this.getUnidad(vo.getProductoAlquiler()));

		salon.setTelefono(vo.getTelefono());
		salon.setActivo(vo.getActivo());
		if (vo.getEsComisionista()) {
			salon.setComisionista(cService.updateBySalon(salon));
		} else {
			salon.setComisionista(null);
		}
		return salon;
	}

	private Producto getUnidad(ProductoVO unidadAlquiler) {
		if (unidadAlquiler == null || StringUtils.isBlank(unidadAlquiler.getCodigo())) {
			throw new IllegalArgumentException("Debe indicar un parametro para el producto");
		}
		Producto u = this.productoRepo.findByCodigo(unidadAlquiler.getCodigo());
		if (u == null) {
			throw new EntityNotFoundException("No existe Producto con código " + unidadAlquiler.getCodigo());
		}
		return u;
	}

	@Override
	protected SalonRepository getRepository() {
		return this.repo;
	}

	@Override
	protected SalonVO toVO(Salon ent) {
		return new SalonVO(ent);
	}

	@Override
	protected Salon newEnt() {
		return new Salon();
	}

	@Override
	public Salon getByDescriptivo(IDescriptivo descriptivo) {
		if (descriptivo == null || StringUtils.isEmpty(descriptivo.getCodigo())) {
			throw new IllegalArgumentException("El parametro Descriptivo es Obligatorio");
		}
		Salon a = this.getRepository().findOne(Long.valueOf(descriptivo.getCodigo()));
		if (a == null) {
			throw new IllegalArgumentException("No existe Salón con id " + descriptivo.getCodigo());
		}
		return a;

	}

	@Override
	public Salon getByCodigo(String codigo) {
		return this.repo.findOne(Long.valueOf(codigo));
	}

}

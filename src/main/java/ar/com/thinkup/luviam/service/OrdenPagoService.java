package ar.com.thinkup.luviam.service;

import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.exceptions.UsuarioNoAutorizadoException;
import ar.com.thinkup.luviam.model.Cheque;
import ar.com.thinkup.luviam.model.Proveedor;
import ar.com.thinkup.luviam.model.TipoComprobante;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.documentos.factura.Factura;
import ar.com.thinkup.luviam.model.documentos.pagos.ItemOrdenPago;
import ar.com.thinkup.luviam.model.documentos.pagos.OrdenPago;
import ar.com.thinkup.luviam.model.documentos.pagos.Pago;
import ar.com.thinkup.luviam.model.enums.EstadoCheque;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.enums.TipoCheque;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.repository.AsientoRepository;
import ar.com.thinkup.luviam.repository.ChequeRepository;
import ar.com.thinkup.luviam.repository.FacturaRepository;
import ar.com.thinkup.luviam.repository.ItemOrdenPagoRepository;
import ar.com.thinkup.luviam.repository.ItemOrdenPagoRepository.ItemOrdenPagoCabecera;
import ar.com.thinkup.luviam.repository.OrdenPagoRepository;
import ar.com.thinkup.luviam.repository.OrdenPagoRepository.OrdenPagoCabecera;
import ar.com.thinkup.luviam.repository.ProveedorRepository;
import ar.com.thinkup.luviam.repository.TipoComprobanteRepository;
import ar.com.thinkup.luviam.service.def.IContableService;
import ar.com.thinkup.luviam.service.def.IOrdenPagoService;
import ar.com.thinkup.luviam.vo.OrdenPagoCabeceraVO;
import ar.com.thinkup.luviam.vo.documentos.pagos.ItemOrdenPagoVO;
import ar.com.thinkup.luviam.vo.documentos.pagos.OrdenPagoVO;
import ar.com.thinkup.luviam.vo.documentos.pagos.PagoVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroProveedor;

@Service("IOrdenPagoService")
@Transactional
public class OrdenPagoService extends DocumentoPagoService<Pago, PagoVO, OrdenPago, OrdenPagoVO>
		implements IOrdenPagoService {

	@Autowired
	private OrdenPagoRepository ordenPagoRepo;

	@Autowired
	private ProveedorRepository proveedorRepo;

	@Autowired
	private FacturaRepository facturaRepo;

	@Autowired
	private IContableService contableService;

	@Autowired
	private AsientoRepository asientoRepo;

	@Autowired
	private ItemOrdenPagoRepository itemsRepo;

	@Autowired
	private TipoComprobanteRepository tcRepo;

	@Autowired
	private ChequeRepository chequeRepo;

	@Override
	public OrdenPagoVO guardarDocumento(OrdenPagoVO ordenPago, Usuario responsable) {
		final OrdenPago op;
		Boolean modificable = false;
		if (ordenPago.getId() != null) {
			op = ordenPagoRepo.findOne(ordenPago.getId());
			// SOlo se puede modificar si el estado no es aprobado y el usuario es quien
			// creó la ordenPago o un administrador
			if (op == null) {
				throw new EntityNotFoundException("No existe la ordenPago con ID: " + ordenPago.getId());
			}
			modificable = (responsable.esAdministrador()
					|| op.getResponsable().getUsername().equals(responsable.getUsername()))
					&& !op.getEstadoOperacion().equals(EstadoOperacionEnum.APROBADO);

		} else {
			op = new OrdenPago();
			modificable = true;
			op.setResponsable(responsable);
		}

		if (modificable) {
			op.setImporte(0d);
			this.parseToEnt(ordenPago, op);

		}
		if (op.getEstadoOperacion().equals(EstadoOperacionEnum.APROBADO) && op.getAsientoGenerado() == null) {
			this.contabilizar(op);
		}

		OrdenPago o = ordenPagoRepo.save(op);
		if (StringUtils.isBlank(o.getNumero())) {
			o.setNumero(String.valueOf(op.getId()));

			ordenPagoRepo.save(op);
		}
		return new OrdenPagoVO(o);

	}

	private Factura crearPagoACuenta(Double pagoACuenta) {
		if (pagoACuenta > 0) {

			Factura doc = new Factura();
			TipoComprobante tc = this.tcRepo.findByCodigo(TipoComprobante.PAGO_ADELANTADO);
			doc.setTipoComprobante(tc);
			doc.setImporte(Math.abs(pagoACuenta) * -1);
			doc.setSaldo(doc.getImporte());
			return doc;
		}
		return null;
	}

	private ItemOrdenPago parseToEntity(ItemOrdenPago entity, ItemOrdenPagoVO vo) {

		entity.setFactura(this.getFactura(vo.getCabeceraFactura().getId()));

		entity.setImporte(vo.getImporte());
		return entity;
	}

	private Factura getFactura(Long id) {
		if (id == null) {
			throw new IllegalArgumentException("El id de la factura es obligatorio");
		}
		Factura f = this.facturaRepo.getOne(id);
		if (f == null) {
			throw new EntityNotFoundException("No se encuentra la factura con id: " + id);
		}
		return f;
	}

	private Proveedor getProveedor(DescriptivoGeneric proveedor) {
		if (proveedor == null || StringUtils.isBlank(proveedor.getCodigo())) {
			throw new IllegalArgumentException("El Proveedor es obligatorio");
		}

		Proveedor c = this.proveedorRepo.findOne(Long.valueOf(proveedor.getCodigo()));
		if (c == null) {
			throw new EntityNotFoundException("No se encuentra el Proveedor con cuit: " + proveedor.getCodigo());
		}
		return c;
	}

	@Override
	public Boolean cambioEstadoDocumento(Long idOrdenPago, DescriptivoGeneric estado, Usuario responsable) {
		if (idOrdenPago == null || estado == null) {
			throw new IllegalArgumentException("El id de la OrdenPago y el estado son obligatorios");
		}

		OrdenPago op = this.ordenPagoRepo.findOne(idOrdenPago);
		if (op == null) {
			throw new EntityNotFoundException("No se encuentra la ordenPago con id: " + idOrdenPago);
		}
		if (EstadoOperacionEnum.getByCodigo(estado.getCodigo()) != null) {
			op.setCodigoEstado(estado.getCodigo());
			if (op.getPagoACuenta() != null)
				op.getPagoACuenta().setEstado(estado.getCodigo());
			if (op.getAsientoGenerado() == null && op.getEstadoOperacion().equals(EstadoOperacionEnum.APROBADO)) {
				if (op.getPagos().size() == 0 && op.getImporte() > 0) {
					op.setEstado(EstadoOperacionEnum.POR_PAGAR);
				} else {
					op.setAsientoGenerado(this.contableService.contabilizar(op));
					List<Factura> aActuralizar = new Vector<>();
					op.getItems().stream().forEach(i -> {
						i.getFactura().setSaldo(i.getFactura().getSaldo() - i.getImporte());
						if (i.getFactura().getSaldo() < 0) {
							i.getFactura().setSaldo(0d);
						}
						aActuralizar.add(i.getFactura());
					});
					this.facturaRepo.save(aActuralizar);
				}

			}
			ordenPagoRepo.save(op);
		}

		return true;
	}

	private void contabilizar(OrdenPago op) {
		op.setAsientoGenerado(this.contableService.contabilizar(op));
		List<Factura> aActuralizar = new Vector<>();
		op.getItems().forEach(i -> {
			i.getFactura().setSaldo(i.getFactura().getSaldo() - i.getImporte());
			aActuralizar.add(i.getFactura());
		});
		this.facturaRepo.save(aActuralizar);
	}

	@Override
	public Boolean borrar(Long idOrdenPago, Usuario responsable) throws UsuarioNoAutorizadoException {
		if (idOrdenPago == null || responsable == null) {
			throw new IllegalArgumentException("El ID de la ordenPago y el responsbale son obligatorios");
		}
		OrdenPago op = this.ordenPagoRepo.findOne(idOrdenPago);
		if (op == null) {
			throw new EntityNotFoundException("No se encuentra la OrdenPago con ID: " + idOrdenPago);
		}
		List<Factura> aActuralizar = new Vector<>();
		List<Cheque> cActualizar = new Vector<>();
		if (op.getAsientoGenerado() != null) {
			if (responsable.esAdministrador()) {
				this.asientoRepo.delete(op.getAsientoGenerado());

				op.getItems().forEach(i -> {
					i.getFactura().setSaldo(i.getFactura().getSaldo() + i.getImporte());
					aActuralizar.add(i.getFactura());

				});

				op.getPagos().stream().filter(p -> p.getCheque() != null).forEach(p -> {
					p.getCheque().setEstadoCheque(EstadoCheque.DISPONIBLE);
					p.getCheque().setDestino(null);
					if (p.getCheque().getTipoCheque().equals(TipoCheque.PROPIO)) {
						p.getCheque().setImporte(null);
						p.getCheque().setFechaEmision(null);
						p.getCheque().setFechaIngreso(null);
						p.getCheque().setFechaPago(null);

					}
					cActualizar.add(p.getCheque());
				});

				this.facturaRepo.save(aActuralizar);
				this.chequeRepo.save(cActualizar);
				if (op.getPagoACuenta() != null) {
					this.facturaRepo.delete(op.getPagoACuenta());
				}
				this.ordenPagoRepo.delete(op);

			} else {
				throw new UsuarioNoAutorizadoException();
			}

		} else {
			if (op.getEstadoOperacion().equals(EstadoOperacionEnum.APROBADO)) {
				op.getItems().forEach(i -> {

					i.getFactura().setSaldo(i.getFactura().getSaldo() + i.getImporte());
					aActuralizar.add(i.getFactura());

				});
				this.facturaRepo.save(aActuralizar);
			}
			op.getPagos().stream().filter(p -> p.getCheque() != null).forEach(p -> {
				p.getCheque().setEstadoCheque(EstadoCheque.DISPONIBLE);
				p.getCheque().setDestino(null);
				p.getCheque().setPago(null);
				if (p.getCheque().getTipoCheque().equals(TipoCheque.PROPIO)) {
					p.getCheque().setImporte(0d);
				}
			});
			if (op.getPagoACuenta() != null) {
				this.facturaRepo.delete(op.getPagoACuenta());
			}
			this.ordenPagoRepo.delete(op);

		}

		return true;
	}

	@Override
	public List<OrdenPagoCabeceraVO> getCabeceras(FiltroProveedor filtros) {
		Long idProveedor = StringUtils.isBlank(filtros.getCodigoProveedor()) ? -1l
				: Long.valueOf(filtros.getCodigoProveedor());
		if (filtros.getFechaDesde() == null) {
			filtros.setFechaDesde(new DateTime().minusYears(1).withTimeAtStartOfDay().toDate());
		}
		if (filtros.getFechaHasta() == null) {

			filtros.setFechaHasta(new DateTime().plusDays(1).withTimeAtStartOfDay().minusSeconds(1).toDate());
		}

		if (filtros.getEstados().size() == 0) {
			EstadoOperacionEnum.values();
			for (EstadoOperacionEnum e : EstadoOperacionEnum.values()) {
				filtros.getEstados().add(e.getCodigo());
			}
		}
		List<OrdenPagoCabecera> ordenesPago = this.ordenPagoRepo.getCabeceras(filtros.getCodigoCentro(), idProveedor,
				filtros.getFechaDesde(), filtros.getFechaHasta());
		return ordenesPago.stream().map(op -> new OrdenPagoCabeceraVO(op)).collect(Collectors.toList());
	}

	@Override
	protected Pago getNewValores() {
		return new Pago();
	}

	@Override
	public List<ItemOrdenPagoVO> getItems(Long idOrdenPago) {
		if (idOrdenPago == null) {
			throw new IllegalArgumentException("El id de orden es obligatorio");
		}
		List<ItemOrdenPagoCabecera> items = this.itemsRepo.findByOrdenPagoId(idOrdenPago);
		return items.parallelStream().map(i -> new ItemOrdenPagoVO(i)).collect(Collectors.toList());
	}

	@Override
	public List<ItemOrdenPagoVO> getItemsByFactura(Long idFactura) {
		if (idFactura == null) {
			throw new IllegalArgumentException("El id de orden es obligatorio");
		}
		List<ItemOrdenPagoCabecera> items = this.itemsRepo.findByFacturaIdAndOrdenPagoCodigoEstado(idFactura,
				EstadoOperacionEnum.APROBADO.getCodigo());
		return items.parallelStream().map(i -> new ItemOrdenPagoVO(i)).collect(Collectors.toList());
	}

	@Override
	protected OrdenPagoRepository getRepository() {
		return this.ordenPagoRepo;
	}

	@Override
	protected OrdenPagoVO toVO(OrdenPago ent) {
		return new OrdenPagoVO(ent);
	}

	@Override
	protected OrdenPago newEnt() {
		return new OrdenPago();
	}

	@Override
	public OrdenPago parseToEnt(OrdenPagoVO ordenPago, OrdenPago op) {
		super.parseToEnt(ordenPago, op);

		op.setProveedor(this.getProveedor(ordenPago.getProveedor()));

		// Busco nuevos items
		ordenPago.getItems().stream().filter(i -> i.getId() == null)
				.forEach(i -> op.addItem(parseToEntity(new ItemOrdenPago(), i)));

		// Busco items modificados
		ordenPago.getItems().stream().filter(i -> i.getId() != null)
				.forEach(i -> this.parseToEntity(op.getItem(i.getId()), i));

		// Quito Eliminados
		op.getItems().removeIf(i -> i.getId() != null && ordenPago.getItem(i.getId()) == null);
		if (ordenPago.getEstado() != null
				&& ordenPago.getEstado().getCodigo().equals(EstadoOperacionEnum.POR_PAGAR.getCodigo())) {
			if (ordenPago.getPagos().size() > 0 || (ordenPago.getPagos().size() == 0 && ordenPago.getImporte() == 0)) {
				op.setEstado(EstadoOperacionEnum.APROBADO);
			} else {
				op.setEstado(EstadoOperacionEnum.POR_PAGAR);
			}

		} else {
			op.setEstado(EstadoOperacionEnum.PENDIENTE);
		}

		if (ordenPago.getPagoACuenta() != null && ordenPago.getPagoACuenta() > 0) {
			if (op.getPagoACuenta() != null) {
				op.getPagoACuenta().setImporte(Math.abs(ordenPago.getPagoACuenta()) * -1);
				op.getPagoACuenta().setSaldo(op.getPagoACuenta().getImporte());
			} else {
				op.setPagoACuenta(this.crearPagoACuenta(ordenPago.getPagoACuenta()));
			}

		} else {
			if (op.getPagoACuenta() != null) {
				this.facturaRepo.delete(op.getPagoACuenta());
			}
			op.setPagoACuenta(null);
		}
		op.setImporte(op.getTotal());

		return op;
	}

}

package ar.com.thinkup.luviam.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.exceptions.UsuarioNoAutorizadoException;
import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.Proveedor;
import ar.com.thinkup.luviam.model.TipoComprobante;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.documentos.factura.Factura;
import ar.com.thinkup.luviam.model.documentos.factura.ItemFactura;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.enums.TipoIVAEnum;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.repository.AsientoRepository;
import ar.com.thinkup.luviam.repository.CentroRepository;
import ar.com.thinkup.luviam.repository.CuentaAplicacionRepository;
import ar.com.thinkup.luviam.repository.FacturaRepository;
import ar.com.thinkup.luviam.repository.ProveedorRepository;
import ar.com.thinkup.luviam.repository.TipoComprobanteRepository;
import ar.com.thinkup.luviam.service.def.IContableService;
import ar.com.thinkup.luviam.service.def.IFacturaService;
import ar.com.thinkup.luviam.service.def.IOrdenPagoService;
import ar.com.thinkup.luviam.vo.FacturaVO;
import ar.com.thinkup.luviam.vo.ItemFacturaVO;
import ar.com.thinkup.luviam.vo.documentos.facturas.FacturaCabeceraVO;
import ar.com.thinkup.luviam.vo.documentos.pagos.ItemOrdenPagoVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroProveedor;
import ar.com.thinkup.utils.MathUtils;

@Service("IFacturaService")
@Transactional
public class FacturaService implements IFacturaService {

	@Autowired
	private FacturaRepository facturaRepo;
	@Autowired
	private CentroRepository centroRepo;
	@Autowired
	private TipoComprobanteRepository tipoComprobanteRepo;
	@Autowired
	private ProveedorRepository proveedorRepo;

	@Autowired
	private CuentaAplicacionRepository cuentaRepo;
	@Autowired
	private IContableService contableService;

	@Autowired
	private AsientoRepository asientoRepo;
	@Autowired
	private IOrdenPagoService opService;

	@Override
	public FacturaVO guardarFactura(FacturaVO factura, Usuario responsable) {
		final Factura f;
		Boolean modificable = false;
		if (factura.getId() != null) {
			f = facturaRepo.findOne(factura.getId());
			// SOlo se puede modificar si el estado no es aprobado y el usuario es quien
			// creó la factura o un administrador
			if (f == null) {
				throw new EntityNotFoundException("No existe la factura con ID: " + factura.getId());
			}
			modificable = (responsable.esAdministrador()
					|| f.getResponsable().getUsername().equals(responsable.getUsername()))
					&& !f.getEstado().equals(EstadoOperacionEnum.APROBADO.getCodigo());
			

		} else {
			f = new Factura();
			modificable = true;
			Long idProveedor = Long.valueOf(factura.getProveedor().getCodigo());
			Factura existente = facturaRepo.findByTipoComprobanteCodigoAndNumeroAndProveedorId(
					factura.getTipoComprobante().getCodigo(), factura.getNumero(), idProveedor);
			if (existente != null) {
				throw new DuplicateKeyException("Ya existe un " + factura.getTipoComprobante().getDescripcion()
						+ " con número " + factura.getNumero());
			}
			f.setResponsable(responsable);

			f.setItems(new Vector<>());
		}

		if (modificable) {
			f.setImporte(0d);
			f.setEstado(EstadoOperacionEnum.PENDIENTE.getCodigo());
			f.setCentroCosto(this.getCentro(factura.getCentroCosto()));
			f.setTipoComprobante(this.getTipoComprobante(factura.getTipoComprobante()));
			f.setProveedor(this.getProveedor(factura.getProveedor()));
			f.setDescripcion(factura.getDescripcion());
			f.setFecha(factura.getFecha());
			f.setFechaIVA(factura.getFechaIVA());
			f.setIibbBSAS(factura.getIibbBSAS());
			f.setIibbCABA(factura.getIibbCABA());
			f.setIibbOtros(factura.getIibbOtros());
			f.setImpuestoSellos(factura.getImpuestoSellos());
			

			f.setNumero(factura.getNumero());

			// Quito Eliminados
			f.getItems().removeIf(i -> i.getId() != null && factura.getItem(i.getId()) == null);
			
			// Busco nuevos items
			factura.getItems().stream().filter(i -> i.getId() == null)
					.forEach(i -> f.addItemFactura(parseToEntity(new ItemFactura(), i)));

			// Busco items modificados
			factura.getItems().stream().filter(i -> i.getId() != null)
					.forEach(i -> this.parseToEntity(f.getItem(i.getId()), i));

		

			f.getItems().stream().forEach(i -> {
				f.sumarATotal(MathUtils.round(i.getImporte() * (1 + TipoIVAEnum.getByCodigo(i.getTipoIVA()).getValor()),2));
			});
			f.sumarATotal(f.getIibbBSAS() + f.getIibbCABA() + f.getIibbOtros() + f.getImpuestoSellos());

			if(f.getId()!= null) {
				List<ItemOrdenPagoVO> items = this.opService.getItemsByFactura(f.getId());
				Double total = items.stream().mapToDouble(o -> o.getImporte()).sum();
				f.setSaldo(f.getImporte() - total);
			}else {
				f.setSaldo(f.getImporte());
			}
			if (f.getSaldo() > f.getImporte()) {
				f.setSaldo(f.getImporte());
			}
			if (f.getTipoComprobante().getCodigo().substring(0, 2).equals("NC")
					|| f.getTipoComprobante().getCodigo().substring(0, 2).equals("PA")) {
				f.setSaldo(Math.abs(f.getSaldo()) * -1);
				f.setImporte(Math.abs(f.getImporte()) * -1);
			}

		}
		return new FacturaVO(facturaRepo.save(f));

	}

	private ItemFactura parseToEntity(ItemFactura entity, ItemFacturaVO vo) {

		entity.setCentroCosto(this.getCentro(vo.getCentroCosto()));
		entity.setCuenta(this.getCuenta(vo.getCuenta(), vo.getCentroCosto()));
		entity.setDescripcion(vo.getDescripcion());
		entity.setImporte(vo.getImporte());
		entity.setTipoIVA(vo.getTipoIVA().getCodigo());
		return entity;
	}

	private Proveedor getProveedor(DescriptivoGeneric proveedor) {
		if (proveedor == null || StringUtils.isBlank(proveedor.getCodigo())) {
			throw new IllegalArgumentException("El Proveedor es obligatorio");
		}

		Proveedor c = this.proveedorRepo.findOne(Long.valueOf(proveedor.getCodigo()));
		if (c == null) {
			throw new EntityNotFoundException("No se encuentra el Proveedor con cuit: " + proveedor.getCodigo());
		}
		return c;
	}

	private TipoComprobante getTipoComprobante(DescriptivoGeneric tipoComprobante) {
		if (tipoComprobante == null || StringUtils.isBlank(tipoComprobante.getCodigo())) {
			throw new IllegalArgumentException("El Tipo de Comprobante es obligatorio");
		}

		TipoComprobante c = this.tipoComprobanteRepo.findByCodigo(tipoComprobante.getCodigo());
		if (c == null) {
			throw new EntityNotFoundException(
					"No se encuentra el Tipo de Comprobante con codigo: " + tipoComprobante.getCodigo());
		}
		return c;
	}

	public Boolean cambioEstadoFactura(Long idFactura, DescriptivoGeneric estado, Usuario responsable) {
		if (idFactura == null || estado == null) {
			throw new IllegalArgumentException("El id de la Factura y el estado son obligatorios");
		}

		Factura f = this.facturaRepo.findOne(idFactura);
		if (f == null) {
			throw new EntityNotFoundException("No se encuentra la factura con id: " + idFactura);
		}
		if (EstadoOperacionEnum.getByCodigo(estado.getCodigo()) != null) {
			f.setEstado(estado.getCodigo());

			if (f.getAsientoGenerado() == null && f.getEstado().equals(EstadoOperacionEnum.APROBADO.getCodigo())) {
				f.setAsientoGenerado(this.contableService.contabilizar(f));
			}
			facturaRepo.save(f);
		}

		return true;
	}

	private CuentaAplicacion getCuenta(DescriptivoGeneric cuenta, DescriptivoGeneric centro) {
		if (cuenta == null || StringUtils.isBlank(cuenta.getCodigo())) {
			throw new IllegalArgumentException("La cuenta es obligatoria");
		}
		if (centro == null || StringUtils.isBlank(centro.getCodigo())) {
			throw new IllegalArgumentException("El Centro es obligatorio");
		}
		CuentaAplicacion c = this.cuentaRepo
				.findByCodigoAndCentroCostoCodigoAndCentroCostoActivoTrue(cuenta.getCodigo(), centro.getCodigo());
		if (c == null) {
			throw new EntityNotFoundException("No se encuentra la cuenta con codigo: " + cuenta.getCodigo());
		}
		return c;
	}

	private CentroCosto getCentro(DescriptivoGeneric centroCosto) {
		if (centroCosto == null || StringUtils.isBlank(centroCosto.getCodigo())) {
			throw new IllegalArgumentException("El Centro e obligatorio");
		}

		CentroCosto c = this.centroRepo.findOneByCodigo(centroCosto.getCodigo());
		if (c == null) {
			throw new EntityNotFoundException("No se encuentra el centro con codigo: " + centroCosto.getCodigo());
		}
		return c;
	}

	@Override
	public FacturaVO findById(Long idFactura) {
		if (idFactura == null) {
			throw new IllegalArgumentException("El id es obligatorio");
		}
		Factura f = this.facturaRepo.findOne(idFactura);
		if (f == null) {
			throw new EntityNotFoundException("No hay facturas con ID: " + idFactura);
		}
		return new FacturaVO(f);
	}

	@Override
	public Boolean borrar(Long idFactura, Usuario responsable) throws UsuarioNoAutorizadoException {
		if (idFactura == null || responsable == null) {
			throw new IllegalArgumentException("El ID de la factura y el responsbale son obligatorios");
		}
		Factura f = this.facturaRepo.findOne(idFactura);
		if (f == null) {
			throw new EntityNotFoundException("No se encuentra la Factura con ID: " + idFactura);
		}
		if (f.getAsientoGenerado() != null) {
			if (responsable.esAdministrador()) {
				this.asientoRepo.delete(f.getAsientoGenerado());
				this.facturaRepo.delete(f);
			} else {
				throw new UsuarioNoAutorizadoException();
			}

		} else {
			this.facturaRepo.delete(f);
		}

		return true;
	}

	@Override
	public List<FacturaCabeceraVO> getImpagas(Long idProveedor, String codigoCentro) {

		return this.facturaRepo.getCabecerasPorProveedorEstadoSaldo(codigoCentro, idProveedor, "A", new BigDecimal(0)).stream()
				.map(f -> new FacturaCabeceraVO(f)).collect(Collectors.toList());
	}

	@Override
	public List<FacturaCabeceraVO> getCabeceras(FiltroProveedor filtros) {

		Long idProveedor = StringUtils.isBlank(filtros.getCodigoProveedor()) ? -1l
				: Long.valueOf(filtros.getCodigoProveedor());
		if (filtros.getFechaDesde() == null) {
			filtros.setFechaDesde(new DateTime().minusYears(1).withTimeAtStartOfDay().toDate());
		}
		if (filtros.getFechaHasta() == null) {

			filtros.setFechaHasta(new DateTime().plusDays(1).withTimeAtStartOfDay().minusSeconds(1).toDate());
		}

		if (filtros.getEstados().size() == 0) {
			EstadoOperacionEnum.values();
			for (EstadoOperacionEnum e : EstadoOperacionEnum.values()) {
				filtros.getEstados().add(e.getCodigo());
			}
		}
		if (filtros.getTiposComprobante().size() == 0) {
			filtros.setTiposComprobante(this.tipoComprobanteRepo.findAll().stream().map(TipoComprobante::getCodigo)
					.collect(Collectors.toList()));
		}
		return this.facturaRepo
				.getCabeceras(idProveedor, filtros.getCodigoCentro(), filtros.getTiposComprobante(),
						filtros.getEstados(), filtros.getFechaDesde(), filtros.getFechaHasta())
				.stream().map(f -> new FacturaCabeceraVO(f)).collect(Collectors.toList());
	}

}

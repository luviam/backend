package ar.com.thinkup.luviam.service;

import java.io.File;
import java.util.List;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.thinkup.luviam.model.SistemParameter;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.repository.SistemParametersRepository;
import ar.com.thinkup.luviam.service.def.ISistemParametersService;
@Service("ISistemParametersService")
public class SistemParameterService implements ISistemParametersService{

	@Autowired
	private SistemParametersRepository sysRepo;
	
	@Override
	public void initialize() {
		List<SistemParameter> params=  sysRepo.findAll();
		List<SistemParameter> paramsToUpdate = new Vector<>();
		if(params.stream().noneMatch(p -> p.getCodigo().equals(SistemParameter.URL_LINKS))){
			paramsToUpdate.add(new SistemParameter(null,SistemParameter.URL_LINKS, "http://localhost:4200/"));
		}
	
	
		if(paramsToUpdate.size()> 0) {
			this.sysRepo.save(paramsToUpdate);
		}
		 File directory;
			SistemParameter logopath = params.stream().filter(p -> p.getCodigo().equals(SistemParameter.LOGO_MARCA)).findFirst().orElse(null);
			if(logopath!= null) {
				directory = new File(logopath.getDescripcion());
				    if (! directory.exists()){
				        directory.mkdir();
				    }
			}
			directory = new File(SistemParameter.TEMPORAL_URL);
		    if (! directory.exists()){
		        directory.mkdir();
		    }
		    
		    directory = new File(SistemParameter.BASE_URL + SistemParameter.LOGO_MARCA);
		    if (! directory.exists()){
		        directory.mkdir();
		    }
		
	}

	@Override
	public DescriptivoGeneric getByCodigo(String codigo) {
		SistemParameter s = this.sysRepo.findByCodigo(codigo);
		if(s == null) {
			throw new IllegalAccessError("NO EXISTE EL PARAMETrO " + codigo);
		}
		return new DescriptivoGeneric(s);
	}
	
	

}

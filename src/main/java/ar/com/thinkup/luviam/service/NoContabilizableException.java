package ar.com.thinkup.luviam.service;

import ar.com.thinkup.luviam.service.contabilizacion.IContabilizable;

public class NoContabilizableException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private IContabilizable object;

	public NoContabilizableException(IContabilizable o) {
		super();
		this.object = o;
	}

	@Override
	public String getMessage() {
	
		return "No se puede contabilizar " + this.object.getClass().getSimpleName();
	}
	
	
	
	

}

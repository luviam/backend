package ar.com.thinkup.luviam.service;

import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.exceptions.UsuarioNoAutorizadoException;
import ar.com.thinkup.luviam.model.Cheque;
import ar.com.thinkup.luviam.model.contratos.Contrato;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.documentos.cobros.Cobro;
import ar.com.thinkup.luviam.model.documentos.cobros.ItemOrdenCobranza;
import ar.com.thinkup.luviam.model.documentos.cobros.OrdenCobranza;
import ar.com.thinkup.luviam.model.enums.EstadoCheque;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.repository.AsientoRepository;
import ar.com.thinkup.luviam.repository.ChequeRepository;
import ar.com.thinkup.luviam.repository.ContratoRepository;
import ar.com.thinkup.luviam.repository.DocumentoRepository;
import ar.com.thinkup.luviam.repository.ItemOrdenCobranzaRepository;
import ar.com.thinkup.luviam.repository.ItemOrdenCobranzaRepository.ItemOrdenCobranzaCabecera;
import ar.com.thinkup.luviam.repository.OrdenCobranzaRepository;
import ar.com.thinkup.luviam.repository.OrdenCobranzaRepository.OrdenCobranzaCabecera;
import ar.com.thinkup.luviam.service.def.IComisionService;
import ar.com.thinkup.luviam.service.def.IContableService;
import ar.com.thinkup.luviam.service.def.ICuotaServicioService;
import ar.com.thinkup.luviam.service.def.IOrdenCobranzaService;
import ar.com.thinkup.luviam.vo.ContratoCabeceraVO;
import ar.com.thinkup.luviam.vo.OrdenCobranzaCabeceraVO;
import ar.com.thinkup.luviam.vo.documentos.cobros.CobroVO;
import ar.com.thinkup.luviam.vo.documentos.cobros.ItemOrdenCobranzaVO;
import ar.com.thinkup.luviam.vo.documentos.cobros.OrdenCobranzaVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroCliente;

@Service("IOrdenCobranzaService")
@Transactional
public class OrdenCobranzaService extends DocumentoPagoService<Cobro, CobroVO, OrdenCobranza, OrdenCobranzaVO>
		implements IOrdenCobranzaService {

	@Autowired
	private OrdenCobranzaRepository ordenCobranzaRepo;

	@Autowired
	private ContratoRepository contratoRepo;

	@Autowired
	private ICuotaServicioService cuotaService;

	@Autowired
	private IContableService contableService;

	@Autowired
	private AsientoRepository asientoRepo;

	@Autowired
	private ItemOrdenCobranzaRepository itemsRepo;
	@Autowired
	private ChequeRepository chequeRepo;

	@Autowired
	private IComisionService comisionesService;

	@Override
	public OrdenCobranzaVO guardarDocumento(OrdenCobranzaVO ordenCobranza, Usuario responsable) {
		final OrdenCobranza op;
		Boolean modificable = false;
		if (ordenCobranza.getId() != null) {
			op = ordenCobranzaRepo.findOne(ordenCobranza.getId());
			// SOlo se puede modificar si el estado no es aprobado y el usuario es quien
			// creó la ordenCobranza o un administrador
			if (op == null) {
				throw new EntityNotFoundException("No existe la ordenCobranza con ID: " + ordenCobranza.getId());
			}
			modificable = (responsable.esAdministrador()
					|| op.getResponsable().getUsername().equals(responsable.getUsername()))
					&& !op.getEstadoOperacion().equals(EstadoOperacionEnum.APROBADO);

		} else {
			op = new OrdenCobranza();
			modificable = true;
			op.setResponsable(responsable);
		}

		if (modificable) {
			this.parseToEnt(ordenCobranza, op);
		}

		OrdenCobranza o = ordenCobranzaRepo.save(op);
		if (StringUtils.isBlank(o.getNumero())) {
			o.setNumero(String.valueOf(op.getId()));

			ordenCobranzaRepo.save(op);
		}
		return new OrdenCobranzaVO(o);

	}

	private ItemOrdenCobranza parseToEntity(ItemOrdenCobranza entity, ItemOrdenCobranzaVO vo) {

		entity.setCuotaServicio(this.cuotaService.getEntity(vo.getCuotaServicio().getId()));

		entity.setImporte(vo.getImporte());
		return entity;
	}

	private Contrato getContrato(ContratoCabeceraVO contrato) {
		if (contrato == null) {
			throw new IllegalArgumentException("El Contrato es obligatorio");
		}

		Contrato c = this.contratoRepo.findOne(contrato.getId());
		if (c == null) {
			throw new EntityNotFoundException("No se encuentra el Contrato con id: " + contrato.getId());
		}
		return c;
	}

	@Override
	public List<OrdenCobranzaCabeceraVO> getCabeceras(FiltroCliente filtros) {

		if (filtros.getFechaDesde() == null) {
			filtros.setFechaDesde(new DateTime().minusYears(1).withTimeAtStartOfDay().toDate());
		}
		if (filtros.getFechaHasta() == null) {

			filtros.setFechaHasta(new DateTime().plusDays(1).withTimeAtStartOfDay().minusSeconds(1).toDate());
		}

		List<OrdenCobranzaCabecera> ordenesCobranza = this.ordenCobranzaRepo.getCabeceras(filtros.getIdCliente(),
				filtros.getIdSalon(), filtros.getCodigoCentro(), filtros.getCodigoHorario(), filtros.getFechaDesde(),
				filtros.getFechaHasta());
		return ordenesCobranza.stream().map(op -> new OrdenCobranzaCabeceraVO(op)).collect(Collectors.toList());
	}

	@Override
	public Boolean cambioEstadoDocumento(Long idOrdenCobranza, DescriptivoGeneric estado, Usuario responsable) {
		if (idOrdenCobranza == null || estado == null) {
			throw new IllegalArgumentException("El id de la OrdenCobranza y el estado son obligatorios");
		}

		OrdenCobranza op = this.ordenCobranzaRepo.findOne(idOrdenCobranza);
		if (op == null) {
			throw new EntityNotFoundException("No se encuentra la ordenCobranza con id: " + idOrdenCobranza);
		}
		if (EstadoOperacionEnum.getByCodigo(estado.getCodigo()) != null) {
			op.setCodigoEstado(estado.getCodigo());

			if (op.getAsientoGenerado() == null && op.getEstadoOperacion().equals(EstadoOperacionEnum.APROBADO)) {
				if (op.getCobros().size() == 0) {
					op.setEstado(EstadoOperacionEnum.POR_PAGAR);
				} else {
					op = this.contabilizar(op);
				}

			}
			ordenCobranzaRepo.save(op);
		}

		return true;
	}

	private OrdenCobranza contabilizar(OrdenCobranza op) {
		op.setAsientoGenerado(this.contableService.contabilizar(op));
		this.cuotaService.actualizarSaldos(op.getCuotasCobradas());
		return this.comisionesService.generarPorCobros(op);

	}

	@Override
	public Boolean borrar(Long idOrdenCobranza, Usuario responsable) throws UsuarioNoAutorizadoException {
		if (idOrdenCobranza == null || responsable == null) {
			throw new IllegalArgumentException("El ID de la ordenCobranza y el responsbale son obligatorios");
		}
		OrdenCobranza op = this.ordenCobranzaRepo.findOne(idOrdenCobranza);
		if (op == null) {
			throw new EntityNotFoundException("No se encuentra la OrdenCobranza con ID: " + idOrdenCobranza);
		}

		if (op.getAsientoGenerado() != null) {
			if (responsable.esAdministrador()) {
				this.asientoRepo.delete(op.getAsientoGenerado());
				this.ordenCobranzaRepo.delete(op);

			} else {
				throw new UsuarioNoAutorizadoException();
			}

		} else {
			List<Cheque> aBorrar = new Vector<>();
			op.getCobros().stream().filter(
					c -> c.getCheque() != null && c.getCheque().getEstadoCheque().equals(EstadoCheque.DISPONIBLE))
					.forEach(p -> aBorrar.add(p.getCheque()));

			this.ordenCobranzaRepo.delete(op);
			this.chequeRepo.delete(aBorrar);

		}
		this.cuotaService.actualizarSaldos(op.getCuotasCobradas());

		return true;
	}

	@Override
	protected Cobro getNewValores() {
		return new Cobro();
	}

	@Override
	public DocumentoRepository<OrdenCobranza> getRepository() {
		return this.ordenCobranzaRepo;
	}

	@Override
	public List<ItemOrdenCobranzaVO> getItems(Long idOrdenCobranza) {
		if (idOrdenCobranza == null) {
			throw new IllegalArgumentException("El id de orden es obligatorio");
		}
		List<ItemOrdenCobranzaCabecera> items = this.itemsRepo.findByOrdenCobranzaId(idOrdenCobranza);
		return items.stream().map(i -> new ItemOrdenCobranzaVO(i)).collect(Collectors.toList());
	}

	@Override
	public List<OrdenCobranzaVO> getByContrato(Long idContrato) {

		if (idContrato == null) {

			throw new IllegalArgumentException("El id es nulo");
		}

		List<OrdenCobranza> ordenesCobranza = this.ordenCobranzaRepo.findByContratoIdAndCodigoEstado(idContrato,
				EstadoOperacionEnum.APROBADO.getCodigo());
		return ordenesCobranza.stream().map(op -> new OrdenCobranzaVO(op)).collect(Collectors.toList());
	}

	@Override
	protected OrdenCobranzaVO toVO(OrdenCobranza ent) {

		return new OrdenCobranzaVO(ent);
	}

	@Override
	protected OrdenCobranza newEnt() {
		return new OrdenCobranza();
	}

	@Override
	public OrdenCobranza parseToEnt(OrdenCobranzaVO ordenCobranza, OrdenCobranza op) {
		super.parseToEnt(ordenCobranza, op);

		op.setContrato(this.getContrato(ordenCobranza.getContrato()));

		// Quito Eliminados
		op.getItems().removeIf(i -> i.getId() != null && ordenCobranza.getItem(i.getId()) == null);

		if (ordenCobranza.getEstado() != null
				&& ordenCobranza.getEstado().getCodigo().equals(EstadoOperacionEnum.POR_PAGAR.getCodigo())) {
			if (ordenCobranza.getCobros().size() > 0) {
				op.setEstado(EstadoOperacionEnum.APROBADO);
			} else {
				op.setEstado(EstadoOperacionEnum.POR_PAGAR);
			}

		} else {
			op.setEstado(EstadoOperacionEnum.PENDIENTE);
		}

		// Busco nuevos items
		ordenCobranza.getItems().stream().filter(i -> i.getId() == null)
				.forEach(i -> op.addItem(parseToEntity(new ItemOrdenCobranza(), i)));

		// Busco items modificados
		ordenCobranza.getItems().stream().filter(i -> i.getId() != null)
				.forEach(i -> this.parseToEntity(op.getItem(i.getId()), i));

		op.setObservaciones(ordenCobranza.getObservaciones());
		op.setIvaCobrado(ordenCobranza.getTotalImpuestos());
		op.setImporteNeto(ordenCobranza.getImporteNeto());
		op.setImporte(ordenCobranza.getImporteNeto() + ordenCobranza.getTotalImpuestos());
		this.cuotaService.actualizarSaldos(op.getCuotasCobradas());
		if (op.getEstadoOperacion().equals(EstadoOperacionEnum.APROBADO) && op.getAsientoGenerado() == null) {
			this.contabilizar(op);
		}
		return op;
	}

	@Override
	@Transactional
	public List<OrdenCobranzaCabeceraVO> getCabecerasByContrato(Long idContrato) {

		return this.ordenCobranzaRepo.findByContratoId(idContrato).stream().map(c -> new OrdenCobranzaCabeceraVO(c))
				.collect(Collectors.toList());
	}

}

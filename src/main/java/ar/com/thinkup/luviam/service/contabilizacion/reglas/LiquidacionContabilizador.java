package ar.com.thinkup.luviam.service.contabilizacion.reglas;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.Operacion;
import ar.com.thinkup.luviam.model.documentos.comisiones.LiquidacionComisiones;
import ar.com.thinkup.luviam.model.enums.CuentasPrincipalesEnum;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.service.contabilizacion.IContabilizable;
import ar.com.thinkup.luviam.service.contabilizacion.ICuentasContablesService;
import ar.com.thinkup.luviam.service.contabilizacion.IGeneradorAsiento;

public class LiquidacionContabilizador implements IGeneradorAsiento<LiquidacionComisiones> {

	@Autowired
	private ICuentasContablesService cuentasService;

	@Override
	public Boolean esAplicable(IContabilizable o) {
		if (!LiquidacionComisiones.class.isAssignableFrom(o.getClass())) {
			return false;
		}
		LiquidacionComisiones l = (LiquidacionComisiones) o;
		return l.getPagosLiquidacion().size() == 0 || l.getAsiento() == null;

	}

	@Override
	public Asiento generarAsiento(LiquidacionComisiones o) {
		String codigoDevengar = CuentasPrincipalesEnum.COMISIONES_A_DEVENGAR.getCodigo()
				+ o.getComisionista().getTipoComisionista().getCodigo();
		String codigoPagar = CuentasPrincipalesEnum.COMISIONES_A_PAGAR.getCodigo()
				+ o.getComisionista().getTipoComisionista().getCodigo();
		CuentaAplicacion comDevengar = this.cuentasService.getCuenta(codigoDevengar, o.getCentroCosto());
		CuentaAplicacion comPagar = this.cuentasService.getCuenta(codigoPagar, o.getCentroCosto());

		Asiento a = new Asiento();
		a.setDescripcion("Registro comisiones " + o.getComisionista().getDescripcion());
		a.setEstado(EstadoOperacionEnum.APROBADO.getCodigo());
		a.setMensaje("Generado por sistema");
		a.setFecha(o.getFecha() != null ? o.getFecha() : new Date());
		a.setResponsable(o.getResponsable() == null ? "SISTEMA" : o.getResponsable().getUsername());
		a.setTipoGenerador("AA");

		Operacion devengar = new Operacion();
		devengar.setCentro(o.getCentroCosto());
		devengar.setComprobante("Liq. N° " + o.getNumero());
		devengar.setCuenta(comDevengar);
		devengar.setDebe(o.getTotal());
		devengar.setDetalle(o.getDescripcion());

		a.agregarOperacion(devengar);
		Operacion aPagar = new Operacion();
		aPagar.setCentro(o.getCentroCosto());
		aPagar.setComprobante("Liq. N° " + o.getNumero());
		aPagar.setCuenta(comPagar);
		aPagar.setHaber(o.getTotal());
		aPagar.setDetalle(o.getDescripcion());
		a.agregarOperacion(aPagar);

		return a;
	}

}

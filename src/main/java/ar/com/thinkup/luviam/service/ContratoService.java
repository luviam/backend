package ar.com.thinkup.luviam.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.Cliente;
import ar.com.thinkup.luviam.model.Producto;
import ar.com.thinkup.luviam.model.Salon;
import ar.com.thinkup.luviam.model.TipoEvento;
import ar.com.thinkup.luviam.model.contratos.ConfiguracionComision;
import ar.com.thinkup.luviam.model.contratos.Contrato;
import ar.com.thinkup.luviam.model.contratos.CuotaServicio;
import ar.com.thinkup.luviam.model.contratos.ServicioContratado;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.empleados.Empleado;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.enums.TipoHorarioEnum;
import ar.com.thinkup.luviam.repository.ClienteRepository;
import ar.com.thinkup.luviam.repository.ContratoCabeceraRepository;
import ar.com.thinkup.luviam.repository.ContratoRepository;
import ar.com.thinkup.luviam.repository.EmpleadoRepository;
import ar.com.thinkup.luviam.repository.ProductoRepository;
import ar.com.thinkup.luviam.repository.SalonRepository;
import ar.com.thinkup.luviam.repository.TipoEventoRepository;
import ar.com.thinkup.luviam.service.def.ICentroCostoService;
import ar.com.thinkup.luviam.service.def.IComisionService;
import ar.com.thinkup.luviam.service.def.IContableService;
import ar.com.thinkup.luviam.service.def.IContratoService;
import ar.com.thinkup.luviam.service.def.ICuotaServicioService;
import ar.com.thinkup.luviam.service.def.IOrdenCobranzaService;
import ar.com.thinkup.luviam.vo.ClienteVO;
import ar.com.thinkup.luviam.vo.ContratoCabeceraVO;
import ar.com.thinkup.luviam.vo.SalonVO;
import ar.com.thinkup.luviam.vo.contratos.ConfiguracionComisionVO;
import ar.com.thinkup.luviam.vo.contratos.ContratoVO;
import ar.com.thinkup.luviam.vo.contratos.CuotaServicioVO;
import ar.com.thinkup.luviam.vo.contratos.EstadoIVAVO;
import ar.com.thinkup.luviam.vo.contratos.ServicioContratadoVO;
import ar.com.thinkup.luviam.vo.documentos.cobros.OrdenCobranzaVO;
import ar.com.thinkup.utils.DescriptivoParser;

@Service("IContratoService")
@Transactional
public class ContratoService extends ModelService<Contrato, ContratoVO, ContratoRepository>
		implements IContratoService {

	@Autowired
	private SalonRepository salonRepo;

	@Autowired
	private EmpleadoRepository empleadoRepo;

	@Autowired
	private ClienteRepository clienteRepo;

	@Autowired
	private TipoEventoRepository tipoEventoRepo;

	@Autowired
	private ContratoRepository contratoRepository;

	@Autowired
	private ContratoCabeceraRepository ccRepository;

	@Autowired
	private IComisionService comisionService;

	@Autowired
	private ProductoRepository productoRepo;

	@Autowired
	private ICuotaServicioService cuotaService;

	@Autowired
	private ICentroCostoService centroService;

	@Autowired
	private IContableService contableService;

	@Autowired
	private IOrdenCobranzaService cobranzasService;

	@Override
	public List<ContratoVO> getAll() {
		return this.contratoRepository.findAll().stream().map(c -> new ContratoVO(c)).collect(Collectors.toList());
	}

	private TipoEvento getTipoEvento(DescriptivoGeneric tipoEvento) {
		if (tipoEvento == null || StringUtils.isEmpty(tipoEvento.getCodigo())) {
			throw new IllegalArgumentException("Debe indicar el id del Tipo Evento ");
		}
		TipoEvento c = this.tipoEventoRepo.findByCodigo(tipoEvento.getCodigo());
		if (c == null) {
			throw new EntityNotFoundException("No se encuentra un Tipo Evento con id " + tipoEvento.getCodigo());
		}
		return c;
	}

	private Cliente getCliente(ClienteVO cliente) {
		if (cliente == null || cliente.getId() == null) {
			throw new IllegalArgumentException("Debe indicar el id del Cliente ");
		}
		Cliente c = this.clienteRepo.getOne(cliente.getId());
		if (c == null) {
			throw new EntityNotFoundException("No se encuentra un Cliente con id " + cliente.getId());
		}
		return c;
	}

	private Empleado getEmpleado(DescriptivoGeneric vendedor) {
		if (vendedor == null || StringUtils.isEmpty(vendedor.getCodigo())) {
			throw new IllegalArgumentException("Debe indicar el id del Empleado ");
		}
		Empleado c = this.empleadoRepo.getOne(Long.valueOf(vendedor.getCodigo()));
		if (c == null) {
			throw new EntityNotFoundException("No se encuentra un Empleado con id " + vendedor.getCodigo());
		}
		return c;
	}

	private Salon getSalon(SalonVO salon) {
		if (salon == null || salon.getId() == null) {
			throw new IllegalArgumentException("Debe indicar el id del Salon ");
		}
		Salon c = this.salonRepo.getOne(salon.getId());
		if (c == null) {
			throw new EntityNotFoundException("No se encuentra un Salon con id " + salon.getId());
		}
		return c;
	}

	private Producto getProducto(IDescriptivo producto) {
		if (producto == null || StringUtils.isEmpty(producto.getCodigo())) {
			throw new IllegalArgumentException("Debe indicar el codigo del Producto ");
		}
		Producto p = this.productoRepo.findByCodigo(producto.getCodigo());
		if (p == null) {
			throw new EntityNotFoundException("No se encuentra un Producto con codigo " + producto.getCodigo());
		}
		return p;
	}

	public ServicioContratado parseToEntity(ServicioContratadoVO vo, ServicioContratado entity) {

		ServicioContratado ent;
		if (entity == null) {
			ent = new ServicioContratado();
		} else {
			ent = entity;
		}
		ent.setFechaContrato(vo.getFechaContrato());
		ent.setGrupo(vo.getGrupo());
		if (vo.getProducto() == null) {
			throw new IllegalArgumentException("El producto es obligatorio ");
		}
		if (ent.getProducto() == null || !ent.getProducto().getId().equals(vo.getProducto().getId())) {
			ent.setProducto(this.getProducto(vo.getProducto()));
		}

		// Quito Eliminados
		ent.getPlanPago().removeIf(i -> i.getId() != null && vo.getCuota(i.getId()) == null);

		// Busco nuevos items
		vo.getPlanPago().stream().filter(i -> i.getId() == null)
				.forEach(i -> ent.agregarCuota(this.parseToEntity(i, new CuotaServicio())));

		// Busco items modificados
		vo.getPlanPago().stream().filter(i -> i.getId() != null).forEach(i -> this.parseToEntity(i, ent.getCuota(i)));

		ent.setCostoUnitario(vo.getCostoUnitario());
		ent.setCantidad(vo.getCantidad());
		ent.setCentroCosto(this.centroService.getByDescriptivo(vo.getCentroCosto()));

		return ent;
	}

	public CuotaServicio parseToEntity(CuotaServicioVO vo, CuotaServicio entity) {

		return this.cuotaService.parseToEnt(vo, entity);
	}

	@Override
	public List<ContratoCabeceraVO> getAllByIdCliente(Long idCliente) {
		if (idCliente == null) {
			throw new IllegalArgumentException("El id del cliente es obligatorio");
		}
		return this.ccRepository.findByIdCliente(idCliente).stream().map(c -> new ContratoCabeceraVO(c))
				.collect(Collectors.toList());
	}

	@Override
	public List<DescriptivoGeneric> getAllDescriptivosByIdCliente(Long idCliente) {
		if (idCliente == null) {
			throw new IllegalArgumentException("El id del cliente es obligatorio");
		}
		return this.ccRepository.findByIdCliente(idCliente).stream().map(c -> DescriptivoParser.parse(c))
				.collect(Collectors.toList());
	}

	@Override
	public List<CuotaServicioVO> getCuotas(String codigoContrato) {
		if (StringUtils.isBlank(codigoContrato)) {
			throw new IllegalArgumentException("El contrato es obligatorio");
		}
		return this.cuotaService.getCuotasServicioContratado(Long.valueOf(codigoContrato)).stream()
				.map(c -> new CuotaServicioVO(c)).collect(Collectors.toList());

	}

	@Override
	public List<ContratoCabeceraVO> getAllCabecera() {

		return this.ccRepository.findAll().stream().map(c -> new ContratoCabeceraVO(c)).collect(Collectors.toList());
	}

	@Override
	public List<CuotaServicioVO> getCuotasImpagas(String codigoContrato) {
		if (StringUtils.isBlank(codigoContrato)) {
			throw new IllegalArgumentException("El contrato es obligatorio");
		}
		return this.cuotaService.getCuotasImpagas(Long.valueOf(codigoContrato)).stream()
				.map(c -> new CuotaServicioVO(c)).collect(Collectors.toList());

	}

	@Override
	@Transactional
	public ContratoVO aprobar(Long idContrato) {
		Contrato c = this.contratoRepository.findOne(idContrato);
		if (c == null) {
			throw new IllegalArgumentException("No existe el contrato con id " + idContrato);

		}
		c.setEstado(EstadoOperacionEnum.APROBADO);
		c.setAsientoGenerado(this.contableService.contabilizar(c));
		c = this.comisionService.generarAdelantos(c);
		c = this.contratoRepository.save(c);

		return new ContratoVO(c);
	}

	@Override
	@Transactional
	public ContratoVO rechazar(Long idContrato) {
		Contrato c = this.contratoRepository.findOne(idContrato);
		if (c == null) {
			throw new IllegalArgumentException("No existe el contrato con id " + idContrato);

		}
		if (c.esAprobado()) {
			throw new IllegalArgumentException("No se puede rechazar un contrato ya aprobado");
		}
		c.setEstado(EstadoOperacionEnum.RECHAZADO);
		c = this.contratoRepository.save(c);

		return new ContratoVO(c);
	}

	@Override
	@Transactional
	public ContratoVO reaplicar(Long idContrato) {
		Contrato c = this.contratoRepository.findOne(idContrato);
		if (c == null) {
			throw new IllegalArgumentException("No existe el contrato con id " + idContrato);

		}
		if (c.esAprobado() || !c.getEstado().equals(EstadoOperacionEnum.RECHAZADO)) {
			throw new IllegalArgumentException("No se puede reaplicar un contrato ya aprobado o que no esté rechazado");
		}
		c.setEstado(EstadoOperacionEnum.PENDIENTE);
		c = this.contratoRepository.save(c);

		return new ContratoVO(c);
	}

	@Override
	protected ContratoRepository getRepository() {
		// TODO Auto-generated method stub
		return this.contratoRepository;
	}

	@Override
	protected ContratoVO toVO(Contrato ent) {
		return new ContratoVO(ent);
	}

	@Override
	protected Contrato newEnt() {
		return new Contrato();
	}

	@Override
	public Contrato parseToEnt(ContratoVO vo, Contrato entity) {

		Contrato ent;
		if (entity == null) {
			ent = new Contrato();
		} else {
			ent = entity;
		}
		if (vo.getSalon() != null && vo.getSalon().getId() == null) {
			ent.setNombreSalon(vo.getSalon().getNombre());
			ent.setTelefonoSalon(vo.getSalon().getTelefono());
			ent.setEmailSalon(vo.getSalon().getEmail());
			ent.setDireccionSalon(vo.getSalon().getDireccion());
			ent.setSalon(null);
		} else {
			ent.setNombreSalon(null);
			ent.setTelefonoSalon(null);
			ent.setEmailSalon(null);
			ent.setDireccionSalon(null);
			ent.setSalon(this.getSalon(vo.getSalon()));
		}

		ent.setVendedor(this.getEmpleado(vo.getVendedor()));
		ent.setTipoEvento(this.getTipoEvento(vo.getTipoEvento()));
		ent.setCliente(this.getCliente(vo.getCliente()));
		ent.setFechaContratacion(vo.getFechaContratacion());
		ent.setFechaEvento(vo.getFechaEvento());
		ent.setBaseInvitados(vo.getBaseInvitados());
		ent.setAdultos(vo.getAdultos());
		ent.setAdolescentes(vo.getAdolescentes());
		ent.setMenores(vo.getMenores());
		ent.setBebes(vo.getBebes());
		ent.setNumeroPresupuesto(vo.getNumeroPresupuesto());
		ent.setDescripcion(vo.getDescripcion());
		ent.setEsCongelado(vo.getEsCongelado());
		ent.setObservaciones(vo.getObservaciones());
		ent.setAgasajados(vo.getAgasajados());

		ent.setIpMaximo(vo.getIpMaximo());
		ent.setIpMonto(vo.getIpMonto());

		ent.setCobroGarantia(vo.getCobroGarantia());
		if (vo.getHorario() != null) {
			TipoHorarioEnum th = TipoHorarioEnum.byCodigo(vo.getHorario().getCodigo());
			if (th == null) {
				throw new IllegalArgumentException(
						"No existe un Tipo de Horario con código " + vo.getHorario().getCodigo());
			}
			ent.setTipoHorario(th);
		}
		if (vo.getEstado() != null) {
			EstadoOperacionEnum e = EstadoOperacionEnum.getByCodigo(vo.getEstado().getCodigo());
			if (e == null) {
				throw new IllegalAccessError("No existe el estado con codigo " + vo.getEstado().getCodigo());
			}
			ent.setEstado(e);
		} else {
			ent.setEstado(EstadoOperacionEnum.PENDIENTE);
		}

		// Quito Eliminados
		ent.getServiciosSet().removeIf(i -> i.getId() != null && vo.getServicio(i.getId()) == null);

		// Busco nuevos items
		vo.getServicios().stream().filter(i -> i.getId() == null)
				.forEach(i -> ent.addServicio(this.parseToEntity(i, new ServicioContratado())));

		// Busco items modificados
		vo.getServicios().stream().filter(i -> i.getId() != null)
				.forEach(i -> this.parseToEntity(i, ent.getServicio(i)));

		// Quito Eliminados
		ent.getComisionesSet().removeIf(i -> i.getId() != null && vo.getComision(i.getId()) == null);

		// Busco nuevos items
		vo.getComisiones().stream().filter(i -> i.getId() == null)
				.forEach(i -> ent.addComision(parseToEntity(i, new ConfiguracionComision(), ent)));

		// Busco items modificados
		vo.getComisiones().stream().filter(i -> i.getId() != null)
				.forEach(i -> this.parseToEntity(i, ent.getComision(i), ent));

		ent.setTotalCalculado(vo.getTotalCalculado());
		ent.setUltimaModificacion(vo.getUltimaModificacion());
		return ent;
	}

	private ConfiguracionComision parseToEntity(ConfiguracionComisionVO vo, ConfiguracionComision entity,
			Contrato contrato) {
		ConfiguracionComision ent = entity;
		if (ent == null) {
			ent = new ConfiguracionComision();
		}
		ent = this.comisionService.parseToEnt(vo, ent);
		ent.setServicioComisionado(contrato.getServicioByProducto(vo.getProducto()));
		if (ent.getServicioComisionado() == null) {
			throw new IllegalArgumentException("No se encontro el Servicio " + vo.getProducto().toString());
		}
		return ent;
	}

	@Override
	public List<ContratoCabeceraVO> getAllByIdComisionista(Long idComisionista) {

		return this.ccRepository.findByIdComisionista(idComisionista).stream().map(c -> new ContratoCabeceraVO(c))
				.collect(Collectors.toList());
	}

	@Override
	@Transactional
	public ContratoVO liquidar(Long idContrato) {
		Contrato contrato = this.getEntity(idContrato);
		List<OrdenCobranzaVO> cobros = this.cobranzasService.getByContrato(idContrato);
		Double montoCobrado = cobros.stream()
				.filter(c -> c.getEstado().getCodigo().equals(EstadoOperacionEnum.APROBADO.getCodigo()))
				.mapToDouble(c -> c.getImporte()).sum();

		if (contrato.getTotalCalculado() > montoCobrado) {
			throw new IllegalArgumentException("El evento no se cobró completamente. Verifique las cuotas pendientes");
		}

		contrato.setEstado(EstadoOperacionEnum.LIQUIDADO);
		contrato.setUltimaModificacion(new Date());
		contrato.setAsientoGenerado(this.contableService.contabilizar(contrato));
		contrato = this.getRepository().save(contrato);
		contrato = this.comisionService.generarPorLiquidacion(contrato);
		return new ContratoVO(contrato);
	}

	@Override
	public EstadoIVAVO getEstadoIVA(Long idContrato) {
		return new EstadoIVAVO(this.getRepository().findByIdContratoEstadoIVA(idContrato).iterator().next());
	}

	@Override
	@Transactional
	public ContratoVO update(ContratoVO vo) {
		if (vo == null || vo.getId() == null) {
			throw new IllegalArgumentException("Debe indicar el id para actualizar");
		}
		Contrato ent = this.getRepository().findOne(vo.getId());
		if (ent == null) {
			throw new IllegalArgumentException("No existe " + this.getNombre() + " con ID " + vo.getId());
		}

		ent = this.parseToEnt(vo, ent);
		if (ent.esAprobado() || ent.esCerrado()) {
			if (ent.esContabilizable()) {

				Asiento a = this.contableService.contabilizar(ent);
				ent.getServicios().forEach(s -> s.getPlanPago().stream().filter(p -> p.getAsientoGenerado() == null)
						.forEach(p -> p.setAsientoGenerado(a)));
			}

			ent = this.comisionService.generarAdelantos(ent);
		}
		this.getRepository().save(ent);
		return this.toVO(ent);

	}

}

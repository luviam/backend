package ar.com.thinkup.luviam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.thinkup.luviam.model.TipoEvento;
import ar.com.thinkup.luviam.repository.ParameterRepository;
import ar.com.thinkup.luviam.repository.TipoEventoRepository;
import ar.com.thinkup.luviam.service.def.ITipoEventoService;
import ar.com.thinkup.luviam.vo.parametricos.TipoEventoVO;

@Service("ITipoEventoService")
public class TipoEventoService extends ParametricoService<TipoEventoVO, TipoEvento> implements ITipoEventoService {

	@Autowired
	private TipoEventoRepository tipoEventoRepo;

	@Override
	protected ParameterRepository<TipoEvento> getRepository() {

		return tipoEventoRepo;
	}

	@Override
	protected TipoEventoVO toVO(TipoEvento ent) {
		// TODO Auto-generated method stub
		return new TipoEventoVO(ent);
	}

	@Override
	protected TipoEvento newEnt() {

		return new TipoEvento();
	}

}

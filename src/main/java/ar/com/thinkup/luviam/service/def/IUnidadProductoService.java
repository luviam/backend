package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.model.UnidadProducto;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;

public interface IUnidadProductoService extends IParametricoService<ParametricoVO, UnidadProducto> {

	UnidadProducto getByDescriptivo(IDescriptivo tipoProducto);

}

package ar.com.thinkup.luviam.service.def;

import java.util.List;

import ar.com.thinkup.luviam.vo.ClienteListaVO;
import ar.com.thinkup.luviam.vo.ClienteVO;

public interface IClienteService{

	ClienteVO getByID(Long id);

	ClienteVO update(ClienteVO vo);

	ClienteVO create(ClienteVO vo);

	ClienteVO delete(ClienteVO vo);

	List<ClienteVO> getAll();

	List<ClienteListaVO> getCabeceras();
}

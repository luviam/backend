package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.vo.ProveedorVO;

public interface IProveedorService {

	ProveedorVO getById(Long id);

	ProveedorVO guardarProveedor(ProveedorVO proveedor);

}

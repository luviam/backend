package ar.com.thinkup.luviam.service.def;

import java.util.Map;

import ar.com.thinkup.luviam.exceptions.SendEmailException;
import ar.com.thinkup.luviam.model.EmailTemplate;

public interface MailService {
	
	public void send(String to, String subject, EmailTemplate html_template, Map<String, String> variables) throws SendEmailException;

}

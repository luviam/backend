package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.model.contratos.ConfiguracionComision;
import ar.com.thinkup.luviam.model.contratos.Contrato;
import ar.com.thinkup.luviam.model.documentos.cobros.OrdenCobranza;
import ar.com.thinkup.luviam.vo.contratos.ConfiguracionComisionVO;

public interface IComisionService extends IModelService<ConfiguracionComisionVO, ConfiguracionComision> {

	Contrato generarAdelantos(Contrato c);

	OrdenCobranza generarPorCobros(OrdenCobranza oc);

	Contrato generarPorLiquidacion(Contrato contrato);

}

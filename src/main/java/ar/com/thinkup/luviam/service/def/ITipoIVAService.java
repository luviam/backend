package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.model.TipoIVA;
import ar.com.thinkup.luviam.vo.TipoIVAVO;

public interface ITipoIVAService extends IParametricoService<TipoIVAVO, TipoIVA>{

	
}

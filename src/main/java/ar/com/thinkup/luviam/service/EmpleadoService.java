package ar.com.thinkup.luviam.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.thinkup.luviam.model.Comisionista;
import ar.com.thinkup.luviam.model.Sector;
import ar.com.thinkup.luviam.model.TipoEmpleado;
import ar.com.thinkup.luviam.model.TipoJornada;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.empleados.AltaEmpleado;
import ar.com.thinkup.luviam.model.empleados.Empleado;
import ar.com.thinkup.luviam.repository.EmpleadoRepository;
import ar.com.thinkup.luviam.repository.EmpleadoRepository.IEmpleadoCabecera;
import ar.com.thinkup.luviam.repository.SectoresRepository;
import ar.com.thinkup.luviam.repository.TipoEmpleadoRepository;
import ar.com.thinkup.luviam.repository.TipoJornadaRepository;
import ar.com.thinkup.luviam.service.def.ICentroCostoService;
import ar.com.thinkup.luviam.service.def.IComisionistaService;
import ar.com.thinkup.luviam.service.def.IEmpleadoService;
import ar.com.thinkup.luviam.vo.empleados.AltaEmpleadoVO;
import ar.com.thinkup.luviam.vo.empleados.CabeceraEmpleado;
import ar.com.thinkup.luviam.vo.empleados.EmpleadoVO;

@Service("IEmpleadoService")
public class EmpleadoService implements IEmpleadoService {

	@Autowired
	private EmpleadoRepository repo;

	@Autowired
	private SectoresRepository sectorRepo;

	@Autowired
	private TipoJornadaRepository jornadaRepo;

	@Autowired
	private TipoEmpleadoRepository empleadoRepo;

	@Autowired
	private IComisionistaService cService;

	@Autowired
	private ICentroCostoService centroService;

	@Override
	@Transactional
	public EmpleadoVO getById(Long id) {

		if (id == null) {
			throw new IllegalArgumentException("El id de empleado es obligatorio");
		}
		Empleado e = this.repo.findOne(id);
		if (e == null) {
			throw new EntityNotFoundException("No se encuentra el empleado con ID : " + id);
		}

		return new EmpleadoVO(e);
	}

	@Override
	@Transactional
	public EmpleadoVO guardar(EmpleadoVO empleadoVO) {
		final Empleado empleado;
		if (empleadoVO.getId() != null) {
			empleado = this.repo.getOne(empleadoVO.getId());
			if (empleado == null) {
				throw new EntityNotFoundException("No se encuentra el empleado con ID : " + empleadoVO.getId());
			}
		} else {
			empleado = new Empleado();
		}

		empleado.setNombre(empleadoVO.getNombre());
		empleado.setFechaNacimiento(empleadoVO.getFechaNacimiento());
		empleado.setCuil(empleadoVO.getCuil());
		empleado.setDomicilio(empleadoVO.getDomicilio());
		empleado.setActivo(empleadoVO.getActivo());

		empleado.setCvOk(empleadoVO.getCvOk());
		empleado.setPreocupacionalOk(empleadoVO.getPreocupacionalOk());
		empleado.setFechaRealizacionPreocupacional(empleadoVO.getFechaRealizacionPreocupacional());
		empleado.setDniOK(empleadoVO.getDniOK());
		empleado.setCuilOK(empleadoVO.getCuilOK());
		empleado.setConstanciaDomicilioOK(empleadoVO.getConstanciaDomicilioOK());
		empleado.setSeguroOK(empleadoVO.getSeguroOK());
		empleado.setArtOK(empleadoVO.getArtOK());

		if (empleadoVO.getTipoJornada() != null) {
			TipoJornada jornada = this.jornadaRepo.findByCodigo(empleadoVO.getTipoJornada().getCodigo());
			empleado.setTipoJornada(jornada);
		}
		if (empleadoVO.getSector() != null) {
			Sector sector = this.sectorRepo.findByCodigo(empleadoVO.getSector().getCodigo());
			empleado.setSector(sector);
		}
		empleado.setRemuneracion(empleadoVO.getRemuneracion());
		empleado.setFuncion(empleadoVO.getFuncion());
		empleado.setResponsabilidades(empleadoVO.getResponsabilidades());
		empleado.setFechaInicio(empleadoVO.getFechaInicio());
		empleado.setFechaFin(empleadoVO.getFechaFin());
		empleado.setTelefono(empleadoVO.getTelefono());
		empleado.setEmail(empleadoVO.getEmail());
		empleado.setCentroCosto(this.centroService.getByDescriptivo(empleadoVO.getCentro()));
		if (empleadoVO.getTipoEmpleado() != null) {
			TipoEmpleado tipoEmpleado = this.empleadoRepo.findByCodigo(empleadoVO.getTipoEmpleado().getCodigo());
			empleado.setTipoEmpleado(tipoEmpleado);
		}

		// Busco nuevos items
		empleadoVO.getAltas().stream().filter(i -> i.getId() == null)
				.forEach(i -> empleado.addAlta(parseToEntity(i, new AltaEmpleado())));

		// Busco items modificados
		empleadoVO.getAltas().stream().filter(i -> i.getId() != null)
				.forEach(i -> this.parseToEntity(i, empleado.getAlta(i.getId())));

		// Quito Eliminados
		empleado.getAltas().removeIf(i -> i.getId() != null && empleadoVO.getAlta(i.getId()) == null);
		if (empleado.esVendedor()) {
			empleado.setComisionista(this.updateComisionista(empleado));
		}

		return new EmpleadoVO(this.repo.save(empleado));
	}

	@Transactional
	private Comisionista updateComisionista(Empleado empleado) {

		return this.cService.updateByEmpleado(empleado);
	}

	public AltaEmpleado parseToEntity(AltaEmpleadoVO vo, AltaEmpleado entity) {

		AltaEmpleado ent = entity;
		if (ent == null)
			ent = new AltaEmpleado();
		ent.setFechaAlta(vo.getFechaAlta());
		ent.setFechaBaja(vo.getFechaBaja());
		ent.setObservacion(vo.getObservacion());

		return ent;
	}

	@Override
	@Transactional
	public List<CabeceraEmpleado> getCabeceras() {
		return this.getCabeceras("ALL");
	}

	@Override
	@Transactional
	public List<CabeceraEmpleado> getCabeceras(String codigoTipoEmpleado) {
		List<IEmpleadoCabecera> empleados = this.repo.getCabecerasByTipo(codigoTipoEmpleado);
		return empleados.stream().map(e -> new CabeceraEmpleado(e)).collect(Collectors.toList());
	}

	@Override
	public Empleado getByDescriptivo(DescriptivoGeneric descriptivo) {
		if (descriptivo == null || StringUtils.isEmpty(descriptivo.getCodigo())) {
			throw new IllegalArgumentException("El parametro Descriptivo es Obligatorio");
		}
		Empleado a = this.repo.findOne(Long.valueOf(descriptivo.getCodigo()));
		if (a == null) {
			throw new IllegalArgumentException("No existe Empleado con id " + descriptivo.getCodigo());
		}
		return a;
	}

	@Override
	@Transactional()
	public Boolean delete(Long id) {
		Empleado e = this.repo.findOne(id);
		if (e == null) {
			throw new IllegalArgumentException("No existe Empleado con id " + id);
		}
		this.repo.delete(e);
		return true;
	}

}

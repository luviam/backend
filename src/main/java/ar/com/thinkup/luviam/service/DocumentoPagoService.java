package ar.com.thinkup.luviam.service;

import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.Cheque;
import ar.com.thinkup.luviam.model.documentos.DocumentoPago;
import ar.com.thinkup.luviam.model.documentos.Valores;
import ar.com.thinkup.luviam.model.enums.EstadoCheque;
import ar.com.thinkup.luviam.model.enums.TipoCheque;
import ar.com.thinkup.luviam.model.enums.TipoPagoEnum;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.repository.ChequeRepository;
import ar.com.thinkup.luviam.service.def.IDocumentoPago;
import ar.com.thinkup.luviam.service.def.IUsuarioService;
import ar.com.thinkup.luviam.vo.Identificable;
import ar.com.thinkup.luviam.vo.documentos.DocumentoPagoVO;
import ar.com.thinkup.luviam.vo.documentos.ValoresVO;

@Transactional
public abstract class DocumentoPagoService<V extends Valores, I extends ValoresVO<V>, T extends DocumentoPago<V>, R extends DocumentoPagoVO<V, I, ? extends Identificable>>
		extends ModelService<T, R, JpaRepository<T, Long>> implements IDocumentoPago<T, R> {

	@Autowired
	private ChequeRepository chequeRepo;

	@Autowired
	protected ModelEntityProvider modelProvider;

	@Autowired
	private IUsuarioService usuarioService;

	protected abstract V getNewValores();

	protected V parseToEntityValores(V entity, I vo) {
		entity.setCentroCosto(this.modelProvider.getCentro(vo.getCentroCosto()));
		entity.setCuenta(this.modelProvider.getCuenta(vo.getCuenta(), vo.getCentroCosto()));
		entity.setMonto(vo.getMonto());
		if (vo.getComprobante() != null && vo.getComprobante().getId() != null) {
			entity.setCheque(this.chequeRepo.findOne(vo.getComprobante().getId()));
			if (entity.getCheque().getTipoCheque().equals(TipoCheque.PROPIO)) {
				entity.getCheque().setEstadoCheque(EstadoCheque.ENTREGADO);
				entity.getCheque().setImporte(entity.getMonto());
				entity.getCheque().setDestino(vo.getComprobante().getDestino());
				entity.getCheque().setFechaEmision(vo.getComprobante().getFechaEmision());
				entity.getCheque().setFechaPago(vo.getComprobante().getFechaPago());
			}

		} else {
			if (vo.getComprobante() != null && vo.getComprobante().getId() == null) {
				entity.setCheque(new Cheque());
				entity.getCheque().setEstadoCheque(EstadoCheque.DISPONIBLE);
				entity.getCheque().setImporte(entity.getMonto());
				entity.getCheque().setDestino(vo.getComprobante().getDestino());
				entity.getCheque().setFechaEmision(vo.getComprobante().getFechaEmision());
				entity.getCheque().setFechaPago(vo.getComprobante().getFechaPago());
				entity.getCheque().setFechaIngreso(vo.getComprobante().getFechaIngreso());
				entity.getCheque().setTipoCheque(TipoCheque.CLIENTE);
				entity.getCheque().setTitularCuenta(vo.getComprobante().getTitularCuenta());
				entity.getCheque().setBanco(vo.getComprobante().getBanco());
				entity.getCheque().setCuit(vo.getComprobante().getCuit());
				entity.getCheque().setNumero(vo.getComprobante().getNumero());
				entity.getCheque().setNumeroCuenta(vo.getComprobante().getNumeroCuenta());
				entity.getCheque().setOrigen("Cliente");
			}
		}

		entity.setTipoPago(TipoPagoEnum.byCodigo(vo.getTipoOperacion().getCodigo()));
		return entity;
	}

	@Override
	public R findById(Long id) {
		if (id == null) {
			throw new IllegalArgumentException("El id es obligatorio");
		}
		T f = this.getRepository().findOne(id);
		if (f == null) {
			throw new EntityNotFoundException("No hay documento con ID: " + id);
		}
		return this.toVO(f);
	}

	public abstract R guardarDocumento(R doc, Usuario responsable);

	@Override
	public T parseToEnt(R vo, T entity) {
		T ent = entity != null ? entity : this.newEnt();
		ent.setCentroCosto(this.modelProvider.getCentro(vo.getCentroCosto()));

		ent.setDescripcion(vo.getDescripcion());
		ent.setFecha(vo.getFecha());
		ent.setNumeroComprobante(vo.getNumeroComprobante());
		if (vo.getResponsable() != null) {
			ent.setResponsable(this.usuarioService.getByDescriptivo(vo.getResponsable()));
		}

		ent.setNumero(vo.getNumero());

		// Quito Eliminados
		List<Valores> aEliminar = ent.getValores().stream()
				.filter(i -> i.getId() != null && vo.getValor(i.getId()) == null).collect(Collectors.toList());
		aEliminar.forEach(p -> {
			if (p.getCheque() != null) {
				p.getCheque().setEstadoCheque(EstadoCheque.DISPONIBLE);
				if (p.getCheque().getTipoCheque().equals(TipoCheque.PROPIO)) {
					p.getCheque().setImporte(null);
					p.getCheque().setFechaIngreso(null);
					p.getCheque().setFechaEmision(null);
					p.getCheque().setDestino(null);
					p.getCheque().setFechaPago(null);
				}

			}
		});
		ent.getValores().removeAll(aEliminar);

		// Busco nuevos Pagos
		vo.getValores().stream().filter(i -> i.getId() == null)
				.forEach(i -> ent.addValor(parseToEntityValores(this.getNewValores(), i)));

		// Busco Pagos modificados
		vo.getValores().stream().filter(i -> i.getId() != null).forEach(i -> {
			V v = ent.getValor(i.getId());
			// if (v.getCheque() != null && (i.getComprobante() == null
			// || !i.getComprobante().getNumero().equals(v.getCheque().getNumero()))) {
			// v.getCheque().setEstadoCheque(EstadoCheque.DISPONIBLE);
			// //chequesAActualizar.add(v.getCheque());
			// if(v.getCheque().getTipoCheque().equals(TipoCheque.PROPIO)) {
			// v.getCheque().setImporte(i.getMonto());
			// v.getCheque().setFechaEmision(i.getComprobante().getFechaEmision());
			// v.getCheque().setFechaIngreso(i.getComprobante().getFechaIngreso());
			// }
			//
			//
			//
			// }
			this.parseToEntityValores(v, i);
		});

		return ent;
	}

}

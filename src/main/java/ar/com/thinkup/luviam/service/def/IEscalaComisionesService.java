package ar.com.thinkup.luviam.service.def;

import java.util.List;

import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.EscalaComisiones;
import ar.com.thinkup.luviam.vo.EscalaComisionesVO;

public interface IEscalaComisionesService extends IModelService<EscalaComisionesVO, EscalaComisiones> {

	List<EscalaComisionesVO> getByCentro(String centro);

	EscalaComisiones getEscala(CentroCosto centro, Integer cantidad);

}

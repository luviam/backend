package ar.com.thinkup.luviam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.thinkup.luviam.model.TipoIVA;
import ar.com.thinkup.luviam.repository.TipoIVARepository;
import ar.com.thinkup.luviam.service.def.ITipoIVAService;
import ar.com.thinkup.luviam.vo.TipoIVAVO;

@Service("ITipoIVAService")
public class TipoIVAService extends ParametricoService<TipoIVAVO, TipoIVA> implements ITipoIVAService {

	@Autowired
	private TipoIVARepository repo;

	@Override
	protected TipoIVARepository getRepository() {
		return this.repo;
	}

	@Override
	protected TipoIVAVO toVO(TipoIVA ent) {

		return new TipoIVAVO(ent);
	}

	@Override
	protected TipoIVA newEnt() {
		return new TipoIVA();
	}

}

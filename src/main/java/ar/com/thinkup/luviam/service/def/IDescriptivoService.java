package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.model.def.IDescriptivo;

public interface IDescriptivoService<E extends IDescriptivo>{
	E getByDescriptivo(IDescriptivo descriptivo);
	E getByCodigo(String codigo);
}

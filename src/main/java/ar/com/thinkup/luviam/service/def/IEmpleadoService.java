package ar.com.thinkup.luviam.service.def;

import java.util.List;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.empleados.Empleado;
import ar.com.thinkup.luviam.vo.empleados.CabeceraEmpleado;
import ar.com.thinkup.luviam.vo.empleados.EmpleadoVO;

public interface IEmpleadoService {

	EmpleadoVO getById(Long id);

	EmpleadoVO guardar(EmpleadoVO empleadoVO);

	List<CabeceraEmpleado> getCabeceras();

	List<CabeceraEmpleado> getCabeceras(String codigoTipoEmpleado);

	Empleado getByDescriptivo(DescriptivoGeneric vendedor);

	Boolean delete(Long id);

}

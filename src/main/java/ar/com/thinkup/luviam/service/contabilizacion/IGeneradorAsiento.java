package ar.com.thinkup.luviam.service.contabilizacion;

import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.Asiento;

public interface IGeneradorAsiento<T extends IContabilizable> {

	public Boolean esAplicable(IContabilizable o);

	@Transactional
	public Asiento generarAsiento(T o);

}

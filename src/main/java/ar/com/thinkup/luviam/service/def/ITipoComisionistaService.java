package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.model.TipoComisionista;
import ar.com.thinkup.luviam.vo.parametricos.TipoComisionistaVO;

public interface ITipoComisionistaService extends IParametricoService<TipoComisionistaVO,TipoComisionista> {

	TipoComisionista getByCodigo(String codigo);

}

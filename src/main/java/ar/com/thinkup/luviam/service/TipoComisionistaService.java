package ar.com.thinkup.luviam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.TipoComisionista;
import ar.com.thinkup.luviam.repository.ParameterRepository;
import ar.com.thinkup.luviam.repository.TipoComisionistaRepository;
import ar.com.thinkup.luviam.service.def.ICategoriaProductoService;
import ar.com.thinkup.luviam.service.def.ITipoComisionistaService;
import ar.com.thinkup.luviam.vo.parametricos.TipoComisionistaVO;

@Service("ITipoComisionistaService")
@Transactional
public class TipoComisionistaService extends ParametricoService<TipoComisionistaVO, TipoComisionista>
		implements ITipoComisionistaService {

	@Autowired
	private TipoComisionistaRepository repo;

	@Autowired
	private ICategoriaProductoService catService;

	@Override
	protected ParameterRepository<TipoComisionista> getRepository() {
		return this.repo;
	}

	@Override
	protected TipoComisionista newEnt() {
		return new TipoComisionista();
	}

	@Override
	@Transactional
	public TipoComisionista parseToEnt(TipoComisionistaVO vo, TipoComisionista ent) {

		TipoComisionista tc = super.parseToEnt(vo, ent);

		tc.getCategoriasComisionablesSet().removeIf(c -> vo.getCategoria(c.getId()) == null);
		vo.getCategorias().stream().filter(c -> tc.getCategoria(c)== null).forEach(c -> {
			tc.getCategoriasComisionablesSet().add(this.catService.getByDescriptivo(c));
		});
		return tc;
	}

	@Override
	protected TipoComisionistaVO toVO(TipoComisionista ent) {
		return new TipoComisionistaVO(ent);
	}

}

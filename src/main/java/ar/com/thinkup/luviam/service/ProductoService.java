package ar.com.thinkup.luviam.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.CategoriaProducto;
import ar.com.thinkup.luviam.model.ImputacionProducto;
import ar.com.thinkup.luviam.model.Producto;
import ar.com.thinkup.luviam.model.UnidadProducto;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.repository.CategoriaProductoRepository;
import ar.com.thinkup.luviam.repository.ProductoRepository;
import ar.com.thinkup.luviam.repository.UnidadRepository;
import ar.com.thinkup.luviam.service.def.ICentroCostoService;
import ar.com.thinkup.luviam.service.def.IMarcaService;
import ar.com.thinkup.luviam.service.def.IProductoService;
import ar.com.thinkup.luviam.service.def.ITipoMenuService;
import ar.com.thinkup.luviam.vo.ImputacionProductoVO;
import ar.com.thinkup.luviam.vo.PrecioProductoVO;
import ar.com.thinkup.luviam.vo.ProductoVO;
import ar.com.thinkup.luviam.vo.indicadores.CubiertoPromedioVO;

@Service("IProductoService")
@Transactional
public class ProductoService extends ModelService<Producto, ProductoVO, ProductoRepository>
		implements IProductoService {

	@Autowired
	private ProductoRepository repo;
	@Autowired
	private CategoriaProductoRepository catRepo;

	@Autowired
	private ICentroCostoService centroService;

	@Autowired
	private UnidadRepository uniRepo;
	
	@Autowired
	private IMarcaService marcaService;

	@Autowired
	private ITipoMenuService tipoMenuService;
	
	@Override
	@Transactional
	public Producto parseToEnt(ProductoVO vo, Producto producto) {

		Producto ent = producto != null ? producto : new Producto();

		ent.setId(vo.getId());
		ent.setCodigo(vo.getCodigo());
		ent.setNombre(vo.getNombre());

		if (vo.getUnidad() != null && !StringUtils.isEmpty(vo.getUnidad().getCodigo())) {
			UnidadProducto uniProd = this.uniRepo.findByCodigo(vo.getUnidad().getCodigo());
			ent.setUnidad(uniProd);
		}

		if (vo.getCategoria() != null && !StringUtils.isEmpty(vo.getCategoria().getCodigo())) {
			CategoriaProducto catProd = this.catRepo.findByCodigo(vo.getCategoria().getCodigo());
			ent.setCategoria(catProd);
		}

		ent.setActivo(vo.getActivo());
		ent.setEsBorrable(vo.getEsBorrable());
		ent.setDivisible(vo.getDivisible());
		ent.setAdolescenteProp(vo.getAdolescenteProp());
		ent.setMenorProp(vo.getMenorProp());
		ent.setBebeProp(vo.getBebeProp());
		ent.setImputaIVA(vo.getImputaIVA());

		// Quito Eliminados
		ent.getCuentas().removeIf(i -> i.getId() != null && vo.getCuenta(i.getId()) == null);

		// Busco nuevos items
		vo.getCuentas().stream().filter(i -> i.getId() == null)
				.forEach(i -> ent.addCuenta(parseToEntity(i, new ImputacionProducto())));

		// Busco items modificados
		vo.getCuentas().stream().filter(i -> i.getId() != null)
				.forEach(i -> this.parseToEntity(i, ent.getCuenta(i.getId())));

		if(ent.esMenu() && vo.getTipoMenu() != null) {
			ent.setTipoMenu(this.tipoMenuService.getEntity(vo.getTipoMenu().getId()));
		}else{
			ent.setTipoMenu(null);
		}
		
		if(vo.getMarca()!= null) {
			ent.setMarca(this.marcaService.getEntity(vo.getMarca().getId()));
		}else {
			ent.setActivo(null);
		}
		
		return ent;
	}

	private ImputacionProducto parseToEntity(ImputacionProductoVO vo, ImputacionProducto imputacionProducto) {

		ImputacionProducto ent = imputacionProducto == null ? new ImputacionProducto() : imputacionProducto;

		ent.setCentroCosto(this.centroService.getByDescriptivo(vo.getCentroCosto()));

		if (vo.getCuentaAplicacion() != null) {
			CuentaAplicacion c = ent.getCentroCosto().getCuentaByCodigo(vo.getCuentaAplicacion().getCodigo());
			if (c == null) {
				throw new IllegalArgumentException(
						"No existe la cuenta con codigo " + vo.getCuentaAplicacion().getCodigo());
			}
			ent.setCuentaAplicacion(c);
		}
		ent.setHabilitado(vo.getHabilitado());
		return ent;
	}

	@Override
	public List<DescriptivoGeneric> getByCategoria(String codigoCategoria) {
		if (StringUtils.isBlank(codigoCategoria)) {
			throw new IllegalArgumentException("La categoria no puede ser vacia");
		}

		return this.repo.findByCategoriaCodigo(codigoCategoria).stream()
				.map(p -> new DescriptivoGeneric(p.getCodigo(), p.getNombre())).collect(Collectors.toList());
	}

	@Override
	protected ProductoRepository getRepository() {

		return this.repo;
	}

	@Override
	protected ProductoVO toVO(Producto ent) {

		return new ProductoVO(ent);
	}

	@Override
	protected Producto newEnt() {
		return new Producto();
	}

	@Override
	public CubiertoPromedioVO getCubiertoPromedio(String codigoCentro, String codigoProducto, Date desde, Date hasta) {
		if (StringUtils.isEmpty(codigoProducto)) {
			new IllegalArgumentException("El producto es obligatorio");
		}
		if (StringUtils.isEmpty(codigoCentro)) {
			new IllegalArgumentException("El centro es obligatorio");
		}
		if (desde == null || hasta == null) {
			new IllegalArgumentException("El periodo es obligatorio");
		}

		return this.repo.getPrecioPromedio(codigoCentro, codigoProducto, desde, hasta);
	}

	@Override
	public Producto getByDescriptivo(IDescriptivo desc) {
		if(desc == null || StringUtils.isEmpty(desc.getCodigo())) {
			throw new IllegalArgumentException("El producto es obligatorio");
		}
		return this.repo.findByCodigo(desc.getCodigo());
	}

	@Override
	public List<PrecioProductoVO> getConMarcaByCategoria(String codigoCategoria) {
		return this.repo.getConMarcaByCategoria(codigoCategoria);
	}

}

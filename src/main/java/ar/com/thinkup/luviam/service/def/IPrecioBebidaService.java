package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.model.PrecioBebida;
import ar.com.thinkup.luviam.vo.PrecioBebidaVO;

public interface IPrecioBebidaService extends IPrecioService<PrecioBebidaVO,PrecioBebida > {

}

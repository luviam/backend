package ar.com.thinkup.luviam.service.def;

import java.util.List;

import ar.com.thinkup.luviam.model.Parametrico;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;

public interface IParametricoService<T extends ParametricoVO, V extends Parametrico>
		extends IDescriptivoService<V>, IModelService<T, V> {
	List<T> getHabilitados();

}

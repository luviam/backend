package ar.com.thinkup.luviam.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.thinkup.luviam.model.comisionistas.LayoutIndicadores;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.repository.LayoutIndicadoresRepository;
import ar.com.thinkup.luviam.service.def.ILayoutIndicadoresService;
import ar.com.thinkup.luviam.vo.indicadores.LayoutIndicadoresVO;

@Service("ILayoutIndicadoresService")
public class LayoutIndicadoresService implements ILayoutIndicadoresService {

	@Autowired
	private LayoutIndicadoresRepository repo;

	@Override
	public LayoutIndicadores getLayoutEntity(Usuario usuario) {
		if (usuario == null) {
			throw new IllegalArgumentException("Usuario obligatorio");
		}
		Set<LayoutIndicadores> resultados = this.repo.findByUsuarioId(usuario.getId());
		if (resultados.size() == 0) {
			return null;
		} else {
			return resultados.iterator().next();
		}

	}

	@Override
	public LayoutIndicadoresVO getLayout(Usuario usuario) {
		if (usuario == null) {
			throw new IllegalArgumentException("Usuario obligatorio");
		}
		Set<LayoutIndicadores> resultados = this.repo.findByUsuarioId(usuario.getId());
		if (resultados.size() == 0) {
			return null;
		} else {
			return new LayoutIndicadoresVO(resultados.iterator().next());
		}

	}

	@Override
	public LayoutIndicadoresVO guardarLayout(LayoutIndicadoresVO layout) {
		LayoutIndicadores l = this.getLayoutEntity(layout.getUsuario());
		if (l == null) {
			l = new LayoutIndicadores();
		}
		l.setIndicadoresStr(layout.getIndicadoresStr());
		l.setusuario(layout.getUsuario());
		this.repo.save(l);
		return new LayoutIndicadoresVO(l);
	}

}

package ar.com.thinkup.luviam.service.def;

import java.util.List;

import ar.com.thinkup.luviam.model.contratos.Contrato;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.vo.ContratoCabeceraVO;
import ar.com.thinkup.luviam.vo.contratos.ContratoVO;
import ar.com.thinkup.luviam.vo.contratos.CuotaServicioVO;
import ar.com.thinkup.luviam.vo.contratos.EstadoIVAVO;

public interface IContratoService extends IModelService<ContratoVO, Contrato> {

	List<ContratoCabeceraVO> getAllByIdCliente(Long idCliente);

	List<DescriptivoGeneric> getAllDescriptivosByIdCliente(Long idCliente);

	List<CuotaServicioVO> getCuotas(String codigoContrato);

	List<ContratoCabeceraVO> getAllCabecera();

	List<CuotaServicioVO> getCuotasImpagas(String codigoContrato);

	ContratoVO aprobar(Long idContrato);

	List<ContratoCabeceraVO> getAllByIdComisionista(Long idComisionista);

	ContratoVO liquidar(Long idContrato);

	EstadoIVAVO getEstadoIVA(Long idContrato);

	ContratoVO rechazar(Long idContrato);

	ContratoVO reaplicar(Long idContrato);

}

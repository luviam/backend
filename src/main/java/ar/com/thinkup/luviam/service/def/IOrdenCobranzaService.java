package ar.com.thinkup.luviam.service.def;

import java.util.List;

import ar.com.thinkup.luviam.model.documentos.cobros.OrdenCobranza;
import ar.com.thinkup.luviam.vo.OrdenCobranzaCabeceraVO;
import ar.com.thinkup.luviam.vo.documentos.cobros.ItemOrdenCobranzaVO;
import ar.com.thinkup.luviam.vo.documentos.cobros.OrdenCobranzaVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroCliente;

public interface IOrdenCobranzaService extends IDocumentoPago<OrdenCobranza,OrdenCobranzaVO> {

	List<OrdenCobranzaCabeceraVO> getCabeceras(FiltroCliente filtros);

	List<ItemOrdenCobranzaVO> getItems(Long idOrdenPago);

	List<OrdenCobranzaVO> getByContrato(Long idContrato);

	List<OrdenCobranzaCabeceraVO> getCabecerasByContrato(Long idContrato);

}

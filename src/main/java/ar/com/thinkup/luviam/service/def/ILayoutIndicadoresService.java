package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.model.comisionistas.LayoutIndicadores;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.vo.indicadores.LayoutIndicadoresVO;

public interface ILayoutIndicadoresService {

	LayoutIndicadoresVO getLayout(Usuario usuario);

	LayoutIndicadoresVO guardarLayout(LayoutIndicadoresVO layout);

	LayoutIndicadores getLayoutEntity(Usuario usuario);

}

package ar.com.thinkup.luviam.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.CentroCostosTipoComprobante;
import ar.com.thinkup.luviam.model.Provincia;
import ar.com.thinkup.luviam.model.TipoComprobante;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.enums.CuentasPrincipalesEnum;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.model.plan.SubGrupo;
import ar.com.thinkup.luviam.repository.CentroRepository;
import ar.com.thinkup.luviam.repository.ProvinciaRepository;
import ar.com.thinkup.luviam.repository.SubGrupoRepository;
import ar.com.thinkup.luviam.repository.TipoComprobanteRepository;
import ar.com.thinkup.luviam.service.def.ICentroCostoService;
import ar.com.thinkup.luviam.vo.CentroCostoVO;
import ar.com.thinkup.luviam.vo.CentroCostosComprobantesVO;
import ar.com.thinkup.luviam.vo.CuentaConfig;

@Service("ICentroCostoService")
@Transactional
public class CentroCostoService implements ICentroCostoService {

	@Autowired
	private CentroRepository centroRepo;
	@Autowired
	private TipoComprobanteRepository tipoComprobanteRepo;
	@Autowired
	private ProvinciaRepository provRepo;

	@Autowired
	private SubGrupoRepository subGrupoRepo;

	@Override
	@Transactional
	public CentroCostoVO getByID(Long id) {
		CentroCosto c = this.centroRepo.findOne(id);
		if (c == null) {
			throw new EntityNotFoundException("No existe el centro con id " + id);
		}
		return new CentroCostoVO(c);
	}

	@Override
	@Transactional
	public CentroCostoVO update(CentroCostoVO vo) {

		if (vo.getId() == null) {
			throw new IllegalArgumentException("No se puede actualizar el centro sin ID");
		}

		if (this.centroRepo.countByCodigoAndIdNot(vo.getCodigoCentro(), vo.getId()) > 0) {
			throw new IllegalArgumentException("Ya existe otro centro con el código " + vo.getCodigoCentro());
		}
		;

		CentroCosto centroActual = this.centroRepo.findOne(vo.getId());
		if (centroActual == null) {
			throw new EntityNotFoundException("No existe el centro con id " + vo.getId());
		}
		String codigoActual = centroActual.getCodigo();
		String nombreActual = centroActual.getNombre();

		CentroCosto centroActualizado = this.toCentroDB(vo, centroActual);
		centroActualizado = this.centroRepo.save(centroActualizado);

		if (!codigoActual.equals(centroActualizado.getCodigo()) || nombreActual.equals(centroActualizado.getNombre())) {
			this.actualizarCuentasUnidadNegocio(codigoActual, nombreActual, centroActualizado);
		}
		return new CentroCostoVO(centroActualizado);
	}

	@Override
	@Transactional
	public CentroCostoVO create(CentroCostoVO vo) {
		if (vo.getId() != null) {
			throw new IllegalArgumentException("No se puede crear un centro con id");
		}
		if (this.centroRepo.countByCodigo(vo.getCodigoCentro()) > 0) {
			throw new IllegalArgumentException("Ya existe un centro con codigo " + vo.getCodigoCentro());
		}
		;
		CentroCosto c = this.toCentroDB(vo, null);
		c = this.centroRepo.save(c);
		this.generarCuentasDeUnidadNegocio(c);
		return new CentroCostoVO(c);
	}

	@Override
	@Transactional
	public CentroCostoVO delete(Long idCentro) {
		if (idCentro == null) {
			throw new IllegalArgumentException("No se puede eliminar el centro sin ID");
		}

		CentroCosto centroActual = this.centroRepo.findOne(idCentro);
		if (centroActual == null) {
			throw new EntityNotFoundException("No existe el centro con id " + idCentro);
		}
		String codigoActual = centroActual.getCodigo();

		try {
			this.centroRepo.delete(centroActual);
		} catch (Exception e) {
			centroActual.setActivo(false);
			centroActual.getCuentasAplicacion().forEach(c -> c.setActivo(false));
		}
		this.eliminarCuentaUnidadNegocio(centroActual, codigoActual);
		return new CentroCostoVO(centroActual);
	}

	@Override
	public List<CentroCostoVO> getAll() {

		return this.centroRepo.findAll().stream().map(c -> new CentroCostoVO(c)).collect(Collectors.toList());
	}

	private CentroCosto toCentroDB(CentroCostoVO vo, CentroCosto actual) {

		CentroCosto centro;
		if (actual != null) {
			centro = actual;
		} else {
			centro = new CentroCosto();
		}
		centro.setCodigo(vo.getCodigoCentro());
		centro.setActivo(vo.getActivo() != null ? vo.getActivo() : true);
		centro.setCuit(vo.getCuit());
		centro.setDomicilioFiscal(vo.getDomicilioFiscal());
		centro.setIibb(vo.getIibb());
		centro.setNombre(vo.getDescripcionCentro());
		centro.setRazonSocial(vo.getRazonSocial());
		centro.setCodigoPostal(vo.getCodigoPostal());
		centro.setTipoIIBB(vo.getTipoIIBB());
		if (vo.getProvincia() != null && vo.getProvincia().getCodigo() != null) {
			Long idProv = new Long(vo.getProvincia().getCodigo());
			Provincia prov = this.provRepo.findOne(idProv);
			centro.setProvincia(prov);
		}
		if (!CollectionUtils.isEmpty(vo.getComprobantes())) {

			// Modifico existentes
			vo.getComprobantes().stream().filter(cc -> StringUtils.isNotEmpty(cc.getCodigo())).forEach(cc -> {
				this.parseComprobante(centro.getTipoComprobante(Long.valueOf(cc.getCodigo())), cc);
			});

			// Elimino Eliminados
			centro.getComprobantesDisponibles()
					.removeIf(o -> o.getId() != null && vo.getComprobante(String.valueOf(o.getId())) == null);

			// AGREGO NUEVOS
			vo.getComprobantes().stream().filter(cc -> StringUtils.isEmpty(cc.getCodigo())).forEach(cc -> {
				centro.agregarComprobante(this.parseComprobante(new CentroCostosTipoComprobante(), cc));
			});

		}
		if (!CollectionUtils.isEmpty(vo.getCuentas())) {
			updateCuentas(vo, centro);

		}
		return centro;
	}

	private void updateCuentas(CentroCostoVO vo, CentroCosto centro) {

		// Modifico existentes
		vo.getCuentas().stream().filter(cc -> cc.getId() != null).forEach(cc -> {
			this.parseCuenta(centro.getCuentaAplicacion(cc.getId()), cc);
		});

		// Elimino Eliminados
		centro.getCuentasAplicacion().removeIf(o -> o.getId() != null && vo.getCuenta(o.getId()) == null);

		// AGREGO NUEVOS
		vo.getCuentas().stream().filter(cc -> cc.getId() == null).forEach(cc -> {
			centro.agregarCuenta(this.parseCuenta(new CuentaAplicacion(), cc));
		});

	}

	private CuentaAplicacion parseCuenta(CuentaAplicacion ent, CuentaConfig vo) {
		CuentaAplicacion ca;
		if (vo.getId() == null) {
			ca = new CuentaAplicacion();
		} else {
			ca = ent;
			if (ca == null) {
				throw new IllegalArgumentException("La cuenta  con id " + vo.getId() + " No existe.");
			}
		}
		ca.setActivo(vo.getActivo());
		ca.setNombre(vo.getDescripcion());
		ca.setSubGrupo(this.getSubGrupo(vo.getIdParent()));
		if (StringUtils.isEmpty(vo.getCodigo())) {
			throw new IllegalArgumentException("No se puede crear una cuenta sin código");
		}

		ca.setCodigo(vo.getCodigo());

		return ca;

	}

	private CentroCostosTipoComprobante parseComprobante(CentroCostosTipoComprobante ent,
			CentroCostosComprobantesVO cd) {
		CentroCostosTipoComprobante ctc;
		if (StringUtils.isEmpty(cd.getCodigo())) {
			ctc = new CentroCostosTipoComprobante();
		} else {
			ctc = ent;
			if (ctc == null) {
				throw new IllegalArgumentException("El tipo de comprobante con id " + cd.getCodigo() + " No existe.");
			}
		}

		if (StringUtils.isNoneEmpty(cd.getDescripcion())) {
			TipoComprobante tc = this.tipoComprobanteRepo.findByDescripcion(cd.getDescripcion());
			ctc.setTipoComprobante(tc);
			ctc.setActivo(cd.getActivo());
			if (StringUtils.isNoneEmpty(cd.getNumeracion()))
				ctc.setValorActual(cd.getNumeracion());
		} else {
			// throw new ComprobanteNoExistenteExcepcion("No Existe el tipo de
			// comprobante.");
		}

		return ctc;
	}

	private void eliminarCuentaUnidadNegocio(CentroCosto actual, String codigoOriginal) {
		List<CentroCosto> centros = this.centroRepo.findAll();
		centros = centros.stream().filter(c -> !c.getCodigo().equals(actual.getCodigo())).collect(Collectors.toList());
		for (CentroCosto centroCosto : centros) {
			CuentaAplicacion ca = centroCosto.getCuentaByCodigo("UN_" + codigoOriginal);
			if (ca != null)
				ca.setDeleted(true);
		}
		this.centroRepo.save(centros);

	}

	private void actualizarCuentasUnidadNegocio(String codigoOriginal, String nombreOriginal,
			CentroCosto centroActualizado) {
		List<CentroCosto> centros = this.centroRepo.findAll();
		centros = centros.stream().filter(c -> !c.getCodigo().equals(centroActualizado.getCodigo()))
				.collect(Collectors.toList());
		for (CentroCosto centroCosto : centros) {
			CuentaAplicacion ca = centroCosto.getCuentaByCodigo("UN_" + codigoOriginal);
			if (ca != null) {
				ca.setNombre("UN " + centroActualizado.getDescripcion());
				ca.setCodigo("UN_" + centroActualizado.getCodigo());
			}
		}
		this.centroRepo.save(centros);
	}

	private void generarCuentasDeUnidadNegocio(CentroCosto centroBase) {
		List<CentroCosto> centros = this.centroRepo.findAll();
		centros = centros.stream().filter(c -> !c.getCodigo().equals(centroBase.getCodigo()))
				.collect(Collectors.toList());
		SubGrupo sg = getSubGrupo(CuentasPrincipalesEnum.SG_UNIDADES_NEGOCIO.getCodigo());
		for (CentroCosto centroCosto : centros) {
			CuentaAplicacion cUN = new CuentaAplicacion();
			cUN.setCentroCosto(centroCosto);
			cUN.setActivo(true);
			cUN.setCodigo("UN_" + centroBase.getCodigo());
			cUN.setDeleted(false);
			cUN.setEliminable(false);
			cUN.setNombre("UN " + centroBase.getNombre());
			cUN.setSubGrupo(sg);
			centroCosto.agregarCuenta(cUN);
		}
		this.centroRepo.save(centros);

	}

	private SubGrupo getSubGrupo(String codigo) {
		SubGrupo sg = this.subGrupoRepo.findByCodigo(codigo);
		if (sg == null) {
			throw new EntityNotFoundException("No existe el subgrupo con código " + codigo);
		}
		return sg;
	}

	private SubGrupo getSubGrupo(Long id) {
		SubGrupo sg = this.subGrupoRepo.findOne(id);
		if (sg == null) {
			throw new EntityNotFoundException("No existe el subgrupo con id " + id);
		}
		return sg;
	}

	@Override
	@Transactional
	public CentroCosto getByDescriptivo(IDescriptivo desc) {
		if (desc == null || StringUtils.isEmpty(desc.getCodigo())) {
			throw new IllegalArgumentException("El id es obligatorio");
		}
		return this.centroRepo.findOneByCodigo(desc.getCodigo());

	}
}

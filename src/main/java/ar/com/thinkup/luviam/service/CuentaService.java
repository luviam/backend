package ar.com.thinkup.luviam.service;

import java.util.List;
import java.util.Optional;
import java.util.Vector;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.thinkup.luviam.exceptions.SubGrupoNoExistente;
import ar.com.thinkup.luviam.model.enums.CuentasPrincipalesEnum;
import ar.com.thinkup.luviam.model.plan.SubGrupo;
import ar.com.thinkup.luviam.repository.CentroRepository;
import ar.com.thinkup.luviam.repository.CentroRepository.CentroDescriptivo;
import ar.com.thinkup.luviam.repository.SubGrupoRepository;
import ar.com.thinkup.luviam.service.def.ICuentaService;
import ar.com.thinkup.luviam.vo.CuentaConfig;

@Service("ICuentaService")
@Transactional
public class CuentaService implements ICuentaService {

	@Autowired
	private SubGrupoRepository subGrupoRepo;

	@Autowired
	private CentroRepository centroRepo;

	@Override
	@Transactional
	public List<CuentaConfig> getCuentasDefault() {
		List<SubGrupo> subGrupos = this.subGrupoRepo.findAll();
		List<CuentaConfig> cuentas = new Vector<>();
		// Todos los centros deben nacer con una cuenta de ajuste
		CuentaConfig cuentaAjuste = new CuentaConfig();
		cuentaAjuste.setActivo(true);
		cuentaAjuste.setCodigo(CuentasPrincipalesEnum.AJUSTE.getCodigo());
		cuentaAjuste.setEliminable(false);
		cuentaAjuste.setDescripcion("Cuenta de ajuste");
		cuentaAjuste.setIdParent(getSubgrupo(subGrupos, CuentasPrincipalesEnum.CAJA.getCodigo()).getId());
		cuentas.add(cuentaAjuste);

		CuentaConfig iibbCreditoFiscal = new CuentaConfig();
		iibbCreditoFiscal.setActivo(true);
		iibbCreditoFiscal.setCodigo(CuentasPrincipalesEnum.IIBB_CREDITO_FISCAL.getCodigo());
		iibbCreditoFiscal.setEliminable(false);
		iibbCreditoFiscal.setDescripcion("IIBB Crédito Fiscal");
		iibbCreditoFiscal
				.setIdParent(getSubgrupo(subGrupos, CuentasPrincipalesEnum.CREDITO_FISCAL.getCodigo()).getId());
		cuentas.add(iibbCreditoFiscal);

		CuentaConfig ivaCreditoFiscal = new CuentaConfig();
		ivaCreditoFiscal.setActivo(true);
		ivaCreditoFiscal.setCodigo(CuentasPrincipalesEnum.IVA_CREDITO_FISCAL.getCodigo());
		ivaCreditoFiscal.setEliminable(false);
		ivaCreditoFiscal.setDescripcion("IVA Crédito Fiscal");
		ivaCreditoFiscal.setIdParent(getSubgrupo(subGrupos, CuentasPrincipalesEnum.CREDITO_FISCAL.getCodigo()).getId());
		cuentas.add(ivaCreditoFiscal);

		CuentaConfig otrosImpuestos = new CuentaConfig();
		otrosImpuestos.setActivo(true);
		otrosImpuestos.setCodigo(CuentasPrincipalesEnum.OTROS_IMPUESTOS.getCodigo());
		otrosImpuestos.setEliminable(false);
		otrosImpuestos.setDescripcion("Otros Impuestos");
		otrosImpuestos.setIdParent(getSubgrupo(subGrupos, CuentasPrincipalesEnum.CREDITO_FISCAL.getCodigo()).getId());
		cuentas.add(otrosImpuestos);

		CuentaConfig cuentaChequesOtros = new CuentaConfig();
		cuentaChequesOtros.setActivo(true);
		cuentaChequesOtros.setCodigo(CuentasPrincipalesEnum.CHEQUES_OTROS.getCodigo());
		cuentaChequesOtros.setEliminable(false);
		cuentaChequesOtros.setDescripcion("Otros Cheques Terceros");
		cuentaChequesOtros.setIdParent(getSubgrupo(subGrupos, CuentasPrincipalesEnum.CHEQUES.getCodigo()).getId());
		cuentas.add(cuentaChequesOtros);

		CuentaConfig cuentaChequesCliente = new CuentaConfig();
		cuentaChequesCliente.setActivo(true);
		cuentaChequesCliente.setCodigo(CuentasPrincipalesEnum.CHEQUES_CLIENTES.getCodigo());
		cuentaChequesCliente.setEliminable(false);
		cuentaChequesCliente.setDescripcion("Cheques Terceros Clientes");
		cuentaChequesCliente.setIdParent(getSubgrupo(subGrupos, CuentasPrincipalesEnum.CHEQUES.getCodigo()).getId());
		cuentas.add(cuentaChequesCliente);

		CuentaConfig cuentaProvSC = new CuentaConfig();
		cuentaProvSC.setActivo(true);
		cuentaProvSC.setCodigo(CuentasPrincipalesEnum.CC_PROVEEDORES_SIN_CREDITO.getCodigo());
		cuentaProvSC.setEliminable(false);
		cuentaProvSC.setDescripcion("Proveedores sin crédito");
		cuentaProvSC.setIdParent(getSubgrupo(subGrupos, CuentasPrincipalesEnum.CC_PROVEEDORES.getCodigo()).getId());
		cuentas.add(cuentaProvSC);

		CuentaConfig cuentaProvNH = new CuentaConfig();
		cuentaProvNH.setActivo(true);
		cuentaProvNH.setCodigo(CuentasPrincipalesEnum.CC_PROVEEDORES_NO_HABITUALES.getCodigo());
		cuentaProvNH.setEliminable(false);
		cuentaProvNH.setDescripcion("Proveedores no habituales");
		cuentaProvNH.setIdParent(getSubgrupo(subGrupos, CuentasPrincipalesEnum.CC_PROVEEDORES.getCodigo()).getId());
		cuentas.add(cuentaProvNH);

		CuentaConfig cuentaProvCC = new CuentaConfig();
		cuentaProvCC.setActivo(true);
		cuentaProvCC.setCodigo(CuentasPrincipalesEnum.CC_PROVEEDORES_CON_CREDITO.getCodigo());
		cuentaProvCC.setEliminable(false);
		cuentaProvCC.setDescripcion("Proveedores con crédito");
		cuentaProvCC.setIdParent(getSubgrupo(subGrupos, CuentasPrincipalesEnum.CC_PROVEEDORES.getCodigo()).getId());
		cuentas.add(cuentaProvCC);

		cuentas.addAll(this.getCuentasNegocio(subGrupos));
		return cuentas;
	}

	private SubGrupo getSubgrupo(List<SubGrupo> subGrupos, String codigo) {
		Optional<SubGrupo> op = subGrupos.parallelStream().filter(sg -> sg.getCodigo().equals(codigo)).findFirst();
		if (op.isPresent()) {
			return op.get();
		} else {
			throw new SubGrupoNoExistente("No existe el SG con codigo " + codigo);
		}
	}

	private List<CuentaConfig> getCuentasNegocio(List<SubGrupo> subGrupos) {
		List<CentroDescriptivo> centros = this.centroRepo.findByActivoIsTrue(CentroDescriptivo.class);
		SubGrupo unidadNegocio = this.getSubgrupo(subGrupos, CuentasPrincipalesEnum.SG_UNIDADES_NEGOCIO.getCodigo());
		List<CuentaConfig> cuentas = new Vector<>();
		for (CentroDescriptivo cc : centros) {
			CuentaConfig cuentaUnidadNegocio = new CuentaConfig();
			cuentaUnidadNegocio.setActivo(true);
			cuentaUnidadNegocio.setCodigo("UN_" + cc.getCodigo());
			cuentaUnidadNegocio.setEliminable(false);
			cuentaUnidadNegocio.setDescripcion("UN " + cc.getNombre());
			cuentaUnidadNegocio.setIdParent(unidadNegocio.getId());
			cuentas.add(cuentaUnidadNegocio);

		}
		return cuentas;

	}

}

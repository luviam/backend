package ar.com.thinkup.luviam.service;

import ar.com.thinkup.luviam.model.TipoMenu;
import ar.com.thinkup.luviam.vo.TipoMenuVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.thinkup.luviam.model.TipoMenu;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.repository.TipoMenuRepository;
import ar.com.thinkup.luviam.service.def.ITipoMenuService;

import ar.com.thinkup.luviam.vo.TipoMenuVO;

@Service("ITipoMenuService")
public class TipoMenuService extends ParametricoService<TipoMenuVO, TipoMenu>
	implements ITipoMenuService {

	@Autowired
	private TipoMenuRepository repo;



	@Override
	protected TipoMenuRepository getRepository() {
	return this.repo;
	}

	@Override
	protected TipoMenuVO toVO(TipoMenu ent) {

	return new TipoMenuVO(ent);
	}

	@Override
	protected TipoMenu newEnt() {
	return new TipoMenu();
	}

	
}

package ar.com.thinkup.luviam.service;

import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.stream.Collectors;

import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.Operacion;
import ar.com.thinkup.luviam.model.enums.CuentasPrincipalesEnum;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.enums.TipoTransaccionCajaEnum;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.repository.AsientoRepository;
import ar.com.thinkup.luviam.repository.CuentaAplicacionRepository;
import ar.com.thinkup.luviam.service.contabilizacion.IContabilizable;
import ar.com.thinkup.luviam.service.contabilizacion.IGeneradorAsiento;
import ar.com.thinkup.luviam.service.def.IContableService;

@Service("IContableService")
@Configurable
@Transactional
@SuppressWarnings({ "rawtypes", "unchecked" })
public class ContableService implements IContableService {

	@Autowired
	private AsientoRepository asientoRepo;

	@Autowired
	private CuentaAplicacionRepository cuentaRepo;

	private List<IGeneradorAsiento<? extends IContabilizable>> contabilizadores = new Vector<>();

	@Autowired
	public ContableService(AutowireCapableBeanFactory autowireBeanFactory) {
		super();
		Reflections reflections = new Reflections("ar.com.thinkup.luviam.service.contabilizacion.reglas");
		Set<Class<? extends IGeneradorAsiento>> classes = reflections.getSubTypesOf(IGeneradorAsiento.class);
		for (Class<? extends IGeneradorAsiento> c : classes) {
			if (!Modifier.isAbstract(c.getModifiers())) {
				this.contabilizadores.add(autowireBeanFactory.createBean(c));
			}

		}
	}

	@Transactional
	public Asiento contabilizar(IContabilizable o) {
		List<Asiento> asientos = new Vector<>();
		List<IGeneradorAsiento> generadores = this.contabilizadores.parallelStream().filter(go -> go.esAplicable(o))
				.collect(Collectors.toList());
		if (generadores.size() == 0) {
			throw new NoContabilizableException(o);
		}
		generadores.forEach(g -> {
			Asiento a = g.generarAsiento(o);
			if (!a.getTotalDebe().equals(a.getTotalHaber()) || a.getOperaciones().size() == 0) {
				throw new IllegalArgumentException("Error al generar el Asiento: " + a.toString());
			}
			//para actualizar los estados de las operaciones
			a.setEstado(a.getEstado());
			a = this.asientoRepo.save(a);
			a.setNumero(a.getId());
			asientos.add(a);
		});

		return asientos.get(asientos.size() - 1);

	}

	/**
	 * Permite gestionar las diferencias entre los montos declarados al
	 * cierre/apertura de caja con el saldo real registrado por el sistema
	 * 
	 * @param caja
	 *            La caja sobre la que se está operando
	 * @param diferencia
	 *            La diferencia entre el monto al momento de la transaccion, y el
	 *            saldo real registrado por el sistema
	 *            <ul>
	 *            <li>> 0 Se registra un crédito en la caja de AJUSTE de la cuenta
	 *            correspondiente</li>
	 *            <li>< 0 Se registra un débito en la caja de AJUSTE de la cuenta
	 *            correspondiente</li>
	 *            </ul>
	 * @param tipoTrx
	 *            El tipo de transaccion que se está efectuando
	 *            {@link TipoTransaccionCajaEnum}
	 * @param usuario
	 *            El usuario que está ejecutando la transacción
	 */
	@Transactional
	@Override
	public Asiento ajustarDiferenciaCaja(CuentaAplicacion caja, Double diferencia, TipoTransaccionCajaEnum tipoTrx,
			Usuario usuario) {
		if (diferencia == 0)
			return null;
		boolean positivo = diferencia > 0;
		String mensajeAjuste = (positivo ? "Sobrante " : "Faltante ") + " en " + tipoTrx.name().toLowerCase()
				+ " de caja " + caja.getDescripcion();
		CuentaAplicacion cuentaAjuste = this.cuentaRepo.findByCodigoAndCentroCostoCodigoAndCentroCostoActivoTrue(
				CuentasPrincipalesEnum.AJUSTE.getCodigo(), caja.getCentroCosto().getCodigo());
		Asiento asientoAjuste = new Asiento();
		asientoAjuste.setDescripcion(mensajeAjuste);
		asientoAjuste.setEstado(EstadoOperacionEnum.APROBADO.getCodigo());
		asientoAjuste.setMensaje("Generado por sistema en " + tipoTrx.name().toLowerCase() + " de caja.");
		asientoAjuste.setFecha(new Date());
		asientoAjuste.setResponsable(usuario.getUsername());

		Operacion desdeCaja = new Operacion();
		desdeCaja.setAsiento(asientoAjuste);
		desdeCaja.setCentro(caja.getCentroCosto());
		desdeCaja.setCuenta(caja);
	
		desdeCaja.setDetalle(mensajeAjuste);

		Operacion desdeAjuste = new Operacion();
		desdeAjuste.setAsiento(asientoAjuste);
		desdeAjuste.setCentro(caja.getCentroCosto());
		desdeAjuste.setCuenta(cuentaAjuste);
	
		desdeAjuste.setDetalle(mensajeAjuste);

		if (diferencia < 0) {
			// debito
			desdeCaja.setHaber(diferencia * (-1));
			desdeAjuste.setDebe(diferencia * (-1));

		} else if (diferencia > 0) {
			// credito
			desdeCaja.setDebe(diferencia);
			desdeAjuste.setHaber(diferencia);
		}

		asientoAjuste.agregarOperacion(desdeCaja);
		asientoAjuste.agregarOperacion(desdeAjuste);

		// TODO Whaaaat??
		asientoAjuste = this.asientoRepo.save(asientoAjuste);
		asientoAjuste.setNumero(asientoAjuste.getId());
		return asientoRepo.save(asientoAjuste);
	}

}

package ar.com.thinkup.luviam.service.def;

import java.util.List;

import ar.com.thinkup.luviam.model.documentos.pagos.OrdenPago;
import ar.com.thinkup.luviam.vo.OrdenPagoCabeceraVO;
import ar.com.thinkup.luviam.vo.documentos.pagos.ItemOrdenPagoVO;
import ar.com.thinkup.luviam.vo.documentos.pagos.OrdenPagoVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroProveedor;

public interface IOrdenPagoService extends IDocumentoPago<OrdenPago,OrdenPagoVO> {
	List<OrdenPagoCabeceraVO> getCabeceras(FiltroProveedor filtro);

	List<ItemOrdenPagoVO> getItems(Long idOrdenPago);

	List<ItemOrdenPagoVO> getItemsByFactura(Long idFactura);

}

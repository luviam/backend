package ar.com.thinkup.luviam.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.documentos.comisiones.ItemLiquidacionComisiones;
import ar.com.thinkup.luviam.repository.ItemLiquidacionComisionesRepository;
import ar.com.thinkup.luviam.service.def.IItemLiquidacionComisionesService;
import ar.com.thinkup.luviam.service.def.IPagoComisionService;
import ar.com.thinkup.luviam.vo.ItemLiquidacionComisionesVO;

@Service("IItemLiquidacionComisionesService")
public class ItemLiquidacionComisionesService extends
		ModelService<ItemLiquidacionComisiones, ItemLiquidacionComisionesVO, ItemLiquidacionComisionesRepository>
		implements IItemLiquidacionComisionesService {

	@Autowired
	private ItemLiquidacionComisionesRepository repo;

	@Autowired
	private IPagoComisionService pagoService;

	@Override
	protected ItemLiquidacionComisionesRepository getRepository() {
		return this.repo;
	}

	@Override
	protected ItemLiquidacionComisionesVO toVO(ItemLiquidacionComisiones ent) {

		return new ItemLiquidacionComisionesVO(ent);
	}

	@Override
	protected ItemLiquidacionComisiones newEnt() {
		return new ItemLiquidacionComisiones();
	}

	@Transactional
	public ItemLiquidacionComisiones parseToEnt(ItemLiquidacionComisionesVO vo, ItemLiquidacionComisiones entity) {

		ItemLiquidacionComisiones ent = entity;
		if (ent == null)
			ent = new ItemLiquidacionComisiones();
		ent.setId(vo.getId());
		ent.setPagoComision(this.pagoService.getEntity(vo.getPagoComision().getId()));
		ent.setImporte(vo.getImporte());
		ent.getPagoComision().addLiquidado(vo.getImporte());
		return ent;
	}

	@Override
	@Transactional
	public List<ItemLiquidacionComisionesVO> getByLiquidacionId(Long idLiquidacion) {
		return this.repo.findAllByLiquidacionId(idLiquidacion).stream().map(l -> new ItemLiquidacionComisionesVO(l))
				.collect(Collectors.toList());
	}

}

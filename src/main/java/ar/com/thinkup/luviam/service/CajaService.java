package ar.com.thinkup.luviam.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import ar.com.thinkup.luviam.exceptions.CajaAbiertaException;
import ar.com.thinkup.luviam.exceptions.CajaCerradaException;
import ar.com.thinkup.luviam.exceptions.PlanCuentasInvalidoException;
import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.EstadoTraspaso;
import ar.com.thinkup.luviam.model.Operacion;
import ar.com.thinkup.luviam.model.TransaccionCaja;
import ar.com.thinkup.luviam.model.TraspasoCaja;
import ar.com.thinkup.luviam.model.UltimasTransaccionesCaja;
import ar.com.thinkup.luviam.model.enums.EstadoAsientoEnum;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.enums.TipoTransaccionCajaEnum;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.repository.AsientoRepository;
import ar.com.thinkup.luviam.repository.CuentaAplicacionRepository;
import ar.com.thinkup.luviam.repository.OperacionesRepository;
import ar.com.thinkup.luviam.repository.TransaccionCajaRepository;
import ar.com.thinkup.luviam.repository.TraspasoCajaRepository;
import ar.com.thinkup.luviam.repository.UltimasTransaccionesCajaRepository;
import ar.com.thinkup.luviam.service.def.ICajaService;
import ar.com.thinkup.luviam.service.def.IContableService;
import ar.com.thinkup.luviam.vo.BalanceCajaVO;
import ar.com.thinkup.luviam.vo.operaciones.AltaTraspasoVO;

@Service("ICajaService")
@Transactional
public class CajaService implements ICajaService {

	@Autowired
	private CuentaAplicacionRepository cuentaRepo;

	@Autowired
	private TraspasoCajaRepository traspasoCajaRepo;

	@Autowired
	private AsientoRepository asientoRepository;

	@Autowired
	private OperacionesRepository operacionesRepository;

	@Autowired
	private TransaccionCajaRepository trxCajaRepo;

	@Autowired
	private UltimasTransaccionesCajaRepository ultimasTrxRepo;

	@Autowired
	private IContableService contableService;

	@Override
	@Transactional
	public TraspasoCaja generarTraspaso(AltaTraspasoVO traspaso, Usuario responsable) {
		if (traspaso.getIdCajaOrigen() == null)
			throw new IllegalArgumentException("Cuenta Origen obligatoria");
		if (traspaso.getIdCajaDestino() == null)
			throw new IllegalArgumentException("Cuenta Destino obligatoria");
		if (traspaso.getMonto() == null)
			throw new IllegalArgumentException("Monto obligatorio");
		if (responsable == null)
			throw new IllegalArgumentException("Responsable obligatorio");
		CuentaAplicacion origen = cuentaRepo.findOne(traspaso.getIdCajaOrigen());
		if (origen == null) {
			throw new IllegalArgumentException("No existe la cuenta con id: " + traspaso.getIdCajaOrigen());
		}
		CuentaAplicacion destino = cuentaRepo.findOne(traspaso.getIdCajaDestino());
		if (destino == null) {
			throw new IllegalArgumentException("No existe la cuenta con id: " + traspaso.getIdCajaDestino());
		}

		TraspasoCaja tc = new TraspasoCaja();
		tc.setCajaDestino(destino);
		tc.setCajaOrigen(origen);
		tc.setMonto(traspaso.getMonto());
		tc.setDescripcion(
				StringUtils.isBlank(traspaso.getDescripcion()) ? "Traspaso de Caja" : traspaso.getDescripcion());
		tc.setResponsable(responsable);
		tc.setCodigoEstado(EstadoOperacionEnum.PENDIENTE.getCodigo());
		tc.setFechaOperacion(traspaso.getFecha());

		tc = traspasoCajaRepo.save(tc);

		return tc;
	}

	private Asiento contabilizarTraspaso(Usuario responsable, TraspasoCaja tc) {
		// ESTE ASIENTO SE TIENE QUE GENERAR UNICAMENTE CUANDO SE APRUEBA
		Asiento a = new Asiento();

		a.setEstado(EstadoAsientoEnum.APROBADO.getCodigo());
		a.setFecha(tc.getFechaOperacion());
		a.setMensaje("Generado por sistema");
		a.setResponsable(responsable.getDescripcion());
		String motivo = StringUtils.isBlank(tc.getDescripcion())
				? "Traspaso de Caja a " + tc.getCajaDestino().getDescripcion()
				: tc.getDescripcion();
		a.setDescripcion(motivo);
		Operacion egreso = new Operacion();
		egreso.setAsiento(a);
		egreso.setCentro(tc.getCajaOrigen().centroCosto);
		egreso.setCuenta(tc.getCajaOrigen());
		egreso.setDebe(0d);
		egreso.setHaber(tc.getMonto());
		egreso.setDetalle("Egreso por - " + motivo);
		
		a.agregarOperacion(egreso);

		Operacion ingreso = new Operacion();
		ingreso.setAsiento(a);
		ingreso.setCentro(tc.getCajaDestino().centroCosto);
		ingreso.setCuenta(tc.getCajaDestino());
		ingreso.setDebe(tc.getMonto());
		ingreso.setHaber(0d);
		ingreso.setDetalle("Ingreso por - " + motivo);
		
		a.agregarOperacion(ingreso);

		if (!tc.getCajaDestino().getCentroCosto().getCodigo().equals(tc.getCajaOrigen().getCentroCosto().getCodigo())) {
			// DISTINTOS CENtROS
			Operacion ajusteOrigen = new Operacion();
			ajusteOrigen.setAsiento(a);
			ajusteOrigen.setCentro(tc.getCajaOrigen().centroCosto);
			CuentaAplicacion unOrigen = tc.getCajaOrigen().getCentroCosto()
					.getCuentaByCodigo("UN_" + tc.getCajaDestino().getCentroCosto().getCodigo());
			if (unOrigen == null) {
				throw new PlanCuentasInvalidoException(
						"No existe la cuenta con código " + "UN_" + tc.getCajaDestino().getCentroCosto().getCodigo());
			}
			ajusteOrigen.setCuenta(unOrigen);
			ajusteOrigen.setDebe(tc.getMonto());
			ajusteOrigen.setHaber(0d);
			ajusteOrigen.setDetalle("Ajuste balance por egreso entre Centros");
			
			a.agregarOperacion(ajusteOrigen);

			Operacion ajusteDestino = new Operacion();
			ajusteDestino.setAsiento(a);
			ajusteDestino.setCentro(tc.getCajaDestino().centroCosto);
			CuentaAplicacion unDestino = tc.getCajaDestino().getCentroCosto()
					.getCuentaByCodigo("UN_" + tc.getCajaOrigen().getCentroCosto().getCodigo());
			if (unDestino == null) {
				throw new PlanCuentasInvalidoException(
						"No existe la cuenta con código " + "UN_" + tc.getCajaOrigen().getCentroCosto().getCodigo());
			}
			ajusteDestino.setCuenta(unDestino);
			ajusteDestino.setDebe(0d);
			ajusteDestino.setHaber(tc.getMonto());
			ajusteDestino.setDetalle("Ajuste balance por Ingreso entre Centros");
			
			a.agregarOperacion(ajusteDestino);

		}

		a = asientoRepository.save(a);
		a.setNumero(a.getId());
		a = asientoRepository.save(a);
		return a;
	}

	@Transactional
	public Boolean validarAsiento(Asiento a) {
		return false;
	}

	@Override
	@Transactional
	public void confirmarTraspaso(EstadoTraspaso traspaso, Usuario responsable) {
		TraspasoCaja tc = this.traspasoCajaRepo.findOne(traspaso.getId());
		if (tc == null)
			throw new IllegalArgumentException("No existe el Traspaso con id " + traspaso.getId());
		tc.setCodigoEstado(EstadoOperacionEnum.APROBADO.getCodigo());
		this.traspasoCajaRepo.save(tc);
		this.contabilizarTraspaso(responsable, tc);

	}

	@Override
	@Transactional
	public void rechazarTraspaso(EstadoTraspaso traspaso, Usuario responsable) {
		TraspasoCaja tc = this.traspasoCajaRepo.findOne(traspaso.getId());
		if (tc == null)
			throw new IllegalArgumentException("No existe el Traspaso con id " + traspaso.getId());
		tc.setCodigoEstado(EstadoOperacionEnum.RECHAZADO.getCodigo());
		this.traspasoCajaRepo.save(tc);

	}

	@Override
	public void cerrarCaja(CuentaAplicacion caja, Double monto, Usuario usuario) throws CajaCerradaException {
		TransaccionCaja ultimaTransaccion = this.getEstadoCaja(caja.getId());

		if (ultimaTransaccion != null
				&& TipoTransaccionCajaEnum.APERTURA.getCodigo().equals(ultimaTransaccion.getTipo())) {
			Double saldoReal = this.getSaldoCaja(caja.getId(), ultimaTransaccion);
			if (monto == null)
				monto = saldoReal;
			Double diferenciaMontoSaldo = monto - saldoReal;
			this.contableService.ajustarDiferenciaCaja(caja, diferenciaMontoSaldo, TipoTransaccionCajaEnum.CIERRE,
					usuario);

			this.guardarTransaccionCaja(TipoTransaccionCajaEnum.CIERRE, caja, monto, diferenciaMontoSaldo, usuario);
		} else {
			throw new CajaCerradaException();
		}
	}

	@Override
	public void abrirCaja(CuentaAplicacion caja, Double monto, Usuario usuario) throws CajaAbiertaException {

		TransaccionCaja ultimaTransaccion = this.getEstadoCaja(caja.getId());
		Double diferenciaMontoSaldo = 0d;
		if (ultimaTransaccion != null
				&& TipoTransaccionCajaEnum.APERTURA.getCodigo().equals(ultimaTransaccion.getTipo())) {
			throw new CajaAbiertaException();
		}
		if (ultimaTransaccion != null) {
			Double saldoAlCierre = this.getSaldoCaja(caja.getId(), ultimaTransaccion);
			if (monto != saldoAlCierre) {
				diferenciaMontoSaldo = monto - saldoAlCierre;
				this.contableService.ajustarDiferenciaCaja(caja, diferenciaMontoSaldo, TipoTransaccionCajaEnum.APERTURA,
						usuario);
			}

		}
		this.guardarTransaccionCaja(TipoTransaccionCajaEnum.APERTURA, caja, monto, diferenciaMontoSaldo, usuario);
	}

	@Override
	public Double getSaldoCaja(Long cajaId) {
		return this.getSaldoCaja(cajaId, null);
	}

	@Override
	public Double getSaldoCaja(Long cajaId, TransaccionCaja estadoCaja) {
		TransaccionCaja ultimaTransaccion;
		if (estadoCaja == null) {
			ultimaTransaccion = this.getEstadoCaja(cajaId);
		} else {
			ultimaTransaccion = estadoCaja;
		}

		if (ultimaTransaccion != null) {
			if (TipoTransaccionCajaEnum.APERTURA.getCodigo().equals(ultimaTransaccion.getTipo())) {
				BalanceCajaVO balance = this.operacionesRepository.findBalanceByCuentaId(cajaId,
						ultimaTransaccion.getFechaTransaccion());
				return balance.calcularSaldo(ultimaTransaccion.getMonto());
			} else {
				return ultimaTransaccion.getMonto();
			}
		}
		return 0d;
	}

	private void guardarTransaccionCaja(TipoTransaccionCajaEnum tipo, CuentaAplicacion caja, Double monto,
			Double diferenciaMontoSaldo, Usuario usuario) {
		TransaccionCaja trx = new TransaccionCaja();
		trx.setCajaId(caja.getId());
		trx.setFechaTransaccion(new Date());
		trx.setMonto(monto);
		if (usuario != null)
			trx.setResponsableId(usuario.getId());
		trx.setTipo(tipo.getCodigo());
		trx.setDiferenciaConSaldo(diferenciaMontoSaldo);
		this.trxCajaRepo.save(trx);
	}

	/**
	 * Calcula el estado de una caja en base a las ultimas transacciones
	 * 
	 * @param cajaId
	 * @return te man
	 */
	@Override
	public TransaccionCaja getEstadoCaja(Long cajaId) {
		UltimasTransaccionesCaja ultimasTrx = this.ultimasTrxRepo.findOne(cajaId);
		if (ultimasTrx.getUltimaTransaccion() == null)
			return null;
		List<TransaccionCaja> trxCaja;
		switch (ultimasTrx.getUltimaTransaccion()) {
		case APERTURA:
			trxCaja = this.trxCajaRepo.findUltimaApertura(cajaId, new Date());
			break;
		case CIERRE:
			trxCaja = this.trxCajaRepo.findUltimoCierre(cajaId, new Date());
			break;
		default:
			trxCaja = null;
			break;

		}
		return !CollectionUtils.isEmpty(trxCaja) ? trxCaja.get(0) : null;
	}

}

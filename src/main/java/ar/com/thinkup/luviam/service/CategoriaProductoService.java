package ar.com.thinkup.luviam.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.CategoriaProducto;
import ar.com.thinkup.luviam.repository.CategoriaProductoRepository;
import ar.com.thinkup.luviam.repository.ParameterRepository;
import ar.com.thinkup.luviam.service.def.ICategoriaProductoService;
import ar.com.thinkup.luviam.vo.CategoriaProductoVO;

@Service("ICategoriaProductoService")
@Transactional
public class CategoriaProductoService extends ParametricoService<CategoriaProductoVO, CategoriaProducto>
		implements ICategoriaProductoService {

	@Autowired
	private CategoriaProductoRepository repo;

	@Override
	protected ParameterRepository<CategoriaProducto> getRepository() {
		return this.repo;
	}

	@Override
	protected CategoriaProductoVO toVO(CategoriaProducto ent) {
		return new CategoriaProductoVO(ent);
	}

	@Override
	protected CategoriaProducto newEnt() {
		return new CategoriaProducto();
	}

	@Override
	public CategoriaProducto parseToEnt(CategoriaProductoVO vo, CategoriaProducto ent) {

		CategoriaProducto c = super.parseToEnt(vo, ent);
		c.setEsComisionable(vo.getEsComisionable());
		c.setPeso(vo.getPeso());
		return c;
	}

	@Override
	public List<CategoriaProductoVO> getComisionables() {

		return this.repo.findByEsComisionableIsTrue().stream().map(c -> new CategoriaProductoVO(c))
				.collect(Collectors.toList());
	}

}

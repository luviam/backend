package ar.com.thinkup.luviam.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.thinkup.luviam.model.contratos.PagoComision;
import ar.com.thinkup.luviam.model.enums.TipoPagoComision;
import ar.com.thinkup.luviam.repository.PagoComisionRepository;
import ar.com.thinkup.luviam.service.def.IPagoComisionService;
import ar.com.thinkup.luviam.vo.PagoComisionVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroComisiones;

@Service("IPagoComisionService")
@Transactional
public class PagoComisionService extends ModelService<PagoComision, PagoComisionVO, PagoComisionRepository>
		implements IPagoComisionService {

	@Autowired
	private PagoComisionRepository repo;

	@Override
	protected PagoComisionRepository getRepository() {
		return this.repo;
	}

	@Override
	protected PagoComisionVO toVO(PagoComision ent) {

		return new PagoComisionVO(ent);
	}

	@Override
	protected PagoComision newEnt() {
		return new PagoComision();
	}

	@Override
	public PagoComision parseToEnt(PagoComisionVO vo, PagoComision entity) {
		PagoComision ent = entity;
		if (ent == null)
			ent = new PagoComision();
		ent.setId(vo.getId());
		ent.setTotalComision(vo.getTotalComision());
		ent.setCantidad(vo.getCantidadBase());
		ent.setTotalLiquidado(vo.getTotalLiquidado());
		ent.setFechaComision(vo.getFechaComision() != null ? vo.getFechaComision() : new Date());
		if (vo.getTipoPago() == null) {
			throw new IllegalArgumentException("El tipo de pago es obligatorio");
		}
		ent.setTipoPagoComision(TipoPagoComision.byCodigo(vo.getTipoPago().getCodigo()));

		return ent;
	}

	@Override
	@Transactional
	public List<PagoComisionVO> getImpagos(FiltroComisiones filtro) {
		if (filtro == null) {
			throw new IllegalArgumentException("El filtro no puede estar vacio");
		}
		return this.repo.findByFiltros(filtro.getIdComisionista(), filtro.getCodigoCentro(), filtro.getIdContrato(),
				filtro.getTiposComision(), filtro.getFechaDesde(), filtro.getFechaHasta());
	}

}
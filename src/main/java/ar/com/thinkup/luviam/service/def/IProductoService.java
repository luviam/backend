package ar.com.thinkup.luviam.service.def;

import java.util.Date;
import java.util.List;

import ar.com.thinkup.luviam.model.Producto;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.vo.PrecioProductoVO;
import ar.com.thinkup.luviam.vo.ProductoVO;
import ar.com.thinkup.luviam.vo.indicadores.CubiertoPromedioVO;

public interface IProductoService extends IModelService<ProductoVO,Producto> {

	List<DescriptivoGeneric> getByCategoria(String codigoCategoria);
	
	CubiertoPromedioVO getCubiertoPromedio(String codigoCentro, String codigoProducto, Date desde, Date hasta );

	Producto getByDescriptivo(IDescriptivo descriptivoGeneric);

	List<PrecioProductoVO> getConMarcaByCategoria(String codigoCategoria);

}

package ar.com.thinkup.luviam.service.def;

import java.util.List;

import ar.com.thinkup.luviam.model.contratos.CuotaServicio;
import ar.com.thinkup.luviam.vo.contratos.CuotaServicioVO;

public interface ICuotaServicioService extends IModelService<CuotaServicioVO, CuotaServicio> {

	public List<CuotaServicio> actualizarSaldos(List<CuotaServicio> cuotas);

	public CuotaServicio actualizarSaldo(CuotaServicio cuota);

	public List<CuotaServicio> getCuotasServicioContratado(Long contratoId);

	public List<CuotaServicio> getCuotasImpagas(Long idContrato);
}

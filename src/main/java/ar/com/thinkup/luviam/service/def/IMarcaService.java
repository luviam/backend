package ar.com.thinkup.luviam.service.def;

import org.springframework.web.multipart.MultipartFile;

import ar.com.thinkup.luviam.model.MarcaProductos;
import ar.com.thinkup.luviam.vo.MarcaVO;

public interface IMarcaService extends IParametricoService< MarcaVO,MarcaProductos> {

	
	public MarcaVO create(MultipartFile logo, MarcaVO marca);

	public MarcaVO update(MultipartFile logo, MarcaVO marca);
}

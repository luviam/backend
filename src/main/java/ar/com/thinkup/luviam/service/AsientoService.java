package ar.com.thinkup.luviam.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import ar.com.thinkup.luviam.exceptions.AsientoInvalidoException;
import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.Operacion;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.repository.AsientoRepository;
import ar.com.thinkup.luviam.repository.CentroRepository;
import ar.com.thinkup.luviam.service.def.IAsientoService;
import ar.com.thinkup.luviam.vo.AsientoVO;

@Service("IAsientoService")
@Transactional
public class AsientoService implements IAsientoService {
	@Autowired
	private AsientoRepository asientoRepository;

	@Autowired
	private CentroRepository centroRepo;

	@Override
	public AsientoVO updateAsiento(AsientoVO asiento) throws IllegalArgumentException, AsientoInvalidoException {
		List<CentroCosto> centros = centroRepo.findAll();
		if (asiento.getId() == null) {
			throw new IllegalArgumentException("No se puede modificar un asiento sin id");
		} else {
			final Asiento a = this.asientoRepository.findOne(asiento.getId());
			if (a == null) {
				throw new IllegalArgumentException("No se encuentra asiento con id " + asiento.getId());
			} else {
				a.setDescripcion(asiento.getDescripcion());
				a.setFecha(asiento.getFecha());
				a.setEstado(asiento.getEstado().getCodigo());
				if (StringUtils.isBlank(asiento.getResponsable())) {
					throw new IllegalArgumentException("No se puede crear un asiento sin responsable.");
				} else {
					a.setResponsable(asiento.getResponsable());
				}
				// MODIFICO LOS EXISTENTES
				asiento.getOperaciones().parallelStream().filter(o -> o.getId() != null).forEach(o -> {
					Optional<Operacion> op = a.getOperaciones().parallelStream()
							.filter(oo -> oo.getId().equals(o.getId())).findFirst();
					if (op.isPresent()) {
						op.get().setCentro(this.getCentroCosto(o.getCentroCosto(), centros));
						op.get().setComprobante(o.getComprobante());
						op.get().setCuenta(this.getCuenta(o.getCuenta(), op.get().getCentro().getCuentasAplicacion()));
						op.get().setDebe(o.getDebe());
						op.get().setHaber(o.getHaber());
						op.get().setDetalle(o.getDescripcion());
						

					}
				});

				// AGREGO LOS NUEVOS
				asiento.getOperaciones().parallelStream().filter(o -> o.getId() == null).forEach(o -> {
					Operacion op = new Operacion();
					op.setCentro(this.getCentroCosto(o.getCentroCosto(), centros));
					op.setComprobante(o.getComprobante());
					op.setDetalle(o.getDescripcion());
					op.setCuenta(this.getCuenta(o.getCuenta(), op.getCentro().getCuentasAplicacion()));
					op.setDebe(o.getDebe());
					op.setHaber(o.getHaber());
					
					op.setAsiento(a);
					a.getOperaciones().add(op);
				});

				// ELIMINO ELIMINADOS

				if (!CollectionUtils.isEmpty(asiento.getOperaciones())) {
					a.getOperaciones().removeIf(o -> o.getId() != null && asiento.getOperacion(o.getId()) == null);

				}
				this.validarAsiento(a);
				Asiento aSaved = this.asientoRepository.save(a);
				return new AsientoVO(aSaved);

			}
		}

	}

	private void validarAsiento(Asiento a) throws AsientoInvalidoException {
		Map<String, Double> totalesCentro = new HashMap<>();
		a.getOperaciones().forEach(o -> {
			totalesCentro.merge(o.getCentro().getCodigo(), o.getDebe() - o.getHaber(), (t, b) -> {
				return t + b;
			});
		});
		if (totalesCentro.entrySet().stream().anyMatch(kv -> (Math.round(kv.getValue() * 100) /100) != 0d)) {
			throw new AsientoInvalidoException("El balance de canda centro debe ser 0");
		}
		;

	}

	private CentroCosto getCentroCosto(IDescriptivo centro, List<CentroCosto> centros) {

		if (centro == null || StringUtils.isBlank(centro.getCodigo())) {
			throw new IllegalArgumentException("Centro Obligatorio");
		}
		Optional<CentroCosto> oc = centros.parallelStream().filter(c -> c.getCodigo().equals(centro.getCodigo()))
				.findFirst();
		if (oc.isPresent()) {
			return oc.get();
		}
		throw new IllegalArgumentException("No existe el centro con id: " + centro.getCodigo());
	}

	private CuentaAplicacion getCuenta(IDescriptivo cuenta, Set<CuentaAplicacion> cuentas) {

		if (cuenta == null || StringUtils.isBlank(cuenta.getCodigo())) {
			throw new IllegalArgumentException("Cuenta Obligatoria");
		}
		Optional<CuentaAplicacion> oc = cuentas.parallelStream().filter(c -> c.getCodigo().equals(cuenta.getCodigo()))
				.findFirst();
		if (oc.isPresent()) {
			return oc.get();
		}
		throw new IllegalArgumentException("No existe la cuenta con id: " + cuenta.getCodigo());
	}

	@Override
	public AsientoVO crearAsiento(AsientoVO asiento) throws IllegalArgumentException, AsientoInvalidoException {

		List<CentroCosto> centros = centroRepo.findAll();
		if (asiento.getId() != null) {
			throw new IllegalArgumentException("No se puede crear un asiento con id");
		} else {
			final Asiento a = new Asiento();
			a.setDescripcion(asiento.getDescripcion());
			a.setFecha(asiento.getFecha());
			a.setEstado(asiento.getEstado().getCodigo());
			a.setTipoGenerador("AM");
			if (StringUtils.isBlank(asiento.getResponsable())) {
				throw new IllegalArgumentException("No se puede crear un asiento sin responsable.");
			} else {
				a.setResponsable(asiento.getResponsable());
			}

			// AGREGO LOS NUEVOS
			asiento.getOperaciones().stream().forEach(o -> {
				Operacion op = new Operacion();
				op.setCentro(this.getCentroCosto(o.getCentroCosto(), centros));
				op.setComprobante(o.getComprobante());
				op.setCuenta(this.getCuenta(o.getCuenta(), op.getCentro().getCuentasAplicacion()));
				op.setDebe(o.getDebe());
				op.setHaber(o.getHaber());
				
				op.setAsiento(a);
				op.setDetalle(o.getDescripcion());

				a.getOperaciones().add(op);
			});

			this.validarAsiento(a);
			Asiento aSaved = this.asientoRepository.save(a);
			// TODO : AUTOGENERAR EL NUMERO DE ASIENTO
			aSaved.setNumero(aSaved.getId());
			aSaved = this.asientoRepository.save(a);
			return new AsientoVO(aSaved);

		}

	}

	@Override
	public Boolean delete(Long id) {

		this.asientoRepository.delete(this.asientoRepository.findOne(Long.valueOf(id)));

		return true;
	}

}

package ar.com.thinkup.luviam.service.def;

import ar.com.thinkup.luviam.exceptions.AsientoInvalidoException;
import ar.com.thinkup.luviam.vo.AsientoVO;

public interface IAsientoService {

	AsientoVO updateAsiento(AsientoVO asiento) throws IllegalArgumentException, AsientoInvalidoException;

	AsientoVO crearAsiento(AsientoVO asiento) throws IllegalArgumentException, AsientoInvalidoException;

	Boolean delete(Long id);
}

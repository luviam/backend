package ar.com.thinkup.luviam.service;

import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.Cheque;
import ar.com.thinkup.luviam.model.enums.TipoCheque;
import ar.com.thinkup.luviam.repository.ChequeRepository;
import ar.com.thinkup.luviam.repository.CuentaAplicacionRepository;
import ar.com.thinkup.luviam.service.def.IChequeService;
import ar.com.thinkup.luviam.vo.ChequeVO;

@Service(value = "IChequeService")
@Transactional
public class ChequeService extends ModelService<Cheque, ChequeVO, JpaRepository<Cheque, Long>>
		implements IChequeService {

	@Autowired
	private ChequeRepository chequeRepo;
	@Autowired
	private CuentaAplicacionRepository cuentaAplicacionRepo;

	@Override
	protected JpaRepository<Cheque, Long> getRepository() {
		return this.chequeRepo;
	}

	@Override
	@Transactional
	protected ChequeVO toVO(Cheque ent) {
		return new ChequeVO(ent);
	}

	@Override
	protected Cheque newEnt() {
		return new Cheque();
	}

	@Override
	@Transactional
	public Cheque parseToEnt(ChequeVO vo, Cheque ent) {
		Cheque c = new Cheque();
		if (vo.getId() != null) {
			c = this.chequeRepo.findOne(vo.getId());
			if (c == null) {
				throw new IllegalArgumentException("No existe cheque con id: " + vo.getId());
			}
		}

		c.setCodigoEstadoCheque(vo.getEstado().getCodigo());
		c.setCodigoTipoCheque(vo.getTipoCheque().getCodigo());
		if (vo.getCuentaBancaria() != null) {
			c.setCuentaBancaria(this.cuentaAplicacionRepo.findByCodigoAndCentroCostoCodigoAndCentroCostoActivoTrue(
					vo.getCuentaBancaria().getCodigo(), vo.getCuentaBancaria().getCentroCosto().getCodigo()));
		} else {
			c.setCuentaBancaria(null);
		}

		if (c.getTipoCheque().equals(TipoCheque.PROPIO)) {
			c.setTitularCuenta(c.getCuentaBancaria().getCentroCosto().getRazonSocial());
			c.setCuit(c.getCuentaBancaria().getCentroCosto().getCuit());
			c.setBanco(c.getCuentaBancaria().getNombre());
			c.setOrigen("Propio");
		} else {

			c.setCuit(vo.getCuit());
			c.setBanco(vo.getBanco());
			c.setNumeroCuenta(vo.getNumeroCuenta());
			c.setTitularCuenta(vo.getTitularCuenta());

		}
		c.setDestino(vo.getDestino());
		c.setEsDiferido(vo.getEsDiferido());
		c.setFechaEmision(vo.getFechaEmision());
		c.setFechaPago(vo.getFechaPago());
		c.setImporte(vo.getImporte());
		c.setNumero(vo.getNumero());
		c.setOrigen(vo.getOrigen());
		c.setFechaIngreso(vo.getFechaIngreso());
		c.setNumeroRecibo(vo.getNumeroRecibo());
		return c;
	}

	@Override
	@Transactional
	public List<ChequeVO> getDisponibles() {
		List<Cheque> disponibles = this.chequeRepo.findAllDisponibles();
		if (disponibles.size() == 0) {
			return new Vector<>();
		}
		return disponibles.stream().map(c -> new ChequeVO(c)).collect(Collectors.toList());
	}

}

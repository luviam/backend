package ar.com.thinkup.luviam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.Comisionista;

public interface ComisionistaRepository extends JpaRepository<Comisionista, Long> {

	Comisionista findOneByVendedorId(Long id);

	Comisionista findOneBySalonId(Long id);

	@Query("select c from Comisionista c")
	List<IComisionistaDescriptivo> getDescriptivos();

	public interface IComisionistaDescriptivo {
		Long getId();

		String getNombre();
	}
}

package ar.com.thinkup.luviam.repository;

import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.TipoEvento;

@Transactional
public interface TipoEventoRepository extends ParameterRepository<TipoEvento> {

	TipoEvento findByCodigo(String codigo);

}

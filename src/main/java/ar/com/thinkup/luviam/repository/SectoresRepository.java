package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.Sector;

public interface SectoresRepository extends JpaRepository<Sector, Long> {

	public Sector findByCodigo(String codigo);
	public Sector findByDescripcion(String descripcion);
}

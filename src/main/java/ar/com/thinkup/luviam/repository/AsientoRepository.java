package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.Asiento;

public interface AsientoRepository extends JpaRepository<Asiento, Long> {
	
	

}

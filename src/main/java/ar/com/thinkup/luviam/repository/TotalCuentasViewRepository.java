package ar.com.thinkup.luviam.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.TotalesCuentasView;

public interface TotalCuentasViewRepository extends JpaRepository<TotalesCuentasView, Long> {

	public List<TotalesCuentasView> findByPeriodoAndCentro(Date desde, Date hasta, Long idCentro);
}

package ar.com.thinkup.luviam.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.plan.Grupo;

public interface GrupoRepository extends JpaRepository<Grupo, Long> {

	@Query("select s from Grupo s order By s.nombre")
	Set<IDescriptivo> getDescriptivos();
	
	Set<Grupo> findByRubroCodigo(String codigoRubro);
}

package ar.com.thinkup.luviam.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.documentos.comisiones.ItemLiquidacionComisiones;

public interface ItemLiquidacionComisionesRepository extends JpaRepository<ItemLiquidacionComisiones, Long> {

	Set<ItemLiquidacionComisiones> findAllByLiquidacionId(Long idLiquidacion);

}

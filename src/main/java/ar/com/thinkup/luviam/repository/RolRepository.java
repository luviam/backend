package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.security.Rol;

public interface RolRepository extends JpaRepository<Rol, Long> {

	Rol findByCodigo(String superUser);

	
}

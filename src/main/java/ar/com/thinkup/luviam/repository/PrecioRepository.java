package ar.com.thinkup.luviam.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import ar.com.thinkup.luviam.model.Precio;
@NoRepositoryBean
public interface PrecioRepository<P extends Precio> extends JpaRepository<P, Long>{ 
	@Query("select p from #{#entityName} p where ( -1l = ?1 or p.id <> ?1) "
			+ " and p.producto.codigo = ?2 and "
			+ "  p.fechaDesde <= ?4 and "
			+ "( (p.fechaHasta is not null and p.fechaHasta >= ?3) or (p.fechaHasta is null) )"
			+ " and p.salon.id = ?5 and p.activo = true") 
	List<P> getConflictos(Long id,String codigoProducto, Date desde,Date hasta,Long idSalon);
}

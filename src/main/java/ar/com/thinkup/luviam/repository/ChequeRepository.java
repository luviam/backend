package ar.com.thinkup.luviam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.Cheque;

public interface ChequeRepository extends JpaRepository<Cheque, Long> {

	@Query("select c from Cheque c where c.codigoEstadoCheque = 'D'")
	List<Cheque> findAllDisponibles();

}

package ar.com.thinkup.luviam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.TraspasoCaja;

public interface TraspasoCajaRepository extends JpaRepository<TraspasoCaja, Long> {
	
	List<TraspasoCaja> findByCajaDestinoIdOrCajaOrigenId(Long idCajaDestino, Long idCajaOrigen);

}

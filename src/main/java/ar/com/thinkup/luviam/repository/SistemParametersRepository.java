package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.SistemParameter;

public interface SistemParametersRepository extends JpaRepository<SistemParameter, Integer> {
	
	SistemParameter findByCodigo(String codigo);

}

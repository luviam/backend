package ar.com.thinkup.luviam.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.Producto;
import ar.com.thinkup.luviam.vo.PrecioProductoVO;
import ar.com.thinkup.luviam.vo.indicadores.CubiertoPromedioVO;

public interface ProductoRepository extends JpaRepository<Producto, Long> {

	Producto findByCodigo(String codigo);

	List<ProductoDescriptivo> findByCategoriaCodigo(String codigoCategoria);

	@Query("select p.codigo as codigo, p.nombre as nombre from Producto p")
	List<ProductoDescriptivo> findAllDescriptivo();

	CubiertoPromedioVO getPrecioPromedio(String codigoCentro, String codigoProducto, Date desde, Date hasta);

	public interface ProductoDescriptivo {
		String getCodigo();

		String getNombre();
	}

	@Query("select new  ar.com.thinkup.luviam.vo.PrecioProductoVO(p.id, p.codigo,p.nombre,m) from Producto p left join p.marca m where p.categoria.codigo = ?1 and p.activo=true")
	List<PrecioProductoVO> getConMarcaByCategoria(String codigoCategoria);

}

package ar.com.thinkup.luviam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.documentos.pagos.ItemOrdenPago;
import ar.com.thinkup.luviam.repository.FacturaRepository.FacturaCabecera;
import ar.com.thinkup.luviam.repository.OrdenPagoRepository.OrdenPagoCabecera;

public interface ItemOrdenPagoRepository extends JpaRepository<ItemOrdenPago, Long> {

	public interface ItemOrdenPagoCabecera {
		Long getId();

		OrdenPagoCabecera getOrdenPago();

		FacturaCabecera getFactura();

		Double getImporte();
	}

	public List<ItemOrdenPagoCabecera> findByOrdenPagoId(Long idOrdenPago);

	public List<ItemOrdenPagoCabecera> findByFacturaIdAndOrdenPagoCodigoEstado(Long idFactura, String codigo);

}

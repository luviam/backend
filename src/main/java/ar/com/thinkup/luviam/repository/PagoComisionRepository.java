package ar.com.thinkup.luviam.repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.contratos.PagoComision;
import ar.com.thinkup.luviam.vo.PagoComisionVO;

public interface PagoComisionRepository extends JpaRepository<PagoComision, Long> {

	@Query("select p from PagoComision p where " + " (?1 = -1l or p.configComision.comisionista.id = ?1 ) and "
			+ " (?2 = '-1' or p.configComision.centroCosto.codigo = ?2 ) and "
			+ " (?3 = -1l or p.configComision.servicioComisionado.contrato.id = ?3) and "
			+ " p.codigoTipoPagoComision in ?4 and " + " p.fechaComision between ?5 and ?6 "
			+ " and p.totalLiquidado <> p.totalComision "
	// " o.fecha between ?3 and ?4 group by o.id"
	)
	List<PagoComisionVO> findByFiltros(Long idComisionista, String codigoCentro, Long idContrato,
			List<String> tiposComision, Date fechaDese, Date fechaHasta);

	@Query("select p from PagoComision p where " + " (?1 = -1l or p.configComision.comisionista.id = ?1 ) and "
			+ " (?2 = '-1' or p.configComision.centroCosto.codigo = ?2 ) and "
			+ " (?3 = -1l or p.configComision.servicioComisionado.contrato.id = ?3) and "
			+ " p.codigoTipoPagoComision in ?4 and " + " p.fechaComision between ?5 and ?6 "
			+ " and p.totalLiquidado <> p.totalComision and p.configComision.comisionista.tipoComisionista.codigo in ?7"
	)
	Set<PagoComision> findEntByFiltros(Long idComisionista, String codigoCentro, Long idContrato,
			List<String> tiposComision, Date fechaDese, Date fechaHasta, List<String> tipoComisionstas);

}

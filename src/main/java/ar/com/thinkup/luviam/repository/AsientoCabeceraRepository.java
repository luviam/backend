package ar.com.thinkup.luviam.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.AsientoCabecera;

public interface AsientoCabeceraRepository extends JpaRepository<AsientoCabecera, Long> {

	List<AsientoCabecera> findByFechaBetweenOrderByFechaAsc(Date desde, Date hasta);
}

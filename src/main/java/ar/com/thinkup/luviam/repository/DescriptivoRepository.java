package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import ar.com.thinkup.luviam.model.def.IDescriptivo;

@NoRepositoryBean
public interface DescriptivoRepository<T extends IDescriptivo> extends JpaRepository<T, Long> {
	T findByCodigo(String codigo);

}

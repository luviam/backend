package ar.com.thinkup.luviam.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.plan.SubGrupo;

public interface SubGrupoRepository extends JpaRepository<SubGrupo, Long> {

	SubGrupo findByCodigo(String codigo);

	@Query("select s from SubGrupo s order By s.nombre")
	Set<IDescriptivo> getDescriptivos();

}

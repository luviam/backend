package ar.com.thinkup.luviam.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.plan.Rubro;

public interface RubroRepository extends JpaRepository<Rubro, Long> {

	@Query("select s from Grupo s order By s.nombre")
	Set<IDescriptivo> getDescriptivos();
	
}

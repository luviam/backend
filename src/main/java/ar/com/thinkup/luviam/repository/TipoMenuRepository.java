package ar.com.thinkup.luviam.repository;

import ar.com.thinkup.luviam.model.TipoMenu;

public interface TipoMenuRepository extends ParameterRepository<TipoMenu>{ 

}

package ar.com.thinkup.luviam.repository;

import ar.com.thinkup.luviam.model.MarcaProductos;

public interface MarcaProductosReposirtory extends ParameterRepository<MarcaProductos> {
	
	MarcaProductos findTopByOrderByIdDesc();

}

package ar.com.thinkup.luviam.repository;

import ar.com.thinkup.luviam.model.TipoComisionista;

public interface TipoComisionistaRepository extends ParameterRepository<TipoComisionista> {
}

package ar.com.thinkup.luviam.repository;

import java.util.Date;
import java.util.List;

import ar.com.thinkup.luviam.model.PrecioBebida;

public interface PreciosBebidaCustomRepo {

	List<PrecioBebida> buscarPorFiltros(Date fechaVigencia, List<Long> marcas,
			List<String> productos, List<Long> salones, Boolean activos);
}

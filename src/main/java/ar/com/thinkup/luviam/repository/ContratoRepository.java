package ar.com.thinkup.luviam.repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.contratos.Contrato;

public interface ContratoRepository extends JpaRepository<Contrato, Long> {

	<T> List<T> findByClienteId(Long idCliente);

	@Query(value = " select sum(oc.iva_cobrado) as ivaCobrado ,  c.ip_monto as ivaMaximo "
			+ "from contrato c left join orden_cobranza oc " + "on oc.contrato_id = c.id  " + "where c.id = ?1 "
			+ "group by c.id", nativeQuery = true)
	Set<EstadoIVA> findByIdContratoEstadoIVA(Long idContrato);

	public interface EstadoIVA {
		BigDecimal getIvaCobrado();
		BigDecimal getIvaMaximo();
		
	}

}

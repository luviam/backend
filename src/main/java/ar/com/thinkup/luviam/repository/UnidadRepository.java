package ar.com.thinkup.luviam.repository;

import ar.com.thinkup.luviam.model.UnidadProducto;

public interface UnidadRepository extends ParameterRepository<UnidadProducto> {

}

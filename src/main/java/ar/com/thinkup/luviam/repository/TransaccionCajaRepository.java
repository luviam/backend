package ar.com.thinkup.luviam.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.TransaccionCaja;

public interface TransaccionCajaRepository extends JpaRepository<TransaccionCaja, Long> {

	@Query("select trx from TransaccionCaja trx where trx.tipo = 1 and cajaId = ?1 and date(fechaTransaccion) = date(?2) order by fechaTransaccion desc")
	List<TransaccionCaja> findUltimaApertura(Long cajaId, Date fechaTrx);
	
	@Query("select trx from TransaccionCaja trx where trx.tipo = 2 and cajaId = ?1 and fechaTransaccion < ?2 order by trx.fechaTransaccion desc  ")
	List<TransaccionCaja> findUltimoCierre(Long cajaId, Date fechaPosterior);
	
	
}

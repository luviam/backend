package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.TipoCliente;


public interface TipoClienteRepository extends JpaRepository<TipoCliente, Long> {

	public TipoCliente findByCodigo(String codigo);
	public TipoCliente findByDescripcion(String descripcion);
}

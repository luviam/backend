package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.Chequera;

public interface ChequeraRepository extends JpaRepository<Chequera, Long>{

	Long countByCuentaBancariaId(Long id);
	
}

package ar.com.thinkup.luviam.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.contratos.CuotaServicio;

public interface CuotaServicioRepository extends JpaRepository<CuotaServicio, Long> {

	List<CuotaServicio> findByServicioContratoId(Long servicioContratadoId);

	@Query("select c from CuotaServicio c where c.servicio.contrato.id = ?1 and c.saldo <> 0 order by c.fechaVencimiento, c.id ASC")
	List<CuotaServicio> getCuotasImpagas(Long idContrato);

	@Query(value = "select COALESCE(sum(ic.importe),0) from item_orden_cobranza ic "
			+ " join orden_cobranza oc on oc.id = ic.orden_cobranza_id "
			+ " where ic.cuota_servicio_id = ?1 and oc.codigo_estado = 'A'", nativeQuery = true)
	BigDecimal getPagos(Long id);

}

package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.TipoJornada;

public interface TipoJornadaRepository extends JpaRepository<TipoJornada, Long> {

	public TipoJornada findByCodigo(String codigo);
	public TipoJornada findByDescripcion(String descripcion);
}

package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.ProveedorConSaldo;

public interface ProveedorConSaldoRepository extends JpaRepository<ProveedorConSaldo, Long> {

}

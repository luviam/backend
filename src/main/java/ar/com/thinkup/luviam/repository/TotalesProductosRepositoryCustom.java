package ar.com.thinkup.luviam.repository;

import java.util.Date;
import java.util.List;

import ar.com.thinkup.luviam.model.reportes.TotalesProductos;

public interface TotalesProductosRepositoryCustom {

	
	List<TotalesProductos> totalesPorProducto( Date desde, Date hasta,
			String centro, String cProducto, String cCategoria);

}

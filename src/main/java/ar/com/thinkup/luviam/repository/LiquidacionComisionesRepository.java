package ar.com.thinkup.luviam.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.documentos.comisiones.LiquidacionComisiones;
import ar.com.thinkup.luviam.repository.CentroRepository.CentroDescriptivo;

public interface LiquidacionComisionesRepository extends DocumentoRepository<LiquidacionComisiones> {

	public interface LiquidacionComisionesCabecera {
		Long getId();

		Date getFecha();

		String getNumero();

		Double getImporte();

		String getDescripcion();

		String getCodigoEstado();

		CentroDescriptivo getCentroCosto();

	}

	@Query("select l from LiquidacionComisiones l where " + " (?1 = -1l or l.comisionista.id = ?1 ) and "
			+ " (?2 = '-1' or l.centroCosto.codigo = ?2 ) and ?3 = ?3 and " + " l.fecha between ?4 and ?5 ")
	public List<LiquidacionComisiones> findByFiltros(Long idComisionista, String codigoCentro, Long idContrato,
			Date fechaDesde, Date fechaHasta);

	// @Query("Select o from OrdenCobranza o where " + " (?1 = -1l or
	// o.contrato.cliente.id = ?1) and "
	// + " (?2 = -1l or o.contrato.salon.id = ?2) and " + " (?3 = '-1' or
	// o.centroCosto.codigo = ?3) and "
	// + " (?4 = '-1' or o.contrato.codigoHorario = ?4) and " + " o.fecha between ?5
	// and ?6")
	// List<OrdenCobranzaCabecera> getCabeceras(Long idCliente, Long idSalon, String
	// codigoCentro, String codigoHorario,
	// Date fechaDesde, Date fechaHasta);
	//
	// List<OrdenCobranza> findByContratoIdAndCodigoEstado(Long idContrato, String
	// codigo);

}
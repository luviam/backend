package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.CentroCostosTipoComprobante;

public interface TipoComprobanteCentroRepository extends JpaRepository<CentroCostosTipoComprobante, Long> {

	
}

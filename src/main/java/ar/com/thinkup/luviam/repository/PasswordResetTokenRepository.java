package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.security.PasswordResetToken;

public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken, Long>{

	PasswordResetToken findOneByEmail(String email);
	
	PasswordResetToken findOneByToken(String token);
	
	PasswordResetToken findOneByTokenAndEmail(String token, String email);
	
}

package ar.com.thinkup.luviam.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.contratos.ContratoCabecera;

public interface ContratoCabeceraRepository extends JpaRepository<ContratoCabecera, Long> {

	List<ContratoCabecera> findByIdCliente(Long idCliente);

	@Query(value =  
			"select distinct cct.* from contratos_cabecera_totales cct " + 
			"	join contrato c on c.id = cct.id " + 
			"	join comision_contrato cc on cc.contrato_id = c.id " + 
			" where cc.comisionista_id = ?1 and c.asiento_id is not null " + 
			"", nativeQuery = true)
	Set<ContratoCabecera> findByIdComisionista(Long idComisionista);

}

package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.TipoProveedor;

public interface TipoProveedorRepository extends JpaRepository<TipoProveedor, Long> {

	public TipoProveedor findByCodigo(String codigo);
	public TipoProveedor findByDescripcion(String descripcion);
}

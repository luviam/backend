package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.CabeceraComisionista;

public interface CabeceraComisionistaRepository extends JpaRepository<CabeceraComisionista, Long> {

}

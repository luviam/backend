package ar.com.thinkup.luviam.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.documentos.pagos.OrdenPago;
import ar.com.thinkup.luviam.repository.DocumentoRepository.DocumentoCabecera;
import ar.com.thinkup.luviam.repository.ProveedorRepository.ProveedorDescriptivo;
import ar.com.thinkup.luviam.repository.UsuarioRepository.UsuarioDescriptivo;


public interface OrdenPagoRepository extends DocumentoRepository<OrdenPago> {

	List<OrdenPago> findByCentroCostoCodigoAndProveedorIdAndFechaBetweenOrderByFechaDesc(String codigoCentro,
			Long idProveedor, Date fechaDesde, Date fechaHasta);

	@Query("Select o from OrdenPago o where " + " (?1 ='-1' or o.centroCosto.codigo = ?1 ) and "
			+ " (?2 = -1l or o.proveedor.id = ?2) and " + " o.fecha between ?3 and ?4 group by o.id")
	List<OrdenPagoCabecera> getCabeceras(String codigoCentro, Long idProveedor, Date fechaDesde, Date fechaHasta);
	
	public interface OrdenPagoCabecera extends DocumentoCabecera {

		UsuarioDescriptivo getResponsable();

		ProveedorDescriptivo getProveedor();

	}
}

package ar.com.thinkup.luviam.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.documentos.DocumentoPago;
import ar.com.thinkup.luviam.repository.CentroRepository.CentroDescriptivo;

public interface DocumentoRepository<T extends DocumentoPago<?>> extends JpaRepository<T, Long> {

	public interface DocumentoCabecera {
		Long getId();

		Date getFecha();

		String getNumero();

		Double getImporte();

		String getDescripcion();

		String getCodigoEstado();

		CentroDescriptivo getCentroCosto();
	}

}

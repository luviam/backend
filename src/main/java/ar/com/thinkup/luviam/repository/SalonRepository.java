package ar.com.thinkup.luviam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.Salon;

public interface SalonRepository extends JpaRepository<Salon, Long> {

	public List<SalonDescriptivo> findByActivoIsTrue();

	public interface SalonDescriptivo{
		Long getId();
		String getNombre();
	}

}

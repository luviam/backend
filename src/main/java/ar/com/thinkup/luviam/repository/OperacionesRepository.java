package ar.com.thinkup.luviam.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.Operacion;
import ar.com.thinkup.luviam.vo.BalanceCajaVO;
import ar.com.thinkup.luviam.vo.TotalesGastosVO;
import ar.com.thinkup.luviam.vo.indicadores.TotalGastoVO;

public interface OperacionesRepository extends JpaRepository<Operacion, Long> {

	@Query("select o from Operacion o where (o.centro.id = ?1 or -1 = ?1) and "
			+ "	o.asiento.fecha between ?2 and ?3 and o.cuenta.id = ?4 and "
			+ "	o.asiento.estado = ?5 order by o.asiento.fecha asc , o.asiento.numero asc")
	List<Operacion> getOperaciones(Long centro, Date desde, Date hasta, Long idCuenta, String codigoEstado);

	@Query("select new ar.com.thinkup.luviam.vo.BalanceCajaVO(sum(ope.debe), sum(ope.haber)) from Operacion ope join ope.asiento asi where ope.cuenta.id = ?1 and asi.fecha > ?2")
	BalanceCajaVO findBalanceByCuentaId(Long cuentaId, Date fechaApertura);
	@Query(nativeQuery =true, value = "select sum(o.debe - o.haber) from operacion o  " + 
			"join asiento a on a.id = o.asiento_id " + 
			"where o.cuenta_id = ?1 and a.fecha < ?2  and a.codigo_estado = 'A' group by o.cuenta_id" )
	Double getSaldo( Long cuenta, Date hasta);
	Long countByCuentaId(Long id);

	/*
	 * @Query(value =
	 * "select r.id as idRubro, r.nombre as rubro, g.id as idGrupo, g.nombre as grupo, sg.id as idSubGrupo, "
	 * + "	sg.nombre as subGrupo , ca.id as idCuenta, ca.nombre as cuenta, " +
	 * "	sum(o.debe) as debe, sum(o.haber) as haber" +
	 * " from cuenta_aplicacion ca " + " join operacion o on ca.id = o.cuenta_id " +
	 * " join centro c on c.id = o.centro_id" +
	 * " join asiento a on a.id = o.asiento_id" +
	 * " join sub_grupo sg on sg.id = ca.sub_grupo_id" +
	 * " join grupo g on g.id = sg.grupo_id" + " join rubro r on r.id = g.rubro_id"
	 * + " where r.codigo = 'EG' " + " and ( ?1 = '-1' or c.codigo = ?1)" +
	 * " and ( ?2 = '-1' or g.codigo = ?2 )" +
	 * " and ( ?3 = '-1' or sg.codigo = ?3 )" + " and a.fecha between ?4 and ?5" +
	 * " group by r.nombre, g.nombre, sg.nombre, ca.nombre" +
	 * " order by rubro asc, grupo asc, subgrupo asc, cuenta asc", nativeQuery =
	 * true)
	 */
	List<TotalesGastosVO> getTotalesPorGastos(String codigoCentro, String codigoGrupo, String codigoSubGrupo,
			Date desde, Date hasta);

	List<TotalGastoVO> getTotalSubgrupo(String codigoCentro, String codigoSubGrupo, Date desde, Date hasta);
}

package ar.com.thinkup.luviam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.Cliente;
import ar.com.thinkup.luviam.model.Contacto;
import ar.com.thinkup.luviam.model.TipoCliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {

	List<ClienteDescriptivo> findByActivoIsTrue();

	List<ClienteCabecera> findByActivoIsTrueOrderByIdDesc();

	public interface ClienteCabecera {
		Long getId();

		String getNombreCliente();

		String getNombreContacto();

		String getTelefono();

		String getCelular();

		String getEmail();

		String getRazonSocial();

		String getCuit();

		TipoCliente getTipoCliente();

		Boolean getActivo();

		String getFacebook();

		String getInstagram();

		List<Contacto> getContactos();
	}

	public interface ClienteDescriptivo {
		Long getId();

		String getNombreCliente();
	}

}

package ar.com.thinkup.luviam.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.contratos.Contrato;
import ar.com.thinkup.luviam.model.documentos.cobros.OrdenCobranza;
import ar.com.thinkup.luviam.repository.DocumentoRepository.DocumentoCabecera;
import ar.com.thinkup.luviam.repository.UsuarioRepository.UsuarioDescriptivo;

public interface OrdenCobranzaRepository extends DocumentoRepository<OrdenCobranza> {

	public interface OrdenCobranzaCabecera extends DocumentoCabecera {

		UsuarioDescriptivo getResponsable();

		Contrato getContrato();

		Double getIvaCobrado();

		Double getImporteNeto();

	}

	@Query("Select o from OrdenCobranza o where " + " (?1 = -1l or o.contrato.cliente.id = ?1) and "
			+ " (?2 = -1l or o.contrato.salon.id = ?2) and " + " (?3 = '-1' or o.centroCosto.codigo = ?3) and "
			+ " (?4 = '-1' or o.contrato.codigoHorario = ?4) and " + " o.fecha between ?5 and ?6")
	List<OrdenCobranzaCabecera> getCabeceras(Long idCliente, Long idSalon, String codigoCentro, String codigoHorario,
			Date fechaDesde, Date fechaHasta);

	List<OrdenCobranza> findByContratoIdAndCodigoEstado(Long idContrato, String codigo);

	Set<OrdenCobranzaCabecera> findByContratoId(Long idContrato);

}

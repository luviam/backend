package ar.com.thinkup.luviam.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Component;

import ar.com.thinkup.luviam.model.reportes.TotalesProductos;
@Component
public class TotalesProductoRepoImpl implements TotalesProductosRepositoryCustom  {


@PersistenceContext
EntityManager entityManager;

	
	@Override
	public List<TotalesProductos> totalesPorProducto(Date desde, Date hasta, String centro, String cProducto, String cCategoria) {
		Query query = entityManager.createNamedStoredProcedureQuery("totalesPorProducto");
		query.setParameter("$desde", desde);
	    query.setParameter("$hasta", hasta);
	    query.setParameter("$centro", centro);
	    query.setParameter("$codigo_producto", cProducto);
	    query.setParameter("$codigo_categoria", cCategoria);
	    return query.getResultList();
		
		
		
	}

}

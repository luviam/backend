package ar.com.thinkup.luviam.repository;

import ar.com.thinkup.luviam.model.TipoIVA;

public interface TipoIVARepository extends ParameterRepository<TipoIVA> {

	public TipoIVA findByDescripcion(String descripcion);
}

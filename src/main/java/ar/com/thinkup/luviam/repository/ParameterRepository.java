package ar.com.thinkup.luviam.repository;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import ar.com.thinkup.luviam.model.Parametrico;

@NoRepositoryBean
public interface ParameterRepository<T extends Parametrico> extends DescriptivoRepository<T> {

	List<T> findByHabilitadoIsTrue();
	


}

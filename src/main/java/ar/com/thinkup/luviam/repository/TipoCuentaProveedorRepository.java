package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.TipoCuentaProveedor;

public interface TipoCuentaProveedorRepository extends JpaRepository<TipoCuentaProveedor, Long> {

	public TipoCuentaProveedor findByCodigo(String codigo);
	public TipoCuentaProveedor findByDescripcion(String descripcion);
}

package ar.com.thinkup.luviam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.TipoComprobante;

public interface TipoComprobanteRepository extends JpaRepository<TipoComprobante, Long> {

	TipoComprobante findByCodigo(String codigo);
	TipoComprobante findByDescripcion(String descripcion);
	List<TipoComprobante> findByEsFacturableIsTrue();
	
	
}

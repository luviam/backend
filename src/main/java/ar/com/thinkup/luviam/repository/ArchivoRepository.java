package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.Archivo;

public interface ArchivoRepository extends JpaRepository<Archivo, Long>{

}

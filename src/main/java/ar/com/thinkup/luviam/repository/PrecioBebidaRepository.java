package ar.com.thinkup.luviam.repository;

import ar.com.thinkup.luviam.model.PrecioBebida;

public interface PrecioBebidaRepository extends PrecioRepository<PrecioBebida>, PreciosBebidaCustomRepo {
	
}

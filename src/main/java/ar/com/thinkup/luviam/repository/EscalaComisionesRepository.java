package ar.com.thinkup.luviam.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.EscalaComisiones;

public interface EscalaComisionesRepository extends JpaRepository<EscalaComisiones, Long> {

	Set<EscalaComisiones> findByCentroCostoCodigo(String centro);

	@Query(value = "select e from EscalaComisiones e where e.centroCosto.id = ?1 and e.desde <= ?2 and e.hasta >= ?2")
	Set<EscalaComisiones> getAplicable(Long id, Integer cantidad);

}

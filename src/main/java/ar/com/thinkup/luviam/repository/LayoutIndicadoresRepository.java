package ar.com.thinkup.luviam.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.comisionistas.LayoutIndicadores;

public interface LayoutIndicadoresRepository extends JpaRepository<LayoutIndicadores, Long> {

	Set<LayoutIndicadores> findByUsuarioId(Long id);

}

package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.UltimasTransaccionesCaja;

public interface UltimasTransaccionesCajaRepository extends JpaRepository<UltimasTransaccionesCaja, Long> {

	
	
}

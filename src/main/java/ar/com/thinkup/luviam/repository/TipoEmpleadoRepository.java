package ar.com.thinkup.luviam.repository;

import ar.com.thinkup.luviam.model.TipoEmpleado;


public interface TipoEmpleadoRepository extends ParameterRepository<TipoEmpleado> {

	public TipoEmpleado findByDescripcion(String descripcion);
}

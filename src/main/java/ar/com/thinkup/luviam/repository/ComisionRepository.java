package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.contratos.ConfiguracionComision;

public interface ComisionRepository extends JpaRepository<ConfiguracionComision, Long>{ 

}

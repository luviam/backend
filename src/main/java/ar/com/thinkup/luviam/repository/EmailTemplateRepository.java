package ar.com.thinkup.luviam.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.EmailTemplate;

public interface EmailTemplateRepository extends JpaRepository<EmailTemplate, String> {

	EmailTemplate findOneByCodigo(String codigo);
	
}

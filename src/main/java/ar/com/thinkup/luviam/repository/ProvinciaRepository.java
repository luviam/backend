package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.Provincia;

public interface ProvinciaRepository extends JpaRepository<Provincia, Long> {	

}

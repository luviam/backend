package ar.com.thinkup.luviam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;

public interface CuentaAplicacionRepository extends JpaRepository<CuentaAplicacion, Long> {

	List<CuentaAplicacion> findBySubGrupoCodigoAndCentroCostoActivoTrueOrderByCodigoAsc(String codigoSubgrupo);

	List<CuentaAplicacion> findBySubGrupoCodigoInAndCentroCostoIdAndCentroCostoActivoTrueOrderByCodigoAsc(List<String> codigosSubgrupo,
			Long idCentro);

	List<CuentaAplicacion> findBySubGrupoCodigoInAndCentroCostoActivoTrueOrderByCodigoAsc(List<String> codigosSubgrupo);

	Long countBySubGrupoCodigoAndCentroCostoActivoTrue(String codigoSubgrupo);

	CuentaAplicacion findByCodigoAndCentroCostoCodigoAndCentroCostoActivoTrue(String codigo, String centro);

	List<CuentaAplicacion> findByCodigoAndCentroCostoActivoTrue(String codigo);
	
	CuentaAplicacion findOneByCentroCostoIdAndCentroCostoActivoTrueAndCodigo(Long idCentro, String codigoCuenta);

	List<CuentaAplicacion> findBySubGrupoGrupoRubroCodigoAndCentroCostoIdAndCentroCostoActivoTrueOrderByCodigoAsc(
			String codigo, Long idCentro);

	List<CuentaAplicacion> findBySubGrupoGrupoRubroCodigoAndCentroCostoActivoTrueOrderByCodigoAsc(String codigo);

	Long countByCentroCostoIdAndCodigoAndIdNot(Long idCentroCosto, String codigo, Long id);

	Long countByCentroCostoIdAndCodigo(Long idCentroCosto, String codigo);
}

package ar.com.thinkup.luviam.repository;

import org.springframework.data.repository.CrudRepository;

import ar.com.thinkup.luviam.model.reportes.TotalesProductos;

public interface TotalesProductoRepo
		extends CrudRepository<TotalesProductos, Long>, TotalesProductosRepositoryCustom {

}

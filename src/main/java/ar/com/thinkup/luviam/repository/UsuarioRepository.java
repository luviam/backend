package ar.com.thinkup.luviam.repository;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.security.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	Usuario findByUsernameAndActivoIsTrue(String username);

	Usuario findByUsername(String username);

	public interface UsuarioDescriptivo {
		String getUsername();

		@Value("#{target.nombre+ ' ' + target.apellido}")
		String getFullName();
	}
}

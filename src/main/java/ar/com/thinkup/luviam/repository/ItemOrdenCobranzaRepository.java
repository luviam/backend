package ar.com.thinkup.luviam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.contratos.CuotaServicio;
import ar.com.thinkup.luviam.model.documentos.cobros.ItemOrdenCobranza;
import ar.com.thinkup.luviam.repository.OrdenCobranzaRepository.OrdenCobranzaCabecera;

public interface ItemOrdenCobranzaRepository extends JpaRepository<ItemOrdenCobranza, Long> {

	public interface ItemOrdenCobranzaCabecera {
		Long getId();

		OrdenCobranzaCabecera getOrdenCobranza();

		CuotaServicio getCuotaServicio();

		Double getImporte();
	}

	public List<ItemOrdenCobranzaCabecera> findByOrdenCobranzaId(Long idOrdenCobranza);
}

package ar.com.thinkup.luviam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.CentroCosto;

public interface CentroRepository extends JpaRepository<CentroCosto, Long> {

	CentroCosto findOneByCodigo(String codigo);

	Long countByCodigo(String codigoCentro);

	Long countByCodigoAndIdNot(String codigoCentro, Long id);

	<T> List<T> findByActivoIsTrue(Class<T> type);

	@Query("select c.codigo as codigo, c.nombre as nombre from CentroCosto c")
	List<CentrocCostoDescriptivo> findAllDescriptivo();

	public interface CentroDescriptivo {
		Long getId();

		String getCodigo();

		String getNombre();
	}

	public interface CentrocCostoDescriptivo {
		Long getId();

		String getCodigo();

		String getNombre();
	}

}

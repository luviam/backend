package ar.com.thinkup.luviam.repository;

import java.util.List;

import ar.com.thinkup.luviam.model.CategoriaProducto;

public interface CategoriaProductoRepository extends ParameterRepository<CategoriaProducto> {
	
	List<CategoriaProducto> findByEsComisionableIsTrue();
}

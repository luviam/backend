package ar.com.thinkup.luviam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.CentroSeleccion;

public interface CentroSeleccionRepository extends JpaRepository<CentroSeleccion, Long> {

}

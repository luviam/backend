package ar.com.thinkup.luviam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.thinkup.luviam.model.Proveedor;
import ar.com.thinkup.luviam.model.TipoCuentaProveedor;
import ar.com.thinkup.luviam.model.TipoProveedor;

public interface ProveedorRepository extends JpaRepository<Proveedor, Long> {

	Proveedor findByCuit(String cuit);

	Float getSaldo(Long idProveedor);

	<T> List<T> findByActivoIsTrue(Class<T> type);

	public interface ProveedorDescriptivo {
		Long getId();

		String getNombreProveedor();
	}

	public interface ProveedorCabecera {
		public Long getId();

		public String getNombreProveedor();

		public TipoProveedor getTipoProveedor();

		public TipoCuentaProveedor getTipoCuenta();

		public String getNombreContacto();

		public String getTelefono();

		public String getCelular();

		public String getEmail();

	}
}

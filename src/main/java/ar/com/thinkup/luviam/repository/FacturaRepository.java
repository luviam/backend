package ar.com.thinkup.luviam.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.TipoComprobante;
import ar.com.thinkup.luviam.model.documentos.factura.Factura;
import ar.com.thinkup.luviam.repository.CentroRepository.CentroDescriptivo;
import ar.com.thinkup.luviam.repository.ProveedorRepository.ProveedorDescriptivo;

public interface FacturaRepository extends JpaRepository<Factura, Long> {

	Factura findByTipoComprobanteCodigoAndNumeroAndProveedorId(String tipoComprobante, String numero, Long idProveedor);

	@Query("select f from Factura f where f.centroCosto.codigo = ?1 and f.proveedor.id = ?2 and f.estado = ?3 and f.saldo <> ?4")
	List<FacturaCabecera> getCabecerasPorProveedorEstadoSaldo(String codigoCentro, Long idProveedor, String estado,
			BigDecimal saldo);

	@Query("select f from Factura f where (?1 = -1l or f.proveedor.id = ?1) "
			+ " and (?2 = '-1' or f.centroCosto.codigo = ?2 ) "
			+ "	and f.tipoComprobante.codigo in ?3 and f.estado in ?4 and f.fecha between ?5 and ?6")
	List<FacturaCabecera> getCabeceras(Long idProveedor, String codigoCentro, List<String> tiposComprobante,
			List<String> estados, Date fechaDesde, Date fechaHasta);

	public interface FacturaCabecera {
		Long getId();

		String getNumero();

		String getDescripcion();

		Date getFecha();

		TipoComprobante getTipoComprobante();

		ProveedorDescriptivo getProveedor();

		CentroDescriptivo getCentroCosto();

		Double getSaldo();

		Double getImporte();

		String getEstado();
	}

}

package ar.com.thinkup.luviam.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.com.thinkup.luviam.model.Sector;
import ar.com.thinkup.luviam.model.TipoEmpleado;
import ar.com.thinkup.luviam.model.empleados.AltaEmpleado;
import ar.com.thinkup.luviam.model.empleados.Empleado;

public interface EmpleadoRepository extends JpaRepository<Empleado, Long> {
	@Query("select e from Empleado e where e.activo = 1 and ( ?1 = 'ALL' or e.tipoEmpleado.codigo = ?1 ) order by nombre asc")
	List<Empleado> findByTipoEmpleado(String codigo);

	@Query("select e from Empleado e where e.activo = 1 order by nombre asc")
	List<IEmpleadoCabecera> getCabeceras();

	@Query("select e from Empleado e where e.activo = 1 and ( ?1 = 'ALL' or e.tipoEmpleado.codigo = ?1 ) order by nombre asc")
	List<IEmpleadoCabecera> getCabecerasByTipo(String codigo);

	public interface IEmpleadoCabecera {
		Long getId();

		String getNombre();

		String getTelefono();

		String getEmail();

		TipoEmpleado getTipoEmpleado();

		Sector getSector();

		Set<AltaEmpleado> getAltas();

	}

}

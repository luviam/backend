package ar.com.thinkup.luviam.repository;

import ar.com.thinkup.luviam.model.productoProveedor.TipoProducto;

public interface ITipoProductoRepository extends ParameterRepository<TipoProducto> {

}

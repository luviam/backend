package ar.com.thinkup.luviam.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ar.com.thinkup.luviam.model.PrecioBebida;


@Repository
@Transactional(readOnly = true)
public class PrecioBebidaRepositoryImpl implements PreciosBebidaCustomRepo{

	@PersistenceContext
	EntityManager em;

	@Override
	public List<PrecioBebida> buscarPorFiltros(Date fechaVigencia, List<Long> marcas, List<String> productos, List<Long> salones, Boolean activos) {
		String sql = "select p  from PrecioBebida p where 1 = 1 " ;
		sql += fechaVigencia != null? " and :fecha >= p.fechaDesde and (p.fechaHasta is null or :fecha <= p.fechaHasta)" : "";
		sql += marcas.size() > 0? " and (p.producto.marca is null or p.producto.marca.id in (:marcas)) " : "";
		sql += productos.size() > 0 ? " and (p.producto.codigo in (:productos)) " : "" ;
		sql +=salones.size() > 0 ? " and (p.salon is null or p.salon.id in (:salones)) " : "";
		sql += activos? " and p.activo = true" : "";
		TypedQuery<PrecioBebida> q = em.createQuery(sql,PrecioBebida.class);
		if(fechaVigencia != null ) q.setParameter("fecha", fechaVigencia);
		if(marcas.size()>0) q.setParameter("marcas", marcas);
		if(productos.size()>0) q.setParameter("productos", productos);
		if(salones.size()>0) q.setParameter("salones", salones);
		return q.getResultList();
	}
	

}
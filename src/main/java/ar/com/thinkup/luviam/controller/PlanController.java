package ar.com.thinkup.luviam.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.exceptions.CuentaNoEliminableException;
import ar.com.thinkup.luviam.exceptions.GrupoNoExisteException;
import ar.com.thinkup.luviam.exceptions.RubroNoExistenteException;
import ar.com.thinkup.luviam.exceptions.SubGrupoNoExistente;
import ar.com.thinkup.luviam.model.enums.TipoCuentaEnum;
import ar.com.thinkup.luviam.model.plan.Grupo;
import ar.com.thinkup.luviam.model.plan.Rubro;
import ar.com.thinkup.luviam.model.plan.SubGrupo;
import ar.com.thinkup.luviam.repository.CuentaAplicacionRepository;
import ar.com.thinkup.luviam.repository.GrupoRepository;
import ar.com.thinkup.luviam.repository.RubroRepository;
import ar.com.thinkup.luviam.repository.SubGrupoRepository;
import ar.com.thinkup.luviam.vo.CuentaPlanVO;
import ar.com.thinkup.luviam.vo.GrupoVO;
import ar.com.thinkup.luviam.vo.PlanDeCuentasVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/plan")
@PreAuthorize("isAuthenticated()")
public class PlanController {

	private static final Logger LOG = LogManager.getLogger(PlanController.class);

	@Autowired
	private RubroRepository rubroRepo;
	@Autowired
	private GrupoRepository grupoRepo;
	@Autowired
	private SubGrupoRepository subGrupoRepo;
	@Autowired
	private CuentaAplicacionRepository cuentaAplicacionRepo;

	@GetMapping("/grupos/{rubro}")
	public ResponseEntity<ResponseContainer<List<GrupoVO>>> getGruposByRubro(@PathVariable("rubro") String rubro) {
		ResponseContainer<List<GrupoVO>> respuesta = new ResponseContainer<>();
		try {
			Set<Grupo> grupos = this.grupoRepo.findByRubroCodigo(rubro);

			respuesta.setRespuesta(grupos.stream().map(r -> new GrupoVO(r)).collect(Collectors.toList()));
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			LOG.error(e.getMessage(), e);
			respuesta.setMensaje(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando los grupos", e);
			respuesta.setMensaje("Hubo un error buscando los grupos");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/plan-cuentas")
	public ResponseEntity<ResponseContainer<List<CuentaPlanVO>>> getPlanTemplate() {
		ResponseContainer<List<CuentaPlanVO>> respuesta = new ResponseContainer<>();
		try {
			List<Rubro> rubros = this.rubroRepo.findAll();

			respuesta.setRespuesta(rubros.stream().map(r -> new CuentaPlanVO(r)).collect(Collectors.toList()));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando el plan template", e);
			respuesta.setMensaje("Hubo un error buscando el plan template.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping("/guardar")
	public ResponseEntity<ResponseContainer<Boolean>> guardarPlan(@RequestBody PlanDeCuentasVO planCuenta) {

		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();
		List<Rubro> rubros = this.rubroRepo.findAll();

		try {

			planCuenta.getCuentas().stream().forEach(cuentaRubro -> {
				Rubro rubro;
				if (cuentaRubro.getId() == null) {
					rubro = new Rubro(cuentaRubro);
					rubros.add(rubro);
				} else {
					Optional<Rubro> or = rubros.parallelStream().filter(rr -> rr.getId().equals(cuentaRubro.getId()))
							.findFirst();
					if (or.isPresent()) {
						rubro = or.get();
						rubro.setCodigo(cuentaRubro.getCodigo());
						rubro.setActivo(cuentaRubro.getActivo());
						rubro.setNombre(cuentaRubro.getDescripcion());
						cuentaRubro.getCuentasHijas().stream().forEach(cuentaGrupoVO -> {
							Grupo grupo;
							if (cuentaGrupoVO.getId() == null) {
								grupo = new Grupo(cuentaGrupoVO, rubro);
								rubro.getGrupos().add(grupo);
							} else {
								Optional<Grupo> optionalGrupo = rubro.getGrupos().parallelStream()
										.filter(rr -> rr.getId().equals(cuentaGrupoVO.getId())).findFirst();
								if (optionalGrupo.isPresent()) {
									grupo = optionalGrupo.get();
									grupo.setCodigo(cuentaGrupoVO.getCodigo());
									grupo.setActivo(cuentaGrupoVO.getActivo());
									grupo.setNombre(cuentaGrupoVO.getDescripcion());
									cuentaGrupoVO.getCuentasHijas().stream().forEach(cuentaSubGrupo -> {
										SubGrupo subGrupo;
										if (cuentaSubGrupo.getId() == null) {
											subGrupo = new SubGrupo(cuentaSubGrupo, grupo);
											grupo.getSubGrupos().add(subGrupo);
										} else {
											Optional<SubGrupo> osg = grupo.getSubGrupos().parallelStream()
													.filter(rr -> rr.getId().equals(cuentaSubGrupo.getId()))
													.findFirst();
											if (osg.isPresent()) {
												subGrupo = osg.get();
												subGrupo.setCodigo(cuentaSubGrupo.getCodigo());
												subGrupo.setActivo(cuentaSubGrupo.getActivo());
												subGrupo.setNombre(cuentaSubGrupo.getDescripcion());
											} else {
												LOG.error("SubGrupo con ID no enctontrado", cuentaSubGrupo.getId());
												throw new SubGrupoNoExistente(cuentaSubGrupo.getId());
											}
										}
									});
								} else {
									LOG.error("Grupo con ID no enctontrado", cuentaGrupoVO.getId());
									throw new GrupoNoExisteException(cuentaGrupoVO.getId());
								}

							}
						});
					} else {
						LOG.error("Rubro con ID no enctontrado", cuentaRubro.getId());
						throw new RubroNoExistenteException(cuentaRubro.getId());
					}
				}

			});
			this.rubroRepo.save(rubros);
			LOG.info("Se guardó exitosamente el Plan de Cuentas");
			respuesta.setRespuesta(true);
			respuesta.setMensaje("Se guardó exitosamente el Plan de Cuentas");
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error guardando el Plan de Cuentas", e);
			respuesta.setMensaje("Hubo un error guardando el Plan de Cuentas");
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@DeleteMapping("/{tipo}/{id}")
	@Transactional
	public ResponseEntity<ResponseContainer<String>> delete(@PathVariable("tipo") String tipo,
			@PathVariable("id") Long id) {
		ResponseContainer<String> respuesta = new ResponseContainer<>();
		try {

			switch (TipoCuentaEnum.byCodigo(tipo)) {
			case GRUPO:

				Grupo grupo = this.grupoRepo.findOne(id);
				if (grupo == null)
					throw new EntityNotFoundException();

				if (!CollectionUtils.isEmpty(grupo.getSubGrupos())) {
					throw new CuentaNoEliminableException();
				}

				grupo.setDeleted(true);
				this.grupoRepo.save(grupo);
				break;
			case RUBRO:
				Rubro rubro = this.rubroRepo.findOne(id);
				if (rubro == null)
					throw new EntityNotFoundException();
				if (!CollectionUtils.isEmpty(rubro.getGrupos())) {
					throw new CuentaNoEliminableException();
				}
				rubro.setDeleted(true);
				this.rubroRepo.save(rubro);
				break;
			case SUB_GRUPO:
				SubGrupo subGrupo = subGrupoRepo.findOne(id);
				if (subGrupo == null)
					throw new EntityNotFoundException();

				Long cuentas = cuentaAplicacionRepo.countBySubGrupoCodigoAndCentroCostoActivoTrue(subGrupo.getCodigo());
				if (cuentas > 0) {
					throw new CuentaNoEliminableException();
				}
				subGrupo.setDeleted(true);
				this.subGrupoRepo.save(subGrupo);
				break;
			default:
				break;

			}

			respuesta.setRespuesta("Se ha eliminado la cuenta");
			return ResponseEntity.ok().body(respuesta);
		} catch (EntityNotFoundException e) {
			respuesta.setMensaje("La cuenta especificada no existe.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (CuentaNoEliminableException e) {
			respuesta.setMensaje("No se puede eliminar una cuenta con cuentas hijas.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error eliminando la cuenta", e);
			respuesta.setMensaje("Hubo un error buscando el plan template.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

}

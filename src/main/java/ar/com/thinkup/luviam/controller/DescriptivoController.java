package ar.com.thinkup.luviam.controller;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.contratos.ServicioContratado;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.enums.CuentasPrincipalesEnum;
import ar.com.thinkup.luviam.model.enums.TipoComision;
import ar.com.thinkup.luviam.repository.CategoriaProductoRepository;
import ar.com.thinkup.luviam.repository.CentroRepository;
import ar.com.thinkup.luviam.repository.ClienteRepository;
import ar.com.thinkup.luviam.repository.ComisionistaRepository;
import ar.com.thinkup.luviam.repository.ComisionistaRepository.IComisionistaDescriptivo;
import ar.com.thinkup.luviam.repository.CuentaAplicacionRepository;
import ar.com.thinkup.luviam.repository.GrupoRepository;
import ar.com.thinkup.luviam.repository.ProductoRepository;
import ar.com.thinkup.luviam.repository.ProveedorRepository;
import ar.com.thinkup.luviam.repository.ProveedorRepository.ProveedorDescriptivo;
import ar.com.thinkup.luviam.repository.ProvinciaRepository;
import ar.com.thinkup.luviam.repository.RubroRepository;
import ar.com.thinkup.luviam.repository.SalonRepository;
import ar.com.thinkup.luviam.repository.SectoresRepository;
import ar.com.thinkup.luviam.repository.SubGrupoRepository;
import ar.com.thinkup.luviam.repository.TipoClienteRepository;
import ar.com.thinkup.luviam.repository.TipoComprobanteRepository;
import ar.com.thinkup.luviam.repository.TipoCuentaProveedorRepository;
import ar.com.thinkup.luviam.repository.TipoEmpleadoRepository;
import ar.com.thinkup.luviam.repository.TipoEventoRepository;
import ar.com.thinkup.luviam.repository.TipoIVARepository;
import ar.com.thinkup.luviam.repository.TipoJornadaRepository;
import ar.com.thinkup.luviam.repository.TipoProveedorRepository;
import ar.com.thinkup.luviam.repository.UnidadRepository;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/descriptivos")
@PreAuthorize("isAuthenticated()")
public class DescriptivoController {

	@Autowired
	private TipoComprobanteRepository tipoComprobanteRepo;

	@Autowired
	private ProvinciaRepository provinciasRepo;

	@Autowired
	private ProductoRepository productoRepo;

	@Autowired
	private RubroRepository rubroRepo;

	@Autowired
	private GrupoRepository grupoRepo;

	@Autowired
	private SubGrupoRepository subGrupoRepo;

	@Autowired
	private TipoClienteRepository tipoClienteRepository;

	@Autowired
	private TipoIVARepository tipoIVARepository;

	@Autowired
	private CuentaAplicacionRepository cuentasRepo;

	@Autowired
	private CentroRepository centroRepo;

	@Autowired
	private TipoCuentaProveedorRepository tipoCuentaProveedorRepository;

	@Autowired
	private TipoProveedorRepository tipoProveedorRepository;

	@Autowired
	private TipoJornadaRepository tipoJornadaRepository;

	@Autowired
	private SectoresRepository sectoresRepository;

	@Autowired
	private TipoEmpleadoRepository empleadoRepository;

	@Autowired
	private UnidadRepository uniRepo;

	@Autowired
	private CategoriaProductoRepository catProducto;

	@Autowired
	private ProveedorRepository proveedoresRepo;

	@Autowired
	private ComisionistaRepository comisionistaRepo;

	@Autowired
	private TipoEventoRepository tipoEventoRepo;

	@Autowired
	private SalonRepository salonRepo;
	@Autowired
	private ClienteRepository clienteRepo;

	private ResponseEntity<ResponseContainer<List<IDescriptivo>>> getDesriptivos(
			Collection<? extends IDescriptivo> descriptivos, String nombre) {
		ResponseContainer<List<IDescriptivo>> respuesta = new ResponseContainer<>();

		try {
			respuesta.setRespuesta(descriptivos.parallelStream().map(c -> new DescriptivoGeneric(c))
					.sorted(Comparator.comparing(DescriptivoGeneric::getDescripcion)).collect(Collectors.toList()));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LogManager.getLogger(DescriptivoController.class).error("Hubo un error buscando " + nombre, e);
			respuesta.setMensaje("Hubo un error buscando " + nombre);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/tipo-comprobante")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getComprobantes() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		return getDesriptivos((List<? extends IDescriptivo>) this.tipoComprobanteRepo.findAll(), "Tipo de Comprobante");
	}

	@GetMapping("/tipo-comprobante-facturable")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getComprobantesFacturables() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		return getDesriptivos((List<? extends IDescriptivo>) this.tipoComprobanteRepo.findByEsFacturableIsTrue(),
				"Tipo de Comprobante");
	}

	@GetMapping("/cajas")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getCajas() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		return getDesriptivos((List<? extends IDescriptivo>) this.cuentasRepo
				.findBySubGrupoCodigoAndCentroCostoActivoTrueOrderByCodigoAsc(CuentasPrincipalesEnum.CAJA.getCodigo()),
				"Tipo de Comprobante");
	}

	@GetMapping("/provincias")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getProvincias() {
		return getDesriptivos((List<? extends IDescriptivo>) this.provinciasRepo.findAll(), "Provincias");
	}

	@GetMapping("/centro-costo")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getCentros() {

		List<IDescriptivo> datos = this.centroRepo.findAllDescriptivo().stream()
				.map(s -> new DescriptivoGeneric(s.getCodigo(), s.getNombre())).collect(Collectors.toList());
		return this.getDesriptivos(datos, "Centros");

	}

	@GetMapping("/tipo-iva")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getTipoIva() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		return getDesriptivos((List<? extends IDescriptivo>) this.tipoIVARepository.findAll(), "Tipo IVA");
	}

	@GetMapping("/tipo-cliente")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getTipoCliente() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		return getDesriptivos((List<? extends IDescriptivo>) this.tipoClienteRepository.findAll(), "Tipo cliente");
	}

	@GetMapping("/tipo-proveedor")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getTipoProveedor() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		return getDesriptivos((List<? extends IDescriptivo>) this.tipoProveedorRepository.findAll(), "Tipo Proveedor");
	}

	@GetMapping("/tipo-cuenta-proveedor")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getTipoCuentaProveedor() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		return getDesriptivos((List<? extends IDescriptivo>) this.tipoCuentaProveedorRepository.findAll(),
				"Tipo cuenta proveedor");
	}

	@GetMapping("/tipo-jornada")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getTipoJornada() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		return getDesriptivos((List<? extends IDescriptivo>) this.tipoJornadaRepository.findAll(), "Tipo jornada");
	}

	@GetMapping("/sectores")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getSectores() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		return getDesriptivos((List<? extends IDescriptivo>) this.sectoresRepository.findAll(), "Sectores");
	}

	@GetMapping("/tipo-empleado")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getTipoEmpleado() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		return getDesriptivos((List<? extends IDescriptivo>) this.empleadoRepository.findAll(), "Tipo empleados");
	}

	@GetMapping("/unidades-producto")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getUnidadesProducto() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		return getDesriptivos((List<? extends IDescriptivo>) this.uniRepo.findAll(), "Unidades Producto");
	}

	@GetMapping("/categorias-producto")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getCategoriaProducto() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		return getDesriptivos((List<? extends IDescriptivo>) this.catProducto.findByHabilitadoIsTrue(),
				"Categorias productos");
	}

	@GetMapping("/proveedores")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getProveedores() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();

		List<DescriptivoGeneric> proveedores = this.proveedoresRepo.findByActivoIsTrue(ProveedorDescriptivo.class)
				.stream().map(o -> new DescriptivoGeneric(String.valueOf(o.getId()), o.getNombreProveedor()))
				.sorted(Comparator.comparing(DescriptivoGeneric::getDescripcion)).collect(Collectors.toList());
		return getDesriptivos(proveedores, "Proveedores");
	}

	@GetMapping("/salones")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getSalones() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		List<IDescriptivo> datos = this.salonRepo.findByActivoIsTrue().stream()
				.map(s -> new DescriptivoGeneric(s.getId(), s.getNombre()))
				.sorted(Comparator.comparing(DescriptivoGeneric::getDescripcion)).collect(Collectors.toList());
		return this.getDesriptivos(datos, "Salones");
	}

	@GetMapping("/clientes")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getClientes() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		List<IDescriptivo> datos = this.clienteRepo.findByActivoIsTrue().stream()
				.map(s -> new DescriptivoGeneric(s.getId(), s.getNombreCliente()))
				.sorted(Comparator.comparing(DescriptivoGeneric::getDescripcion)).collect(Collectors.toList());
		ResponseContainer<List<IDescriptivo>> respuesta = new ResponseContainer<>();

		try {
			respuesta.setRespuesta(datos);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LogManager.getLogger(DescriptivoController.class).error("Hubo un error buscando Clientes", e);
			respuesta.setMensaje("Hubo un error buscando Clientes");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/tipos-evento")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getTiposEvento() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		return getDesriptivos((List<? extends IDescriptivo>) this.tipoEventoRepo.findByHabilitadoIsTrue(),
				"Tipos Evento");
	}

	@GetMapping("/tipos-comision")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getTiposComision() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		List<TipoComision> tipos = Arrays.asList(TipoComision.values());
		return getDesriptivos(tipos.stream().map(t -> new DescriptivoGeneric(t.getCodigo(), t.getDescripcion()))
				.collect(Collectors.toList()), "Tipos de ConfiguracionComision");
	}

	@GetMapping("/productos")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getProductos() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		List<IDescriptivo> datos = this.productoRepo.findAllDescriptivo().stream()
				.map(s -> new DescriptivoGeneric(s.getCodigo(), s.getNombre()))
				.sorted(Comparator.comparing(DescriptivoGeneric::getDescripcion)).collect(Collectors.toList());
		ResponseContainer<List<IDescriptivo>> respuesta = new ResponseContainer<>();

		try {
			respuesta.setRespuesta(datos);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LogManager.getLogger(DescriptivoController.class).error("Hubo un error buscando Productos", e);
			respuesta.setMensaje("Hubo un error buscando Productos");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/comisionistas")
	public ResponseEntity<ResponseContainer<List<DescriptivoGeneric>>> getComisionistas() {
		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		List<IComisionistaDescriptivo> datos = this.comisionistaRepo.getDescriptivos();
		ResponseContainer<List<DescriptivoGeneric>> respuesta = new ResponseContainer<>();

		try {
			respuesta.setRespuesta(datos.stream().map(a -> new DescriptivoGeneric(a.getId() + "", a.getNombre()))
					.sorted(Comparator.comparing(DescriptivoGeneric::getDescripcion)).collect(Collectors.toList()));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LogManager.getLogger(DescriptivoController.class).error("Hubo un error buscando Productos", e);
			respuesta.setMensaje("Hubo un error buscando Productos");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/subgrupos")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getSubGrupos() {
		return getDesriptivos(this.subGrupoRepo.getDescriptivos(), "SubGrupos");

	}

	@GetMapping("/grupos")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getGrupos() {
		return getDesriptivos(this.grupoRepo.getDescriptivos(), "Grupos");

	}

	@GetMapping("/rubros")
	public ResponseEntity<ResponseContainer<List<IDescriptivo>>> getRubros() {
		return getDesriptivos(this.rubroRepo.getDescriptivos(), "Rubros");

	}

}

package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.UnidadProducto;
import ar.com.thinkup.luviam.service.def.IParametricoService;
import ar.com.thinkup.luviam.service.def.IUnidadProductoService;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/unidadproducto")
@PreAuthorize("isAuthenticated()")
public class UnidadProductoController extends ParametricosController<ParametricoVO, UnidadProducto> {

	private static final Logger LOG = LogManager.getLogger(UnidadProductoController.class);


	@Autowired
	private IUnidadProductoService UnidadProductoService;

	@GetMapping("/parametrico/all")
	public ResponseEntity<ResponseContainer<List<ParametricoVO>>> getParametricos() {
		return super.getParametricos();
	}

	@GetMapping("/all")
	public ResponseEntity<ResponseContainer<List<ParametricoVO>>> getAll() {
		return super.getAll();
	}
	
	@PutMapping()
	public ResponseEntity<ResponseContainer<ParametricoVO>> create(@RequestBody ParametricoVO vo) {
		return super.create(vo);
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<ParametricoVO>> update(@RequestBody ParametricoVO vo) {
		return super.update(vo);
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		return super.delete(id);
	}

	@Override
	protected IParametricoService<ParametricoVO, UnidadProducto> getService() {
		return this.UnidadProductoService;
	}

	@Override
	protected Logger getLog() {
		return UnidadProductoController.LOG;
	}

	@Override
	protected String getNombre() {

		return "Unidad Producto";
	}
}

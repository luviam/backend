package ar.com.thinkup.luviam.controller;


import java.sql.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.config.jwt.JwtAuthenticationRequest;
import ar.com.thinkup.luviam.config.jwt.JwtAuthenticationResponse;
import ar.com.thinkup.luviam.config.jwt.JwtTokenUtil;
import ar.com.thinkup.luviam.exceptions.SendEmailException;
import ar.com.thinkup.luviam.model.EmailTemplate;
import ar.com.thinkup.luviam.model.enums.EmailTemplateEnum;
import ar.com.thinkup.luviam.model.security.PasswordResetToken;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.repository.EmailTemplateRepository;
import ar.com.thinkup.luviam.repository.PasswordResetTokenRepository;
import ar.com.thinkup.luviam.repository.UsuarioRepository;
import ar.com.thinkup.luviam.service.def.MailService;
import ar.com.thinkup.luviam.vo.RefreshTokenVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;
import ar.com.thinkup.luviam.vo.security.CambiarPasswordVO;
import ar.com.thinkup.luviam.vo.security.RecuperarPasswordVO;


@RestController
public class LoginController {

	private static final Logger LOG = LogManager.getLogger(LoginController.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;
    
    @Autowired
    private PasswordResetTokenRepository tokenRepository;
    
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    private MailService mailService;
    
	@Autowired
	private EmailTemplateRepository templateRepository;

    @PostMapping("/login")
    public ResponseEntity<ResponseContainer<JwtAuthenticationResponse>> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest, HttpServletRequest request) throws AuthenticationException {

        try{
            final Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            authenticationRequest.getUsername(),
                            authenticationRequest.getPassword()
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
            
            Device device = DeviceUtils.getCurrentDevice(request);
            
            // Reload password post-security so we can generate token
            final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
            final String token = jwtTokenUtil.generateToken(userDetails, device);

            // Return the token
            return ResponseEntity.ok(new ResponseContainer<JwtAuthenticationResponse>(new JwtAuthenticationResponse(token, userDetails), null));
        }catch(UsernameNotFoundException | BadCredentialsException bad){
        	return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ResponseContainer<JwtAuthenticationResponse>(null, "El usuario y contraseña no coinciden."));
        }catch(Exception e){
        	return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResponseContainer<JwtAuthenticationResponse>(null, "Hubo un error ingresando a la aplicación."));
        }

    }

    
    @PostMapping("/refresh-token")
    public ResponseEntity<JwtAuthenticationResponse> refreshAndGetAuthenticationToken(@RequestBody RefreshTokenVO body, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        String username = jwtTokenUtil.getUsernameFromToken(token);
        Usuario user = this.usuarioRepository.findByUsernameAndActivoIsTrue(username);
        
        if (jwtTokenUtil.canTokenBeRefreshed(token, user.getUltimaActualizacionPassword() /*user.getLastPasswordResetDate()*/)) {
            String refreshedToken = jwtTokenUtil.refreshToken(token);
            return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken, user));
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }
    
    @PostMapping(value= "/recuperar-password")
    public ResponseEntity<ResponseContainer<String>> recuperarPassword(@RequestBody RecuperarPasswordVO datos) {
        
    	UserDetails userDetails = null;
    	try{
    		userDetails = userDetailsService.loadUserByUsername(datos.getEmail());
    	}catch(UsernameNotFoundException e){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResponseContainer<>(Boolean.FALSE.toString(), "El email igresado no existe."));
    	}
    	
    	if (userDetails == null){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResponseContainer<>(Boolean.FALSE.toString(), "El email igresado no existe."));
    	}
    	
    	//Chequeo que el token no se haya generado antes, si es asi elimino el anterior
		PasswordResetToken prevToken = this.tokenRepository.findOneByEmail(datos.getEmail());
		if (prevToken != null){
			this.tokenRepository.delete(prevToken);
		}
    	
    	String token = UUID.randomUUID().toString();
    	PasswordResetToken passToken = new PasswordResetToken();
    	passToken.setEmail(datos.getEmail());
    	passToken.setToken(token);
    	passToken.setExpiryDate(DateTime.now().plusDays(1).toDate());
    	
    	this.tokenRepository.save(passToken);
    	
    	EmailTemplate et = this.templateRepository.findOneByCodigo(EmailTemplateEnum.RECUPERAR_PASSWORD.getCodigo());
    	Map<String, String> mailParams = new HashMap<>();
    	mailParams.put(":token", token);
    	mailParams.put(":email", DigestUtils.md5DigestAsHex(datos.getEmail().getBytes()));
    	
    	try {
			this.mailService.send(datos.getEmail(), "Luviam - Recuperación de Password", et, mailParams);
		} catch (SendEmailException e) {
			LOG.error("No se pudo enviar el mail para recuperar la contraseña", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResponseContainer<>(Boolean.FALSE.toString(), "No se pudo enviar el mail de recuperación. Intente nuevamente mas tarde."));
		}
    	
        return ResponseEntity.ok(new ResponseContainer<>(Boolean.TRUE.toString()));
    }
    
    @PostMapping("/validar-token/{token}")
    public ResponseEntity<ResponseContainer<Boolean>> validarToken(@PathVariable String token, @RequestBody RecuperarPasswordVO datos) {

    	if (this.resolveToken(token, datos.getEmail()) == null){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResponseContainer<>(Boolean.FALSE, "El token ingresado no es válido."));
    	}
    	
        return ResponseEntity.ok(new ResponseContainer<Boolean>(Boolean.TRUE));
    }
    
    @PostMapping("/cambiar-password")
    public ResponseEntity<ResponseContainer<Boolean>> cambiarPassword(@RequestBody CambiarPasswordVO datos) {
            
    	
    	PasswordResetToken passToken = this.resolveToken(datos.getToken(), datos.getEmail());
    	if (passToken == null){
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResponseContainer<>(Boolean.FALSE, "El token ingresado no es válido."));
    	}
    	
    	Usuario us = this.usuarioRepository.findByUsernameAndActivoIsTrue(passToken.getEmail());
    	us.actualizarPassword(DigestUtils.md5DigestAsHex(datos.getNuevaPassword().getBytes()));
    	
    	//actualizo la contraseña
    	this.usuarioRepository.save(us);
    	//elimino el token utilizado
    	this.tokenRepository.delete(passToken);
    	    	
        return ResponseEntity.ok(new ResponseContainer<>(Boolean.TRUE));
    }
    
    private PasswordResetToken resolveToken(String token, String email){
    	PasswordResetToken passToken = this.tokenRepository.findOneByToken(token);

    	//Verifico que el token exista y que el mail informado sea el correspondiente
    	if (passToken == null || 
    			!DigestUtils.md5DigestAsHex(passToken.getEmail().getBytes()).equals(email) ||
    			passToken.getExpiryDate().before(new Date(System.currentTimeMillis()))){
    		return null;
    	}
    	return passToken;
    }
}

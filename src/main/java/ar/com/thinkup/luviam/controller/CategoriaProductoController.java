package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.CategoriaProducto;
import ar.com.thinkup.luviam.service.def.ICategoriaProductoService;
import ar.com.thinkup.luviam.vo.CategoriaProductoVO;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/categorias")
@PreAuthorize("isAuthenticated()")
public class CategoriaProductoController extends ParametricosController<CategoriaProductoVO, CategoriaProducto> {

	private static final Logger LOG = LogManager.getLogger(CategoriaProductoController.class);

	@Autowired
	private ICategoriaProductoService CategoriaProductoService;

	@GetMapping("/parametrico/all")
	public ResponseEntity<ResponseContainer<List<ParametricoVO>>> getParametricos() {
		return super.getParametricos();
	}
	
	@GetMapping("/comisionables")
	public ResponseEntity<ResponseContainer<List<CategoriaProductoVO>>> getComisionable() {
		ResponseContainer<List<CategoriaProductoVO>> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.getService().getComisionables());
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);

		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando	 " + this.getNombre();
			getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/all")
	public ResponseEntity<ResponseContainer<List<CategoriaProductoVO>>> getAll() {
		return super.getAll();
	}
	@GetMapping("/{id}")
	public ResponseEntity<ResponseContainer<CategoriaProductoVO>> getById(@PathVariable("id") Long id) {
		return super.getByID(id);
	}

	@PutMapping()
	public ResponseEntity<ResponseContainer<CategoriaProductoVO>> create(@RequestBody CategoriaProductoVO vo) {
		return super.create(vo);
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<CategoriaProductoVO>> update(@RequestBody CategoriaProductoVO vo) {
		return super.update(vo);
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		return super.delete(id);
	}

	@Override
	protected ICategoriaProductoService getService() {
		return this.CategoriaProductoService;
	}

	@Override
	protected Logger getLog() {
		return CategoriaProductoController.LOG;
	}

	@Override
	protected String getNombre() {

		return "Categoria Producto";
	}
}

package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import ar.com.thinkup.luviam.model.MarcaProductos;
import ar.com.thinkup.luviam.service.def.IMarcaService;
import ar.com.thinkup.luviam.vo.MarcaVO;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;
@RestController
@RequestMapping(path = "/marcas")
@PreAuthorize("isAuthenticated()")
public class MarcasController extends ParametricosController<MarcaVO, MarcaProductos> {

	private static final Logger LOG = LogManager.getLogger(MarcasController.class);

	@Autowired
	private IMarcaService marcaService;

	@GetMapping("/parametrico/all")
	public ResponseEntity<ResponseContainer<List<ParametricoVO>>> getParametricos() {
		return super.getParametricos();
	}

	@GetMapping("/all")
	public ResponseEntity<ResponseContainer<List<MarcaVO>>> getAll() {
		return super.getAll();
	}

	@PutMapping()
	public ResponseEntity<ResponseContainer<MarcaVO>> create(
			@RequestParam("logo") MultipartFile logo,
			@RequestParam("marca") String marca ) {
		
		ResponseContainer<MarcaVO> respuesta = new ResponseContainer<>();
		try {
			MarcaVO vo = new Gson().fromJson(marca, MarcaVO.class);
			
			respuesta.setRespuesta(this.getService().create(logo,vo));
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error creando " + this.getNombre();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping
	public ResponseEntity<ResponseContainer<MarcaVO>> update(
		@RequestParam("logo") MultipartFile logo,
		@RequestParam("marca") String marca ) {
	
	ResponseContainer<MarcaVO> respuesta = new ResponseContainer<>();
	try {
		MarcaVO vo = new Gson().fromJson(marca, MarcaVO.class);
		
		respuesta.setRespuesta(this.getService().update(logo,vo));
		return ResponseEntity.ok().body(respuesta);
	} catch (IllegalArgumentException e) {
		String m = e.getMessage();
		this.getLog().error(m, e);
		respuesta.setMensaje(m);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
	} catch (Exception e) {
		String m = "Hubo un error creando " + this.getNombre();
		this.getLog().error(m, e);
		respuesta.setMensaje(m);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
	}
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		return super.delete(id);
	}

	@Override
	protected IMarcaService getService() {
		return this.marcaService;
	}

	@Override
	protected Logger getLog() {
		return MarcasController.LOG;
	}

	@Override
	protected String getNombre() {

		return "Tipo Empleado";
	}

}

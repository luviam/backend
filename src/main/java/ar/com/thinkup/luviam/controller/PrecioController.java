package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import ar.com.thinkup.luviam.model.Precio;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.service.def.IPrecioService;
import ar.com.thinkup.luviam.vo.PrecioVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroPrecio;
import ar.com.thinkup.luviam.vo.parametricos.ConflictoPrecio;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;


public abstract class PrecioController<V extends PrecioVO, P extends Precio> extends CommonController<V, P> {
	
	protected abstract IPrecioService<V,P> getService();

	protected abstract Logger getLog();

	protected abstract String getNombre();


	public ResponseEntity<ResponseContainer<List<ConflictoPrecio>>> create(List<V> vo) {
		updateResponsable(vo);
		ResponseContainer<List<ConflictoPrecio>> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.getService().create(vo));
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error creando " + this.getNombre();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	public ResponseEntity<ResponseContainer<List<ConflictoPrecio>>> update(List<V> vo) {
		updateResponsable(vo);
		ResponseContainer<List<ConflictoPrecio>> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.getService().update(vo));
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error guardando " + this.getNombre();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	private void updateResponsable(List<V> vo) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Usuario u = (Usuario)authentication.getPrincipal();
		vo.stream().forEach(v -> v.setResponsable(u.getDescriptivo()));
	}

	public ResponseEntity<ResponseContainer<Boolean>> delete(Long id) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.getService().delete(id));
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error guardando " + this.getNombre();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}
	
	public ResponseEntity<ResponseContainer<List<V>>> getAll(FiltroPrecio filtro) {
		ResponseContainer<List<V>> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.getService().getByFiltro(filtro));
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);
	
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando " + this.getNombre();
			getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	
}

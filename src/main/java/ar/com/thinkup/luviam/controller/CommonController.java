package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import ar.com.thinkup.luviam.vo.Identificable;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

public abstract class CommonController<V extends Identificable, E extends Identificable> {

	public CommonController() {
		super();
	}

	protected abstract Logger getLog();

	protected abstract String getNombre();
	
	protected abstract ICommonService<V,E> getService();

	public ResponseEntity<ResponseContainer<V>> getByID(Long id) {
		ResponseContainer<V> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.getService().getByID(id));
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);
	
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando " + this.getNombre();
			getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	public ResponseEntity<ResponseContainer<List<V>>> getAll() {
		ResponseContainer<List<V>> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.getService().getAll());
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);
	
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando " + this.getNombre();
			getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

}
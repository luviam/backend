package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.Proveedor;
import ar.com.thinkup.luviam.model.ProveedorConSaldo;
import ar.com.thinkup.luviam.repository.ProveedorConSaldoRepository;
import ar.com.thinkup.luviam.repository.ProveedorRepository;
import ar.com.thinkup.luviam.service.def.IProveedorService;
import ar.com.thinkup.luviam.vo.ProveedorVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/proveedor")
@PreAuthorize("isAuthenticated()")
public class ProveedorController {

	private static final Logger LOG = LogManager.getLogger(ProveedorController.class);

	@Autowired
	private ProveedorRepository repo;

	@Autowired
	private IProveedorService proveedorService;
	
	@Autowired 
	private ProveedorConSaldoRepository conSaldoRepo;



	@GetMapping("/cabecera")
	public ResponseEntity<ResponseContainer<List<ProveedorConSaldo>>> getCabeceras() {
		ResponseContainer<List<ProveedorConSaldo>> respuesta = new ResponseContainer<>();
		try {
			List<ProveedorConSaldo> proveedores = this.conSaldoRepo.findAll();
			
			respuesta.setRespuesta(proveedores);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando proveedores", e);
			respuesta.setMensaje("Hubo un error buscando proveedores.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@GetMapping("saldo/{id}")
	public ResponseEntity<ResponseContainer<Float>> getSaldoProveedor(@PathVariable Long id) {
		ResponseContainer<Float> respuesta = new ResponseContainer<>();
		try {

			respuesta.setRespuesta(this.repo.getSaldo(id));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando el proveedor", e);
			respuesta.setMensaje("Hubo un error buscando el proveedor.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<ResponseContainer<ProveedorVO>> getProveedoresById(@PathVariable Long id) {
		ResponseContainer<ProveedorVO> respuesta = new ResponseContainer<>();
		try {

			respuesta.setRespuesta(this.proveedorService.getById(id));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando el proveedor", e);
			respuesta.setMensaje("Hubo un error buscando el proveedor.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PutMapping()
	public ResponseEntity<ResponseContainer<Boolean>> crear(@RequestBody ProveedorVO proveedor) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();

		if (proveedor.getId() == null) {
			try {
				proveedor.setActivo(true);
				this.proveedorService.guardarProveedor(proveedor);

				LOG.info("Se guardó exitosamente el proveedor");
				respuesta.setRespuesta(true);
				respuesta.setMensaje("Se guardó exitosamente el proveedor");
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			} catch (Exception e) {
				LOG.error("Hubo un error guardando el Proveedor.", e);
				respuesta.setMensaje("Hubo un error guardando el Proveedor.");
				respuesta.setRespuesta(false);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
		} else {
			LOG.warn("Ya existe el centro de costo.");
			respuesta.setMensaje("Ya existe el centro de costo.");
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<Boolean>> actualizarCentro(@RequestBody ProveedorVO proveedor) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();

		if (proveedor.getId() != null) {
			try {

				this.proveedorService.guardarProveedor(proveedor);
				LOG.info("Se guardó exitosamente el proveedor");
				respuesta.setRespuesta(true);
				respuesta.setMensaje("Se guardó exitosamente el proveedor");
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			} catch (Exception e) {
				LOG.error("Hubo un error guardando el cliente de costo.", e);
				respuesta.setMensaje("Hubo un error guardando el cliente de costos.");
				respuesta.setRespuesta(false);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
		} else {
			LOG.warn("No existe el cliente de costo que desea modificar.");
			respuesta.setMensaje("Ya existe el cliente de costo.");
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") String id) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();
		Long proveedorId = Long.valueOf(id);
		try {
			Proveedor proveedor = this.repo.findOne(proveedorId);
			if (proveedor != null) {
				proveedor.setActivo(false);

				respuesta.setMensaje("Se elimino el proveedor correctamente.");
				this.repo.save(proveedor);
				respuesta.setRespuesta(true);
				return ResponseEntity.ok().body(respuesta);

			}
		} catch (Exception e) {
			respuesta.setMensaje("Hubo un error eliminando el proveedor.");
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
		respuesta.setMensaje("El proveedor que desea eliminar no existe");
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
	}

}

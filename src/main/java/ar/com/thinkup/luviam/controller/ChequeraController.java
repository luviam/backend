package ar.com.thinkup.luviam.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.exceptions.CuentaNoExistenteException;
import ar.com.thinkup.luviam.model.Cheque;
import ar.com.thinkup.luviam.model.Chequera;
import ar.com.thinkup.luviam.model.SecuenciaCheques;
import ar.com.thinkup.luviam.model.enums.EstadoCheque;
import ar.com.thinkup.luviam.model.enums.TipoCheque;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.repository.ChequeRepository;
import ar.com.thinkup.luviam.repository.ChequeraRepository;
import ar.com.thinkup.luviam.repository.CuentaAplicacionRepository;
import ar.com.thinkup.luviam.vo.ChequeraVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/chequera")
@PreAuthorize("isAuthenticated()")
public class ChequeraController {

	private static final Logger LOG = LogManager.getLogger(ChequeraController.class);

	@Autowired
	private ChequeraRepository repo;

	@Autowired
	private ChequeRepository chequeRepo;

	@Autowired
	private CuentaAplicacionRepository cuentaRepo;

	@GetMapping("/all")
	@Transactional
	public ResponseEntity<ResponseContainer<List<ChequeraVO>>> getCentros() {

		ResponseContainer<List<ChequeraVO>> respuesta = new ResponseContainer<>();
		try {
			List<Chequera> chequeras = this.repo.findAll();

			respuesta.setRespuesta(chequeras.stream().map(che -> new ChequeraVO(che)).collect(Collectors.toList()));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando las chequeras", e);
			respuesta.setMensaje("Hubo un error buscando chequeras.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(path = "/{id}")
	@Transactional()
	public ResponseEntity<ResponseContainer<ChequeraVO>> getChequera(@PathVariable Long id) {
		ResponseContainer<ChequeraVO> respuesta = new ResponseContainer<>();
		try {
			Chequera chequera = this.repo.findOne(id);
			if (chequera == null) {
				LOG.error("No existe chequera con ID: " + id);
				respuesta.setMensaje("Hubo un error buscando el chequera.");
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
			ChequeraVO chq = new ChequeraVO(chequera);
			respuesta.setRespuesta(chq);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando la chequera.", e);
			respuesta.setMensaje("Hubo un error buscando la chequeras.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@PutMapping()
	@Transactional
	public ResponseEntity<ResponseContainer<Boolean>> crearChequera(@RequestBody ChequeraVO chq) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();

		if (chq.getId() == null) {
			try {
				Chequera chequera = this.toChequera(chq);
				generarCheques(chequera);
				this.repo.save(chequera);
				LOG.info("Se guardó exitosamente la chequera");
				respuesta.setRespuesta(true);
				respuesta.setMensaje("Se guardó exitosamente la chequera");
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			} catch (Exception e) {
				LOG.error("Hubo un error guardando la chequera.", e);
				respuesta.setMensaje("Hubo un error guardando la chequera.");
				respuesta.setRespuesta(false);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
		} else {
			LOG.warn("Ya existe la chequera.");
			respuesta.setMensaje("Ya existe la chequera.");
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	private void generarCheques(Chequera chequera) {
		for (SecuenciaCheques seq : chequera.getSecuenciasCheques()) {

			for (long i = seq.getMinNumeracion(); i < seq.getMaxNumeracion() + 1; i++) {
				Cheque c = new Cheque();
				c.setTitularCuenta(chequera.getCuentaBancaria().getCentroCosto().getRazonSocial());
				c.setCuit(chequera.getCuentaBancaria().getCentroCosto().getCuit());
				c.setBanco(chequera.getCuentaBancaria().getNombre());
				c.setEstadoCheque(EstadoCheque.DISPONIBLE);
				c.setTipoCheque(TipoCheque.PROPIO);
				c.setCuentaBancaria(chequera.getCuentaBancaria());
				c.setEsDiferido(chequera.isEsDiferido());
				c.setNumero(String.valueOf(i));
				c.setOrigen("Chequera de " + chequera.getCuentaBancaria().getCentroCosto().getRazonSocial());
				chequera.addCheque(c);
			}

		}
	}

	@PostMapping()
	@Transactional()
	public ResponseEntity<ResponseContainer<Boolean>> actualizarChequera(@RequestBody ChequeraVO chq) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();

		if (chq.getId() != null) {
			try {
				Chequera chequera = this.toChequera(chq);
				List<Cheque> chequesABorrar = chequera.getCheques().stream()
								.filter(c -> c.getEstadoCheque().equals(EstadoCheque.DISPONIBLE) && !chequera.pertenece(c)).collect(Collectors.toList());
				chequera.getCheques().removeAll(chequesABorrar);
				this.generarCheques(chequera);
				this.chequeRepo.delete(chequesABorrar);
				this.repo.save(chequera);
				LOG.info("Se guardó exitosamente la chequera");
				respuesta.setRespuesta(true);
				respuesta.setMensaje("Se guardó exitosamente la chequera");
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			} catch (Exception e) {
				LOG.error("Hubo un error guardando la chequera.", e);
				respuesta.setMensaje("Hubo un error guardando la chequera.");
				respuesta.setRespuesta(false);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
		} else {
			LOG.warn("No existe la chequera que desea modificar " + chq.getId());
			respuesta.setMensaje("No existe la chequera que desea modificar");
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@DeleteMapping()
	@Transactional
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();
		try {
			Chequera chequera = this.repo.findOne(id);
			if (chequera != null) {

				List<Cheque> chequesUsados =
							chequera.getCheques().stream().filter(c -> c.getEstadoCheque()!= EstadoCheque.DISPONIBLE).collect(Collectors.toList());
				
				chequesUsados.stream().forEach(c -> c.setChequera(null));
				List<Cheque> chequesABorrar =
						chequera.getCheques().stream().filter(c -> c.getEstadoCheque()== EstadoCheque.DISPONIBLE).collect(Collectors.toList());
			

				this.chequeRepo.delete(chequesABorrar);
				this.chequeRepo.save(chequesUsados);
				chequera.getCheques().removeAll(chequesABorrar);
				chequera.getCheques().removeAll(chequesUsados);
				this.repo.delete(chequera);
				if(chequesUsados.size() > 0) {
					respuesta.setMensaje("Se elimino la chequera correctamente. Los cheques ya usuados quedarán como cheques independientes.");
				}else {
					respuesta.setMensaje("Se elimino la chequera correctamente.");	
				}
				
				respuesta.setRespuesta(true);
				return ResponseEntity.ok().body(respuesta);
			}
		} catch (Exception e) {
			respuesta.setMensaje("Hubo un error eliminando la chequera.");
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
		respuesta.setMensaje("La chequera que desea eliminar no existe");
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
	}

	private Chequera toChequera(ChequeraVO chq) {

		Chequera chequera;
		if (chq.getId() != null) {
			chequera = this.repo.findOne(chq.getId());
		} else {
			chequera = new Chequera();
			chequera.setDeleted(false);
		}

		
		chequera.getSecuenciasCheques().clear();

		CuentaAplicacion cuentaBancaria = this.cuentaRepo.findOne(chq.getCuentaBancaria().getId());
		if (cuentaBancaria == null) {
			LOG.error("Cuenta con ID no se encuentra en la base de datos", chq.getCuentaBancaria().getCodigo());
			throw new CuentaNoExistenteException();
		}
		chequera.setCuentaBancaria(cuentaBancaria);
		chequera.setEsDiferido(chq.isEsDiferido());

		chequera.getSecuenciasCheques().addAll(chq.getSecuencias());

		return chequera;
	}

}

package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.service.def.IFacturaService;
import ar.com.thinkup.luviam.vo.FacturaVO;
import ar.com.thinkup.luviam.vo.documentos.facturas.FacturaCabeceraVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroProveedor;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/facturas")
@PreAuthorize("isAuthenticated()")
public class FacturaController {

	private static final Logger LOG = LogManager.getLogger(FacturaController.class);

	@Autowired
	private IFacturaService facturasService;

	@GetMapping(path = "/{id}")
	public ResponseEntity<ResponseContainer<FacturaVO>> getById(@PathVariable("id") Long idFactura) {
		ResponseContainer<FacturaVO> respuesta = new ResponseContainer<>();
		String m = "Hubo un error al buscar la Factura";
		try {

			respuesta.setRespuesta(this.facturasService.findById(idFactura));
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);
		} catch (Exception e) {
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping(path = "/cambioEstadoFactura")
	public ResponseEntity<ResponseContainer<FacturaCabeceraVO>> cambioEstadoFactura(Authentication authentication,
			@RequestBody FacturaCabeceraVO cabeceraFactura) {

		ResponseContainer<FacturaCabeceraVO> respuesta = new ResponseContainer<>();
		String m = "Hubo un error actualizando la Factura";
		if (cabeceraFactura.getId() == null) {
			String s = "La Factura debe tener id";
			LOG.error(s);
			respuesta.setMensaje(s);
			respuesta.setRespuesta(null);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} else {
			try {
				EstadoOperacionEnum e = EstadoOperacionEnum.getByCodigo(cabeceraFactura.getEstado());
				DescriptivoGeneric estado = new DescriptivoGeneric(e.getCodigo(), e.getDescripcion());
				if (this.facturasService.cambioEstadoFactura(cabeceraFactura.getId(), estado,
						(Usuario) authentication.getPrincipal())) {
					cabeceraFactura.setEstado(e.getCodigo());
					respuesta.setRespuesta(cabeceraFactura);

					return ResponseEntity.status(HttpStatus.OK).body(respuesta);
				} else {

					LOG.error(m, e);
					respuesta.setMensaje(m);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
				}

			} catch (Exception e) {
				LOG.error(m, e);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}

		}

	}

	@PostMapping("/cabeceras/")
	public ResponseEntity<ResponseContainer<List<FacturaCabeceraVO>>> getFacturaCabeceraVOs(
			@RequestBody FiltroProveedor filtros) {

		ResponseContainer<List<FacturaCabeceraVO>> respuesta = new ResponseContainer<>();
		try {
			List<FacturaCabeceraVO> facturas = this.facturasService.getCabeceras(filtros);
			respuesta.setRespuesta(facturas);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando las facturas";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@GetMapping("/cabeceras/impagas/{idProveedor}/{codigoCentro}")
	public ResponseEntity<ResponseContainer<List<FacturaCabeceraVO>>> getFacturaCabeceraVOs(
			@PathVariable("idProveedor") Long idProveedor, @PathVariable("codigoCentro") String codigoCentro) {

		ResponseContainer<List<FacturaCabeceraVO>> respuesta = new ResponseContainer<>();
		try {
			if (idProveedor == null) {

				String m = "Proveedor obligatorio";
				LOG.error(m);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}

			List<FacturaCabeceraVO> facturas = this.facturasService.getImpagas(idProveedor, codigoCentro);

			respuesta.setRespuesta(facturas);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando las facturas";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@PutMapping()
	public ResponseEntity<ResponseContainer<FacturaVO>> crearFactura(Authentication authentication,
			@RequestBody FacturaVO factura) {
		ResponseContainer<FacturaVO> respuesta = new ResponseContainer<>();

		if (factura.getId() == null) {
			try {
				FacturaVO nueva = this.facturasService.guardarFactura(factura, (Usuario) authentication.getPrincipal());
				String m = "Se guardó exitosamente la factura";
				LOG.info(m);
				respuesta.setRespuesta(nueva);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			}catch(IllegalArgumentException e) {
				String m = e.getMessage();
				LOG.error(m, e);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);	
			}catch (Exception e) {
				String m = "Hubo un error guardando la factura";
				LOG.error(m, e);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
		} else {
			String m = "Ya existe la factura.";
			LOG.warn(m);
			respuesta.setMensaje(m);
			respuesta.setRespuesta(factura);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<FacturaVO>> actualizarFactura(Authentication authentication,
			@RequestBody FacturaVO factura) {
		ResponseContainer<FacturaVO> respuesta = new ResponseContainer<>();

		if (factura.getId() != null) {
			try {
				FacturaVO nueva = this.facturasService.guardarFactura(factura, (Usuario) authentication.getPrincipal());
				String m = "Se guardó exitosamente la factura";
				LOG.info(m);
				respuesta.setRespuesta(nueva);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			}catch(IllegalArgumentException e) {
				String m = e.getMessage();
				LOG.error(m, e);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);	
			} catch (Exception e) {
				String m = "Hubo un error guardando la factura";
				LOG.error(m, e);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
		} else {
			String m = "No existe la factura.";
			LOG.warn(m);
			respuesta.setMensaje(m);
			respuesta.setRespuesta(factura);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(Authentication authentication,
			@RequestParam("id") String id) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();
		Usuario responsable = (Usuario) authentication.getPrincipal();

		try {
			this.facturasService.borrar(Long.valueOf(id), responsable);

			respuesta.setMensaje("Factura eliminada correctamente");
			respuesta.setRespuesta(true);
			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setMensaje("Hubo un error eliminando la Factura.");
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

}

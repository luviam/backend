package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.Cheque;
import ar.com.thinkup.luviam.service.def.IChequeService;
import ar.com.thinkup.luviam.vo.ChequeVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController
@RequestMapping(path = "/cheques")
@PreAuthorize("isAuthenticated()")
public class ChequeController extends GenericController<ChequeVO, Cheque> {

	private static final Logger LOG = LogManager.getLogger(ChequeController.class);

	@Autowired
	private IChequeService service;

	
	@GetMapping("/all")
	public ResponseEntity<ResponseContainer<List<ChequeVO>>> getAll() {
		return super.getAll();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ResponseContainer<ChequeVO>> getById(@PathVariable("id") Long id) {
		return super.getByID(id);
	}

	@PutMapping()
	public ResponseEntity<ResponseContainer<ChequeVO>> create(@RequestBody ChequeVO vo) {
		return super.create(vo);
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<ChequeVO>> update(@RequestBody ChequeVO vo) {
		return super.update(vo);
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		return super.delete(id);
	}

	@GetMapping("/disponibles")
	public ResponseEntity<ResponseContainer<List<ChequeVO>>> getDisponibles() {

		ResponseContainer<List<ChequeVO>> respuesta = new ResponseContainer<>();
		try {
			List<ChequeVO> cheques = this.service.getDisponibles();

			respuesta.setRespuesta(cheques);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando los cheques";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@Override
	protected IChequeService getService() {
		return this.service;
	}

	@Override
	protected Logger getLog() {
		return ChequeController.LOG;
	}

	@Override
	protected String getNombre() {

		return "Cheque";
	}

}

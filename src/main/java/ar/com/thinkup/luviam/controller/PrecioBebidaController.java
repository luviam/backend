package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.PrecioBebida;
import ar.com.thinkup.luviam.service.def.IPrecioBebidaService;
import ar.com.thinkup.luviam.vo.PrecioBebidaVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroPrecio;
import ar.com.thinkup.luviam.vo.parametricos.ConflictoPrecio;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;
@RestController
@RequestMapping(path = "/preciobebidas")
@PreAuthorize("isAuthenticated()")
public class PrecioBebidaController extends PrecioController<PrecioBebidaVO, PrecioBebida>{


	private static final Logger LOG = LogManager.getLogger(PrecioBebidaController.class);



	@Autowired
	private IPrecioBebidaService pService;



	@Override
	protected IPrecioBebidaService getService() {
		return this.pService;
	}

	@Override
	protected Logger getLog() {
		return PrecioBebidaController.LOG;
	}



	@Override
	protected String getNombre() {
		return "Precio Bebida";
	}

	@Override
	@PutMapping()
	public ResponseEntity<ResponseContainer<List<ConflictoPrecio>>> create(@RequestBody() List<PrecioBebidaVO> vo) {
		
		return super.create(vo);
	}

	@Override
	@PostMapping()
	public ResponseEntity<ResponseContainer<List<ConflictoPrecio>>> update(@RequestBody() List<PrecioBebidaVO> vo) {
		return super.update(vo);
	}

	@Override
	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		
		return super.delete(id);
	}

	@Override
	@GetMapping("/{id}")
	public ResponseEntity<ResponseContainer<PrecioBebidaVO>> getByID(@PathVariable("id") Long id) {
			return super.getByID(id);
	}

	
	@PostMapping("/all")
	public ResponseEntity<ResponseContainer<List<PrecioBebidaVO>>> getAll(@RequestBody() FiltroPrecio filtro) {
			return super.getAll(filtro);
	}
	
	@GetMapping("/all")
	public ResponseEntity<ResponseContainer<List<PrecioBebidaVO>>> getAll() {
			return super.getAll();
	}
	

	


}

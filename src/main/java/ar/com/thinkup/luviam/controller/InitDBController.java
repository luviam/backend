package ar.com.thinkup.luviam.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.CategoriaProducto;
import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.CentroCostosTipoComprobante;
import ar.com.thinkup.luviam.model.Cliente;
import ar.com.thinkup.luviam.model.EmailTemplate;
import ar.com.thinkup.luviam.model.Proveedor;
import ar.com.thinkup.luviam.model.TipoCliente;
import ar.com.thinkup.luviam.model.TipoComisionista;
import ar.com.thinkup.luviam.model.TipoComprobante;
import ar.com.thinkup.luviam.model.TipoCuentaProveedor;
import ar.com.thinkup.luviam.model.TipoEmpleado;
import ar.com.thinkup.luviam.model.TipoIVA;
import ar.com.thinkup.luviam.model.TipoProveedor;
import ar.com.thinkup.luviam.model.UnidadProducto;
import ar.com.thinkup.luviam.model.enums.CuentasPrincipalesEnum;
import ar.com.thinkup.luviam.model.plan.Grupo;
import ar.com.thinkup.luviam.model.plan.Rubro;
import ar.com.thinkup.luviam.model.plan.SubGrupo;
import ar.com.thinkup.luviam.model.security.Rol;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.repository.CategoriaProductoRepository;
import ar.com.thinkup.luviam.repository.CentroRepository;
import ar.com.thinkup.luviam.repository.ClienteRepository;
import ar.com.thinkup.luviam.repository.EmailTemplateRepository;
import ar.com.thinkup.luviam.repository.ProveedorRepository;
import ar.com.thinkup.luviam.repository.RolRepository;
import ar.com.thinkup.luviam.repository.RubroRepository;
import ar.com.thinkup.luviam.repository.TipoClienteRepository;
import ar.com.thinkup.luviam.repository.TipoComisionistaRepository;
import ar.com.thinkup.luviam.repository.TipoComprobanteRepository;
import ar.com.thinkup.luviam.repository.TipoCuentaProveedorRepository;
import ar.com.thinkup.luviam.repository.TipoEmpleadoRepository;
import ar.com.thinkup.luviam.repository.TipoIVARepository;
import ar.com.thinkup.luviam.repository.TipoProveedorRepository;
import ar.com.thinkup.luviam.repository.UnidadRepository;
import ar.com.thinkup.luviam.repository.UsuarioRepository;

@RestController()
@RequestMapping(path = "/1n1th1nkupdb")
public class InitDBController {

	@Autowired
	private UsuarioRepository userRepo;

	@Autowired
	private TipoComisionistaRepository tcRepo;

	@Autowired
	private RolRepository rolRepo;

	@Autowired
	private EmailTemplateRepository templateRepository;

	@Autowired
	private TipoComprobanteRepository tipoComprobanteRepo;

	@Autowired
	private RubroRepository rubroRepository;

	@Autowired
	private CentroRepository centroRepository;

	@Autowired
	private TipoClienteRepository tipoClienteRepository;

	@Autowired
	private TipoIVARepository tipoIVARepository;

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private TipoCuentaProveedorRepository tipoCuentaProveedorRepository;

	@Autowired
	private TipoProveedorRepository tipoProveedorRepository;

	@Autowired
	private ProveedorRepository proveedorRepository;

	@Autowired
	private TipoEmpleadoRepository tipoEmpleadoRepository;

	@Autowired
	private UnidadRepository uniRepo;

	@Autowired
	private CategoriaProductoRepository catProducto;

	@GetMapping
	public void inicializarBase() {


		this.generarRoles();

		this.generarUsuariosDefault();

		if (templateRepository.count() <= 0) {
			EmailTemplate et = new EmailTemplate();
			et.setCodigo("RECOVER");
			et.setTemplate("Hola, entra a http://localhost:4200/#/public/cambiar-password/:email/:token");
			templateRepository.save(et);
		}

		// Doy de alta los comprobantes comunes.
		if (this.tipoComprobanteRepo.count() <= 0) {
			List<TipoComprobante> comprobantes = new Vector<>();
			TipoComprobante t1 = new TipoComprobante("FA", "Factura A");
			TipoComprobante t2 = new TipoComprobante("FB", "Factura B");
			TipoComprobante t3 = new TipoComprobante("FC", "Factura C");
			TipoComprobante t4 = new TipoComprobante("RE", "Recibo");

			comprobantes.add(t1);
			comprobantes.add(t2);
			comprobantes.add(t3);
			comprobantes.add(t4);

			this.tipoComprobanteRepo.save(comprobantes);
		}

		// Doy de alta plan template
		
		Set<Rubro> plan = this.crearPlanInicial(null);
		

		// Doy de alta un centro de prueba
		if (this.centroRepository.count() <= 0) {
			CentroCosto c = new CentroCosto();

			List<TipoComprobante> comprobantes = this.tipoComprobanteRepo.findAll();
			Set<CentroCostosTipoComprobante> ccc = comprobantes.parallelStream()
					.map(ta -> new CentroCostosTipoComprobante(ta, "0", true)).collect(Collectors.toSet());

			c.setActivo(true);
			c.setComprobantesDisponibles(ccc);
			c.setCuit("20-31117665-0");
			c.setDomicilioFiscal("Av. San Juan 2764 7 A");
			c.setIibb("222-121212-5");
			c.setNombre("Centro de prueba Dario Camarro");
			c.setRazonSocial("Centro Dario Camarro");

			this.centroRepository.save(c);

		}

		// Creo tipo Cliente
		if (this.tipoClienteRepository.count() <= 0) {
			List<TipoCliente> comprobantes = new Vector<>();
			comprobantes.add(new TipoCliente("SO", "Social"));
			comprobantes.add(new TipoCliente("CO", "Corporativo"));
			comprobantes.add(new TipoCliente("OR", "Organizador"));
			comprobantes.add(new TipoCliente("IN", "Interno"));
			comprobantes.add(new TipoCliente("SA", "Salón"));
			this.tipoClienteRepository.save(comprobantes);
		}

		// Creo tipo Iva
		if (this.tipoIVARepository.count() <= 0) {
			List<TipoIVA> comprobantes = new Vector<>();
			comprobantes.add(new TipoIVA(true, "RI", "Responsable Inscripto", false));
			comprobantes.add(new TipoIVA(true, "EX", "Sujeto Excento", false));
			comprobantes.add(new TipoIVA(true, "CF", "Consumidor Final", false));
			comprobantes.add(new TipoIVA(true, "MO", "Monotributista", false));
			comprobantes.add(new TipoIVA(true, "PE", "Proveedor del Exterior", false));
			comprobantes.add(new TipoIVA(true, "CE", "Cliente del Exterior", false));
			comprobantes.add(new TipoIVA(true, "LI", "IVA Liberado - Ley 19.640", false));
			comprobantes.add(new TipoIVA(true, "MS", "Monotributo Social", false));
			comprobantes.add(new TipoIVA(true, "NR", "IVA No Alcanzado", false));
			this.tipoIVARepository.save(comprobantes);
		}

		// Creo un Cliente de prueba
		if (this.clienteRepository.count() <= 0) {

			Cliente cliente = new Cliente();
			cliente.setActivo(true);
			cliente.setNombreCliente("Dario Camarro");
			cliente.setCuit("20-31117665-0");
			cliente.setDomicilio("San Juan 2764");

			TipoIVA tipoIva = this.tipoIVARepository.findAll().get(0);
			cliente.setIva(tipoIva);

			TipoCliente tipoCliente = this.tipoClienteRepository.findAll().get(0);
			cliente.setTipoCliente(tipoCliente);
			this.clienteRepository.save(cliente);
		}

		// Creo tipo Proveedor
		if (this.tipoProveedorRepository.count() <= 0) {
			List<TipoProveedor> comprobantes = new Vector<>();
			comprobantes.add(new TipoProveedor("I", "Inscripto"));
			comprobantes.add(new TipoProveedor("N", "No Inscripto"));
			this.tipoProveedorRepository.save(comprobantes);
		}

		// Creo tipo Cuenta Proveedor
		if (this.tipoCuentaProveedorRepository.count() <= 0) {
			List<TipoCuentaProveedor> comprobantes = new Vector<>();
			comprobantes.add(new TipoCuentaProveedor("CC", "Con Crédito"));
			comprobantes.add(new TipoCuentaProveedor("SC", "Sin Crédito"));
			comprobantes.add(new TipoCuentaProveedor("NH", "No Habituales"));
			this.tipoCuentaProveedorRepository.save(comprobantes);
		}

		// Creo un Proveedor de prueba
		if (this.proveedorRepository.count() <= 0) {

			Proveedor proveedor = new Proveedor();
			proveedor.setActivo(true);
			proveedor.setNombreProveedor("Dario Camarro");
			proveedor.setCuit("20-31117665-0");
			proveedor.setDomicilio("San Juan 2764");

			TipoIVA tipoIva = this.tipoIVARepository.findAll().get(0);
			proveedor.setIva(tipoIva);

			TipoProveedor tipoProveedor = this.tipoProveedorRepository.findAll().get(0);
			proveedor.setTipoProveedor(tipoProveedor);

			TipoCuentaProveedor tipoCuentaProveedor = this.tipoCuentaProveedorRepository.findAll().get(0);
			proveedor.setTipoCuenta(tipoCuentaProveedor);
			this.proveedorRepository.save(proveedor);
		}

		// Creo tipo Proveedor
		if (this.tipoEmpleadoRepository.count() <= 0) {
			List<TipoEmpleado> tiposEmpleado = new Vector<>();
			tiposEmpleado.add(new TipoEmpleado(true, "VEND", "Vendedor", true));
			this.tipoEmpleadoRepository.save(tiposEmpleado);
		}

		// creo categorias de prueba
		if (this.catProducto.count() <= 0) {
			List<CategoriaProducto> cate = new Vector<>();
			cate.add(new CategoriaProducto(true, "M", "Menu", true));
			cate.add(new CategoriaProducto(true, "A", "Alquileres", true));
			cate.add(new CategoriaProducto(true, "B", "Bebidas", true));
			this.catProducto.save(cate);
		}

		// creo Unidad de prueba
		if (this.uniRepo.count() <= 0) {
			List<UnidadProducto> cate = new Vector<>();
			cate.add(new UnidadProducto(true, "U", "Unidad", true));
			cate.add(new UnidadProducto(true, "C", "Cubierto", true));
			cate.add(new UnidadProducto(true, "KG", "Peso", true));
			cate.add(new UnidadProducto(true, "HS", "Horas", true));

			this.uniRepo.save(cate);
		}

		this.updateTipoComisionistas();

	}

	private void generarUsuariosDefault() {

		List<Rol> roles = this.rolRepo.findAll();
		List<Usuario> usuarios = new Vector<>();
		Usuario u;

		for (Rol r : roles) {
			u = this.userRepo.findByUsername(r.getDescripcion().toLowerCase());
			if (u == null) {
				u = new Usuario(r.getDescripcion().toLowerCase(), DigestUtils.md5DigestAsHex(r.getCodigo().getBytes()),
						r.getDescripcion(), "", "", true, Arrays.asList(r));
				usuarios.add(u);
			}

		}

		if (usuarios.size() > 0)
			userRepo.save(usuarios);

	}

	private void generarRoles() {

		List<Rol> roles = new Vector<>();
		Rol rol;

		if (this.rolRepo.findByCodigo(Rol.SUPER_USER) == null) {
			rol = new Rol();
			rol.setActivo(true);
			rol.setCodigo(Rol.SUPER_USER);
			rol.setDescripcion("Superusuario");
			roles.add(rol);
		}
		if (this.rolRepo.findByCodigo(Rol.ADMINISTRATIVO) == null) {
			rol = new Rol();
			rol.setActivo(true);
			rol.setCodigo(Rol.ADMINISTRATIVO);
			rol.setDescripcion("Administrativo");
			roles.add(rol);
		}
		if (this.rolRepo.findByCodigo(Rol.VENTAS) == null) {
			rol = new Rol();
			rol.setActivo(true);
			rol.setCodigo(Rol.VENTAS);
			rol.setDescripcion("Ventas");
			roles.add(rol);
		}
		if (this.rolRepo.findByCodigo(Rol.COMPRAS) == null) {
			rol = new Rol();
			rol.setActivo(true);
			rol.setCodigo(Rol.COMPRAS);
			rol.setDescripcion("Compras");
			roles.add(rol);
		}
		if (this.rolRepo.findByCodigo(Rol.GERENCIA) == null) {

			rol = new Rol();
			rol.setActivo(true);
			rol.setCodigo(Rol.GERENCIA);
			rol.setDescripcion("Gerencia");
			roles.add(rol);
		}
		if (roles.size() > 0)
			this.rolRepo.save(roles);

	}

	private Set<Rubro> crearPlanInicial(CentroCosto c) {
		Set<Rubro> rubros = new TreeSet<>();
		Rubro r = new Rubro();
		Rubro r2 = new Rubro();

		Set<Grupo> grupos1 = new TreeSet<>();
		Set<Grupo> grupos2 = new TreeSet<>();
		Set<Grupo> grupos3 = new TreeSet<>();

		Set<SubGrupo> sgrupos = new TreeSet<>();
		Set<SubGrupo> sgrupos2 = new TreeSet<>();

		Grupo g1 = new Grupo();
		Grupo g2 = new Grupo();
		Grupo g3 = new Grupo();
		Grupo g4 = new Grupo();
		Grupo g5 = new Grupo();

		// Primer rubro
		
		SubGrupo sg1 = new SubGrupo();
		sg1.setActivo(true);
		sg1.setCodigo("A.1.1");
		sg1.setNombre("Caja");
		sg1.setGrupo(g1);
		sg1.setEliminable(false);

		SubGrupo sg2 = new SubGrupo();
		sg2.setActivo(true);
		sg2.setCodigo("A.1.2");
		sg2.setNombre("Bancos");
		sg2.setGrupo(g1);
		sg2.setEliminable(false);

		SubGrupo sg13 = new SubGrupo();
		sg2.setActivo(true);
		sg2.setCodigo("A.1.3");
		sg2.setNombre("Cheques en Cartera");
		sg2.setGrupo(g1);
		sg2.setEliminable(false);

		sgrupos.add(sg1);
		sgrupos.add(sg2);
		sgrupos.add(sg13);

		g1.setActivo(true);
		g1.setNombre("Activo Corriente");
		g1.setCodigo("A.1");
		g1.setRubro(r);
		g1.setSubGrupos(sgrupos);
		g1.setEliminable(false);

		SubGrupo sg3 = new SubGrupo();
		sg3.setActivo(true);
		sg3.setCodigo("EVE_REA");
		sg3.setNombre("Eventos a Cobrar");
		sg3.setGrupo(g2);
		sg3.setEliminable(false);

		sgrupos2.add(sg3);

		SubGrupo sgCreditoFiscal = new SubGrupo();
		sgCreditoFiscal.setActivo(true);
		sgCreditoFiscal.setCodigo(CuentasPrincipalesEnum.CREDITO_FISCAL.getCodigo());
		sgCreditoFiscal.setNombre("Crédito Fiscal");
		sgCreditoFiscal.setGrupo(g1);
		sgCreditoFiscal.setEliminable(false);

		sgrupos2.add(sgCreditoFiscal);

		g2.setActivo(true);
		g2.setNombre("Activo No Corriente");
		g2.setCodigo("A.2");
		g2.setRubro(r);
		g2.setEliminable(false);
		g2.setSubGrupos(sgrupos2);

		grupos1.add(g1);
		grupos1.add(g2);

		r.setActivo(true);
		r.setNombre("Activo");
		r.setCodigo("A");
		r.setEliminable(false);
		r.setGrupos(grupos1);
		r.setEliminable(false);

		rubros.add(r);

		// Segundo rubro
		g3.setActivo(true);
		g3.setNombre("Pasivo Corriente");
		g3.setCodigo("P.1");
		g3.setEliminable(false);
		g3.setRubro(r2);
		g3.setEliminable(false);

		SubGrupo sgCCProveedores = new SubGrupo();
		sgCCProveedores.setActivo(true);
		sgCCProveedores.setNombre("Cuenta Corriente Proveedores");
		sgCCProveedores.setCodigo(CuentasPrincipalesEnum.CC_PROVEEDORES.getCodigo());
		sgCCProveedores.setGrupo(g3);
		sgCCProveedores.setEliminable(false);
		
		SubGrupo debitoFiscal = new SubGrupo();
		debitoFiscal.setNombre("Débitos Fiscales");
		debitoFiscal.setCodigo(CuentasPrincipalesEnum.DEBITOS_FISCALES.getCodigo());
		debitoFiscal.setGrupo(g3);
		debitoFiscal.setEliminable(false);

		g4.setActivo(true);
		g4.setNombre("Pasivo No Corriente");
		g4.setCodigo("P.2");
		g4.setEliminable(false);
		g4.setRubro(r2);
		g4.setEliminable(false);

		SubGrupo unidadNegocioSg = new SubGrupo();
		unidadNegocioSg.setNombre("Unidades de Negocio");
		unidadNegocioSg.setCodigo(CuentasPrincipalesEnum.SG_UNIDADES_NEGOCIO.getCodigo());
		unidadNegocioSg.setGrupo(g4);
		unidadNegocioSg.setEliminable(false);

		
		grupos2.add(g3);
		grupos2.add(g4);

		r2.setActivo(true);
		r2.setNombre("Pasivo");
		r2.setCodigo("P");
		r2.setEliminable(false);
		r2.setGrupos(grupos2);


		

		Rubro r3 = new Rubro();
		r3.setActivo(true);
		r3.setNombre("Patrimonio Neto");
		r3.setCodigo("PN");
		r3.setEliminable(false);
		r3.setGrupos(grupos3);

		// Tercer rubro
		g5.setActivo(true);
		g5.setNombre("Capital");
		g5.setCodigo("PN.1");
		g5.setRubro(r3);
		g5.setEliminable(false);
		grupos3.add(g5);

		Rubro rEgresos = new Rubro();
		rEgresos.setActivo(true);
		rEgresos.setNombre("Egresos");
		rEgresos.setCodigo(CuentasPrincipalesEnum.EGRESOS.getCodigo());
		rEgresos.setEliminable(false);
		rEgresos.setGrupos(grupos3);

		rubros.add(rEgresos);

		return rubros;

	}

	private void updateTipoComisionistas() {
		List<CategoriaProducto> categoriasComisionables = this.catProducto.findByEsComisionableIsTrue();
		if (this.tcRepo.count() == 0) {
			List<TipoComisionista> tipos = new Vector<>();
			tipos.add(new TipoComisionista(null, true, "VEND", "Vendedor", true, categoriasComisionables));
			tipos.add(new TipoComisionista(null, true, "SALON", "Salón", true, categoriasComisionables));
			tipos.add(new TipoComisionista(null, true, "ORGA", "Organizador", false, categoriasComisionables));
			tipos.add(new TipoComisionista(null, true, "CONT", "Contacto", false, categoriasComisionables));
			this.tcRepo.save(tipos);

		}

	}
}

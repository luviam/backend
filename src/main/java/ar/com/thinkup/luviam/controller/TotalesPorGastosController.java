package ar.com.thinkup.luviam.controller;

import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.repository.OperacionesRepository;
import ar.com.thinkup.luviam.vo.TotalesGastosVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroGastos;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/totales_gastos")
@PreAuthorize("isAuthenticated()")
public class TotalesPorGastosController {

	private static final Logger LOG = LogManager.getLogger(TotalesPorGastosController.class);
	@Autowired
	private OperacionesRepository repo;

	@PostMapping("/buscar")
	@PreAuthorize("hasRole('ROLE_GERENCIA')")
	public ResponseEntity<ResponseContainer<List<TotalesGastosVO>>> getTotalesPorGasto(
			@RequestBody FiltroGastos filtros) {
		ResponseContainer<List<TotalesGastosVO>> respuesta = new ResponseContainer<>();
		try {
			List<TotalesGastosVO> totales = repo.getTotalesPorGastos(filtros.getCodigoCentro(), filtros.getCodigoGrupo(),
					filtros.getCodigoSubGrupo(), filtros.getFechaDesde(), filtros.getFechaHasta());

			respuesta.setRespuesta(totales);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando los totales ", e);
			respuesta.setMensaje("Hubo un error buscando totales.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

}

package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.TipoMenu;
import ar.com.thinkup.luviam.service.def.ITipoMenuService;
import ar.com.thinkup.luviam.vo.TipoMenuVO;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/tipomenu")
@PreAuthorize("isAuthenticated()")
public class TipoMenuController extends ParametricosController<TipoMenuVO, TipoMenu> {

	private static final Logger LOG = LogManager.getLogger(TipoMenuController.class);


	@Autowired
	private ITipoMenuService TipoMenuService;

	@GetMapping("/parametrico/all")
	public ResponseEntity<ResponseContainer<List<ParametricoVO>>> getParametricos() {
		return super.getParametricos();
	}

	@GetMapping("/all")
	public ResponseEntity<ResponseContainer<List<TipoMenuVO>>> getAll() {
		return super.getAll();
	}
	
	@PutMapping()
	public ResponseEntity<ResponseContainer<TipoMenuVO>> create(@RequestBody TipoMenuVO vo) {
		return super.create(vo);
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<TipoMenuVO>> update(@RequestBody TipoMenuVO vo) {
		return super.update(vo);
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		return super.delete(id);
	}

	@Override
	protected ITipoMenuService getService() {
		return this.TipoMenuService;
	}

	@Override
	protected Logger getLog() {
		return TipoMenuController.LOG;
	}

	@Override
	protected String getNombre() {

		return "Tipo Menu";
	}
}

package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.service.def.IClienteService;
import ar.com.thinkup.luviam.vo.ClienteListaVO;
import ar.com.thinkup.luviam.vo.ClienteVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/cliente")
@PreAuthorize("isAuthenticated()")
public class ClienteController {

	private static final Logger LOG = LogManager.getLogger(ClienteController.class);

	@Autowired
	private IClienteService clienteService;

	@GetMapping("/cabecera")
	public ResponseEntity<ResponseContainer<List<ClienteListaVO>>> getCabeceras() {
		ResponseContainer<List<ClienteListaVO>> respuesta = new ResponseContainer<>();
		try {
			List<ClienteListaVO> clientesCab = clienteService.getCabeceras();

			respuesta.setRespuesta(clientesCab);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando clientes", e);
			respuesta.setMensaje("Hubo un error buscando clientes.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@GetMapping("/{id}")
	public ResponseEntity<ResponseContainer<ClienteVO>> getClienteById(@PathVariable Long id) {
		ResponseContainer<ClienteVO> respuesta = new ResponseContainer<>();
		try {
			ClienteVO cliente = this.clienteService.getByID(id);
			respuesta.setRespuesta(cliente);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando el cliente", e);
			respuesta.setMensaje("Hubo un error buscando el cliente.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PutMapping()
	public ResponseEntity<ResponseContainer<ClienteVO>> crear(@RequestBody ClienteVO cliente) {
		ResponseContainer<ClienteVO> respuesta = new ResponseContainer<>();

		try {
			ClienteVO c = this.clienteService.create(cliente);
			LOG.info("Se guardó exitosamente el cliente");
			respuesta.setRespuesta(c);
			respuesta.setMensaje("Se guardó exitosamente el Cliente");
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error guardando el Cliente";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			respuesta.setRespuesta(cliente);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<ClienteVO>> actualizarCentro(@RequestBody ClienteVO cliente) {
		ResponseContainer<ClienteVO> respuesta = new ResponseContainer<>();

		try {

			ClienteVO vo = this.clienteService.update(cliente);
			LOG.info("Se guardó exitosamente el cliente");
			respuesta.setRespuesta(vo);
			respuesta.setMensaje("Se guardó exitosamente el cliente");
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error guardando el Cliente", e);
			respuesta.setMensaje("Hubo un error guardando el Cliente");
			respuesta.setRespuesta(cliente);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") String id) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();
		try {
			ClienteVO c = new ClienteVO();
			c.setId(Long.valueOf(id));
			c = this.clienteService.delete(c);
			respuesta.setMensaje("Se elimino el cliente correctamente.");
			respuesta.setRespuesta(true);
			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setMensaje("Hubo un error eliminando el cliente.");
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

}

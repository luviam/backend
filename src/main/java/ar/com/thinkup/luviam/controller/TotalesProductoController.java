package ar.com.thinkup.luviam.controller;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.reportes.TotalesProductos;
import ar.com.thinkup.luviam.repository.TotalesProductoRepo;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/totales_producto")
//@PreAuthorize("isAuthenticated()")
public class TotalesProductoController {



	private static final Logger LOG = LogManager.getLogger(TotalesProductoController.class);
	@Autowired
	private TotalesProductoRepo repo;

	@GetMapping("/buscar/{desde}/{hasta}/{centro}/{c_producto}/{c_categoria}")
	public ResponseEntity<ResponseContainer<List<TotalesProductos>>> getCabeceras(
			@PathVariable("desde") Date desde,@PathVariable("hasta") Date hasta,
			@PathVariable("centro") String centro,
			@PathVariable("c_producto") String cProducto,
			@PathVariable("c_categoria") String cCategoria
			
			) {
		ResponseContainer<List<TotalesProductos>> respuesta = new ResponseContainer<>();
		try {
			List<TotalesProductos> totales = repo.totalesPorProducto(desde, hasta, centro, cProducto, cCategoria);

			respuesta.setRespuesta(totales);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando clientes", e);
			respuesta.setMensaje("Hubo un error buscando clientes.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

}

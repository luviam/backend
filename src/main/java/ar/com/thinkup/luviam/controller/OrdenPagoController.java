package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.service.def.IOrdenPagoService;
import ar.com.thinkup.luviam.vo.OrdenPagoCabeceraVO;
import ar.com.thinkup.luviam.vo.documentos.pagos.ItemOrdenPagoVO;
import ar.com.thinkup.luviam.vo.documentos.pagos.OrdenPagoVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroProveedor;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/ordenPago")
@PreAuthorize("isAuthenticated()")
public class OrdenPagoController {

	private static final Logger LOG = LogManager.getLogger(OrdenPagoController.class);

	@Autowired
	private IOrdenPagoService ordenesPagoService;

	@GetMapping(path = "/{id}")
	public ResponseEntity<ResponseContainer<OrdenPagoVO>> getById(@PathVariable("id") Long idOrdenPago) {
		ResponseContainer<OrdenPagoVO> respuesta = new ResponseContainer<>();
		String m = "Hubo un error al buscar la Orden de Pago";
		try {

			respuesta.setRespuesta(this.ordenesPagoService.findById(idOrdenPago));
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);
		} catch (Exception e) {
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(path = "/items/{id}")
	public ResponseEntity<ResponseContainer<List<ItemOrdenPagoVO>>> getItemsByOrden(
			@PathVariable("id") Long idOrdenPago) {
		ResponseContainer<List<ItemOrdenPagoVO>> respuesta = new ResponseContainer<>();
		String m = "Hubo un error al buscar la Orden de Pago";
		try {

			respuesta.setRespuesta(this.ordenesPagoService.getItems(idOrdenPago));
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);
		} catch (Exception e) {
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping(path = "/cambioEstado")
	public ResponseEntity<ResponseContainer<OrdenPagoCabeceraVO>> cambioEstadoOrdenPago(Authentication authentication,
			@RequestBody OrdenPagoCabeceraVO cabeceraOrdenPago) {

		ResponseContainer<OrdenPagoCabeceraVO> respuesta = new ResponseContainer<>();
		String m = "Hubo un error actualizando la Orden de Pago";
		if (cabeceraOrdenPago.getId() == null) {
			String s = "La OrdenPago debe tener id";
			LOG.error(s);
			respuesta.setMensaje(s);
			respuesta.setRespuesta(null);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} else {
			try {
				EstadoOperacionEnum e = EstadoOperacionEnum.getByCodigo(cabeceraOrdenPago.getEstado());
				DescriptivoGeneric estado = new DescriptivoGeneric(e.getCodigo(), e.getDescripcion());
				if (this.ordenesPagoService.cambioEstadoDocumento(cabeceraOrdenPago.getId(), estado,
						(Usuario) authentication.getPrincipal())) {
					cabeceraOrdenPago.setEstado(e);
					respuesta.setRespuesta(cabeceraOrdenPago);

					return ResponseEntity.status(HttpStatus.OK).body(respuesta);
				} else {

					LOG.error(m, e);
					respuesta.setMensaje(m);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
				}

			} catch (Exception e) {
				LOG.error(m, e);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}

		}

	}

	@PostMapping("/cabeceras/")
	public ResponseEntity<ResponseContainer<List<OrdenPagoCabeceraVO>>> getCabeceraOrdenPagos(
			@RequestBody FiltroProveedor filtro) {

		ResponseContainer<List<OrdenPagoCabeceraVO>> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.ordenesPagoService.getCabeceras(filtro));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando las Ordenes de Pago";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@PutMapping()
	public ResponseEntity<ResponseContainer<OrdenPagoVO>> crearOrdenPago(Authentication authentication,
			@RequestBody OrdenPagoVO ordenPago) {
		ResponseContainer<OrdenPagoVO> respuesta = new ResponseContainer<>();

		if (ordenPago.getId() == null) {
			try {
				OrdenPagoVO nueva = this.ordenesPagoService.guardarDocumento(ordenPago,
						(Usuario) authentication.getPrincipal());
				String m = "Se guardó exitosamente la orden de pago";
				LOG.info(m);
				respuesta.setRespuesta(nueva);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			} catch (Exception e) {
				String m = "Hubo un error guardando la orden de pago";
				LOG.error(m, e);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
		} else {
			String m = "Ya existe la orden de pago.";
			LOG.warn(m);
			respuesta.setMensaje(m);
			respuesta.setRespuesta(ordenPago);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<OrdenPagoVO>> actualizarOrdenPago(Authentication authentication,
			@RequestBody OrdenPagoVO ordenPago) {
		ResponseContainer<OrdenPagoVO> respuesta = new ResponseContainer<>();

		if (ordenPago.getId() != null) {
			try {
				OrdenPagoVO nueva = this.ordenesPagoService.guardarDocumento(ordenPago,
						(Usuario) authentication.getPrincipal());
				String m = "Se guardó exitosamente la Orden de Pago";
				LOG.info(m);
				respuesta.setRespuesta(nueva);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			} catch (Exception e) {
				String m = "Hubo un error guardando la orden de pago";
				LOG.error(m, e);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
		} else {
			String m = "No existe la orden de pago.";
			LOG.warn(m);
			respuesta.setMensaje(m);
			respuesta.setRespuesta(ordenPago);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(Authentication authentication,
			@RequestParam("id") String id) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();
		Usuario responsable = (Usuario) authentication.getPrincipal();

		try {
			this.ordenesPagoService.borrar(Long.valueOf(id), responsable);

			respuesta.setMensaje("OrdenPago eliminada correctamente");
			respuesta.setRespuesta(true);
			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			LOG.error(e.getMessage());
			respuesta.setMensaje("Hubo un error eliminando la Orden de Pago.");
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

}

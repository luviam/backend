package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.service.def.IContratoService;
import ar.com.thinkup.luviam.vo.ContratoCabeceraVO;
import ar.com.thinkup.luviam.vo.contratos.ContratoVO;
import ar.com.thinkup.luviam.vo.contratos.CuotaServicioVO;
import ar.com.thinkup.luviam.vo.contratos.EstadoIVAVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController
@RequestMapping(path = "/contrato")
@PreAuthorize("isAuthenticated()")
public class ContratoController {

	private static final Logger LOG = LogManager.getLogger(ContratoController.class);

	@Autowired
	private IContratoService service;

	@GetMapping("/all")
	public ResponseEntity<ResponseContainer<List<ContratoVO>>> getAll() {

		ResponseContainer<List<ContratoVO>> respuesta = new ResponseContainer<>();
		try {
			List<ContratoVO> lista = this.service.getAll();

			respuesta.setRespuesta(lista);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error en la busqueda";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/estadoIva/{id}")
	public ResponseEntity<ResponseContainer<EstadoIVAVO>> getEstadoIVA(@PathVariable("id") Long idContrato) {

		ResponseContainer<EstadoIVAVO> respuesta = new ResponseContainer<>();
		try {

			respuesta.setRespuesta(this.service.getEstadoIVA(idContrato));
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error en la busqueda";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/cabecera/all")
	public ResponseEntity<ResponseContainer<List<ContratoCabeceraVO>>> getAllCabeceras() {

		ResponseContainer<List<ContratoCabeceraVO>> respuesta = new ResponseContainer<>();
		try {
			List<ContratoCabeceraVO> lista = this.service.getAllCabecera();

			respuesta.setRespuesta(lista);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error en la busqueda";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/byCliente/{idCliente}")
	public ResponseEntity<ResponseContainer<List<ContratoCabeceraVO>>> getAllByCliente(
			@PathVariable("idCliente") Long idCliente) {

		ResponseContainer<List<ContratoCabeceraVO>> respuesta = new ResponseContainer<>();
		try {
			List<ContratoCabeceraVO> lista = this.service.getAllByIdCliente(idCliente);

			respuesta.setRespuesta(lista);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error en la busqueda";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("descriptivo/byCliente/{idCliente}")
	public ResponseEntity<ResponseContainer<List<DescriptivoGeneric>>> getAllByClienteDescriptivo(
			@PathVariable("idCliente") Long idCliente) {

		ResponseContainer<List<DescriptivoGeneric>> respuesta = new ResponseContainer<>();
		try {
			List<DescriptivoGeneric> lista = this.service.getAllDescriptivosByIdCliente(idCliente);

			respuesta.setRespuesta(lista);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error en la busqueda";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("cuotas/{codigoContrato}")
	public ResponseEntity<ResponseContainer<List<CuotaServicioVO>>> getCuotasContrato(
			@PathVariable("codigoContrato") String codigoContrato) {

		ResponseContainer<List<CuotaServicioVO>> respuesta = new ResponseContainer<>();
		try {
			List<CuotaServicioVO> lista = this.service.getCuotas(codigoContrato);

			respuesta.setRespuesta(lista);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error en la busqueda";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("cuotas/impagas/{codigoContrato}")
	public ResponseEntity<ResponseContainer<List<CuotaServicioVO>>> getCuotasImpagas(
			@PathVariable("codigoContrato") String codigoContrato) {

		ResponseContainer<List<CuotaServicioVO>> respuesta = new ResponseContainer<>();
		try {
			List<CuotaServicioVO> lista = this.service.getCuotasImpagas(codigoContrato);

			respuesta.setRespuesta(lista);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error en la busqueda";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<ResponseContainer<ContratoVO>> getById(@PathVariable Long id) {

		ResponseContainer<ContratoVO> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.service.getByID(id));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando la Entidad";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PutMapping()
	@Transactional
	public ResponseEntity<ResponseContainer<ContratoVO>> create(@RequestBody ContratoVO vo) {
		ResponseContainer<ContratoVO> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.service.create(vo));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error creando el Contrato";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<ContratoVO>> update(@RequestBody ContratoVO vo) {
		ResponseContainer<ContratoVO> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.service.update(vo));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error guardando el Contrato";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping(path = "/aprobar")
	public ResponseEntity<ResponseContainer<ContratoVO>> aprobar(@RequestBody Long idContrato) {
		ResponseContainer<ContratoVO> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.service.aprobar(idContrato));
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error guardando el Contrato";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping(path = "/rechazar")
	public ResponseEntity<ResponseContainer<ContratoVO>> rechazar(@RequestBody Long idContrato) {
		ResponseContainer<ContratoVO> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.service.rechazar(idContrato));
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error guardando el Contrato";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping(path = "/reaplicar")
	public ResponseEntity<ResponseContainer<ContratoVO>> reaplicar(@RequestBody Long idContrato) {
		ResponseContainer<ContratoVO> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.service.reaplicar(idContrato));
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error guardando el Contrato";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping(path = "/liquidar")
	public ResponseEntity<ResponseContainer<ContratoVO>> liquidar(@RequestBody Long idContrato) {
		ResponseContainer<ContratoVO> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.service.liquidar(idContrato));
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error guardando el Contrato";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") String id) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.service.delete(Long.valueOf(id)));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error eliminando el Contrato";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/byComisionista/{idComisionista}")
	public ResponseEntity<ResponseContainer<List<ContratoCabeceraVO>>> getAllByComisionista(
			@PathVariable("idComisionista") Long idComisionista) {

		ResponseContainer<List<ContratoCabeceraVO>> respuesta = new ResponseContainer<>();
		try {
			List<ContratoCabeceraVO> lista = this.service.getAllByIdComisionista(idComisionista);

			respuesta.setRespuesta(lista);
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error en la busqueda";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

}
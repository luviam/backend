package ar.com.thinkup.luviam.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.Operacion;
import ar.com.thinkup.luviam.model.TotalesCuentasView;
import ar.com.thinkup.luviam.model.enums.CuentasPrincipalesEnum;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.repository.ChequeraRepository;
import ar.com.thinkup.luviam.repository.CuentaAplicacionRepository;
import ar.com.thinkup.luviam.repository.OperacionesRepository;
import ar.com.thinkup.luviam.repository.TotalCuentasViewRepository;
import ar.com.thinkup.luviam.service.def.ICuentaService;
import ar.com.thinkup.luviam.vo.CuentaAplicacionVO;
import ar.com.thinkup.luviam.vo.CuentaConfig;
import ar.com.thinkup.luviam.vo.OperacionVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/cuentas")
@PreAuthorize("isAuthenticated()")
public class CuentasController {

	private static final Logger LOG = LogManager.getLogger(CuentasController.class);

	@Autowired
	private CuentaAplicacionRepository cuentasRepo;

	@Autowired
	private OperacionesRepository operacionesRepo;

	@Autowired
	private TotalCuentasViewRepository totalesRepo;

	@Autowired
	private ChequeraRepository chequeraRepo;
	@Autowired
	private ICuentaService cuentaService;

	@GetMapping(path = "/{id}")
	@Transactional()
	public ResponseEntity<ResponseContainer<CuentaAplicacionVO>> getChequera(@PathVariable Long id) {
		ResponseContainer<CuentaAplicacionVO> respuesta = new ResponseContainer<>();
		try {
			CuentaAplicacion cuenta = this.cuentasRepo.findOne(id);
			if (cuenta == null) {
				LOG.error("No existe chequera con ID: " + id);
				respuesta.setMensaje("Hubo un error buscando la cuenta.");
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
			CuentaAplicacionVO chq = new CuentaAplicacionVO(cuenta);
			respuesta.setRespuesta(chq);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando la cuenta.", e);
			respuesta.setMensaje("Hubo un error buscando la cuenta.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@GetMapping("/operaciones/{desde}/{hasta}/{cuenta}/{centro}")
	@Transactional
	public ResponseEntity<ResponseContainer<List<OperacionVO>>> getCabeceras(@PathVariable("desde") String desde,
			@PathVariable("hasta") String hasta, @PathVariable("cuenta") Long cuenta, @PathVariable("centro") Long centro) {
		ResponseContainer<List<OperacionVO>> respuesta = new ResponseContainer<>();
		try {
			SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd hh:mm");
			Date desdeDate;
			try {
				desdeDate = sf.parse(desde);
			} catch (ParseException e) {
				desdeDate = new DateTime().minusYears(1).withTimeAtStartOfDay().toDate();
				// TODO: Buscar fecha de cierre de ultimo balance
			}
			Date hastaDate;
			try {
				hastaDate = sf.parse(hasta);
			} catch (ParseException e) {
				hastaDate = new DateTime().plusDays(1).withTimeAtStartOfDay().minusSeconds(1).toDate();
			}
			
			centro = centro != null ? centro : -1;
			List<Operacion> asientos = this.operacionesRepo.getOperaciones(centro, desdeDate, hastaDate, cuenta,
					EstadoOperacionEnum.APROBADO.getCodigo());

			respuesta.setRespuesta(asientos.stream().map(o -> new OperacionVO(o)).collect(Collectors.toList()));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando Asientos";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/saldo/{idCuenta}/{hasta}")
	@Transactional
	public ResponseEntity<ResponseContainer<Double>> getSaldoCuenta(@PathVariable("idCuenta") Long idCuenta,
			@PathVariable("hasta") Date hasta) {
		ResponseContainer<Double> respuesta = new ResponseContainer<>();
		try {

			Date hastaFixed = hasta != null ? new DateTime(hasta).withTimeAtStartOfDay().toDate()
					: new DateTime().withTimeAtStartOfDay().toDate();
			respuesta.setRespuesta(this.operacionesRepo.getSaldo(idCuenta, hastaFixed));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando Asientos";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}
	
	@GetMapping("/totales/{desde}/{hasta}/{idCentro}")
	public ResponseEntity<ResponseContainer<List<TotalesCuentasView>>> getTotales(@PathVariable("desde") String desde,
			@PathVariable("hasta") String hasta, @PathVariable("idCentro") Long idCentro) {
		ResponseContainer<List<TotalesCuentasView>> respuesta = new ResponseContainer<>();
		try {
			SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd hh:mm");
			Date desdeDate;
			try {
				desdeDate = sf.parse(desde);
			} catch (ParseException e) {
				desdeDate = new DateTime().minusYears(1).withTimeAtStartOfDay().toDate();
				// TODO: Buscar fecha de cierre de ultimo balance
			}
			Date hastaDate;
			try {
				hastaDate = sf.parse(hasta);
			} catch (ParseException e) {
				hastaDate = new DateTime().plusDays(1).withTimeAtStartOfDay().minusSeconds(1).toDate();
			}
			Long centro = idCentro == null ? -1 : idCentro;

			List<TotalesCuentasView> asientos = this.totalesRepo.findByPeriodoAndCentro(desdeDate, hastaDate, centro);

			respuesta.setRespuesta(asientos);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando Totales";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/cuentasDefault")
	public ResponseEntity<ResponseContainer<List<CuentaConfig>>> getCuentasDefault() {
		ResponseContainer<List<CuentaConfig>> respuesta = new ResponseContainer<>();
		try {
			List<CuentaConfig> cuentas = this.cuentaService.getCuentasDefault();
					
			respuesta.setRespuesta(cuentas);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando cuentas Default";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/bancarias")
	@Transactional
	public ResponseEntity<ResponseContainer<List<CuentaAplicacionVO>>> getCuentasBancarias() {

		ResponseContainer<List<CuentaAplicacionVO>> respuesta = new ResponseContainer<>();
		try {
			List<CuentaAplicacion> cuentasBancarias = this.cuentasRepo
					.findBySubGrupoCodigoAndCentroCostoActivoTrueOrderByCodigoAsc(
							CuentasPrincipalesEnum.BANCOS.getCodigo());
			respuesta.setRespuesta(
					cuentasBancarias.stream().map(o -> new CuentaAplicacionVO(o)).collect(Collectors.toList()));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando cuentas bancarias";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@GetMapping("/chequesClientes")
	@Transactional
	public ResponseEntity<ResponseContainer<List<CuentaAplicacionVO>>> getCuentasChequesCliente() {

		ResponseContainer<List<CuentaAplicacionVO>> respuesta = new ResponseContainer<>();
		try {
			List<CuentaAplicacion> cuentasBancarias = this.cuentasRepo
					.findByCodigoAndCentroCostoActivoTrue(CuentasPrincipalesEnum.CHEQUES_CLIENTES.getCodigo());
			respuesta.setRespuesta(
					cuentasBancarias.stream().map(o -> new CuentaAplicacionVO(o)).collect(Collectors.toList()));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando cuentas de cheques de clientes";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@GetMapping("/chequesOtros")
	@Transactional
	public ResponseEntity<ResponseContainer<List<CuentaAplicacionVO>>> getCuentasChequesOtros() {

		ResponseContainer<List<CuentaAplicacionVO>> respuesta = new ResponseContainer<>();
		try {
			List<CuentaAplicacion> cuentasBancarias = this.cuentasRepo
					.findByCodigoAndCentroCostoActivoTrue(CuentasPrincipalesEnum.CHEQUES_OTROS.getCodigo());
			respuesta.setRespuesta(
					cuentasBancarias.stream().map(o -> new CuentaAplicacionVO(o)).collect(Collectors.toList()));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando cuentas de cheques de otros";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@GetMapping("/facturacion/{idCentro}")
	@Transactional
	public ResponseEntity<ResponseContainer<List<CuentaAplicacionVO>>> getCuentasFacturacion(
			@PathVariable("idCentro") Long idCentro) {

		ResponseContainer<List<CuentaAplicacionVO>> respuesta = new ResponseContainer<>();
		try {
			List<String> subgrupos = new Vector<>();
			subgrupos.add(CuentasPrincipalesEnum.BIENES_USO.getCodigo());

			List<CuentaAplicacion> cuentasBienesUso = new Vector<>();
			if (idCentro > 0) {
				cuentasBienesUso = this.cuentasRepo
						.findBySubGrupoCodigoInAndCentroCostoIdAndCentroCostoActivoTrueOrderByCodigoAsc(subgrupos,
								idCentro);
			} else {
				cuentasBienesUso = this.cuentasRepo
						.findBySubGrupoCodigoInAndCentroCostoActivoTrueOrderByCodigoAsc(subgrupos);
			}

			List<CuentaAplicacion> cuentasEgresos = new Vector<>();
			if (idCentro > 0) {
				cuentasEgresos = this.cuentasRepo
						.findBySubGrupoGrupoRubroCodigoAndCentroCostoIdAndCentroCostoActivoTrueOrderByCodigoAsc(
								CuentasPrincipalesEnum.EGRESOS.getCodigo(), idCentro);
			} else {
				cuentasEgresos = this.cuentasRepo
						.findBySubGrupoGrupoRubroCodigoAndCentroCostoActivoTrueOrderByCodigoAsc(
								CuentasPrincipalesEnum.EGRESOS.getCodigo());

			}
			List<CuentaAplicacion> cuentas = new Vector<>();
			cuentas.addAll(cuentasBienesUso);
			cuentas.addAll(cuentasEgresos);
			cuentas.sort((c1, c2) -> c1.getDescripcion().compareTo(c2.getDescripcion()));

			
			List<CuentaAplicacionVO> resultado = cuentas.stream().map(o -> new CuentaAplicacionVO(o)).collect(Collectors.toList());
			// CARGAR CUENTA PN.1.2.1 TEMPORAL
			if(idCentro > 0) {
				CuentaAplicacion ajuste = this.cuentasRepo.findOneByCentroCostoIdAndCentroCostoActivoTrueAndCodigo( idCentro,"PN.1.2.1");
				if(ajuste== null) {
					LOG.error("NO SE ENCUENTRA LA CUENTA PN.1.2.1");
				}else {
					resultado.add(new CuentaAplicacionVO(ajuste));
				}
			}else {
				List<CuentaAplicacion> ajustes = this.cuentasRepo.findByCodigoAndCentroCostoActivoTrue("PN.1.2.1");
				if(ajustes.size() <= 0) {
					LOG.error("NO SE ENCUENTRA LA CUENTA PN.1.2.1");
				}else {
					resultado.addAll(ajustes.stream().map(c -> new CuentaAplicacionVO(c)).collect(Collectors.toList()));
				}
			}
			
			
			respuesta.setRespuesta(resultado);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando cuentas de facturación";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@GetMapping("/cajas")
	@Transactional
	public ResponseEntity<ResponseContainer<List<CuentaAplicacionVO>>> getCuentasCajas() {

		ResponseContainer<List<CuentaAplicacionVO>> respuesta = new ResponseContainer<>();
		try {
			List<CuentaAplicacion> cuentasBancarias = this.cuentasRepo
					.findBySubGrupoCodigoAndCentroCostoActivoTrueOrderByCodigoAsc(
							CuentasPrincipalesEnum.CAJA.getCodigo());
			respuesta.setRespuesta(
					cuentasBancarias.stream().map(o -> new CuentaAplicacionVO(o)).collect(Collectors.toList()));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando cuentas de Caja";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<ResponseContainer<Boolean>> delete(@PathVariable("id") Long id) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();
		try {
			CuentaAplicacion ca = this.cuentasRepo.findOne(id);
			if (ca != null) {

				if (!ca.getEliminable()) {
					respuesta.setMensaje("La cuenta no puede ser eliminada");
					respuesta.setRespuesta(false);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
				}

				Long chequeras = this.chequeraRepo.countByCuentaBancariaId(id);

				Long operaciones = this.operacionesRepo.countByCuentaId(id);

				if (chequeras == 0 && operaciones == 0) {
					ca.setDeleted(true);
					this.cuentasRepo.save(ca);

					respuesta.setMensaje("Se eliminó la cuenta correctamente.");
					respuesta.setRespuesta(true);
					return ResponseEntity.ok().body(respuesta);
				} else {

					if (operaciones > 0) {
						respuesta.setMensaje("No se puede eliminar una cuenta con operaciones imputadas.");
					} else {
						respuesta.setMensaje("No se puede eliminar una cuenta con " + chequeras.intValue()
								+ " chequera/s asociada/s.");
					}

					respuesta.setRespuesta(false);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
				}
			}
		} catch (Exception e) {
			respuesta.setMensaje("Hubo un error eliminando la cuenta.");
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
		respuesta.setMensaje("La cuenta que desea eliminar no existe");
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
	}

}

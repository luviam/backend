package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.TipoEvento;
import ar.com.thinkup.luviam.service.def.IParametricoService;
import ar.com.thinkup.luviam.service.def.ITipoEventoService;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;
import ar.com.thinkup.luviam.vo.parametricos.TipoEventoVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/tipoEvento")
@PreAuthorize("isAuthenticated()")
public class TipoEventoController extends ParametricosController<TipoEventoVO, TipoEvento> {

	private static final Logger LOG = LogManager.getLogger(TipoEventoController.class);


	@Autowired
	private ITipoEventoService tipoEventoService;

	@GetMapping("/parametrico/all")
	public ResponseEntity<ResponseContainer<List<ParametricoVO>>> getParametricos() {
		return super.getParametricos();
	}
	
	@GetMapping("/all")
	public ResponseEntity<ResponseContainer<List<TipoEventoVO>>> getAll() {
		return super.getAll();
	}

	@GetMapping("/{id}")
	public ResponseEntity<ResponseContainer<TipoEventoVO>> getById(@PathVariable("id") Long id) {
		return super.getByID(id);
	}
	@PutMapping()
	public ResponseEntity<ResponseContainer<TipoEventoVO>> create(@RequestBody TipoEventoVO vo) {
		return super.create(vo);
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<TipoEventoVO>> update(@RequestBody TipoEventoVO vo) {
		return super.update(vo);
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		return super.delete(id);
	}

	@Override
	protected IParametricoService<TipoEventoVO, TipoEvento> getService() {
		return this.tipoEventoService;
	}

	@Override
	protected Logger getLog() {
		return TipoEventoController.LOG;
	}

	@Override
	protected String getNombre() {

		return "Tipo Evento";
	}
}

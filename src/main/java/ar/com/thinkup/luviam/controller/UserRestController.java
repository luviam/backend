package ar.com.thinkup.luviam.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.config.jwt.JwtTokenUtil;
import ar.com.thinkup.luviam.model.security.Usuario;

@RestController
public class UserRestController {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;

    @GetMapping("/user")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Usuario getAuthenticatedUser(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        String username = jwtTokenUtil.getUsernameFromToken(token);
        Usuario user = (Usuario) userDetailsService.loadUserByUsername(username);
        return user;
    }

}

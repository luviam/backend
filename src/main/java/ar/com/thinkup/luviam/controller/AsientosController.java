package ar.com.thinkup.luviam.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.Asiento;
import ar.com.thinkup.luviam.model.AsientoCabecera;
import ar.com.thinkup.luviam.repository.AsientoCabeceraRepository;
import ar.com.thinkup.luviam.repository.AsientoRepository;
import ar.com.thinkup.luviam.service.def.IAsientoService;
import ar.com.thinkup.luviam.vo.AsientoVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/asientos")
@PreAuthorize("isAuthenticated()")
public class AsientosController {

	private static final Logger LOG = LogManager.getLogger(AsientosController.class);

	@Autowired
	private IAsientoService asientoService;
	
	@Autowired
	private AsientoRepository asientoRepo;
	
	@Autowired
	private AsientoCabeceraRepository cabecerasRepo;

	
	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();
		try {
			
			respuesta.setRespuesta(this.asientoService.delete(id));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error eliminando el Asiento. Compruebe que no haya documentos vinculados.";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}
	@GetMapping("/cabeceras/{desde}/{hasta}")
	public ResponseEntity<ResponseContainer<List<AsientoCabecera>>> getCabeceras(@PathVariable("desde") String desde, @PathVariable("hasta") String hasta) {
		ResponseContainer<List<AsientoCabecera>> respuesta = new ResponseContainer<>();
		try {
			SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd hh:mm");
			Date desdeDate;
			try {
				desdeDate = sf.parse(desde);
			} catch (ParseException e) {
				desdeDate = new DateTime().minusYears(1).withTimeAtStartOfDay().toDate();
			}
			Date hastaDate;
			try {
				hastaDate = sf.parse(hasta);
			} catch (ParseException e) {
				hastaDate = new DateTime().plusDays(1).withTimeAtStartOfDay().minusSeconds(1).toDate();
			}
			List<AsientoCabecera> asientos = this.cabecerasRepo.findByFechaBetweenOrderByFechaAsc(desdeDate,hastaDate);
			
			respuesta.setRespuesta(asientos);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando Asientos";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}
	
	@GetMapping(path = "/{id}")
	@Transactional()
	public ResponseEntity<ResponseContainer<AsientoVO>> getCentro(@PathVariable Long id) {
		ResponseContainer<AsientoVO> respuesta = new ResponseContainer<>();
		try {
			Asiento asiento = this.asientoRepo.findOne(id);
			if (asiento == null) {
				String m = "No existe centro de costo con ID: " + id;
				LOG.error(m);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
			AsientoVO a = new AsientoVO(asiento);
			respuesta.setRespuesta(a);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando el centro de costo.";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}
	@PutMapping()
	public ResponseEntity<ResponseContainer<AsientoVO>> crearAsientoManual(@RequestBody AsientoVO asiento) {
		ResponseContainer<AsientoVO> respuesta = new ResponseContainer<>();
		if (asiento.getNumeroAsiento() != null) {
			LOG.warn("No se puede crear un asiento con número");
			respuesta.setMensaje("No se puede crear un asiento con número");
			respuesta.setRespuesta(null);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} else {
			try {
				respuesta.setRespuesta(this.asientoService.crearAsiento(asiento));
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			} catch (Exception e) {
				String m = "Hubo un error creando el asiento";
				LOG.error(m, e);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}

		}

	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<AsientoVO>> actualizarAsiento(@RequestBody AsientoVO asiento) {

		ResponseContainer<AsientoVO> respuesta = new ResponseContainer<>();
		if (asiento.getNumeroAsiento() == null) {
			String s = "No se puede actualizar un asiento sin número";
			LOG.warn(s);
			respuesta.setMensaje(s);
			respuesta.setRespuesta(null);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} else {
			try {
				respuesta.setRespuesta(this.asientoService.updateAsiento(asiento));
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			} catch (Exception e) {
				String m = "Hubo un error actualizando el asiento";
				LOG.error(m, e);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
		}
	}
}

package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.Producto;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.service.def.IProductoService;
import ar.com.thinkup.luviam.vo.PrecioProductoVO;
import ar.com.thinkup.luviam.vo.ProductoVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController
@RequestMapping(path = "/producto")
@PreAuthorize("isAuthenticated()")
public class ProductoController extends GenericController<ProductoVO, Producto> {

	private static final Logger LOG = LogManager.getLogger(ProductoController.class);

	@Autowired
	private IProductoService service;

	@GetMapping("/all")
	public ResponseEntity<ResponseContainer<List<ProductoVO>>> getAll() {
		return super.getAll();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ResponseContainer<ProductoVO>> getById(@PathVariable("id") Long id) {
		return super.getByID(id);
	}
	@PutMapping()
	public ResponseEntity<ResponseContainer<ProductoVO>> create(@RequestBody ProductoVO vo) {
		return super.create(vo);
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<ProductoVO>> update(@RequestBody ProductoVO vo) {
		return super.update(vo);
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		return super.delete(id);
	}

	@GetMapping(path = "/categoria/{codigoCategoria}")
	public ResponseEntity<ResponseContainer<List<DescriptivoGeneric>>> getByCategoria(
			@PathVariable("codigoCategoria") String codigoCategoria) {
		ResponseContainer<List<DescriptivoGeneric>> respuesta = new ResponseContainer<>();

		try {
			respuesta.setRespuesta(this.service.getByCategoria(codigoCategoria));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando productos.", e);
			respuesta.setMensaje("Hubo un error buscando productos.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}
	
	@GetMapping(path = "/marca/categoria/{codigoCategoria}")
	public ResponseEntity<ResponseContainer<List<PrecioProductoVO>>> getConMarcaByCategoria(
			@PathVariable("codigoCategoria") String codigoCategoria) {
		ResponseContainer<List<PrecioProductoVO>> respuesta = new ResponseContainer<>();

		try {
			respuesta.setRespuesta(this.service.getConMarcaByCategoria(codigoCategoria));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando productos.", e);
			respuesta.setMensaje("Hubo un error buscando productos.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@Override
	protected IProductoService getService() {
		return this.service;
	}

	@Override
	protected Logger getLog() {
		return ProductoController.LOG;
	}

	@Override
	protected String getNombre() {

		return "Producto";
	}

}

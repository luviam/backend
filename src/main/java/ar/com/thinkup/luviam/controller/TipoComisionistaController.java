package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.TipoComisionista;
import ar.com.thinkup.luviam.service.def.IParametricoService;
import ar.com.thinkup.luviam.service.def.ITipoComisionistaService;
import ar.com.thinkup.luviam.vo.parametricos.TipoComisionistaVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController
@RequestMapping(path = "/tipo-comisionistas")
@PreAuthorize("isAuthenticated()")
public class TipoComisionistaController extends ParametricosController<TipoComisionistaVO, TipoComisionista> {

	@Autowired
	private ITipoComisionistaService service;

	private static final Logger LOG = LogManager.getLogger(TipoComisionistaController.class);

	@GetMapping("/parametrico/all")
	public ResponseEntity<ResponseContainer<List<TipoComisionistaVO>>> getAll() {

		ResponseContainer<List<TipoComisionistaVO>> respuesta = new ResponseContainer<>();
		try {

			respuesta.setRespuesta(this.service.getAll());
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);

		} catch (Exception e) {
			String m = "Hubo un error buscando las categorías";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@Override
	protected IParametricoService<TipoComisionistaVO,TipoComisionista> getService() {

		return this.service;
	}

	@Override
	protected Logger getLog() {
		return TipoComisionistaController.LOG;
	}

	@Override
	protected String getNombre() {
		return "Tipo Comisionista";
	}

	@PutMapping()
	public ResponseEntity<ResponseContainer<TipoComisionistaVO>> create(@RequestBody TipoComisionistaVO vo) {
		return super.create(vo);
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<TipoComisionistaVO>> update(@RequestBody TipoComisionistaVO vo) {
		return super.update(vo);
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		return super.delete(id);
	}

}

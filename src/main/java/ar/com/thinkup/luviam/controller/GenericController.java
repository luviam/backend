package ar.com.thinkup.luviam.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import ar.com.thinkup.luviam.service.def.IModelService;
import ar.com.thinkup.luviam.vo.Identificable;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

public abstract class GenericController<V extends Identificable, E extends Identificable> extends CommonController<V, E> {

	protected abstract IModelService<V,E> getService();

	public ResponseEntity<ResponseContainer<V>> create(V vo) {
		ResponseContainer<V> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.getService().create(vo));
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error creando " + this.getNombre();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	public ResponseEntity<ResponseContainer<V>> update(V vo) {
		ResponseContainer<V> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.getService().update(vo));
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error creando " + this.getNombre();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	public ResponseEntity<ResponseContainer<Boolean>> delete(Long id) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.getService().delete(id));
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error creando " + this.getNombre();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

}
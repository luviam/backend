package ar.com.thinkup.luviam.controller;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.repository.OperacionesRepository;
import ar.com.thinkup.luviam.service.def.ILayoutIndicadoresService;
import ar.com.thinkup.luviam.service.def.IProductoService;
import ar.com.thinkup.luviam.vo.indicadores.CubiertoPromedioVO;
import ar.com.thinkup.luviam.vo.indicadores.LayoutIndicadoresVO;
import ar.com.thinkup.luviam.vo.indicadores.TotalGastoVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController
@RequestMapping(path = "/indicadores")
@PreAuthorize("isAuthenticated()")
public class IndicadoresController {
	private static final Logger LOG = LogManager.getLogger(IndicadoresController.class);

	@Autowired
	private IProductoService service;

	@Autowired
	private OperacionesRepository operacionesRepo;

	@Autowired
	private ILayoutIndicadoresService lService;

	@PostMapping("/guardarLayout")
	public ResponseEntity<ResponseContainer<LayoutIndicadoresVO>> guardarLayout(Authentication authentication,
			@RequestBody() LayoutIndicadoresVO layout) {
		ResponseContainer<LayoutIndicadoresVO> respuesta = new ResponseContainer<>();

		try {
			layout.setUsuario((Usuario) authentication.getPrincipal());
			respuesta.setRespuesta(this.lService.guardarLayout(layout));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando productos.", e);
			respuesta.setMensaje("Hubo un error buscando productos.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@GetMapping("/getLayout")
	public ResponseEntity<ResponseContainer<LayoutIndicadoresVO>> getLayout(Authentication authentication) {
		ResponseContainer<LayoutIndicadoresVO> respuesta = new ResponseContainer<>();

		try {
			respuesta.setRespuesta(this.lService.getLayout((Usuario) authentication.getPrincipal()));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando productos.", e);
			respuesta.setMensaje("Hubo un error buscando productos.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@GetMapping("/precioPromedio/{centro}/{producto}/{desde}/{hasta}")
	public ResponseEntity<ResponseContainer<CubiertoPromedioVO>> getPromedio(
			@PathVariable("centro") String codigoCentro, @PathVariable("producto") String codigoProducto,
			@PathVariable("desde") Date desde, @PathVariable("hasta") Date hasta) {
		ResponseContainer<CubiertoPromedioVO> respuesta = new ResponseContainer<>();

		try {
			respuesta.setRespuesta(this.service.getCubiertoPromedio(codigoCentro, codigoProducto, desde, hasta));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando productos.", e);
			respuesta.setMensaje("Hubo un error buscando productos.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@GetMapping("/totalGasto/{centro}/{gasto}/{desde}/{hasta}")
	public ResponseEntity<ResponseContainer<TotalGastoVO>> getTotalGasto(@PathVariable("centro") String codigoCentro,
			@PathVariable("gasto") String codigoGasto, @PathVariable("desde") Date desde,
			@PathVariable("hasta") Date hasta) {
		ResponseContainer<TotalGastoVO> respuesta = new ResponseContainer<>();

		try {
			List<TotalGastoVO> r = this.operacionesRepo.getTotalSubgrupo(codigoCentro, codigoGasto, desde, hasta);
			if (r.size() == 0) {
				respuesta.setRespuesta(null);
			} else {
				respuesta.setRespuesta(r.iterator().next());
			}
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando gastos.", e);
			respuesta.setMensaje("Hubo un error buscando gastos.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

}

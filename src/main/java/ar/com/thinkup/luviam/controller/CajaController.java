package ar.com.thinkup.luviam.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.exceptions.CajaAbiertaException;
import ar.com.thinkup.luviam.exceptions.CajaCerradaException;
import ar.com.thinkup.luviam.model.EstadoTraspaso;
import ar.com.thinkup.luviam.model.TransaccionCaja;
import ar.com.thinkup.luviam.model.TraspasoCaja;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.plan.CuentaAplicacion;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.repository.CuentaAplicacionRepository;
import ar.com.thinkup.luviam.repository.TraspasoCajaRepository;
import ar.com.thinkup.luviam.service.def.ICajaService;
import ar.com.thinkup.luviam.vo.TransaccionCajaVO;
import ar.com.thinkup.luviam.vo.operaciones.AltaTraspasoVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/caja")
@PreAuthorize("isAuthenticated()")
public class CajaController {

	private static final Logger LOG = LogManager.getLogger(CajaController.class);

	@Autowired
	private ICajaService cajaService;

	@Autowired
	private TraspasoCajaRepository traspasoCajaRepo;

	@Autowired
	private CuentaAplicacionRepository cuentaRepo;

	@GetMapping(path = "/traspasos/{idCaja}")
	public ResponseEntity<ResponseContainer<List<EstadoTraspaso>>> getTraspasos(@PathVariable Long idCaja) {
		ResponseContainer<List<EstadoTraspaso>> respuesta = new ResponseContainer<>();
		try {
			List<TraspasoCaja> traspasos = this.traspasoCajaRepo.findByCajaDestinoIdOrCajaOrigenId(idCaja, idCaja);

			respuesta.setRespuesta(traspasos.stream().map(t -> new EstadoTraspaso(t)).collect(Collectors.toList()));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando los Traspasos.";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping(path = "/traspaso")
	public ResponseEntity<ResponseContainer<Boolean>> actualizarAsiento(Authentication authentication,
			@RequestBody AltaTraspasoVO traspaso) {

		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();
		if (traspaso.getIdCajaOrigen() == null || traspaso.getIdCajaDestino() == null || traspaso.getMonto() == null) {
			String s = "No se puede realizar el traspaso sin monto, caja destino y caja origen";
			LOG.error(s);
			respuesta.setMensaje(s);
			respuesta.setRespuesta(null);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} else {
			try {

				cajaService.generarTraspaso(traspaso, (Usuario) authentication.getPrincipal());
				respuesta.setRespuesta(true);
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			} catch (Exception e) {
				String m = "Hubo un error actualizando el asiento";
				LOG.error(m, e);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
		}
	}

	@PostMapping(path = "/confirmarTraspaso")
	public ResponseEntity<ResponseContainer<Boolean>> confirmarTraspaso(Authentication authentication,
			@RequestBody EstadoTraspaso traspaso) {

		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();

		if (traspaso.getId() == null) {
			String s = "El traspaso debe tener id";
			LOG.error(s);
			respuesta.setMensaje(s);
			respuesta.setRespuesta(null);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} else {
			try {

				this.cajaService.confirmarTraspaso(traspaso, (Usuario) authentication.getPrincipal());
				respuesta.setRespuesta(true);
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			} catch (Exception e) {
				String m = "Hubo un error actualizando el traspaso";
				LOG.error(m, e);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}

		}

	}

	@PostMapping(path = "/cancelarTraspaso")
	public ResponseEntity<ResponseContainer<Boolean>> cancelarTraspaso(Authentication authentication,
			@RequestBody EstadoTraspaso traspaso) {

		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();

		if (traspaso.getId() == null) {
			String s = "El traspaso debe tener id";
			LOG.error(s);
			respuesta.setMensaje(s);
			respuesta.setRespuesta(null);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} else {
			try {

				this.cajaService.rechazarTraspaso(traspaso, (Usuario) authentication.getPrincipal());
				respuesta.setRespuesta(true);
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			} catch (Exception e) {
				String m = "Hubo un error actualizando el traspaso";
				LOG.error(m, e);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}

		}
	}

	@DeleteMapping(path = "/traspaso")
	public ResponseEntity<ResponseContainer<Boolean>> cancelarTraspaso(@RequestParam("id") Long id) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();

		try {
			TraspasoCaja traspaso = this.traspasoCajaRepo.findOne(id);
			String m;
			if (traspaso == null) {
				m = "No existe el traspaso";
				LOG.error(m);
				respuesta.setMensaje(m);
				respuesta.setRespuesta(false);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			} else if (traspaso.getCodigoEstado().equals(EstadoOperacionEnum.APROBADO.getCodigo())) {
				m = "No se puede cancelar un traspaso ya aprobado";
				LOG.error(m);
				respuesta.setMensaje(m);
				respuesta.setRespuesta(false);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			} else {
				this.traspasoCajaRepo.delete(traspaso);
				respuesta.setRespuesta(true);
				respuesta.setMensaje("Traspaso cancelado");
				return ResponseEntity.ok().body(respuesta);

			}

		} catch (Exception e) {
			String m = "Hubo un error al eliminar al cancelar el traspaso";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@Transactional
	@PostMapping(path = "/cierre/{idCaja}")
	public ResponseEntity<ResponseContainer<Boolean>> cerrarCaja(@PathVariable Long idCaja,
			@RequestBody TransaccionCajaVO cierre, Authentication authentication) {

		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();

		try {
			CuentaAplicacion caja = this.cuentaRepo.findOne(idCaja);
			this.cajaService.cerrarCaja(caja, cierre.getMonto(), (Usuario) authentication.getPrincipal());

			respuesta.setRespuesta(true);
			respuesta.setMensaje("Se cerró la caja");
			return ResponseEntity.ok().body(respuesta);
		} catch (CajaCerradaException e) {
			String m = "La caja ya está cerrada";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error al cerrar la caja";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@Transactional
	@PostMapping(path = "/apertura/{idCaja}")
	public ResponseEntity<ResponseContainer<Boolean>> abrirCaja(@PathVariable Long idCaja,
			@RequestBody TransaccionCajaVO apertura, Authentication authentication) {

		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();

		try {
			CuentaAplicacion caja = this.cuentaRepo.findOne(idCaja);
			this.cajaService.abrirCaja(caja, apertura.getMonto(), (Usuario) authentication.getPrincipal());

			respuesta.setRespuesta(true);
			respuesta.setMensaje("Se efectuó la apertura de la caja");
			return ResponseEntity.ok().body(respuesta);
		} catch (CajaAbiertaException e) {
			String m = "La caja ya está abierta";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error al realizar la apertura de la caja";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(path = "/estado/{idCaja}")
	public ResponseEntity<ResponseContainer<Integer>> getEstadoCaja(@PathVariable Long idCaja) {

		ResponseContainer<Integer> respuesta = new ResponseContainer<>();

		try {
			TransaccionCaja trx = this.cajaService.getEstadoCaja(idCaja);
			Integer estadoCaja = 0;
			if (trx != null) {
				estadoCaja = trx.getTipo();
			}

			respuesta.setRespuesta(estadoCaja);
			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			String m = "Hubo un error al consultar el estado de la caja";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(path = "/saldo/{idCaja}")
	public ResponseEntity<ResponseContainer<Double>> getSaldoCaja(@PathVariable Long idCaja) {

		ResponseContainer<Double> respuesta = new ResponseContainer<>();

		try {
			Double saldo = this.cajaService.getSaldoCaja(idCaja);

			respuesta.setRespuesta(saldo);
			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			String m = "Hubo un error al consultar el saldo de la caja";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

}

package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.enums.EstadoOperacionEnum;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.service.def.IOrdenCobranzaService;
import ar.com.thinkup.luviam.vo.OrdenCobranzaCabeceraVO;
import ar.com.thinkup.luviam.vo.documentos.cobros.ItemOrdenCobranzaVO;
import ar.com.thinkup.luviam.vo.documentos.cobros.OrdenCobranzaVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroCliente;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/ordenCobranza")
@PreAuthorize("isAuthenticated()")
public class OrdenCobranzaController {

	private static final Logger LOG = LogManager.getLogger(OrdenCobranzaController.class);

	@Autowired
	private IOrdenCobranzaService ordenesCobranzaService;

	@GetMapping(path = "/items/{id}")
	public ResponseEntity<ResponseContainer<List<ItemOrdenCobranzaVO>>> getItemsByOrden(
			@PathVariable("id") Long idOrdenPago) {
		ResponseContainer<List<ItemOrdenCobranzaVO>> respuesta = new ResponseContainer<>();
		String m = "Hubo un error al buscar la Orden";
		try {

			respuesta.setRespuesta(this.ordenesCobranzaService.getItems(idOrdenPago));
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);
		} catch (Exception e) {
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(path = "/contrato/{idContrato}")
	public ResponseEntity<ResponseContainer<List<OrdenCobranzaVO>>> getByContrato(
			@PathVariable("idContrato") Long idContrato) {
		ResponseContainer<List<OrdenCobranzaVO>> respuesta = new ResponseContainer<>();
		String m = "Hubo un error al buscar la Orden";
		try {

			respuesta.setRespuesta(this.ordenesCobranzaService.getByContrato(idContrato));
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);
		} catch (Exception e) {
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}
	
	@GetMapping(path = "/cabeceras/contrato/{idContrato}")
	public ResponseEntity<ResponseContainer<List<OrdenCobranzaCabeceraVO>>> getCabecerasByContrato(
			@PathVariable("idContrato") Long idContrato) {
		ResponseContainer<List<OrdenCobranzaCabeceraVO>> respuesta = new ResponseContainer<>();
		String m = "Hubo un error al buscar la Orden";
		try {
			
			respuesta.setRespuesta(this.ordenesCobranzaService.getCabecerasByContrato(idContrato));
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);
		} catch (Exception e) {
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<ResponseContainer<OrdenCobranzaVO>> getById(@PathVariable("id") Long idOrdenCobranza) {
		ResponseContainer<OrdenCobranzaVO> respuesta = new ResponseContainer<>();
		String m = "Hubo un error al buscar la Orden";
		try {

			respuesta.setRespuesta(this.ordenesCobranzaService.findById(idOrdenCobranza));
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);
		} catch (Exception e) {
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping(path = "/cambioEstado")
	public ResponseEntity<ResponseContainer<OrdenCobranzaCabeceraVO>> cambioEstadoOrdenCobranza(Authentication authentication,
			@RequestBody OrdenCobranzaCabeceraVO cabeceraOrdenCobranza) {

		ResponseContainer<OrdenCobranzaCabeceraVO> respuesta = new ResponseContainer<>();
		String m = "Hubo un error actualizando la Orden";
		if (cabeceraOrdenCobranza.getId() == null) {
			String s = "La OrdenCobranza debe tener id";
			LOG.error(s);
			respuesta.setMensaje(s);
			respuesta.setRespuesta(null);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} else {
			try {
				EstadoOperacionEnum e = EstadoOperacionEnum.getByCodigo(cabeceraOrdenCobranza.getEstado());
				DescriptivoGeneric estado = new DescriptivoGeneric(e.getCodigo(), e.getDescripcion());
				if (this.ordenesCobranzaService.cambioEstadoDocumento(cabeceraOrdenCobranza.getId(), estado,
						(Usuario) authentication.getPrincipal())) {
					cabeceraOrdenCobranza.setEstado(e);
					respuesta.setRespuesta(cabeceraOrdenCobranza);

					return ResponseEntity.status(HttpStatus.OK).body(respuesta);
				} else {

					LOG.error(m, e);
					respuesta.setMensaje(m);
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
				}

			} catch (Exception e) {
				LOG.error(m, e);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}

		}

	}

	@PostMapping("/cabeceras/")
	public ResponseEntity<ResponseContainer<List<OrdenCobranzaCabeceraVO>>> getCabeceraOrdenCobranzas(@RequestBody FiltroCliente filtros) {
	
		ResponseContainer<List<OrdenCobranzaCabeceraVO>> respuesta = new ResponseContainer<>();
		try {
				respuesta.setRespuesta(this.ordenesCobranzaService.getCabeceras(filtros));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando las Ordenes de Pago";
			LOG.error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@PutMapping()
	public ResponseEntity<ResponseContainer<OrdenCobranzaVO>> crearOrdenCobranza(Authentication authentication,
			@RequestBody OrdenCobranzaVO ordenCobranza) {
		ResponseContainer<OrdenCobranzaVO> respuesta = new ResponseContainer<>();

		if (ordenCobranza.getId() == null) {
			try {
				OrdenCobranzaVO nueva = this.ordenesCobranzaService.guardarDocumento(ordenCobranza,
						(Usuario) authentication.getPrincipal());
				String m = "Se guardó exitosamente la orden de pago";
				LOG.info(m);
				respuesta.setRespuesta(nueva);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			} catch (Exception e) {
				String m = "Hubo un error guardando la orden de pago";
				LOG.error(m, e);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
		} else {
			String m = "Ya existe la orden de pago.";
			LOG.warn(m);
			respuesta.setMensaje(m);
			respuesta.setRespuesta(ordenCobranza);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<OrdenCobranzaVO>> actualizarOrdenCobranza(Authentication authentication,
			@RequestBody OrdenCobranzaVO ordenCobranza) {
		ResponseContainer<OrdenCobranzaVO> respuesta = new ResponseContainer<>();

		if (ordenCobranza.getId() != null) {
			try {
				OrdenCobranzaVO nueva = this.ordenesCobranzaService.guardarDocumento(ordenCobranza,
						(Usuario) authentication.getPrincipal());
				String m = "Se guardó exitosamente la Orden";
				LOG.info(m);
				respuesta.setRespuesta(nueva);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			} catch (Exception e) {
				String m = "Hubo un error guardando la orden de pago";
				LOG.error(m, e);
				respuesta.setMensaje(m);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
		} else {
			String m = "No existe la orden de pago.";
			LOG.warn(m);
			respuesta.setMensaje(m);
			respuesta.setRespuesta(ordenCobranza);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(Authentication authentication,
			@RequestParam("id") String id) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();
		Usuario responsable = (Usuario) authentication.getPrincipal();

		try {
			this.ordenesCobranzaService.borrar(Long.valueOf(id), responsable);

			respuesta.setMensaje("OrdenCobranza eliminada correctamente");
			respuesta.setRespuesta(true);
			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setMensaje("Hubo un error eliminando la Orden.");
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

}

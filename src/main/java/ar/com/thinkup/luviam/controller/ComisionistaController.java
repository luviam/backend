package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.Comisionista;
import ar.com.thinkup.luviam.service.def.IComisionistaService;
import ar.com.thinkup.luviam.vo.CabeceraComisionistaVO;
import ar.com.thinkup.luviam.vo.ComisionistaVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController
@RequestMapping(path = "/comisionista")
@PreAuthorize("isAuthenticated()")
public class ComisionistaController extends GenericController<ComisionistaVO, Comisionista> {

	private static final Logger LOG = LogManager.getLogger(ComisionistaController.class);

	@Autowired
	private IComisionistaService service;

	@GetMapping("/all")
	public ResponseEntity<ResponseContainer<List<ComisionistaVO>>> getAll() {
		return super.getAll();
	}
	
	
	
	@GetMapping("/cabeceras")
	public ResponseEntity<ResponseContainer<List<CabeceraComisionistaVO>>> getcabeceras() {
		ResponseContainer<List<CabeceraComisionistaVO>> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.service.getCabeceras());
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);

		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando " + this.getNombre();
			getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ResponseContainer<ComisionistaVO>> getById(@PathVariable("id") Long id) {
		return super.getByID(id);
	}
	@PutMapping()
	public ResponseEntity<ResponseContainer<ComisionistaVO>> create(@RequestBody ComisionistaVO vo) {
		return super.create(vo);
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<ComisionistaVO>> update(@RequestBody ComisionistaVO vo) {
		return super.update(vo);
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		return super.delete(id);
	}

	@Override
	protected IComisionistaService getService() {
		return this.service;
	}

	@Override
	protected Logger getLog() {
		return ComisionistaController.LOG;
	}

	@Override
	protected String getNombre() {

		return "Comisionistas";
	}

}

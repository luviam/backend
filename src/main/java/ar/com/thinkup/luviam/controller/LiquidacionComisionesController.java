package ar.com.thinkup.luviam.controller;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.documentos.comisiones.LiquidacionComisiones;
import ar.com.thinkup.luviam.model.enums.TipoPagoComision;
import ar.com.thinkup.luviam.model.security.Usuario;
import ar.com.thinkup.luviam.service.def.ILiquidacionComisionesService;
import ar.com.thinkup.luviam.vo.ItemLiquidacionComisionesVO;
import ar.com.thinkup.luviam.vo.LiquidacionComisionesVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroComisiones;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController
@RequestMapping(path = "/liquidacionComisiones")
@PreAuthorize("isAuthenticated()")
public class LiquidacionComisionesController extends GenericController<LiquidacionComisionesVO, LiquidacionComisiones> {

	private static final Logger LOG = LogManager.getLogger(LiquidacionComisionesController.class);

	@Autowired
	private ILiquidacionComisionesService service;

	@GetMapping("/all")
	public ResponseEntity<ResponseContainer<List<LiquidacionComisionesVO>>> getAll() {
		return super.getAll();
	}

	@GetMapping("/generarMensual")
	public ResponseEntity<ResponseContainer<List<LiquidacionComisionesVO>>> generarMensual() {
		ResponseContainer<List<LiquidacionComisionesVO>> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.getService().generarLiquidacionesVendedoresMensuales());
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error al liquidar el mes ";
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping("/cabeceras")
	public ResponseEntity<ResponseContainer<List<LiquidacionComisionesVO>>> getAllCabeceras(
			@RequestBody FiltroComisiones filtro) {
		ResponseContainer<List<LiquidacionComisionesVO>> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.getService().getComisionesByFitro(filtro));
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando las comisiones no liquidadas ";
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/items/{idLiquidacion}")
	public ResponseEntity<ResponseContainer<List<ItemLiquidacionComisionesVO>>> getItemsById(
			@PathVariable("idLiquidacion") Long idLiquidacion) {
		ResponseContainer<List<ItemLiquidacionComisionesVO>> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.getService().getItems(idLiquidacion));
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando las comisiones no liquidadas ";
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<ResponseContainer<LiquidacionComisionesVO>> getById(@PathVariable("id") Long id) {
		return super.getByID(id);
	}

	@PutMapping()
	public ResponseEntity<ResponseContainer<LiquidacionComisionesVO>> create(Authentication authentication,
			@RequestBody LiquidacionComisionesVO vo) {
		vo.setResponsable(new DescriptivoGeneric((Usuario) authentication.getPrincipal()));
		return super.create(vo);
	}

	@PostMapping(path = "/cambioEstado")
	public ResponseEntity<ResponseContainer<Boolean>> cambioEstado(Authentication authentication,
			@RequestBody LiquidacionComisionesVO liquidacion) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.getService().cambioEstadoDocumento(liquidacion.getId(), liquidacion.getEstado(),
					(Usuario) authentication.getPrincipal()));
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando las comisiones no liquidadas ";
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<LiquidacionComisionesVO>> update(@RequestBody LiquidacionComisionesVO vo) {
		return super.update(vo);
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		return super.delete(id);
	}

	@Override
	protected ILiquidacionComisionesService getService() {
		return this.service;
	}

	@Override
	protected Logger getLog() {
		return LiquidacionComisionesController.LOG;
	}

	@Override
	protected String getNombre() {

		return "Liquidacion Comisiones";
	}

}

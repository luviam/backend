package ar.com.thinkup.luviam.controller;

import java.util.List;

public interface ICommonService<V, E> {

	V getByID(Long id);
	List<V> getAll();
	Boolean delete(Long id);

	
	E getEntity(Long id);
	
	E parseToEnt(V vo, E entity);
}

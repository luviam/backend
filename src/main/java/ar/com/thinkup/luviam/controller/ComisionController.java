package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.contratos.ConfiguracionComision;
import ar.com.thinkup.luviam.service.def.IComisionService;
import ar.com.thinkup.luviam.vo.contratos.ConfiguracionComisionVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController
@RequestMapping(path = "/comision")
@PreAuthorize("isAuthenticated()")
public class ComisionController extends GenericController<ConfiguracionComisionVO, ConfiguracionComision> {

	private static final Logger LOG = LogManager.getLogger(ComisionController.class);

	@Autowired
	private IComisionService service;

	@GetMapping("/all")
	public ResponseEntity<ResponseContainer<List<ConfiguracionComisionVO>>> getAll() {
		return super.getAll();
	}

	@GetMapping("/{id}")
	public ResponseEntity<ResponseContainer<ConfiguracionComisionVO>> getById(@PathVariable("id") Long id) {
		return super.getByID(id);
	}
	
	@PutMapping()
	public ResponseEntity<ResponseContainer<ConfiguracionComisionVO>> create(@RequestBody ConfiguracionComisionVO vo) {
		return super.create(vo);
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<ConfiguracionComisionVO>> update(@RequestBody ConfiguracionComisionVO vo) {
		return super.update(vo);
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		return super.delete(id);
	}

	@Override
	protected IComisionService getService() {
		return this.service;
	}

	@Override
	protected Logger getLog() {
		return ComisionController.LOG;
	}

	@Override
	protected String getNombre() {

		return "ConfiguracionComision";
	}

}

package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.TipoIVA;
import ar.com.thinkup.luviam.service.def.IParametricoService;
import ar.com.thinkup.luviam.service.def.ITipoIVAService;
import ar.com.thinkup.luviam.vo.TipoIVAVO;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/tipoiva")
@PreAuthorize("isAuthenticated()")
public class TipoIVAController extends ParametricosController<TipoIVAVO, TipoIVA> {

	private static final Logger LOG = LogManager.getLogger(TipoIVAController.class);


	@Autowired
	private ITipoIVAService TipoIVAService;

	@GetMapping("/parametrico/all")
	public ResponseEntity<ResponseContainer<List<ParametricoVO>>> getParametricos() {
		return super.getParametricos();
	}

	@GetMapping("/all")
	public ResponseEntity<ResponseContainer<List<TipoIVAVO>>> getAll() {
		return super.getAll();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ResponseContainer<TipoIVAVO>> getById(@PathVariable("id") Long id) {
		return super.getByID(id);
	}
	
	@PutMapping()
	public ResponseEntity<ResponseContainer<TipoIVAVO>> create(@RequestBody TipoIVAVO vo) {
		return super.create(vo);
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<TipoIVAVO>> update(@RequestBody TipoIVAVO vo) {
		return super.update(vo);
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		return super.delete(id);
	}

	@Override
	protected IParametricoService<TipoIVAVO, TipoIVA> getService() {
		return this.TipoIVAService;
	}

	@Override
	protected Logger getLog() {
		return TipoIVAController.LOG;
	}

	@Override
	protected String getNombre() {

		return "Tipo IVA";
	}
}

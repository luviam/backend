package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.contratos.PagoComision;
import ar.com.thinkup.luviam.service.def.IPagoComisionService;
import ar.com.thinkup.luviam.vo.PagoComisionVO;
import ar.com.thinkup.luviam.vo.filtros.FiltroComisiones;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController
@RequestMapping(path = "/pagoComision")
@PreAuthorize("isAuthenticated()")
public class PagoComisionController extends GenericController<PagoComisionVO, PagoComision> {

	private static final Logger LOG = LogManager.getLogger(PagoComisionController.class);

	@Autowired
	private IPagoComisionService service;

	@GetMapping("/all")
	public ResponseEntity<ResponseContainer<List<PagoComisionVO>>> getAll() {
		return super.getAll();
	}

	@GetMapping("/{id}")
	public ResponseEntity<ResponseContainer<PagoComisionVO>> getById(@PathVariable("id") Long id) {
		return super.getByID(id);
	}
	
	@PutMapping()
	public ResponseEntity<ResponseContainer<PagoComisionVO>> create(@RequestBody PagoComisionVO vo) {
		return super.create(vo);
	}

	
	@PostMapping(path="/impagas/")
	public ResponseEntity<ResponseContainer<List<PagoComisionVO>>> getImpagos(@RequestBody FiltroComisiones filtro) {
		ResponseContainer<List<PagoComisionVO>> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.getService().getImpagos(filtro));
			return ResponseEntity.ok().body(respuesta);
		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando las comisiones no liquidadas ";
			this.getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}
	@PostMapping()
	public ResponseEntity<ResponseContainer<PagoComisionVO>> update(@RequestBody PagoComisionVO vo) {
		return super.update(vo);
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		return super.delete(id);
	}

	@Override
	protected IPagoComisionService getService() {
		return this.service;
	}

	@Override
	protected Logger getLog() {
		return PagoComisionController.LOG;
	}

	@Override
	protected String getNombre() {

		return "Pago Comision";
	}

}

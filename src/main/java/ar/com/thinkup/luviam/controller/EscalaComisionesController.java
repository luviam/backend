package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.EscalaComisiones;
import ar.com.thinkup.luviam.service.def.IEscalaComisionesService;
import ar.com.thinkup.luviam.vo.EscalaComisionesVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController
@RequestMapping(path = "/escala-comisiones")
@PreAuthorize("isAuthenticated()")
public class EscalaComisionesController extends GenericController<EscalaComisionesVO, EscalaComisiones> {

	private static final Logger LOG = LogManager.getLogger(EscalaComisionesController.class);

	@Autowired
	private IEscalaComisionesService service;

	@GetMapping("/all")
	public ResponseEntity<ResponseContainer<List<EscalaComisionesVO>>> getAll() {
		return super.getAll();
	}

	@GetMapping("/byCentro/{centro}")
	public ResponseEntity<ResponseContainer<List<EscalaComisionesVO>>> getByCentro(@PathVariable("centro") String centro) {
		ResponseContainer<List<EscalaComisionesVO>> respuesta = new ResponseContainer<>();
		try {
			respuesta.setRespuesta(this.getService().getByCentro(centro));
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);

		} catch (IllegalArgumentException e) {
			String m = e.getMessage();
			getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		} catch (Exception e) {
			String m = "Hubo un error buscando " + this.getNombre();
			getLog().error(m, e);
			respuesta.setMensaje(m);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	
	@GetMapping("/{id}")
	public ResponseEntity<ResponseContainer<EscalaComisionesVO>> getById(@PathVariable("id") Long id) {
		return super.getByID(id);
	}

	@PutMapping()
	public ResponseEntity<ResponseContainer<EscalaComisionesVO>> create(@RequestBody EscalaComisionesVO vo) {
		return super.create(vo);
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<EscalaComisionesVO>> update(@RequestBody EscalaComisionesVO vo) {
		return super.update(vo);
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		return super.delete(id);
	}

	@Override
	protected IEscalaComisionesService getService() {
		return this.service;
	}

	@Override
	protected Logger getLog() {
		return EscalaComisionesController.LOG;
	}

	@Override
	protected String getNombre() {

		return "Escala Comisiones";
	}

}

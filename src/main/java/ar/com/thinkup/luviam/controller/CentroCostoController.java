package ar.com.thinkup.luviam.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.CentroCosto;
import ar.com.thinkup.luviam.model.CentroSeleccion;
import ar.com.thinkup.luviam.repository.CentroRepository;
import ar.com.thinkup.luviam.repository.CentroSeleccionRepository;
import ar.com.thinkup.luviam.service.def.ICentroCostoService;
import ar.com.thinkup.luviam.vo.CabeceraCentro;
import ar.com.thinkup.luviam.vo.CentroCostoVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/centro-costo")
@PreAuthorize("isAuthenticated()")
public class CentroCostoController {

	private static final Logger LOG = LogManager.getLogger(CentroCostoController.class);

	@Autowired
	private CentroRepository centroRepo;

	@Autowired
	private CentroSeleccionRepository centroSeleccionRepo;

	@Autowired
	private ICentroCostoService centroService;

	@GetMapping("/all")
	@Transactional
	public ResponseEntity<ResponseContainer<List<CentroSeleccion>>> getCentros() {

		ResponseContainer<List<CentroSeleccion>> respuesta = new ResponseContainer<>();
		try {
			List<CentroSeleccion> centros = this.centroSeleccionRepo.findAll();

			respuesta.setRespuesta(centros);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando los centros", e);
			respuesta.setMensaje("Hubo un error buscando centros.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/cabeceras")
	public ResponseEntity<ResponseContainer<List<CabeceraCentro>>> getPlanTemplate() {
		ResponseContainer<List<CabeceraCentro>> respuesta = new ResponseContainer<>();
		try {
			List<CentroCosto> centro = this.centroRepo.findAll();
			List<CabeceraCentro> centroResp = centro.parallelStream().map(c -> new CabeceraCentro(c))
					.collect(Collectors.toList());
			respuesta.setRespuesta(centroResp);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando el plan template", e);
			respuesta.setMensaje("Hubo un error buscando el plan template.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<ResponseContainer<CentroCostoVO>> getCentro(@PathVariable Long id) {
		ResponseContainer<CentroCostoVO> respuesta = new ResponseContainer<>();
		try {
			CentroCostoVO centro = this.centroService.getByID(id);
			if (centro == null) {
				LOG.error("No existe centro de costo con ID: " + id);
				respuesta.setMensaje("Hubo un error buscando el centro de costo.");
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}

			respuesta.setRespuesta(centro);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando el centro de costo.", e);
			respuesta.setMensaje("Hubo un error buscando el centro de costos.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@PutMapping()
	public ResponseEntity<ResponseContainer<CentroCostoVO>> crearCentro(@RequestBody CentroCostoVO ccv) {
		ResponseContainer<CentroCostoVO> respuesta = new ResponseContainer<>();

		if (ccv.getId() == null) {
			try {
				CentroCostoVO nuevo = this.centroService.create(ccv);
				LOG.info("Se guardó exitosamente el centro");
				respuesta.setRespuesta(nuevo);
				respuesta.setMensaje("Se guardó exitosamente el centro");
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			} catch (Exception e) {
				LOG.error("Hubo un error guardando el centro de costo.", e);
				respuesta.setMensaje("Hubo un error guardando el centro de costos.");
				respuesta.setRespuesta(ccv);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
		} else {
			LOG.warn("Ya existe el centro de costo.");
			respuesta.setMensaje("Ya existe el centro de costo.");
			respuesta.setRespuesta(ccv);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<CentroCostoVO>> actualizarCentro(@RequestBody CentroCostoVO ccv) {
		ResponseContainer<CentroCostoVO> respuesta = new ResponseContainer<>();

		if (ccv.getId() != null) {
			try {
				// TODO: Tengo que buscar las cuentas y buscar los tipos de comprobantes
				CentroCostoVO centro = this.centroService.update(ccv);
				respuesta.setRespuesta(centro);
				respuesta.setMensaje("Se guardó exitosamente el centro");
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			} catch (Exception e) {
				LOG.error("Hubo un error guardando el centro de costo.", e);
				respuesta.setMensaje("Hubo un error guardando el centro de costos.");
				respuesta.setRespuesta(ccv);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
		} else {
			LOG.warn("No existe el centro de costo que desea modificar.");
			respuesta.setMensaje("Ya existe el centro de costo.");
			respuesta.setRespuesta(ccv);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") String id) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();
		Long centroCostoId = Long.valueOf(id);
		try {
			this.centroService.delete(centroCostoId);
			respuesta.setMensaje("Se elimino el centro correctamente.");
			respuesta.setRespuesta(true);
			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			LOG.error(e);
			respuesta.setMensaje("Hubo un error eliminando el centro.");
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

}

package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.model.Salon;
import ar.com.thinkup.luviam.service.def.ISalonService;
import ar.com.thinkup.luviam.vo.SalonVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController
@RequestMapping(path = "/salon")
@PreAuthorize("isAuthenticated()")
public class SalonController extends GenericController<SalonVO, Salon> {

	private static final Logger LOG = LogManager.getLogger(SalonController.class);

	@Autowired
	private ISalonService service;

	@GetMapping("/all")
	public ResponseEntity<ResponseContainer<List<SalonVO>>> getAll() {
		return super.getAll();
	}
	@GetMapping("/{id}")
	public ResponseEntity<ResponseContainer<SalonVO>> getById(@PathVariable("id") Long id) {
		return super.getByID(id);
	}
	@PutMapping()
	public ResponseEntity<ResponseContainer<SalonVO>> create(@RequestBody SalonVO vo) {
		return super.create(vo);
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<SalonVO>> update(@RequestBody SalonVO vo) {
		return super.update(vo);
	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") Long id) {
		return super.delete(id);
	}

	
	
	@Override
	protected ISalonService getService() {
		return this.service;
	}

	@Override
	protected Logger getLog() {
		return SalonController.LOG;
	}

	@Override
	protected String getNombre() {

		return "Salon";
	}

}

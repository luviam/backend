package ar.com.thinkup.luviam.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import ar.com.thinkup.luviam.model.Parametrico;
import ar.com.thinkup.luviam.vo.parametricos.ParametricoVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

public abstract class ParametricosController<T extends ParametricoVO, V extends Parametrico>
		extends GenericController<T, V> {

	protected ResponseEntity<ResponseContainer<List<ParametricoVO>>> getParametricos() {
		ResponseContainer<List<ParametricoVO>> respuesta = new ResponseContainer<>();

		try {
			respuesta.setRespuesta(this.getService().getAll().parallelStream().map(c -> new ParametricoVO(c))
					.collect(Collectors.toList()));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			this.getLog().error("Hubo un error buscando " + this.getNombre(), e);
			respuesta.setMensaje("Hubo un error buscando " + this.getNombre());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}
	

}

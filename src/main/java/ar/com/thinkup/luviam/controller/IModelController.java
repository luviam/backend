package ar.com.thinkup.luviam.controller;

import java.io.Serializable;
import java.util.List;

import org.springframework.http.ResponseEntity;

import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

public interface IModelController<T> extends Serializable {

	ResponseEntity<ResponseContainer<List<T>>> getAll();

	ResponseEntity<ResponseContainer<T>> getByID(Long id);

	ResponseEntity<ResponseContainer<T>> create(T vo);

	ResponseEntity<ResponseContainer<T>> update(T vo);

	ResponseEntity<ResponseContainer<T>> delete(Long id);
}

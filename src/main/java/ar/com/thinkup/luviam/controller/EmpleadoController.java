package ar.com.thinkup.luviam.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.com.thinkup.luviam.service.def.IEmpleadoService;
import ar.com.thinkup.luviam.vo.empleados.CabeceraEmpleado;
import ar.com.thinkup.luviam.vo.empleados.EmpleadoVO;
import ar.com.thinkup.luviam.vo.rest.ResponseContainer;

@RestController()
@RequestMapping(path = "/empleado")
@PreAuthorize("isAuthenticated()")
public class EmpleadoController {

	private static final Logger LOG = LogManager.getLogger(EmpleadoController.class);

	@Autowired
	private IEmpleadoService service;

	@GetMapping("/cabecera/{codigoTipoEmpleado}")
	public ResponseEntity<ResponseContainer<List<CabeceraEmpleado>>> getCabeceras(
			@PathVariable String codigoTipoEmpleado) {
		ResponseContainer<List<CabeceraEmpleado>> respuesta = new ResponseContainer<>();
		try {

			List<CabeceraEmpleado> cebeceras = this.service.getCabeceras(codigoTipoEmpleado);
			respuesta.setRespuesta(cebeceras);
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando Empleados", e);
			respuesta.setMensaje("Hubo un error buscando Empleados.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}

	}

	@DeleteMapping()
	public ResponseEntity<ResponseContainer<Boolean>> delete(@RequestParam("id") String id) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();
		Long idEmpleado = Long.valueOf(id);
		try {

			respuesta.setRespuesta(this.service.delete(idEmpleado));
			return ResponseEntity.ok().body(respuesta);
		} catch (JpaSystemException e) {
			EmpleadoVO emp = this.service.getById(idEmpleado);
			emp.setActivo(false);
			try {
				this.service.guardar(emp);
				respuesta.setRespuesta(true);
				return ResponseEntity.ok().body(respuesta);
			} catch (Exception ex) {
				LOG.error("Error borrando", ex);
				respuesta.setMensaje("Hubo un error eliminando el empleado.");
				respuesta.setRespuesta(false);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}

		} catch (Exception e) {
			respuesta.setMensaje("Hubo un error eliminando el empleado.");
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PutMapping()
	public ResponseEntity<ResponseContainer<Boolean>> crear(@RequestBody EmpleadoVO empleadoVo) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();

		if (empleadoVo.getId() == null) {
			try {
				empleadoVo.setActivo(true);
				this.service.guardar(empleadoVo);

				LOG.info("Se guardó exitosamente el empleado");
				respuesta.setRespuesta(true);
				respuesta.setMensaje("Se guardó exitosamente el empleado");
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			} catch (Exception e) {
				LOG.error("Hubo un error guardando el empleado.", e);
				respuesta.setMensaje("Hubo un error guardando el empleado.");
				respuesta.setRespuesta(false);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
		} else {
			LOG.warn("Ya existe el empleado.");
			respuesta.setMensaje("Ya existe el empleado.");
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PostMapping()
	public ResponseEntity<ResponseContainer<Boolean>> actualizarCentro(@RequestBody EmpleadoVO empleado) {
		ResponseContainer<Boolean> respuesta = new ResponseContainer<>();

		if (empleado.getId() != null) {
			try {
				empleado.setActivo(true);
				this.service.guardar(empleado);
				LOG.info("Se guardó exitosamente el empleado");
				respuesta.setRespuesta(true);
				respuesta.setMensaje("Se guardó exitosamente el empleado");
				return ResponseEntity.status(HttpStatus.OK).body(respuesta);
			} catch (Exception e) {
				LOG.error("Hubo un error guardando el elmpleado.", e);
				respuesta.setMensaje("Hubo un error guardando el empleado.");
				respuesta.setRespuesta(false);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
			}
		} else {
			LOG.warn("No existe el empleado que desea modificar.");
			respuesta.setMensaje("No existe el empleado.");
			respuesta.setRespuesta(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<ResponseContainer<EmpleadoVO>> getById(@PathVariable Long id) {
		ResponseContainer<EmpleadoVO> respuesta = new ResponseContainer<>();
		try {

			respuesta.setRespuesta(this.service.getById(id));
			return ResponseEntity.ok().body(respuesta);
		} catch (Exception e) {
			LOG.error("Hubo un error buscando el empleado", e);
			respuesta.setMensaje("Hubo un error buscando el empleado.");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

}

package ar.com.thinkup.utils;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

import ar.com.thinkup.luviam.model.contratos.ContratoCabecera;
import ar.com.thinkup.luviam.model.contratos.ServicioContratado;
import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import ar.com.thinkup.luviam.model.enums.TipoHorarioEnum;
import ar.com.thinkup.luviam.repository.SalonRepository.SalonDescriptivo;

public class DescriptivoParser {
	public static SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");

	public static DescriptivoGeneric parse(ContratoCabecera c) {
		String codigo = String.valueOf(c.getId());
		String salon = c.getLocacion();

		String descripcion = salon + " - " + sf.format(c.getFechaEvento()) + " "
				+ TipoHorarioEnum.byCodigo(c.getCodigoHorario());
		return new DescriptivoGeneric(codigo, descripcion);

	}

	public static DescriptivoGeneric parse(ServicioContratado c) {

		String codigo = String.valueOf(c.getId());
		String producto = c.getProducto() == null ? " S/A " : c.getProducto().getDescripcion();

		return new DescriptivoGeneric(codigo, producto);

	}

	public static DescriptivoGeneric parse(SalonDescriptivo salon) {
		return salon != null? new DescriptivoGeneric(salon.getId(), salon.getNombre()) : null;
	}
	public static List<String> getCodigos(List<DescriptivoGeneric> lista) {
		return lista.stream().map(DescriptivoGeneric::getCodigo).collect(Collectors.toList());
	}
}

package ar.com.thinkup.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

import com.google.common.base.Charsets;

import ar.com.thinkup.luviam.model.EscalaComisiones;
import ar.com.thinkup.luviam.model.Parametrico;
import ar.com.thinkup.luviam.model.Precio;
import ar.com.thinkup.luviam.model.TipoMenu;
import ar.com.thinkup.luviam.model.contratos.PagoComision;
import ar.com.thinkup.luviam.model.def.IDescriptivo;
import javassist.Modifier;

public class ModelClassGenerator {

	public static String NL = "\r\n";
	public static String root = "src/main/java/";
	public static String rootPackage = "ar/com/thinkup/luviam/";
	public static String voPackagePath = "vo";
	public static String modelPath = "model";
	public static String controllerPackage = "controller";
	public static String repoPackage = "repository";
	public static String servicioDefPackage = "service/def";
	public static String servicioPackage = "service";
	public static boolean override = false;

	public static void main(String[] args) {

		Class<?> modelClass = Precio.class;
		System.out.println("----------CREANDO VO");
		createFile(generarVO(modelClass), voPackagePath, modelClass.getSimpleName() + "VO.java");

		System.out.println("----------CREANDO REPO");
		createFile(generarRepo(modelClass), repoPackage, modelClass.getSimpleName() + "Repository.java");

		System.out.println("----------CREANDO SERVICIO DEF");
		createFile(generarServicioDef(modelClass), servicioDefPackage,
				"I" + modelClass.getSimpleName() + "Service.java");

		System.out.println("----------CREANDO SERVICIO");
		createFile(generarServicio(modelClass), servicioPackage, modelClass.getSimpleName() + "Service.java");

		System.out.println("----------CREANDO CONTROLLER");
		createFile(generarController(modelClass), controllerPackage, modelClass.getSimpleName() + "Controller.java");

		System.out.println(generarParseToEntity(modelClass));
		System.out.println(" --------------------- INICIO Typescript ---------------------------");
		System.out.println(NL);
		System.out.println(generarTypescript(modelClass));
		System.out.println(NL);
		System.out.println(" --------------------- FIN Typescript ---------------------------");
	}

	private static void createFile(String content, String pack, String fileName) {
		File controller = new File(root + rootPackage + pack + "/" + fileName);
		if (!controller.exists() || (controller.exists() && override)) {
			PrintWriter out;
			try {
				out = new PrintWriter(controller);

				out.print(content);
				out.close();
				System.out.println("ARCHIVO GENERADO");
			} catch (FileNotFoundException e) {
				e.printStackTrace();

			}
		} else {
			System.out.println("ARCHIVO EXISTENTE - OVERRIDE = FALSE");
		}

	}

	private static String getKeyType(Class<?> entidad) {
		String key = "";
		try {
			key = entidad.getDeclaredField("id").getType().getSimpleName();
		} catch (NoSuchFieldException | SecurityException e) {
			key = "Integer";
			e.printStackTrace();
		}
		return key;
	}

	public static String generarServicio(Class<?> entidad) {
		Boolean esDescriptivo = IDescriptivo.class.isAssignableFrom(entidad);
		Boolean esParametrico = Parametrico.class.isAssignableFrom(entidad);

		StringBuilder sb = new StringBuilder();
		sb.append("package " + (rootPackage + servicioPackage).replaceAll("/", ".") + ";" + NL);
		sb.append("\r\n");
		sb.append(getModelImport(entidad));
		sb.append(getVOImport(entidad));
		sb.append("import org.apache.commons.lang3.StringUtils;\r\n");
		sb.append("import org.springframework.beans.factory.annotation.Autowired;\r\n");
		sb.append("import org.springframework.stereotype.Service;\r\n");
		sb.append("\r\n");

		// BuildMyString.com generated code. Please enjoy your string responsibly.

		sb.append("import ar.com.thinkup.luviam.model.$E;\r\n");
		sb.append("import ar.com.thinkup.luviam.model.def.IDescriptivo;\r\n");
		sb.append("import ar.com.thinkup.luviam.repository.$ERepository;\r\n");
		sb.append("import ar.com.thinkup.luviam.service.def.I$EService;\r\n");
		sb.append("\r\n");
		sb.append("import ar.com.thinkup.luviam.vo.$EVO;\r\n");
		sb.append("\r\n");
		sb.append("@Service(\"I$EService\")\r\n");

		if (esParametrico) {
			sb.append("public class $EService extends ParametricoService<$EVO, $E>\r\n");
			sb.append("\timplements I$EService {\r\n");
		} else {
			sb.append("public class $EService extends ModelService<$E, $EVO, $ERepository>\r\n");
			sb.append("\timplements I$EService {\r\n");
		}

		sb.append("\r\n");
		sb.append("\t@Autowired\r\n");
		sb.append("\tprivate $ERepository repo;\r\n");
		sb.append("\r\n");
		sb.append("\r\n");
		sb.append("\r\n");
		if (esDescriptivo && !esParametrico) {
			sb.append("\t@Override\r\n");
			sb.append("\tpublic $E getByDescriptivo(IDescriptivo descriptivo) {\r\n");
			sb.append("\tif (descriptivo == null || StringUtils.isEmpty(descriptivo.getCodigo())) {\r\n");
			sb.append("\tthrow new IllegalArgumentException(\"El $E es obligatorio\");\r\n");
			sb.append("\t}\r\n");
			sb.append("\t$E a = this.getRepository().findOne(Long.valueOf(descriptivo.getCodigo()));\r\n");
			sb.append("\treturn a;\r\n");
			sb.append("\t}\r\n");
			sb.append("\r\n");

		}
		sb.append("\t@Override\r\n");
		sb.append("\tprotected $ERepository getRepository() {\r\n");
		sb.append("\treturn this.repo;\r\n");
		sb.append("\t}\r\n");
		sb.append("\r\n");
		sb.append("\t@Override\r\n");
		sb.append("\tprotected $EVO toVO($E ent) {\r\n");
		sb.append("\r\n");
		sb.append("\treturn new $EVO(ent);\r\n");
		sb.append("\t}\r\n");
		sb.append("\r\n");
		sb.append("\t@Override\r\n");
		sb.append("\tprotected $E newEnt() {\r\n");
		sb.append("\treturn new $E();\r\n");
		sb.append("\t}\r\n");
		sb.append("\r\n");
		sb.append("\t\r\n");
		sb.append("}\r\n");

		if (!esParametrico) {
			sb.append(generarParseToEntity(entidad));
		}

		String str = sb.toString();
		str = str.replace("$E", entidad.getSimpleName());
		return str;
	}

	public static String generarServicioDef(Class<?> entidad) {
		Boolean esDescriptivo = IDescriptivo.class.isAssignableFrom(entidad);
		Boolean esParametrico = Parametrico.class.isAssignableFrom(entidad);

		StringBuilder sb = new StringBuilder();
		sb.append("package " + (rootPackage + servicioDefPackage).replaceAll("/", ".") + ";" + NL);
		sb.append("\r\n");
		sb.append(getModelImport(entidad));
		sb.append(getVOImport(entidad));
		sb.append("\r\n");
		if (esParametrico) {
			sb.append("public interface I$EService extends IParametricoService<$EVO, $E>{\r\n");
		} else if (esDescriptivo) {
			sb.append("public interface I$EService extends IModelService<$EVO,$E> , IDescriptivoService<$E>{\r\n");
		} else {
			sb.append("public interface I$EService extends IModelService<$EVO,$E>{\r\n");
		}

		sb.append("\r\n");
		sb.append("\t\r\n");
		sb.append("}\r\n");

		String str = sb.toString();
		str = str.replace("$E", entidad.getSimpleName());
		return str;
	}

	private static String getVOImport(Class<?> entidad) {
		return "import " + (rootPackage + voPackagePath).replaceAll("/", ".") + "." + entidad.getSimpleName() + "VO;"
				+ NL;
	}

	public static String generarController(Class<?> entidad) {
		Boolean esParametrico = Parametrico.class.isAssignableFrom(entidad);
		String str = "";
		if (esParametrico) {
			str = readFile("ParametricoController");
		} else {
			str = readFile("ControllerTemplate");
		}

		str = str.replace("$path", entidad.getSimpleName().toLowerCase());
		str = str.replace("$E", entidad.getSimpleName());
		str = str.replace("$NOMBRE", WordUtils
				.capitalize(String.join(" ", StringUtils.splitByCharacterTypeCamelCase(entidad.getSimpleName()))));

		return str;
	}

	public static String generarRepo(Class<?> entidad) {

		// BuildMyString.com generated code. Please enjoy your string responsibly.

		StringBuilder sb = new StringBuilder();
		Boolean esDescriptivo = IDescriptivo.class.isAssignableFrom(entidad);
		Boolean esParametrico = Parametrico.class.isAssignableFrom(entidad);

		sb.append("package ar.com.thinkup.luviam.repository;\r\n");
		sb.append("\r\n");
		sb.append(getModelImport(entidad));
		sb.append("\r\n");
		if (esParametrico) {
			sb.append("public interface $ERepository extends ParameterRepository<$E>{ \r\n");
		} else if (esDescriptivo) {
			sb.append("public interface $ERepository extends DescriptivoRepository<$E>{ \r\n");
		} else {
			sb.append("import org.springframework.data.jpa.repository.JpaRepository;\r\n");
			sb.append("\r\n");
			sb.append("public interface $ERepository extends JpaRepository<$E, Long>{ \r\n");
		}

		sb.append("\r\n");
		sb.append("}\r\n");

		String str = sb.toString();
		str = str.replace("$E", entidad.getSimpleName());
		return str;
	}

	public static String generarTypescript(Class<?> ent) {
		Class<?> entidad;
		try {
			entidad = Class
					.forName((rootPackage + voPackagePath).replaceAll("/", ".") + "." + ent.getSimpleName() + "VO");

			String tsStr = NL + "export class " + ent.getSimpleName() + "{" + NL;
			tsStr += "	constructor(" + NL;
			for (Field campo : entidad.getDeclaredFields()) {
				if (!Modifier.isStatic(campo.getModifiers())) {
					tsStr += NL + "		public " + campo.getName() + "? : " + getJSType(campo) + ",";
				}

			}
			tsStr += NL + "	){}";
			tsStr += NL;
			tsStr += NL + "	public static fromData(data : any) : " + ent.getSimpleName() + "{";
			tsStr += NL + "		if(!data) return null; ";
			tsStr += NL + "		let o : " + ent.getSimpleName() + "  = new " + ent.getSimpleName() + "(";
			for (Field campo : entidad.getDeclaredFields()) {
				if (!Modifier.isStatic(campo.getModifiers())) {

					tsStr += NL + "		";
					String tipoJS = getJSType(campo);
					if (List.class.isAssignableFrom(campo.getType())) {
						tsStr += " data." + campo.getName() + "?  data." + campo.getName() + ".map(a => "
								+ getGenericTypeName(campo) + ".fromData(a)) : [],";
					} else if (tipoJS.equals("Descriptivo")) {
						tsStr += " Descriptivo.fromData(data." + campo.getName() + "),";
					} else if (tipoJS.equals("Date")) {
						tsStr += " data." + campo.getName() + "? new Date(data." + campo.getName() + ") : null,";
					} else {
						tsStr += " data." + campo.getName() + ",";
					}
				}

			}

			tsStr += "	);" + NL + "		return o; " + NL + NL + "	}";

			return tsStr += NL + NL + "}";
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return "";

		}
	}

	private static String getJSType(Field campo) {
		if (campo.getType().equals(String.class)) {
			return "string";
		} else if (Number.class.isAssignableFrom(campo.getType())) {
			return "number";

		} else if (campo.getType().equals(Date.class)) {
			return "Date";

		} else if (List.class.isAssignableFrom(campo.getType())) {
			return getGenericTypeName(campo) + "[]";
		} else if (Boolean.class.equals(campo.getType())) {
			return "boolean";
		} else
			return "Descriptivo";

	}

	public static String generarParseToEntity(Class<?> entidad) {
		String toEntity = NL + "public " + entidad.getSimpleName() + " parseToEntity(" + entidad.getSimpleName()
				+ "VO vo, " + entidad.getSimpleName() + " entity) {" + NL;

		String voName = entidad.getSimpleName() + "VO";

		toEntity += NL + "	" + entidad.getSimpleName() + " ent = entity;";

		toEntity += NL + "	if(ent == null) ent = new " + entidad.getSimpleName() + "();" + NL;
		for (Field campo : entidad.getDeclaredFields()) {
			if (!Modifier.isStatic(campo.getModifiers())) {
				toEntity += "	ent.set" + Character.toUpperCase(campo.getName().charAt(0))
						+ campo.getName().substring(1) + "(";
				if (esPrimitivo(campo.getType()) || campo.getType().equals(Date.class)) {
					toEntity += " vo." + getGetter(campo.getName()) + ");" + NL;
				} else {
					toEntity += " this.getEEEEE(vo." + getGetter(campo.getName()) + "));" + NL;
				}
			}

		}
		toEntity += NL + "	return ent;" + NL + "}" + NL;
		return toEntity;
	}

	public static String readFile(String name) {
		ClassLoader classLoader = ModelClassGenerator.class.getClassLoader();
		File file = new File(classLoader.getResource("classTemplates/" + name + ".java").getFile());

		byte[] encoded;
		try {
			encoded = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
		} catch (IOException e) {
			return null;
		}
		return new String(encoded, Charsets.UTF_8);
	}

	public static String generarVO(Class<?> entidad) {
		String claseVO = "package " + (rootPackage + voPackagePath).replaceAll("/", ".") + ";" + NL;

		Boolean esDescriptivo = IDescriptivo.class.isAssignableFrom(entidad);
		Boolean esParametrico = Parametrico.class.isAssignableFrom(entidad);
		String voName = entidad.getSimpleName() + "VO";
		if (esParametrico) {

			claseVO += getImportParametricoVO();
			claseVO += getModelImport(entidad);
			claseVO += NL + "public class " + entidad.getSimpleName() + "VO extends ParametricoVO{" + NL;
		} else if (esDescriptivo) {
			claseVO += "import ar.com.thinkup.luviam.model.def.DescriptivoGeneric;" + NL;
			claseVO += getModelImport(entidad);
			claseVO += NL + "public class " + entidad.getSimpleName()
					+ "VO extends DescriptivoGeneric  implements Identificable{" + NL;
		} else {
			claseVO += "public class " + voName + " implements Serializable, Identificable {" + NL;
		}

		claseVO += NL + "	private static final long serialVersionUID =" + new Random().nextLong() + "L;";

		List<Field> campos = new Vector<>();

		// Declaro atributos
		for (Field campo : entidad.getDeclaredFields()) {
			if (!Modifier.isStatic(campo.getModifiers())) {
				campos.add(campo);

				claseVO += NL + "	private " + getTipoVO(campo) + " " + campo.getName() + ";" + NL;
			}

		}

		// Constructor sin parametros
		claseVO += NL + "	public " + voName + "(){" + NL + "		super();" + NL + "	}" + NL;

		// Constructor para entidad
		claseVO += NL + "	public " + voName + "(" + entidad.getSimpleName() + " ent){" + NL + "		super(ent);"
				+ NL;
		for (Field campo : campos) {
			claseVO += "		this." + campo.getName() + " = ";
			if (esPrimitivo(campo.getType()) || campo.getType().equals(Date.class)) {
				claseVO += " ent." + getGetter(campo.getName()) + ";" + NL;
			} else if (List.class.isAssignableFrom(campo.getType())) {
				claseVO += " ent.get " + campo.getName() + "().stream().map( o -> new " + getGenericTypeName(campo)
						+ "VO" + "(o)).collect(Collectors.toList());" + NL;
			} else {

				claseVO += " new DescriptivoGeneric(ent." + getGetter(campo.getName()) + ");" + NL;
			}

		}

		claseVO += NL + "	}" + NL;
		if (!esParametrico) {
			claseVO += "	@Override" + NL + "	public Long getId() {" + NL + "		return this.id;" + NL + "	}";
		}

		return claseVO + NL + NL + "}";

	}

	private static String getImportParametricoVO() {
		return "import " + (rootPackage + voPackagePath).replaceAll("/", ".") + ".parametricos.ParametricoVO" + NL;
	}

	private static String getModelImport(Class<?> entidad) {
		return "import " + (rootPackage + modelPath).replaceAll("/", ".") + "." + entidad.getSimpleName() + ";" + NL;
	}

	public static String getGetter(String field) {

		return "get" + Character.toUpperCase(field.charAt(0)) + field.substring(1) + "()";
	}

	public static String getTipoVO(Field campo) {
		if (esPrimitivo(campo.getType()) || campo.getType().equals(Date.class)) {
			return campo.getType().getSimpleName();
		} else if (campo.getType().equals(BigDecimal.class) || campo.getType().equals(Double.class)) {
			return " Double ";
		} else if (List.class.isAssignableFrom(campo.getType())) {
			return " List<" + getGenericTypeName(campo) + "> ";
		} else {

			return " DescriptivoGeneric ";
		}

	}

	private static String getGenericTypeName(Field campo) {
		String fullName = ((ParameterizedType) campo.getGenericType()).getActualTypeArguments()[0].getTypeName();
		try {
			return Class.forName(fullName).getSimpleName().replace("VO", "");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return "";
		}

	}

	private static boolean esPrimitivo(Class<?> type) {
		return ClassUtils.isPrimitiveOrWrapper(type) || type.equals(String.class);
	}
}


##### Intro


Backend de Field-Tech donde tendremos los Servicios a consumir por parte del front y la app mobile.

##### How do I get set up?


*   Descargar la rama develop desde el repo.
*   Actualizar Java a la versión 8.
*   Desde eclipse agreagar el proyecto como existente de Maven.
*   Update de Maven dependencies
*   Tener Tomcat 8 (minimo) ya instalado en la computadora.
*   Buildear el proyecto mediante maven (mvn clean package)
*   Agregarlo a tomcat como proyecto web.

NOTA: Todas las conecciones a los servicios son meiante HTTPS. En un ambiente local se debe tener al menos un certificado autofirmado instalado.


##### Contacto


info@thinkup.com.ar
